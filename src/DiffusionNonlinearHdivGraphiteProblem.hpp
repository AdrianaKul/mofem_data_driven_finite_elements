#ifndef __DIFFUSION_NONLINEAR_HDIV_GRAPHITE_HPP__
#define __DIFFUSION_NONLINEAR_HDIV_GRAPHITE_HPP__

#include <alt_boost_geometry_includes.hpp>
#include <DiffusionNonlinearGraphiteProblem.hpp>
#include <HDivDiffusionProblem.hpp>
#include <RtreeManipulation.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

typedef boost::function<double(
    const FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 2>)>
    MixFunc;

struct DiffusionNonlinearHdivGraphite : public HDivDiffusionProblem,
                                        public DiffusionNonlinearGraphite {
public:
  DiffusionNonlinearHdivGraphite(MoFEM::Interface &m_field)
      : DiffusionNonlinearGraphite(m_field), HDivDiffusionProblem(m_field),
        DiffusionDD(m_field), ClassicDiffusionProblem(m_field) {
    valueField = "T";
    fluxField = "Q";
  };
  // DiffusionSnesDD(m_field),

  MoFEMErrorCode runProblem() override;
  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode boundaryCondition() override;
  // MoFEMErrorCode calculateHdivJacobian(boost::shared_ptr<FaceEle> fe_ptr);
  MoFEMErrorCode assembleSystem() override;
  MoFEMErrorCode setSnesMonitors() override;
  MoFEMErrorCode getErrorIndicators();

  std::string fluxField; // flux field

  struct CommonDataGraphiteHDiv : public CommonDataHDiv,
                                  public CommonDataSnesDD {};

  boost::shared_ptr<CommonDataGraphiteHDiv> commonDataPtr;

  // Operators

  struct OpNonlinearLhs : public OpFaceEle {
  public:
    OpNonlinearLhs(std::string row_field_name, std::string col_field_name,
                   boost::shared_ptr<VectorDouble> val_pointer,
                   double nonlinear_a = 1., double nonlinear_b = 1.,
                   double nonlinear_c = 1.)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          valPtr(val_pointer), nonlinearA(nonlinear_a), nonlinearB(nonlinear_b),
          nonlinearC(nonlinear_c) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    double nonlinearA;
    double nonlinearB;
    double nonlinearC;
    boost::shared_ptr<VectorDouble> valPtr;
    MatrixDouble locLhs;
  };

  struct OpNonlinearRhs : public OpFaceEle {
  public:
    OpNonlinearRhs(std::string field_name,
                   boost::shared_ptr<VectorDouble> val_pointer,
                   double nonlinear_a = 1., double nonlinear_b = 1.,
                   double nonlinear_c = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), valPtr(val_pointer),
          nonlinearA(nonlinear_a), nonlinearB(nonlinear_b),
          nonlinearC(nonlinear_c) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    double nonlinearA;
    double nonlinearB;
    double nonlinearC;
    boost::shared_ptr<VectorDouble> valPtr;
    VectorDouble locRhs;
  };

  template <int N> struct OpErrorIndicator : public OpFaceEle {
  public:
    OpErrorIndicator(std::string field_name,
                     boost::shared_ptr<CommonDataHDiv> common_data_ptr,
                     MoFEM::Interface &m_field,
                     boost::shared_ptr<MatrixDouble> mat_flux_ptr,
                     boost::shared_ptr<MatrixDouble> mat_grad_ptr,
                     boost::shared_ptr<VectorDouble> vec_val_ptr,
                     ScalarFunc s_function)
        : OpFaceEle(field_name, OPROW), commonDataPtr(common_data_ptr),
          mField(m_field), valPtr(vec_val_ptr), gradPtr(mat_grad_ptr),
          fluxPtr(mat_flux_ptr), sFunc(s_function) {
      std::fill(&doEntities[MBVERTEX], &doEntities[MBMAXTYPE], false);
      doEntities[MBTRI] = doEntities[MBQUAD] = true;
    }
    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<CommonDataHDiv> commonDataPtr;
    MoFEM::Interface &mField;
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    boost::shared_ptr<MatrixDouble> fluxPtr;
    ScalarFunc sFunc;
  };
};

#endif //__DIFFUSION_NONLINEAR_HDIV_GRAPHITE_HPP__
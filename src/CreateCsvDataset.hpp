#include <MoFEM.hpp>
#include <BasicFiniteElements.hpp>
#include <iostream>

// Random boost requirements
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/normal_distribution.hpp>

#define DDLogTag                                                               \
  MOFEM_LOG_CHANNEL("WORLD");                                                  \
  MOFEM_LOG_FUNCTION();                                                        \
  MOFEM_LOG_TAG("WORLD", "DD");

struct CreatingCsvMaterialModel {

  // value to be later saved and used for scaling the dataset and results
  PetscBool flg_scaling = PETSC_TRUE;
  double scaling = 1.0;

  // param file values
  // name of output file
  char csv_out_file[255] = "dummy_tree.csv";

  PetscInt csvDim = 4;
  PetscReal dummy_k = 3.;
  // ranges
  PetscReal dummy_range_dp = 4.;
  PetscReal dummy_range_q = 12.;
  // number of points created
  PetscInt dummy_count = 10000;
  // noise
  PetscReal dummy_noise_k = 0.1;
  PetscReal dummy_noise_dp = 0.;
  PetscReal dummy_noise_q = 0.;
  // random seed for point generation
  PetscBool rand_seed = PETSC_TRUE;

  // basic nonlinear material dataset to compare to FEM
  PetscReal nonlinear_alpha = 0;
  PetscBool flg_bnmd = PETSC_FALSE;

  // Forchheirmer's equation
  PetscBool flg_forch = PETSC_FALSE;
  PetscReal forch_coeff_2 = 0.25;
  PetscReal viscosity = 1.0518; // mPa*s at 18 degree C
  PetscReal porosity = 0.5;     // <0.6 for natural materials
  // For natural media, porosity does not normally exceed 0.6. For beds of solid
  // spheres of uniform diameter porosity can vary between the limits
  // 0.2595(rhombohedral packing) and 0.4764(cubic packing).
  PetscReal K = 1.;
  // PetscReal K = 10e-9; // m^2 clean gravel/sand
  PetscReal drag_const = 1 / porosity; // dimensionless form-drag constant
  // PetscReal density_fluid = 999; // Density of water (kg/m3)
  PetscReal density_fluid = 0.01;

  // boost random
  boost::random::mt11213b gen; // generator

  double square(double x) { return x * x; }

  MoFEMErrorCode setParamValues();

  MoFEMErrorCode setSeed() {
    MoFEMFunctionBegin;
    if (rand_seed == PETSC_TRUE) {
      gen.seed(static_cast<unsigned int>(std::time(0)));
    }
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode startCsvFile();

  MoFEMErrorCode createBasicMaterialModelData();

  MoFEMErrorCode createBasicNonlinearMaterialModelData();

  MoFEMErrorCode createForchheirmerMaterialModelData();

  MoFEMErrorCode saveScalingFactor();
};
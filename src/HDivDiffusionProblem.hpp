#ifndef __HDIV_DIFFUSION_PROBLEM_HPP__
#define __HDIV_DIFFUSION_PROBLEM_HPP__

#include <ClassicDiffusionProblem.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;
using SideEle = MoFEM::FaceElementForcesAndSourcesCoreOnSide;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;
using OpSideEle = SideEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

#define DDLogTag                                                               \
  MOFEM_LOG_CHANNEL("WORLD");                                                  \
  MOFEM_LOG_FUNCTION();                                                        \
  MOFEM_LOG_TAG("WORLD", "DD");

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct HDivDiffusionProblem : virtual public ClassicDiffusionProblem {

public:
  HDivDiffusionProblem(MoFEM::Interface &m_field)
      : ClassicDiffusionProblem(m_field), fluxField("Q") {
    valueField = "T";
  };

  // Declaration of main functions
  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode orderUpdateSetUp();
  MoFEMErrorCode createHdivCommonData();
  MoFEMErrorCode markFluxBc();
  MoFEMErrorCode calculateHdivJacobian(boost::shared_ptr<FaceEle> fe_ptr);
  MoFEMErrorCode assembleSystem() override;
  virtual MoFEMErrorCode assembleEssentialBoundary();
  MoFEMErrorCode refineMesh();
  MoFEMErrorCode outputHdivVtkFields(int ii,
                                     std::string vtk_name = "out_result_");
  MoFEMErrorCode outputResults(int ii);
  MoFEMErrorCode getJumps(int iter_num);
  virtual MoFEMErrorCode
  postGetAnalyticalValuesHdiv(boost::shared_ptr<FaceEle> fe_ptr);
  MoFEMErrorCode checkError(int iter_num);
  virtual MoFEMErrorCode recheckOrder();
  MoFEMErrorCode refineOrder();
  MoFEMErrorCode runLoop();
  virtual MoFEMErrorCode checkRefinement();

  Range meshRefEntAdditional;

  // common data
  struct CommonDataHDiv : virtual public CommonDataClassic {

    // set negative connectivity
    double k_minus;

    enum VecRefineErrorElements {
      ERROR_L2_NORM = 0, // start of Hdiv indicators
      ERROR_H1_SEMINORM,
      ERROR_INDICATOR_GRAD,
      ERROR_INDICATOR_DIV,
      JUMP_L2,
      JUMP_HDIV,
      ERROR_ESTIMATOR,
      TOTAL_NUMBER,
      INTEG_COUNT,
      VOLUME,
      LAST_REFINE_ELEMENT
    };

    SmartPetscObj<Vec> petscRefineErrorVec;
  };

  using MatSideArray = std::array<MatrixDouble, 2>;
  using VecSideArray = std::array<VectorDouble, 2>;
  struct SideData {

    VecSideArray valueVec; // values of the L2 field
    MatSideArray gradVec;  // values of the L2 gradient field
    MatSideArray fluxVec;  // values of the Hdiv field

    double val_jump;
    double flux_jump;

    enum ElementSide { LEFT_SIDE = 0, RIGHT_SIDE };

    std::array<EntityHandle, 2> feSideHandle;
    std::array<double, 2> areaMap; // area/volume of elements on the side
                                   //  from plate [end]
  };

  // Field name and approximation order
  std::string fluxField; // flux field

  boost::shared_ptr<CommonDataHDiv> commonDataPtr;

  // Hdiv refinement
  Range domainEntities;
  double errorIndicatorIntegral;
  int totalElementNumber;
  int refIterNum;
  int refinementStyle;
  double refControl = 1.;
  double refControlMesh = 0.0;

  // result vectors
  PetscBool flg_out_error = PETSC_TRUE;
  std::vector<double> errorL2;
  std::vector<double> errorH1;
  std::vector<double> errorIndicatorGrad;
  std::vector<double> errorIndicatorDiv;
  std::vector<double> jumpL2;
  std::vector<double> errorEstimator;
  std::vector<double> jumpHdiv;
  int orderRefinementCounter;
  int meshRefinementCounter;

  //! [Analytical function]
  static double analyticalFunction(const double x, const double y,
                                   const double z) {
    // // octopus
    // return exp(-10 * x * x * y * y);
    // // L-shape
    // return pow(x * x + y * y, 1. / 3.) *
    //        sin((2. * (M_PI / 2. + atan2(y, x))) / 3.);
    // mexi_hat
    return exp(-100. * (square(x) + square(y))) * cos(M_PI * x) * cos(M_PI * y);
  }
  //! [Analytical function]

  //! [Analytical function gradient]
  static VectorDouble analyticalFunctionGrad(const double x, const double y,
                                             const double z) {
    VectorDouble res;
    res.resize(2);
    // // octopus
    // double ee = -10 * x * x * y * y;
    // res[0] = -20 * exp(ee) * x * y * y;
    // res[1] = -20 * exp(ee) * x * x * y;
    // // L-shape
    // res[0] = (-2. * y * cos((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
    //              (3. * pow(x * x + y * y, 2. / 3.)) +
    //          (2. * x * sin((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
    //              (3. * pow(x * x + y * y, 2. / 3.));
    // res[1] = (2. * x * cos((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
    //              (3. * pow(x * x + y * y, 2. / 3.)) +
    //          (2. * y * sin((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
    //              (3. * pow(x * x + y * y, 2. / 3.));
    // mexi_hat
    res[0] = -exp(-100. * (square(x) + square(y))) *
             (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) * cos(M_PI * y);
    res[1] = -exp(-100. * (square(x) + square(y))) *
             (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) * cos(M_PI * x);
    return res;
  }
  //! [Analytical function gradient]

  template <int N> struct OpErrorIndGrad : public OpFaceEle {
    boost::shared_ptr<MatrixDouble> gradPtr;
    boost::shared_ptr<MatrixDouble> fieldPtr;
    boost::shared_ptr<CommonDataHDiv> commonDataPtr;
    SmartPetscObj<Vec> dataVec;
    const int iNdex;
    const double K;
    MoFEM::Interface &mField;
    std::string tagName;
    OpErrorIndGrad(boost::shared_ptr<MatrixDouble> grad_ptr,
                   boost::shared_ptr<MatrixDouble> field_ptr,
                   SmartPetscObj<Vec> data_vec, const int index,
                   MoFEM::Interface &m_field,
                   std::string tag_name = "ERROR_INDICATOR_GRAD",
                   const double k = 1.0)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), gradPtr(grad_ptr),
          fieldPtr(field_ptr), dataVec(data_vec), iNdex(index), K(k),
          mField(m_field), tagName(tag_name) {
      std::fill(&doEntities[MBVERTEX], &doEntities[MBMAXTYPE], false);
      doEntities[MBTRI] = doEntities[MBQUAD] = true;
    }
    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);
  };

  struct OpErrorIndDiv : public OpFaceEle {
    boost::shared_ptr<VectorDouble> divPtr;
    boost::shared_ptr<VectorDouble> sourcePtr;
    SmartPetscObj<Vec> dataVec;
    const int iNdex;
    MoFEM::Interface &mField;
    std::string tagName;
    OpErrorIndDiv(boost::shared_ptr<VectorDouble> div_ptr,
                  boost::shared_ptr<VectorDouble> source_ptr,
                  SmartPetscObj<Vec> data_vec, const int index,
                  MoFEM::Interface &m_field,
                  std::string tag_name = "ERROR_INDICATOR_DIV")
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), divPtr(div_ptr),
          sourcePtr(source_ptr), dataVec(data_vec), iNdex(index),
          mField(m_field), tagName(tag_name) {
      // std::fill(&doEntities[MBVERTEX], &doEntities[MBMAXTYPE], false);
      // doEntities[MBTRI] = doEntities[MBQUAD] = true;
    }
    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data);
  };

  template <int SPACE_DIM, int N> struct OpSideGetData : public OpSideEle {
    OpSideGetData(const std::string field_name,
                  boost::shared_ptr<CommonDataHDiv> common_data_ptr,
                  boost::shared_ptr<SideData> side_data_ptr)
        : OpSideEle(field_name, OpSideEle::OPROW, false),
          commonDataPtr(common_data_ptr), sideDataPtr(side_data_ptr) {
      std::fill(&doEntities[MBVERTEX], &doEntities[MBMAXTYPE], false);
      for (auto t = moab::CN::TypeDimensionMap[SPACE_DIM].first;
           t <= moab::CN::TypeDimensionMap[SPACE_DIM].second; ++t)
        doEntities[t] = true;
      sYmm = false;
      std::fill(&doEntities[MBVERTEX], &doEntities[MBMAXTYPE], false);
      doEntities[MBVERTEX] = true;
    }

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
      MoFEMFunctionBegin;

      const auto nb_in_loop = getFEMethod()->nInTheLoop;

      sideDataPtr->feSideHandle[nb_in_loop] = getFEEntityHandle();

      auto clear = [&](auto nb) {
        sideDataPtr->valueVec[nb].clear();
        sideDataPtr->fluxVec[nb].clear();
      };

      if (type == MBVERTEX) {
        sideDataPtr->areaMap[nb_in_loop] = getMeasure();
        if (!nb_in_loop) {
          clear(0);
          clear(1);
          sideDataPtr->areaMap[1] = 0;
        }
      }

      const auto nb_dofs = data.getIndices().size();
      // if (nb_dofs) {
      // get number of integration points on face
      const size_t nb_integration_pts = getGaussPts().size2();
      sideDataPtr->valueVec[nb_in_loop] = *(commonDataPtr->mValPtr);
      sideDataPtr->fluxVec[nb_in_loop] = *(commonDataPtr->mFluxPtr);
      // }

      MoFEMFunctionReturn(0);
    }

  protected:
    boost::shared_ptr<CommonDataHDiv> commonDataPtr;
    boost::shared_ptr<SideData> sideDataPtr;
  };

  struct OpSkeletonJump : public OpEdgeEle {
  public:
    /**
     * @brief Construct a new OpSkeletonJump
     *
     * @param side_fe_ptr pointer to FE to evaluate side elements
     */
    OpSkeletonJump(boost::shared_ptr<SideData> side_data_ptr,
                   boost::shared_ptr<CommonDataHDiv> common_data_ptr)
        : OpEdgeEle(NOSPACE, OpEdgeEle::OPSPACE), sideDataPtr(side_data_ptr),
          commonDataPtr(common_data_ptr) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data) {
      MoFEMFunctionBegin;

      FTensor::Index<'i', 2> i;

      // get normal of the face or edge
      auto t_normal = getFTensor1Normal();
      t_normal.normalize();

      // get number of integration points on face
      const size_t nb_integration_pts = getGaussPts().size2();

      sideDataPtr->val_jump = 0.0;
      sideDataPtr->flux_jump = 0.0;

      auto integrate = [&]() {
        MoFEMFunctionBeginHot;

        auto t_w = getFTensor0IntegrationWeight();

        auto t_val_0 = getFTensor0FromVec(sideDataPtr->valueVec[0]);
        auto t_flux_0 = getFTensor1FromMat<3>(sideDataPtr->fluxVec[0]);
        auto t_val_1 = getFTensor0FromVec(sideDataPtr->valueVec[1]);
        auto t_flux_1 = getFTensor1FromMat<3>(sideDataPtr->fluxVec[1]);

        double area = getMeasure();

        // representative length of the element
        double length_repr =
            (sqrt(sideDataPtr->areaMap[0]) + sqrt(sideDataPtr->areaMap[1])) /
            2.;
        // representative measure h to be used as h^{-1/2}
        double length_ammended = 1. / sqrt(length_repr);

        double val_diff;
        double flux_normal_diff;

        // iterate integration points on face/edge
        for (size_t gg = 0; gg != nb_integration_pts; ++gg) {

          // t_w is integration weight, and measure is element area, or
          // volume, depending if problem is in 2d/3d.
          const double alpha = area * t_w * length_ammended;

          val_diff = t_val_1 - t_val_0;
          sideDataPtr->val_jump += alpha * val_diff * val_diff;

          flux_normal_diff =
              t_flux_1(i) * t_normal(i) - t_flux_0(i) * t_normal(i);
          sideDataPtr->flux_jump += alpha * flux_normal_diff * flux_normal_diff;

          ++t_w;
          ++t_val_0;
          ++t_flux_0;
          ++t_val_1;
          ++t_flux_1;
        }

        MoFEMFunctionReturnHot(0);
      };

      CHK_THROW_MESSAGE(integrate(),
                        "Error in integration in jump calculation.");

      CHKERR VecSetValue(commonDataPtr->petscRefineErrorVec,
                         CommonDataHDiv::JUMP_L2, sideDataPtr->val_jump,
                         ADD_VALUES);
      CHKERR VecSetValue(commonDataPtr->petscRefineErrorVec,
                         CommonDataHDiv::JUMP_HDIV, sideDataPtr->flux_jump,
                         ADD_VALUES);

      MoFEMFunctionReturn(0);
    }

  private:
    boost::shared_ptr<SideData> sideDataPtr;
    boost::shared_ptr<CommonDataHDiv> commonDataPtr;
  };

  struct OpJumpTags : public OpEdgeEle {
  public:
    OpJumpTags(boost::shared_ptr<SideData> side_data_ptr,
               boost::shared_ptr<CommonDataHDiv> common_data_ptr,
               MoFEM::Interface &m_field)
        : OpEdgeEle(NOSPACE, OpEdgeEle::OPSPACE), sideDataPtr(side_data_ptr),
          commonDataPtr(common_data_ptr), mField(m_field) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data) {
      MoFEMFunctionBegin;

      FTensor::Index<'i', 2> i;

      Tag th_jump_l2;
      CHKERR ClassicDiffusionProblem::getTagHandle(mField, "JUMP_L2",
                                                   MB_TYPE_DOUBLE, th_jump_l2);

      for (auto s0 : {SideData::LEFT_SIDE, SideData::RIGHT_SIDE}) {

        const EntityHandle entity_handle = sideDataPtr->feSideHandle[s0];
        double jupm_to_ele;
        CHKERR mField.get_moab().tag_get_data(th_jump_l2, &entity_handle, 1,
                                              &jupm_to_ele);
        jupm_to_ele += sqrt(sideDataPtr->val_jump);
        CHKERR mField.get_moab().tag_set_data(th_jump_l2, &entity_handle, 1,
                                              &jupm_to_ele);
      }

      MoFEMFunctionReturn(0);
    }

  private:
    boost::shared_ptr<SideData> sideDataPtr;
    boost::shared_ptr<CommonDataHDiv> commonDataPtr;
    MoFEM::Interface &mField;
  };

  struct OpScalarTags : public OpFaceEle {
  public:
    OpScalarTags(boost::shared_ptr<VectorDouble> data_ptr, std::string tag_name,
                 MoFEM::Interface &m_field)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), dataPtr(data_ptr),
          tagName(tag_name), mField(m_field) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data) {
      MoFEMFunctionBegin;

      const EntityHandle ent = getFEEntityHandle();
      const int nb_integration_pts = getGaussPts().size2();
      const double area = getMeasure();
      auto t_w = getFTensor0IntegrationWeight();

      auto t_val = getFTensor0FromVec(*dataPtr);

      Tag th_data;
      CHKERR ClassicDiffusionProblem::getTagHandle(mField, tagName.c_str(),
                                                   MB_TYPE_DOUBLE, th_data);

      double val_norm = 0.0;

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double alpha = t_w * area;

        val_norm += t_val * t_val * alpha;

        ++t_w;
      }

      val_norm = sqrt(val_norm);

      CHKERR mField.get_moab().tag_set_data(th_data, &ent, 1, &val_norm);

      MoFEMFunctionReturn(0);
    }

  private:
    boost::shared_ptr<VectorDouble> dataPtr;
    std::string tagName;
    MoFEM::Interface &mField;
  };
};

#endif //__HDIV_DIFFUSION_PROBLEM_HPP__
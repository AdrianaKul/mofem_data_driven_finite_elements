#ifndef __PATCH_INTEG_2D_HPP__
#define __PATCH_INTEG_2D_HPP__

// Patch integration for 2D face elements

struct PatchIntegFaceEle : public FaceElementForcesAndSourcesCore {

  int &numSide;

  PatchIntegFaceEle(MoFEM::Interface &m_field, int &num_side)
      : FaceElementForcesAndSourcesCore(m_field), numSide(num_side) {}
  int getRule(int order) { return -1; };

  MoFEMErrorCode setGaussPts(int order) {
    MoFEMFunctionBeginHot;

    const EntityType type = numeredEntFiniteElementPtr->getEntType();

    int total_gp, counter, side;

    // triangles
    if (type == MBTRI) {

      side = numSide;
      total_gp = side * side;

      const double mu = 1. / (side * 3);
      const double nu = 1. / (side * 3);

      gaussPts.resize(3, total_gp);
      gaussPts.clear();

      counter = 0;

      for (int i = 0; i < side; ++i) {
        for (int j = 0; j < (side - i); ++j) {

          gaussPts(0, counter) = (mu * 3 * i) + mu;
          gaussPts(1, counter) = (nu * 3 * j) + nu;
          ++counter;

          if (i > 0) {
            gaussPts(0, counter) = (mu * 3 * i) - mu;
            gaussPts(1, counter) = (nu * 3 * j) + (2 * nu);
            ++counter;
          }
        }
      }

      for (unsigned int ii = 0; ii != gaussPts.size2(); ii++) {
        gaussPts(2, ii) = 1. / total_gp;
      }

      MoFEMFunctionReturnHot(0);
    }

    // quadrangles
    if (type == MBQUAD) {

      side = numSide;
      total_gp = side * side;

      const double ksi = 1. / (side * 2);
      const double eta = 1. / (side * 2);

      gaussPts.resize(3, total_gp);
      gaussPts.clear();

      counter = 0;

      for (int i = 0; i < side; ++i) {
        for (int j = 0; j < side; ++j) {
          gaussPts(0, counter) = ksi + (ksi * i * 2);
          gaussPts(1, counter) = eta + (eta * j * 2);
          ++counter;
        }
      }

      for (unsigned int ii = 0; ii != gaussPts.size2(); ii++) {
        gaussPts(2, ii) = 1. / total_gp;
      }

      MoFEMFunctionReturnHot(0);
    }
  }
};

#endif //__PATCH_INTEG_2D_HPP__
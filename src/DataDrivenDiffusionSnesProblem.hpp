#ifndef __DIFFUSION_SNES_DD_HPP__
#define __DIFFUSION_SNES_DD_HPP__

#include <alt_boost_geometry_includes.hpp>
#include <DataDrivenDiffusionProblem.hpp>
#include <RtreeManipulation.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct DiffusionSnesDD : public virtual DiffusionDD {
public:
  DiffusionSnesDD(MoFEM::Interface &m_field)
      : DiffusionDD(m_field), ClassicDiffusionProblem(m_field) {};

  MoFEMErrorCode runProblem() override;
  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode createDDsnesCommonData();
  MoFEMErrorCode assembleSystem() override;
  MoFEMErrorCode virtual setRandomInitialFields();
  MoFEMErrorCode getSnesInitial();
  MoFEMErrorCode solveSnesSystem();
  MoFEMErrorCode virtual setSnesMonitors();
  MoFEMErrorCode runMonteCarlo() override;
  static MoFEMErrorCode
  MyConvergenceTest(SNES snes, PetscInt it, PetscReal xnorm, PetscReal gnorm,
                    PetscReal f, SNESConvergedReason *reason, void *ctx);
  MoFEMErrorCode checkErrorDD(int iter_num, bool print,
                              std::string vtk_name = "out_error_");

  PetscBool flg_part_star = PETSC_FALSE;
  PetscReal partial_part_a = 1.;
  PetscReal partial_part_b = 0.;
  bool flg_partial_part = false;

  // common data
  struct CommonDataSnesDD : virtual public CommonDataDD {
    boost::shared_ptr<MatrixDouble> mGradLagrangeScalarPtr;
  };

  // common data
  boost::shared_ptr<CommonDataSnesDD> commonDataPtr;

  struct CombinedPointers {
    boost::shared_ptr<CommonDataSnesDD> commonDataPtr;
    boost::shared_ptr<MoFEM::SnesCtx> snesCtxPtr;
  };

  boost::shared_ptr<CombinedPointers> combinedPointers;

  // nonliner SNES solver using SmartPetscObj
  SmartPetscObj<SNES> snesSolver;
};

#endif //__DIFFUSION_SNES_DD_HPP__
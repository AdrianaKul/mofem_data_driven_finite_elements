#ifndef __NATURAL_DOMAIN_DD_BCS__
#define __NATURAL_DOMAIN_DD_BCS__

/**
 * @file NaturalDomainBC.hpp
 * @brief Boundary conditions in domain, i.e. body forces.
 * @version 0.13.2
 * @date 2022-11-22
 *
 * @copyright Copyright (c) 2022
 *
 */

namespace DDconstrains {

template <CubitBC BC> struct SourceBcType {};
template <CubitBC BC> struct SourceBcGet {};

template <CubitBC> struct GetSourceBlocksets {
  GetSourceBlocksets() = delete;

  static MoFEMErrorCode GetBlocksets(double &block_scale,
                                     boost::shared_ptr<Range> &ents,
                                     MoFEM::Interface &m_field, int ms_id) {
    MoFEMFunctionBegin;

    auto cubit_meshset_ptr =
        m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(ms_id,
                                                                    BLOCKSET);
    std::vector<double> block_data;
    CHKERR cubit_meshset_ptr->getAttributes(block_data);
    if (block_data.size() >= 1)
      block_scale = block_data[0];
    else
      block_scale = 1.0;

    ents = boost::make_shared<Range>();
    CHKERR
    m_field.get_moab().get_entities_by_handle(cubit_meshset_ptr->meshset,
                                              *(ents), true);

    MoFEMFunctionReturn(0);
  }
};
} // namespace DDconstrains

template <int FIELD_DIM, AssemblyType A, typename EleOp>
struct OpFluxRhsImpl<DDconstrains::SourceBcType<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp> : OpBaseImpl<A, EleOp> {

  using OpBase = OpBaseImpl<A, EleOp>;

  OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id, std::string field_name,
                ScalarFun source_fun, double scale);

protected:
  double blockScale;
  double rhsScale;
  ScalarFun sourceFun;
  MoFEMErrorCode iNtegrate(EntitiesFieldData::EntData &data);
};

template <int FIELD_DIM, AssemblyType A, typename EleOp>
struct OpFluxRhsImpl<DDconstrains::SourceBcGet<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp> : OpBaseImpl<A, EleOp> {

  using OpBase = OpBaseImpl<A, EleOp>;

  OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id, std::string field_name,
                ScalarFun source_fun, boost::shared_ptr<VectorDouble> m_val_prt,
                double scale);

  // redifine aSsemble to stop trying to add to RHS of nonexistent ksp solver
  MoFEMErrorCode aSsemble(EntData &row_data, EntData &col_data,
                          const bool trans) {
    MoFEMFunctionBegin;
    MoFEMFunctionReturn(0);
  };
  MoFEMErrorCode aSsemble(EntData &data) {
    MoFEMFunctionBegin;
    MoFEMFunctionReturn(0);
  };

protected:
  double blockScale;
  double rhsScale;
  ScalarFun sourceFun;
  boost::shared_ptr<VectorDouble> mValPtr;
  MoFEMErrorCode iNtegrate(EntitiesFieldData::EntData &data);
};

template <int FIELD_DIM, AssemblyType A, typename EleOp>
OpFluxRhsImpl<DDconstrains::SourceBcType<BLOCKSET>, 1, FIELD_DIM, A, GAUSS,
              EleOp>::OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id,
                                    std::string field_name,
                                    ScalarFun source_fun, double scale)
    : OpBase(field_name, field_name, OpBase::OPROW), sourceFun(source_fun),
      rhsScale(scale) {
  CHK_THROW_MESSAGE(DDconstrains::GetSourceBlocksets<BLOCKSET>::GetBlocksets(
                        this->blockScale, this->entsPtr, m_field, ms_id),
                    "Can not read source data from blockset");
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
OpFluxRhsImpl<DDconstrains::SourceBcGet<BLOCKSET>, 1, FIELD_DIM, A, GAUSS,
              EleOp>::OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id,
                                    std::string field_name,
                                    ScalarFun source_fun,
                                    boost::shared_ptr<VectorDouble> m_val_prt,
                                    double scale)
    : OpBase(field_name, field_name, OpBase::OPROW), sourceFun(source_fun),
      mValPtr(m_val_prt), rhsScale(scale) {
  CHK_THROW_MESSAGE(DDconstrains::GetSourceBlocksets<BLOCKSET>::GetBlocksets(
                        this->blockScale, this->entsPtr, m_field, ms_id),
                    "Can not read source data from blockset");
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
MoFEMErrorCode
MoFEM::OpFluxRhsImpl<DDconstrains::SourceBcType<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp>::iNtegrate(EntitiesFieldData::EntData
                                                  &row_data) {
  FTensor::Index<'i', FIELD_DIM> i;
  MoFEMFunctionBegin;
  // get element volume
  const double vol = OpBase::getMeasure();
  // get integration weights
  auto t_w = OpBase::getFTensor0IntegrationWeight();
  // get base function gradient on rows
  auto t_row_base = row_data.getFTensor0N();

  // get coordinate at integration points
  auto t_coords = OpBase::getFTensor1CoordsAtGaussPts();

  // loop over integration points
  for (int gg = 0; gg != OpBase::nbIntegrationPts; gg++) {
    // take into account Jacobian
    const double alpha = t_w * vol * rhsScale;

    // source function
    auto t_source = sourceFun(t_coords(0), t_coords(1), t_coords(2));

    // loop over rows base functions
    int rr = 0;
    for (; rr != OpBase::nbRows / FIELD_DIM; ++rr) {
      // add to Rhs
      OpBase::locF[rr] += alpha * t_row_base * blockScale * t_source;
      ++t_row_base;
    }

    for (; rr < OpBase::nbRowBaseFunctions; ++rr)
      ++t_row_base;
    ++t_coords;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
MoFEMErrorCode
MoFEM::OpFluxRhsImpl<DDconstrains::SourceBcGet<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp>::iNtegrate(EntitiesFieldData::EntData
                                                  &row_data) {
  FTensor::Index<'i', FIELD_DIM> i;
  MoFEMFunctionBegin;

  const size_t nb_gauss_pts = OpBase::nbIntegrationPts;
  mValPtr->resize(nb_gauss_pts);
  mValPtr->clear();
  auto t_val = getFTensor0FromVec(*(mValPtr));

  // get coordinate at integration points
  auto t_coords = OpBase::getFTensor1CoordsAtGaussPts();

  // loop over integration points
  for (int gg = 0; gg != OpBase::nbIntegrationPts; gg++) {

    // source function
    t_val = sourceFun(t_coords(0), t_coords(1), t_coords(2)) * blockScale *
            rhsScale;

    ++t_coords;
    ++t_val;
  }
  MoFEMFunctionReturn(0);
}

template <CubitBC BCTYPE, int BASE_DIM, int FIELD_DIM, AssemblyType A,
          IntegrationType I, typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<DDconstrains::SourceBcType<BCTYPE>, BASE_DIM, FIELD_DIM, A, I,
                  OpBase>,
    A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;

  static MoFEMErrorCode add(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, ScalarFun source_fun,
      double scale, std::string block_name, Sev sev

  ) {
    MoFEMFunctionBegin;

    using OP = OpFluxRhsImpl<DDconstrains::SourceBcType<BLOCKSET>, BASE_DIM,
                             FIELD_DIM, PETSC, GAUSS, OpBase>;

    auto add_op = [&](auto &&meshset_vec_ptr) {
      for (auto m : meshset_vec_ptr) {
        MOFEM_TAG_AND_LOG("WORLD", sev, "OpSourceRhs") << "Add " << *m;
        pipeline.push_back(
            new OP(m_field, m->getMeshsetId(), field_name, source_fun, scale));
      }
      MOFEM_LOG_CHANNEL("WORLD");
    };

    switch (BCTYPE) {
    case BLOCKSET:
      add_op(

          m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(
              std::regex(

                  (boost::format("%s(.*)") % block_name).str()

                      ))

      );

      break;
    default:
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED,
              "Handling of bc type not implemented");
      break;
    }
    MoFEMFunctionReturn(0);
  }
};

template <CubitBC BCTYPE, int BASE_DIM, int FIELD_DIM, AssemblyType A,
          IntegrationType I, typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<DDconstrains::SourceBcGet<BCTYPE>, BASE_DIM, FIELD_DIM, A, I,
                  OpBase>,
    A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;

  static MoFEMErrorCode get_val(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, ScalarFun source_fun,
      boost::shared_ptr<VectorDouble> m_val_prt, double scale,
      std::string block_name, Sev sev

  ) {
    MoFEMFunctionBegin;

    using OP = OpFluxRhsImpl<DDconstrains::SourceBcGet<BLOCKSET>, BASE_DIM,
                             FIELD_DIM, PETSC, GAUSS, OpBase>;

    auto add_op = [&](auto &&meshset_vec_ptr) {
      for (auto m : meshset_vec_ptr) {
        MOFEM_TAG_AND_LOG("WORLD", sev, "OpSourceGet") << "Add " << *m;
        pipeline.push_back(new OP(m_field, m->getMeshsetId(), field_name,
                                  source_fun, m_val_prt, scale));
      }
      MOFEM_LOG_CHANNEL("WORLD");
    };

    switch (BCTYPE) {
    case BLOCKSET:
      add_op(

          m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(
              std::regex(

                  (boost::format("%s(.*)") % block_name).str()

                      ))

      );

      break;
    default:
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED,
              "Handling of bc type not implemented");
      break;
    }
    MoFEMFunctionReturn(0);
  }
};

template <int BASE_DIM, int FIELD_DIM, AssemblyType A, IntegrationType I,
          typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<DomainBCs, BASE_DIM, FIELD_DIM, A, I, OpBase>, A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;

  using T =
      typename NaturalBC<OpBase>::template Assembly<A>::template LinearForm<I>;

  using OpBodySource = typename T::template OpFlux<NaturalMeshsetType<BLOCKSET>,
                                                   BASE_DIM, FIELD_DIM>;

  using OpBodyFuncSource =
      typename T::template OpFlux<DDconstrains::SourceBcType<BLOCKSET>,
                                  BASE_DIM, 1>;

  using OpBodySourceGet =
      typename T::template OpFlux<DDconstrains::SourceBcGet<BLOCKSET>, BASE_DIM,
                                  1>;

  static MoFEMErrorCode add(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, double scale, Sev sev

  ) {
    MoFEMFunctionBegin;

    CHKERR T::template AddFluxToPipeline<OpBodySource>::add(
        pipeline, m_field, field_name, {}, "SOURCE_UNIFORM", sev);

    auto source_mexi = [](const double x, const double y, const double z) {
      return -exp(-100. * (x * x + y * y)) *
             (400. * M_PI *
                  (x * cos(M_PI * y) * sin(M_PI * x) +
                   y * cos(M_PI * x) * sin(M_PI * y)) +
              2. * (20000. * (x * x + y * y) - 200. - M_PI * M_PI) *
                  cos(M_PI * x) * cos(M_PI * y));
    };

    CHKERR T::template AddFluxToPipeline<OpBodyFuncSource>::add(
        pipeline, m_field, field_name, source_mexi, scale, "SOURCE_MEXI", sev);

    auto source_nonlinear_L = [](const double x, const double y,
                                 const double z) {
      double a = 1.;
      double b = 1.;
      return -4. * b / (9. * pow(x * x + y * y, 1. / 3.));
    };

    CHKERR T::template AddFluxToPipeline<OpBodyFuncSource>::add(
        pipeline, m_field, field_name, source_nonlinear_L, scale, "SOURCE_NL",
        sev);

    auto source_octopus = [](const double x, const double y, const double z) {
      double a = 1.;
      double b = 1.;
      double ee = -10 * x * x * y * y;

      double source =
          20 * exp(ee) * (a + b * exp(ee)) * x * x +
          20 * exp(ee) * (a + b * exp(ee)) * y * y -
          400 * b * exp(2 * ee) * x * x * x * x * y * y -
          400 * exp(ee) * (a + b * exp(ee)) * x * x * x * x * y * y -
          400 * b * exp(2 * ee) * x * x * y * y * y * y -
          400 * exp(ee) * (a + b * exp(ee)) * x * x * y * y * y * y;
      return source;
    };

    CHKERR T::template AddFluxToPipeline<OpBodyFuncSource>::add(
        pipeline, m_field, field_name, source_octopus, scale, "SOURCE_OCTOPUS",
        sev);

    auto source_square_sincos = [](const double x, const double y,
                                   const double z) {
      double a = 2.;

      double source =
          2 * a * a * M_PI * M_PI * cos(a * M_PI * y) * sin(a * M_PI * x);
      return source;
    };

    CHKERR T::template AddFluxToPipeline<OpBodyFuncSource>::add(
        pipeline, m_field, field_name, source_square_sincos, scale,
        "SOURCE_SQUARE_SINCOS", sev);

    auto source_square_nonlinear_sincos = [](const double x, const double y,
                                             const double z) {
      double a = 2.;
      double aa = 1.;
      double bb = 1.;
      double cc = 1.;

      double source =
          1.0 / 8.0 * a * a * M_PI * M_PI *
          (-4 * bb * cos(2 * a * M_PI * x) +
           4 * bb * (1 - 2 * cos(2 * a * M_PI * x)) * cos(2 * a * M_PI * y) -
           2 * (-8 * aa + cc + 5 * cc * cos(2 * a * M_PI * x)) *
               cos(a * M_PI * y) * sin(a * M_PI * x) +
           cc * cos(3 * a * M_PI * y) *
               (5 * sin(a * M_PI * x) - 3 * sin(3 * a * M_PI * x)));
      return source;
    };

    CHKERR T::template AddFluxToPipeline<OpBodyFuncSource>::add(
        pipeline, m_field, field_name, source_square_nonlinear_sincos, scale,
        "SOURCE_SQUARE_NONLINEAR_SINCOS", sev);

    MoFEMFunctionReturn(0);
  }

  static MoFEMErrorCode get_val(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name,
      boost::shared_ptr<VectorDouble> m_val_prt, double scale, Sev sev

  ) {
    MoFEMFunctionBegin;

    auto source_zero = [](const double x, const double y, const double z) {
      return 0.0;
    };

    CHKERR T::template AddFluxToPipeline<OpBodySourceGet>::get_val(
        pipeline, m_field, field_name, source_zero, m_val_prt, scale, "", sev);

    auto source_one = [](const double x, const double y, const double z) {
      return 1.0;
    };

    CHKERR T::template AddFluxToPipeline<OpBodySourceGet>::get_val(
        pipeline, m_field, field_name, source_one, m_val_prt, scale,
        "SOURCE_UNIFORM", sev);

    auto source_mexi = [](const double x, const double y, const double z) {
      return -exp(-100. * (x * x + y * y)) *
             (400. * M_PI *
                  (x * cos(M_PI * y) * sin(M_PI * x) +
                   y * cos(M_PI * x) * sin(M_PI * y)) +
              2. * (20000. * (x * x + y * y) - 200. - M_PI * M_PI) *
                  cos(M_PI * x) * cos(M_PI * y));
    };

    CHKERR T::template AddFluxToPipeline<OpBodySourceGet>::get_val(
        pipeline, m_field, field_name, source_mexi, m_val_prt, scale,
        "SOURCE_MEXI", sev);

    auto source_nonlinear_L = [](const double x, const double y,
                                 const double z) {
      double a = 1.;
      double b = 1.;
      return -4. * b / (9. * pow(x * x + y * y, 1. / 3.));
    };

    CHKERR T::template AddFluxToPipeline<OpBodySourceGet>::get_val(
        pipeline, m_field, field_name, source_nonlinear_L, m_val_prt, scale,
        "SOURCE_NL", sev);

    auto source_octopus = [](const double x, const double y, const double z) {
      double a = 1.;
      double b = 1.;
      double ee = -10 * x * x * y * y;

      double source =
          20 * exp(ee) * (a + b * exp(ee)) * x * x +
          20 * exp(ee) * (a + b * exp(ee)) * y * y -
          400 * b * exp(2 * ee) * x * x * x * x * y * y -
          400 * exp(ee) * (a + b * exp(ee)) * x * x * x * x * y * y -
          400 * b * exp(2 * ee) * x * x * y * y * y * y -
          400 * exp(ee) * (a + b * exp(ee)) * x * x * y * y * y * y;
      return source;
    };

    CHKERR T::template AddFluxToPipeline<OpBodySourceGet>::get_val(
        pipeline, m_field, field_name, source_octopus, m_val_prt, scale,
        "SOURCE_OCTOPUS", sev);

    auto source_square_sincos = [](const double x, const double y,
                                   const double z) {
      double a = 2.;

      double source =
          2 * a * a * M_PI * M_PI * cos(a * M_PI * y) * sin(a * M_PI * x);
      return source;
    };

    CHKERR T::template AddFluxToPipeline<OpBodySourceGet>::get_val(
        pipeline, m_field, field_name, source_square_sincos, m_val_prt, scale,
        "SOURCE_SQUARE_SINCOS", sev);

    auto source_square_nonlinear_sincos = [](const double x, const double y,
                                             const double z) {
      double a = 2.;
      double aa = 1.;
      double bb = 1.;
      double cc = 1.;

      double source =
          1.0 / 8.0 * a * a * M_PI * M_PI *
          (-4 * bb * cos(2 * a * M_PI * x) +
           4 * bb * (1 - 2 * cos(2 * a * M_PI * x)) * cos(2 * a * M_PI * y) -
           2 * (-8 * aa + cc + 5 * cc * cos(2 * a * M_PI * x)) *
               cos(a * M_PI * y) * sin(a * M_PI * x) +
           cc * cos(3 * a * M_PI * y) *
               (5 * sin(a * M_PI * x) - 3 * sin(3 * a * M_PI * x)));
      return source;
    };

    CHKERR T::template AddFluxToPipeline<OpBodySourceGet>::get_val(
        pipeline, m_field, field_name, source_square_nonlinear_sincos,
        m_val_prt, scale, "SOURCE_SQUARE_NONLINEAR_SINCOS", sev);

    MoFEMFunctionReturn(0);
  }
};

#endif //__NATURAL_DOMAIN_DD_BCS__
#ifndef __DIFFUSION_NONLINEAR_SNES_HPP__
#define __DIFFUSION_NONLINEAR_SNES_HPP__

#include <alt_boost_geometry_includes.hpp>
#include <DataDrivenDiffusionSnesProblem.hpp>
#include <RtreeManipulation.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

typedef boost::function<double(
    const FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 2>)>
    MixFunc;

struct DiffusionNonlinearSnes : public DiffusionSnesDD {
public:
  DiffusionNonlinearSnes(MoFEM::Interface &m_field)
      : DiffusionSnesDD(m_field), DiffusionDD(m_field),
        ClassicDiffusionProblem(m_field) {
    valueField = "P_reference";
  };

  MoFEMErrorCode runProblem() override;
  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode assembleSystem() override;
  MoFEMErrorCode setSnesMonitors() override;
  MoFEMErrorCode outputCsvDataset(bool header = true);

  PetscReal nonlinear_alpha = 0.;
  PetscReal nonlinear_k = 1.;
  PetscBool flgNonlinearToP = PETSC_TRUE;
  PetscBool flgNonlinearToG = PETSC_FALSE;

  // Operators

  struct OpGradGradNonlinearLhs : public OpFaceEle {
  public:
    OpGradGradNonlinearLhs(std::string row_field_name,
                           std::string col_field_name,
                           boost::shared_ptr<MatrixDouble> grad_pointer,
                           double nonlinear_alpha = 1., double nonlinear_k = 1.)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          gradPtr(grad_pointer), nonlinearAlpha(nonlinear_alpha),
          nonlinearK(nonlinear_k) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    double nonlinearAlpha;
    double nonlinearK;
    boost::shared_ptr<MatrixDouble> gradPtr;
    MatrixDouble locLhs;
  };

  struct OpGradGradNonlinearPLhs : public OpFaceEle {
  public:
    OpGradGradNonlinearPLhs(std::string row_field_name,
                            std::string col_field_name,
                            boost::shared_ptr<VectorDouble> val_pointer,
                            boost::shared_ptr<MatrixDouble> grad_pointer,
                            double nonlinear_alpha = 1.,
                            double nonlinear_k = 1.)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          valPtr(val_pointer), gradPtr(grad_pointer),
          nonlinearAlpha(nonlinear_alpha), nonlinearK(nonlinear_k) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    double nonlinearAlpha;
    double nonlinearK;
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    MatrixDouble locLhs;
  };

  struct OpGradNonlinearRhs : public OpFaceEle {
  public:
    OpGradNonlinearRhs(std::string field_name,
                       boost::shared_ptr<MatrixDouble> grad_pointer,
                       double nonlinear_alpha = 1., double nonlinear_k = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), gradPtr(grad_pointer),
          nonlinearAlpha(nonlinear_alpha), nonlinearK(nonlinear_k) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    double nonlinearAlpha;
    double nonlinearK;
    boost::shared_ptr<MatrixDouble> gradPtr;
    VectorDouble locRhs;
  };

  struct OpGradNonlinearPRhs : public OpFaceEle {
  public:
    OpGradNonlinearPRhs(std::string field_name,
                        boost::shared_ptr<VectorDouble> val_pointer,
                        boost::shared_ptr<MatrixDouble> grad_pointer,
                        double nonlinear_alpha = 1., double nonlinear_k = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), valPtr(val_pointer),
          gradPtr(grad_pointer), nonlinearAlpha(nonlinear_alpha),
          nonlinearK(nonlinear_k) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    double nonlinearAlpha;
    double nonlinearK;
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    VectorDouble locRhs;
  };

  struct OpGetFluxFromGrad : public OpFaceEle {
  public:
    OpGetFluxFromGrad(std::string field_name,
                      boost::shared_ptr<MatrixDouble> grad_pointer,
                      boost::shared_ptr<MatrixDouble> flux_pointer,
                      MixFunc k_Function)
        : OpFaceEle(field_name, OpFaceEle::OPROW), gradPtr(grad_pointer),
          fluxPtr(flux_pointer), kFunc(k_Function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> gradPtr;
    boost::shared_ptr<MatrixDouble> fluxPtr;
    MixFunc kFunc;
  };

  struct OpGetFluxFromVal : public OpFaceEle {
  public:
    OpGetFluxFromVal(std::string field_name,
                     boost::shared_ptr<VectorDouble> val_pointer,
                     boost::shared_ptr<MatrixDouble> grad_pointer,
                     boost::shared_ptr<MatrixDouble> flux_pointer,
                     ScalarFunc k_Function)
        : OpFaceEle(field_name, OpFaceEle::OPROW), valPtr(val_pointer),
          gradPtr(grad_pointer), fluxPtr(flux_pointer), kFunc(k_Function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    boost::shared_ptr<MatrixDouble> fluxPtr;
    ScalarFunc kFunc;
  };
};

#endif //__DIFFUSION_NONLINEAR_SNES_HPP__
#ifndef __DIFFUSION_DD_HPP__
#define __DIFFUSION_DD_HPP__

#include <alt_boost_geometry_includes.hpp>
#include <ClassicDiffusionProblem.hpp>
#include <RtreeManipulation.hpp>
#include <OperatorsForDD.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct DiffusionDD : virtual public ClassicDiffusionProblem {
public:
  DiffusionDD(MoFEM::Interface &m_field)
      : ClassicDiffusionProblem(m_field), fluxField("Q"), lagrangeField("L") {
    valueField = "T";
  };

  MoFEMErrorCode SetParamDDValues();
  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode createDDCommonData();
  virtual MoFEMErrorCode removeLambda();
  MoFEMErrorCode assembleSystem() override;
  MoFEMErrorCode outputHVtkFields(int ii, std::string vtk_name = "out_result_");
  MoFEMErrorCode outputResults();
  MoFEMErrorCode errorFileNameSetUp();
  MoFEMErrorCode errorCalculation(bool print_on_integ = false);
  MoFEMErrorCode errorHandlingWithinLoop(bool allowed);
  MoFEMErrorCode solveDDLoopSystem();
  MoFEMErrorCode setParamMonteCarlo();
  virtual MoFEMErrorCode runMonteCarlo();
  MoFEMErrorCode monteSaveHResults();
  virtual MoFEMErrorCode saveSumanalys(std::string csv_name = "sumanalys.csv");

  SearchData searchData;

  // DD param file values
  char csv_tree_file[255];
  int dummy_count = 0;
  PetscInt datasetDim = 4;
  PetscBool skip_vtk = PETSC_FALSE;
  PetscBool flg_long_error_file = PETSC_FALSE;
  PetscBool flg_out_integ = PETSC_FALSE;
  PetscBool print_moab = PETSC_FALSE;
  PetscReal tol_poi_dist = 1e-5;
  PetscInt max_iter = 100;
  PetscBool flg_max_iter = PETSC_FALSE;
  PetscBool flg_scaling = PETSC_FALSE;
  PetscBool flg_use_line = PETSC_FALSE;
  PetscBool flg_rand_ini = PETSC_FALSE;
  PetscBool dd_use_data_value = PETSC_FALSE;

  PetscBool flg_point_average = PETSC_FALSE;
  PetscInt point_average = 10;

  // monte-carlo parameters
  PetscBool flg_monte_carlo = PETSC_FALSE;
  int solution_size;
  Mat matGlobalResults;

  // common data
  struct CommonDataDD : virtual public CommonDataClassic {
    // boost::shared_ptr<MatrixDouble> mFluxPtr;
    boost::shared_ptr<VectorDouble> mValStarPtr;
    boost::shared_ptr<MatrixDouble> mGradStarPtr;
    boost::shared_ptr<MatrixDouble> mFluxStarPtr;

    boost::shared_ptr<MatrixDouble> mKStarPtr;
    boost::shared_ptr<MatrixDouble> mKStarCorrelationPtr;

    boost::shared_ptr<VectorDouble> mValRefPtr;

    int counter;
    bgi::rtree<point4D, bgi::rstar<16, 4>> rtree4D;
    bgi::rtree<point5D, bgi::rstar<16, 4>> rtree5D;
    boost::shared_ptr<VectorDouble>
        pointDistance; // distance between point searched for and point found in
                       // data set at each gauss point

    // vector of accumulated distance results
    std::vector<double> distVec;
    std::vector<double> rmsPointDistErr;
    double tolPoiDist = 0.0; // tolerance for point distance

    bool pReference = false;
    bool flgScaling;
    double linear_k = 1.;

    bool flgPointAverage;
    int pointAverage;

    bool flgPrintK = false;
    bool skip_vtk = false;

    // print values on itntegration points
    bool flgPrintInteg = false;

    // monte-carlo

    int numberMonteCarlo;
    int monte_carlo_counter;
    // max sigma for for integ point
    double integ_sigma_max_p;
    double integ_sigma_max_grad_x, integ_sigma_max_grad_y;
    double integ_sigma_max_flux_x, integ_sigma_max_flux_y;

    // monte saving to vtk
    // average values
    boost::shared_ptr<VectorDouble> mValAvePtr;
    boost::shared_ptr<MatrixDouble> mGradAvePtr;
    boost::shared_ptr<MatrixDouble> mFluxAvePtr;
    // standard_deviation values
    boost::shared_ptr<VectorDouble> mValSigmaPtr;
    boost::shared_ptr<MatrixDouble> mGradSigmaPtr;
    boost::shared_ptr<MatrixDouble> mFluxSigmaPtr;

    // perturbation multiplier
    double perturb_multiplier;
    int monte_patch_number = 1;
  };

  // Function to calculate S_p term (change later)
  static double s_p_Function(const double x, const double y, const double z) {
    return 1.;
  }
  // Function to calculate S_q term (change later)
  static double s_q_Function(const double x, const double y, const double z) {
    return 1.;
  }

  // Field name and approximation order
  std::string fluxField;
  std::string lagrangeField;

  // common data
  boost::shared_ptr<CommonDataDD> commonDataPtr;

  // errors
  string csvErrorsName;
  Vec go_per_proc;

  // Operators

  struct OpPointError : public OpFaceEle {
  public:
    OpPointError(std::string field_name,
                 boost::shared_ptr<CommonDataDD> common_data_ptr)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<CommonDataDD> commonDataPtr;
  };

  // post processing

  template <int N> struct OpPostProcErrorFluxIntegPts : public OpFaceEle {
  public:
    OpPostProcErrorFluxIntegPts(std::string field_name,
                                boost::shared_ptr<CommonDataDD> common_data_ptr,
                                VectorFunc analytical_flux)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr), analyticalFlux(analytical_flux){};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    VectorFunc analyticalFlux;
    boost::shared_ptr<CommonDataDD> commonDataPtr;
  };

  template <int N> struct OpPostProcPrintingIntegPts : public OpFaceEle {
  public:
    OpPostProcPrintingIntegPts(std::string field_name,
                               boost::shared_ptr<CommonDataDD> common_data_ptr,
                               moab::Interface &output_mesh,
                               ScalarFunc analytical_function)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr), outputMesh(output_mesh),
          analyticalFunc(analytical_function){};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    ScalarFunc analyticalFunc;
    moab::Interface &outputMesh;
    boost::shared_ptr<CommonDataDD> commonDataPtr;
  };

  template <int N> struct OpPostProcPrintFarIntegPts : public OpFaceEle {
  public:
    OpPostProcPrintFarIntegPts(std::string field_name,
                               boost::shared_ptr<CommonDataDD> common_data_ptr,
                               double comperative_point_distance,
                               std::string csv_name = "far_points.csv")
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr),
          comperativePointDistance(comperative_point_distance),
          csvName(csv_name){};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<CommonDataDD> commonDataPtr;
    double comperativePointDistance;
    std::string csvName;
  };

  template <int N> struct OpPrintStarIntegPts : public OpFaceEle {
  public:
    OpPrintStarIntegPts(std::string field_name,
                        boost::shared_ptr<CommonDataDD> common_data_ptr,
                        moab::Interface &output_mesh)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr), outputMesh(output_mesh){};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    moab::Interface &outputMesh;
    boost::shared_ptr<CommonDataDD> commonDataPtr;
  };

  // monte-carlo operators
  struct OpResizeMonteMatrix : public OpFaceEle {

  public:
    OpResizeMonteMatrix(std::string field_name, int monte_number,
                        MoFEM::Interface &m_field)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          monteNumberTotal(monte_number), mField(m_field) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    MoFEM::Interface &mField;
    int monteNumberTotal;
  };

  template <int N> struct OpAddResultToMonteMatrix : public OpFaceEle {

  public:
    OpAddResultToMonteMatrix(std::string field_name,
                             boost::shared_ptr<CommonDataDD> common_data_ptr,
                             MoFEM::Interface &m_field)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr), mField(m_field) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    MoFEM::Interface &mField;
    boost::shared_ptr<CommonDataDD> commonDataPtr;
  };

  struct OpGetMeanVar : public ForcesAndSourcesCore::UserDataOperator {

  public:
    OpGetMeanVar(boost::shared_ptr<CommonDataDD> common_data_ptr,
                 MoFEM::Interface &m_field, moab::Interface &output_mesh)
        : ForcesAndSourcesCore::UserDataOperator(
              L2, ForcesAndSourcesCore::UserDataOperator::OPSPACE),
          commonDataPtr(common_data_ptr), mField(m_field),
          outputMesh(output_mesh) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<CommonDataDD> commonDataPtr;
    MoFEM::Interface &mField;
    moab::Interface &outputMesh;
  };

  struct OpTagsMeanVar : public OpFaceEle {

  public:
    OpTagsMeanVar(std::string field_name,
                  boost::shared_ptr<CommonDataDD> common_data_ptr,
                  MoFEM::Interface &m_field, moab::Interface &output_mesh)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr), mField(m_field),
          outputMesh(output_mesh) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<CommonDataDD> commonDataPtr;
    MoFEM::Interface &mField;
    moab::Interface &outputMesh;
  };

  struct OpPostProcRefErrorIntegPts : public OpFaceEle {
  public:
    OpPostProcRefErrorIntegPts(std::string field_name,
                               boost::shared_ptr<CommonDataDD> common_data_ptr)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr){};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<CommonDataDD> commonDataPtr;
  };
};

#endif //__DIFFUSION_DD_HPP__
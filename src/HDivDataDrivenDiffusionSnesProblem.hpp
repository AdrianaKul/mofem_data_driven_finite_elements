#ifndef __HDIV_DIFFUSION_SNES_DD_HPP__
#define __HDIV_DIFFUSION_SNES_DD_HPP__

#include <DataDrivenDiffusionSnesProblem.hpp>
#include <HDivDataDrivenDiffusionProblem.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct DiffusionHDivSnesDD : public DiffusionSnesDD, DiffusionHDivDD {
public:
  DiffusionHDivSnesDD(MoFEM::Interface &m_field)
      : DiffusionSnesDD(m_field), DiffusionHDivDD(m_field),
        DiffusionDD(m_field), ClassicDiffusionProblem(m_field) {};

  MoFEMErrorCode runProblem() override;
  MoFEMErrorCode createDDHdivsnesCommonData();
  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode boundaryCondition() override;
  MoFEMErrorCode assembleSystem() override;
  MoFEMErrorCode assembleEssentialBoundary() override;
  MoFEMErrorCode setRandomInitialFields() override;
  MoFEMErrorCode setSnesMonitors() override;
  MoFEMErrorCode addMeshRefForDistanceVar();
  MoFEMErrorCode refineOrderLoop();
  MoFEMErrorCode recheckOrder() override;
  MoFEMErrorCode outputFarPointsCsv(std::string vtk_name = "far_points.csv");
  MoFEMErrorCode runMonteCarlo() override;
  MoFEMErrorCode saveSumanalys(std::string csv_name = "sumanalys.csv") override;

  struct CommonDataHDivSnesDD : public CommonDataSnesDD, CommonDataHDivDD {

    boost::shared_ptr<VectorDouble> mLagrangePtr;
    boost::shared_ptr<MatrixDouble> mTauPtr;

    boost::shared_ptr<VectorDouble> mDivFluxPtr;
    boost::shared_ptr<VectorDouble> mDivTauPtr;
  };

  boost::shared_ptr<CommonDataHDivSnesDD> commonDataPtr;

  struct CombinedPointers {
    boost::shared_ptr<CommonDataHDivSnesDD> commonDataPtr;
    boost::shared_ptr<MoFEM::SnesCtx> snesCtxPtr;
  };

  boost::shared_ptr<CombinedPointers> combinedPointers;

  struct OpPostProcPrintNormalPts : public OpEdgeEle {
  public:
    OpPostProcPrintNormalPts(std::string field_name,
                             moab::Interface &output_mesh)
        : OpEdgeEle(field_name, OpEdgeEle::OPROW), outputMesh(output_mesh) {};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    moab::Interface &outputMesh;
  };
};

#endif //__HDIV_DIFFUSION_SNES_DD_HPP__
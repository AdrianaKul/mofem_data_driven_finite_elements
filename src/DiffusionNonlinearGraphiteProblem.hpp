#ifndef __DIFFUSION_NONLINEAR_GRAPHITE_HPP__
#define __DIFFUSION_NONLINEAR_GRAPHITE_HPP__

#include <alt_boost_geometry_includes.hpp>
#include <DataDrivenDiffusionSnesProblem.hpp>
#include <RtreeManipulation.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

typedef boost::function<double(
    const FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 2>)>
    MixFunc;

struct DiffusionNonlinearGraphite : public DiffusionSnesDD {
public:
  DiffusionNonlinearGraphite(MoFEM::Interface &m_field)
      : DiffusionSnesDD(m_field), DiffusionDD(m_field),
        ClassicDiffusionProblem(m_field) {
    valueField = "T";
  };

  MoFEMErrorCode runProblem() override;
  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode boundaryCondition() override;
  MoFEMErrorCode assembleSystem() override;
  MoFEMErrorCode setSnesMonitors() override;
  MoFEMErrorCode outputCsvDataset(bool header = true);

  // q = (a + bT + cT^2) g
  PetscReal nonlinear_a = 134.;
  PetscReal nonlinear_b = -0.1074;
  PetscReal nonlinear_c = 3.719e-5;

  // Operators

  struct OpNonlinearLhs : public OpFaceEle {
  public:
    OpNonlinearLhs(std::string row_field_name, std::string col_field_name,
                   boost::shared_ptr<VectorDouble> val_pointer,
                   boost::shared_ptr<MatrixDouble> grad_pointer,
                   double nonlinear_a = 1., double nonlinear_b = 1.,
                   double nonlinear_c = 1.)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          valPtr(val_pointer), gradPtr(grad_pointer), nonlinearA(nonlinear_a),
          nonlinearB(nonlinear_b), nonlinearC(nonlinear_c) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    double nonlinearA;
    double nonlinearB;
    double nonlinearC;
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    MatrixDouble locLhs;
  };

  struct OpNonlinearRhs : public OpFaceEle {
  public:
    OpNonlinearRhs(std::string field_name,
                   boost::shared_ptr<VectorDouble> val_pointer,
                   boost::shared_ptr<MatrixDouble> grad_pointer,
                   double nonlinear_a = 1., double nonlinear_b = 1.,
                   double nonlinear_c = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), valPtr(val_pointer),
          gradPtr(grad_pointer), nonlinearA(nonlinear_a),
          nonlinearB(nonlinear_b), nonlinearC(nonlinear_c) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    double nonlinearA;
    double nonlinearB;
    double nonlinearC;
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    VectorDouble locRhs;
  };

  struct OpGetFluxFromVal : public OpFaceEle {
  public:
    OpGetFluxFromVal(std::string field_name,
                     boost::shared_ptr<VectorDouble> val_pointer,
                     boost::shared_ptr<MatrixDouble> grad_pointer,
                     boost::shared_ptr<MatrixDouble> flux_pointer,
                     ScalarFunc k_Function)
        : OpFaceEle(field_name, OpFaceEle::OPROW), valPtr(val_pointer),
          gradPtr(grad_pointer), fluxPtr(flux_pointer), kFunc(k_Function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    boost::shared_ptr<MatrixDouble> fluxPtr;
    ScalarFunc kFunc;
  };

  struct OpGetMaxVals : public OpFaceEle {
  public:
    OpGetMaxVals(std::string field_name,
                 boost::shared_ptr<VectorDouble> val_pointer,
                 boost::shared_ptr<MatrixDouble> grad_pointer,
                 boost::shared_ptr<MatrixDouble> flux_pointer,
                 double *max_value, double *max_grad, double *max_flux)
        : OpFaceEle(field_name, OpFaceEle::OPROW), valPtr(val_pointer),
          gradPtr(grad_pointer), fluxPtr(flux_pointer), maxValue(max_value),
          maxGrad(max_grad), maxFlux(max_flux) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<VectorDouble> valPtr;
    boost::shared_ptr<MatrixDouble> gradPtr;
    boost::shared_ptr<MatrixDouble> fluxPtr;

    double *maxValue;
    double *maxGrad;
    double *maxFlux;
  };
};

#endif //__DIFFUSION_NONLINEAR_GRAPHITE_HPP__
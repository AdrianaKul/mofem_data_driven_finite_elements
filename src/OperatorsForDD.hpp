#ifndef __OPERATORS_FOR_DD__
#define __OPERATORS_FOR_DD__

#include <iostream>
#include <MoFEM.hpp>
#include <RtreeManipulation.hpp>
#include <alt_boost_geometry_includes.hpp>
// Random boost requirements
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/normal_distribution.hpp>

using namespace MoFEM;

using FaceEle = FaceElementForcesAndSourcesCore;
using EdgeEle = EdgeElementForcesAndSourcesCore;

using OpFaceEle = FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point4D;
typedef bg::model::point<double, 5, bg::cs::cartesian> point5D;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct SearchData {
  // main field pointers
  boost::shared_ptr<VectorDouble> mValPtr;
  boost::shared_ptr<MatrixDouble> mGradPtr;
  boost::shared_ptr<MatrixDouble> mFluxPtr;
  boost::shared_ptr<VectorDouble> mValStarPtr;
  boost::shared_ptr<MatrixDouble> mGradStarPtr;
  boost::shared_ptr<MatrixDouble> mFluxStarPtr;
  boost::shared_ptr<VectorDouble> pointDistance;
  boost::shared_ptr<MatrixDouble> mKStarPtr;
  boost::shared_ptr<MatrixDouble> mKStarCorrelationPtr;
  bgi::rtree<point4D, bgi::rstar<16, 4>> rtree4D;
  bgi::rtree<point5D, bgi::rstar<16, 4>> rtree5D;
  bool flgPointAverage = false;
  int pointAverage;
  double linear_k = 1.;
};

struct OperatorsForDD {

  static double square(double x) { return x * x; }

  struct OpScalarToField : public OpFaceEle {
  public:
    OpScalarToField(std::string field_name,
                    boost::shared_ptr<VectorDouble> data_ptr)
        : OpFaceEle(field_name, OpFaceEle::OPROW), dataPtr(data_ptr) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data);

  private:
    boost::shared_ptr<VectorDouble> dataPtr;
  };

  struct OpScalarVarTags : public OpFaceEle {
  public:
    OpScalarVarTags(boost::shared_ptr<VectorDouble> data_ptr,
                    std::string tag_ave_name, std::string tag_var_name,
                    MoFEM::Interface &m_field)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), dataPtr(data_ptr),
          tagAveName(tag_ave_name), tagVarName(tag_var_name), mField(m_field) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data);

  private:
    boost::shared_ptr<VectorDouble> dataPtr;
    std::string tagAveName;
    std::string tagVarName;
    MoFEM::Interface &mField;
  };

  struct OpScalarTags : public OpFaceEle {
  public:
    OpScalarTags(boost::shared_ptr<VectorDouble> data_ptr, std::string tag_name,
                 MoFEM::Interface &m_field)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), dataPtr(data_ptr),
          tagName(tag_name), mField(m_field) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data);

  private:
    boost::shared_ptr<VectorDouble> dataPtr;
    std::string tagName;
    MoFEM::Interface &mField;
  };

  template <int N> struct OpVectorNormTags : public OpFaceEle {
  public:
    OpVectorNormTags(boost::shared_ptr<MatrixDouble> data_ptr,
                     std::string tag_name, MoFEM::Interface &m_field)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), dataPtr(data_ptr),
          tagName(tag_name), mField(m_field) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          EntitiesFieldData::EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> dataPtr;
    std::string tagName;
    MoFEM::Interface &mField;
  };

  struct OpGradStarRhs : public OpFaceEle {
  public:
    OpGradStarRhs(std::string field_name,
                  boost::shared_ptr<MatrixDouble> star_pointer,
                  ScalarFunc s_Function)
        : OpFaceEle(field_name, OpFaceEle::OPROW), starPtr(star_pointer),
          sFunc(s_Function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> starPtr;
    ScalarFunc sFunc;
    VectorDouble locRhs;
  };

  template <int N> struct OpL2StarRhs : public OpFaceEle {
  public:
    OpL2StarRhs(const std::string field_name,
                boost::shared_ptr<MatrixDouble> star_pointer,
                ScalarFunc s_function)
        : OpFaceEle(field_name, OpFaceEle::OPROW), starPtr(star_pointer),
          sFunc(s_function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> starPtr;
    ScalarFunc sFunc;
    VectorDouble locRhs;
  };

  struct Op1dStarRhs : public OpFaceEle {
  public:
    Op1dStarRhs(const std::string field_name,
                boost::shared_ptr<VectorDouble> star_pointer,
                ScalarFunc s_function)
        : OpFaceEle(field_name, OpFaceEle::OPROW), starPtr(star_pointer),
          sFunc(s_function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<VectorDouble> starPtr;
    ScalarFunc sFunc;
    VectorDouble locRhs;
  };

  template <int N> struct OpHdivStarRhs : public OpFaceEle {
  public:
    OpHdivStarRhs(const std::string field_name,
                  boost::shared_ptr<MatrixDouble> star_pointer,
                  ScalarFunc s_function)
        : OpFaceEle(field_name, OpFaceEle::OPROW), starPtr(star_pointer),
          sFunc(s_function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> starPtr;
    ScalarFunc sFunc;
    VectorDouble locRhs;
  };

  struct OpMixHdivTimesVecLhs : public OpFaceEle {
  public:
    OpMixHdivTimesVecLhs(std::string row_field_name, std::string col_field_name,
                         ScalarFunc s_function, bool transform = true,
                         bool only_transform = false)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          sFunc(s_function), tRansform(transform),
          onlyTransform(only_transform) {
      sYmm = false;
      // sYmm = true;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    ScalarFunc sFunc;
    MatrixDouble locLhs;
    bool tRansform;
    bool onlyTransform;
  };

  template <int N> struct OpHdivNormalBoundaryStarRhs : public OpEdgeEle {
  public:
    OpHdivNormalBoundaryStarRhs(const std::string field_name,
                                boost::shared_ptr<MatrixDouble> star_pointer,
                                ScalarFunc s_function)
        : OpEdgeEle(field_name, OpEdgeEle::OPROW), starPtr(star_pointer),
          sFunc(s_function) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> starPtr;
    ScalarFunc sFunc;
    VectorDouble locRhs;
  };

  // peturbating fields
  template <int N> struct PerturbateField : public OpFaceEle {
  public:
    PerturbateField(std::string field_name,
                    boost::shared_ptr<MatrixDouble> star_pointer,
                    boost::shared_ptr<VectorDouble> point_distance_pointer,
                    double perturb_multiplier = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), starPtr(star_pointer),
          pointDistancePtr(point_distance_pointer),
          perturbMultiplier(perturb_multiplier) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> starPtr;
    boost::shared_ptr<VectorDouble> pointDistancePtr;
    double perturbMultiplier;
  };

  // searching the data set
  template <int N> struct OpFindData4D : public OpFaceEle {
  public:
    OpFindData4D(std::string field_name, SearchData search_data,
                 double scale = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), searchData(search_data),
          sCale(scale) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    SearchData searchData;
    double sCale;
  };

  template <int N> struct OpFindData5D : public OpFaceEle {
  public:
    OpFindData5D(std::string field_name, SearchData search_data,
                 double scale_val = 1., double scale_flux = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), searchData(search_data),
          scaleVal(scale_val), scaleFlux(scale_flux) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    SearchData searchData;
    double scaleVal;
    double scaleFlux;
  };

  template <int N> struct OpFindClosest : public OpFaceEle {
  public:
    OpFindClosest(std::string field_name, SearchData search_data,
                  double scale = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), searchData(search_data),
          sCale(scale) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    SearchData searchData;
    double sCale;
  };

  template <int N> struct OpFindKstar : public OpFaceEle {
  public:
    OpFindKstar(std::string field_name, SearchData search_data,
                double scale = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), searchData(search_data),
          sCale(scale) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    SearchData searchData;
    double sCale;
  };

  // (Q,grad(P)) * (k_star / (1 + k_star^2))
  struct OpGstarQLhs : public OpFaceEle {
  public:
    OpGstarQLhs(std::string row_field_name, std::string col_field_name,
                boost::shared_ptr<MatrixDouble> k_star_pointer,
                ScalarFunc s_Function, bool transform = true,
                bool only_transform = false)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          kStarPtr(k_star_pointer), sFunc(s_Function), tRansform(transform),
          onlyTransform(only_transform) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    boost::shared_ptr<MatrixDouble> kStarPtr;
    ScalarFunc sFunc;
    MatrixDouble locLhs;
    bool tRansform;
    bool onlyTransform;
  };

  // (Q,Q) * (k_star^2 / (1 + k_star^2))
  struct OpQstarQLhs : public OpFaceEle {
  public:
    OpQstarQLhs(std::string row_field_name, std::string col_field_name,
                boost::shared_ptr<MatrixDouble> k_star_pointer,
                ScalarFunc s_Function)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          kStarPtr(k_star_pointer), sFunc(s_Function) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    boost::shared_ptr<MatrixDouble> kStarPtr;
    ScalarFunc sFunc;
    MatrixDouble locLhs;
  };

  // (G,G) * (1 / (1 + k_star^2))
  struct OpGstarGLhs : public OpFaceEle {
  public:
    OpGstarGLhs(std::string row_field_name, std::string col_field_name,
                boost::shared_ptr<MatrixDouble> k_star_pointer,
                ScalarFunc s_Function)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          kStarPtr(k_star_pointer), sFunc(s_Function) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    boost::shared_ptr<MatrixDouble> kStarPtr;
    ScalarFunc sFunc;
    MatrixDouble locLhs;
  };

  // (Q,G) * k_star
  struct OpMixHdivTangentStarLhs : public OpFaceEle {
  public:
    OpMixHdivTangentStarLhs(std::string row_field_name,
                            std::string col_field_name,
                            boost::shared_ptr<MatrixDouble> k_star_pointer,
                            ScalarFunc s_Function)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          kStarPtr(k_star_pointer), sFunc(s_Function) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    boost::shared_ptr<MatrixDouble> kStarPtr;
    ScalarFunc sFunc;
    MatrixDouble locLhs;
  };

  // (G,Q) / k_star
  struct OpMixHdivTimesHdivTangentStarLhs : public OpFaceEle {
  public:
    OpMixHdivTimesHdivTangentStarLhs(
        std::string row_field_name, std::string col_field_name,
        boost::shared_ptr<MatrixDouble> k_star_pointer, ScalarFunc s_Function)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          kStarPtr(k_star_pointer), sFunc(s_Function) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    boost::shared_ptr<MatrixDouble> kStarPtr;
    ScalarFunc sFunc;
    MatrixDouble locLhs;
  };

  struct OpMassTangentStarLhs : public OpFaceEle {
  public:
    OpMassTangentStarLhs(std::string row_field_name, std::string col_field_name,
                         boost::shared_ptr<MatrixDouble> k_star_pointer,
                         ScalarFunc s_Function)
        : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
          kStarPtr(k_star_pointer), sFunc(s_Function) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data);

  private:
    boost::shared_ptr<MatrixDouble> kStarPtr;
    ScalarFunc sFunc;
    MatrixDouble locLhs;
  };

  template <int DIM_GRAD, int DIM_FLUX>
  struct OpPostProcSaveCsvDatasetGQ : public OpFaceEle {
  public:
    OpPostProcSaveCsvDatasetGQ(std::string field_name,
                               boost::shared_ptr<MatrixDouble> m_grad_ptr,
                               boost::shared_ptr<MatrixDouble> m_flux_ptr,
                               std::string csv_name, double s_flux = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), mGradPtr(m_grad_ptr),
          mFluxPtr(m_flux_ptr), csvName(csv_name), sFlux(s_flux) {};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> mGradPtr;
    boost::shared_ptr<MatrixDouble> mFluxPtr;
    std::string csvName;
    double sFlux;
  };

  template <int DIM_GRAD, int DIM_FLUX>
  struct OpPostProcSaveCsvDatasetVGQ : public OpFaceEle {
  public:
    OpPostProcSaveCsvDatasetVGQ(std::string field_name,
                                boost::shared_ptr<VectorDouble> m_val_ptr,
                                boost::shared_ptr<MatrixDouble> m_grad_ptr,
                                boost::shared_ptr<MatrixDouble> m_flux_ptr,
                                std::string csv_name, const double s_flux = 1.,
                                const double s_val = 1.)
        : OpFaceEle(field_name, OpFaceEle::OPROW), mValPtr(m_val_ptr),
          mGradPtr(m_grad_ptr), mFluxPtr(m_flux_ptr), csvName(csv_name),
          sFlux(s_flux), sVal(s_val) {};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<VectorDouble> mValPtr;
    boost::shared_ptr<MatrixDouble> mGradPtr;
    boost::shared_ptr<MatrixDouble> mFluxPtr;
    std::string csvName;
    double sFlux;
    double sVal;
  };
};

#endif //__OPERATORS_FOR_DD__
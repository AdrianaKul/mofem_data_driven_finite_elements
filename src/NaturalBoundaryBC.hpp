#ifndef __NATURAL_BOUNDARY_DD_BCS__
#define __NATURAL_BOUNDARY_DD_BCS__

/**
 * @file NaturalBoundaryBC.hpp
 * @brief Boundary conditions in domain, i.e. body forces.
 * @version 0.13.2
 * @date 2022-11-22
 *
 * @copyright Copyright (c) 2022
 *
 */

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

namespace DDconstrains {

template <CubitBC BC> struct FluxFunBcType {};
template <CubitBC BC> struct ScalarFunBcType {};

template <CubitBC> struct GetFluxBlocksets {
  GetFluxBlocksets() = delete;

  static MoFEMErrorCode GetBlocksets(double &block_scale,
                                     boost::shared_ptr<Range> &ents,
                                     MoFEM::Interface &m_field, int ms_id) {
    MoFEMFunctionBegin;

    auto cubit_meshset_ptr =
        m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(ms_id,
                                                                    BLOCKSET);
    std::vector<double> block_data;
    CHKERR cubit_meshset_ptr->getAttributes(block_data);
    if (block_data.size() != 1) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Expected that block has two attribute");
    }
    block_scale = block_data[0];

    ents = boost::make_shared<Range>();
    CHKERR
    m_field.get_moab().get_entities_by_handle(cubit_meshset_ptr->meshset,
                                              *(ents), true);

    MoFEMFunctionReturn(0);
  }
};
} // namespace DDconstrains

template <int FIELD_DIM, AssemblyType A, typename EleOp>
struct OpFluxRhsImpl<DDconstrains::FluxFunBcType<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp> : OpBaseImpl<A, EleOp> {

  using OpBase = OpBaseImpl<A, EleOp>;

  OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id, std::string field_name,
                VectorFunc source_fun, double rhs_scale);

protected:
  double blockScale;
  double rhsScale;
  VectorFunc sourceFun;
  MoFEMErrorCode iNtegrate(EntitiesFieldData::EntData &data);
};

template <int FIELD_DIM, AssemblyType A, typename EleOp>
struct OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp> : OpBaseImpl<A, EleOp> {

  using OpBase = OpBaseImpl<A, EleOp>;

  OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id, std::string field_name,
                ScalarFunc source_fun, double rhs_scale);

protected:
  double blockScale;
  double rhsScale;
  ScalarFunc sourceFun;
  MoFEMErrorCode iNtegrate(EntitiesFieldData::EntData &data);
};

template <int FIELD_DIM, AssemblyType A, typename EleOp>
struct OpFluxRhsImpl<DDconstrains::FluxFunBcType<BLOCKSET>, 3, FIELD_DIM, A,
                     GAUSS, EleOp> : OpBaseImpl<A, EleOp> {

  using OpBase = OpBaseImpl<A, EleOp>;

  OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id, std::string field_name,
                VectorFunc source_fun, double rhs_scale);

protected:
  double blockScale;
  double rhsScale;
  VectorFunc sourceFun;
  MoFEMErrorCode iNtegrate(EntitiesFieldData::EntData &data);
};

template <int FIELD_DIM, AssemblyType A, typename EleOp>
struct OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BLOCKSET>, 3, FIELD_DIM, A,
                     GAUSS, EleOp> : OpBaseImpl<A, EleOp> {

  using OpBase = OpBaseImpl<A, EleOp>;

  OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id, std::string field_name,
                ScalarFunc source_fun, double rhs_scale);

protected:
  double blockScale;
  double rhsScale;
  ScalarFunc sourceFun;
  MoFEMErrorCode iNtegrate(EntitiesFieldData::EntData &data);
};

#define sign(a) (((a) < 0) ? -1 : ((a) > 0))

template <int FIELD_DIM, AssemblyType A, typename EleOp>
OpFluxRhsImpl<DDconstrains::FluxFunBcType<BLOCKSET>, 1, FIELD_DIM, A, GAUSS,
              EleOp>::OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id,
                                    std::string field_name,
                                    VectorFunc source_fun, double rhs_scale)
    : OpBase(field_name, field_name, OpBase::OPROW), sourceFun(source_fun),
      rhsScale(rhs_scale) {
  CHK_THROW_MESSAGE(DDconstrains::GetFluxBlocksets<BLOCKSET>::GetBlocksets(
                        this->blockScale, this->entsPtr, m_field, ms_id),
                    "Can not read source data from blockset");
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BLOCKSET>, 1, FIELD_DIM, A, GAUSS,
              EleOp>::OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id,
                                    std::string field_name,
                                    ScalarFunc source_fun, double rhs_scale)
    : OpBase(field_name, field_name, OpBase::OPROW), sourceFun(source_fun),
      rhsScale(rhs_scale) {
  CHK_THROW_MESSAGE(DDconstrains::GetFluxBlocksets<BLOCKSET>::GetBlocksets(
                        this->blockScale, this->entsPtr, m_field, ms_id),
                    "Can not read source data from blockset");
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
OpFluxRhsImpl<DDconstrains::FluxFunBcType<BLOCKSET>, 3, FIELD_DIM, A, GAUSS,
              EleOp>::OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id,
                                    std::string field_name,
                                    VectorFunc source_fun, double rhs_scale)
    : OpBase(field_name, field_name, OpBase::OPROW), sourceFun(source_fun),
      rhsScale(rhs_scale) {
  CHK_THROW_MESSAGE(DDconstrains::GetFluxBlocksets<BLOCKSET>::GetBlocksets(
                        this->blockScale, this->entsPtr, m_field, ms_id),
                    "Can not read source data from blockset");
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BLOCKSET>, 3, FIELD_DIM, A, GAUSS,
              EleOp>::OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id,
                                    std::string field_name,
                                    ScalarFunc source_fun, double rhs_scale)
    : OpBase(field_name, field_name, OpBase::OPROW), sourceFun(source_fun),
      rhsScale(rhs_scale) {
  CHK_THROW_MESSAGE(DDconstrains::GetFluxBlocksets<BLOCKSET>::GetBlocksets(
                        this->blockScale, this->entsPtr, m_field, ms_id),
                    "Can not read source data from blockset");
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
MoFEMErrorCode
MoFEM::OpFluxRhsImpl<DDconstrains::FluxFunBcType<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp>::iNtegrate(EntitiesFieldData::EntData
                                                  &row_data) {
  FTensor::Index<'i', 2> i;
  MoFEMFunctionBegin;
  // get element volume
  const double vol = OpBase::getMeasure();
  // get integration weights
  auto t_w = OpBase::getFTensor0IntegrationWeight();
  // get base function gradient on rows
  auto t_row_base = row_data.getFTensor0N();
  // get coordinate at integration points
  auto t_coords = OpBase::getFTensor1CoordsAtGaussPts();

  // get normal
  auto t_normal = OpBase::getFTensor1Normal();
  // get normal vector length
  double length_normal = sqrt(t_normal(i) * t_normal(i));

  // loop over integration points
  for (int gg = 0; gg != OpBase::nbIntegrationPts; gg++) {
    // take into account Jacobian
    const double alpha = t_w * vol * rhsScale;

    // source function
    auto vec_source = sourceFun(t_coords(0), t_coords(1), t_coords(2));
    auto t_source = getFTensor1FromArray<2, 1>(vec_source);

    // loop over rows base functions
    int rr = 0;
    for (; rr != OpBase::nbRows / FIELD_DIM; ++rr) {
      // add to Rhs
      OpBase::locF[rr] += alpha * t_row_base * blockScale * t_source(i) *
                          t_normal(i) / length_normal;
      ++t_row_base;
    }

    for (; rr < OpBase::nbRowBaseFunctions; ++rr)
      ++t_row_base;
    ++t_coords;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
MoFEMErrorCode
MoFEM::OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BLOCKSET>, 1, FIELD_DIM, A,
                     GAUSS, EleOp>::iNtegrate(EntitiesFieldData::EntData
                                                  &row_data) {
  FTensor::Index<'i', 2> i;
  MoFEMFunctionBegin;
  // get element volume
  const double vol = OpBase::getMeasure();
  // get integration weights
  auto t_w = OpBase::getFTensor0IntegrationWeight();
  // get base function gradient on rows
  auto t_row_base = row_data.getFTensor0N();
  // get coordinate at integration points
  auto t_coords = OpBase::getFTensor1CoordsAtGaussPts();

  // loop over integration points
  for (int gg = 0; gg != OpBase::nbIntegrationPts; gg++) {
    // take into account Jacobian
    const double alpha = t_w * vol * rhsScale;

    // source function
    auto source = sourceFun(t_coords(0), t_coords(1), t_coords(2));

    // loop over rows base functions
    int rr = 0;
    for (; rr != OpBase::nbRows / FIELD_DIM; ++rr) {
      // add to Rhs
      OpBase::locF[rr] += alpha * t_row_base * blockScale * source;
      ++t_row_base;
    }

    for (; rr < OpBase::nbRowBaseFunctions; ++rr)
      ++t_row_base;
    ++t_coords;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
MoFEMErrorCode
MoFEM::OpFluxRhsImpl<DDconstrains::FluxFunBcType<BLOCKSET>, 3, FIELD_DIM, A,
                     GAUSS, EleOp>::iNtegrate(EntitiesFieldData::EntData
                                                  &row_data) {
  FTensor::Index<'i', 2> i;
  MoFEMFunctionBegin;

  const size_t nb_base_functions = row_data.getN().size2() / 3;
  // get element volume
  const double vol = OpBase::getMeasure();
  // get integration weights
  auto t_w = OpBase::getFTensor0IntegrationWeight();
  // get base function gradient on rows
  auto t_row_base = row_data.getFTensor1N<3>();
  // get coordinate at integration points
  auto t_coords = OpBase::getFTensor1CoordsAtGaussPts();

  // get normal
  auto t_normal = OpBase::getFTensor1Normal();

  // loop over integration points
  for (int gg = 0; gg != OpBase::nbIntegrationPts; gg++) {

    const double alpha = t_w * vol * rhsScale;

    // source function
    auto vec_source = sourceFun(t_coords(0), t_coords(1), t_coords(2));
    auto t_source = getFTensor1FromArray<2, 1>(vec_source);

    // loop over rows base functions
    int rr = 0;
    for (; rr != OpBase::nbRows; ++rr) {
      // add to Rhs
      OpBase::locF[rr] += alpha * t_row_base(i) * blockScale * t_source(i);
      ++t_row_base;
    }

    for (; rr < nb_base_functions; ++rr)
      ++t_row_base;
    ++t_coords;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
MoFEMErrorCode
MoFEM::OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BLOCKSET>, 3, FIELD_DIM, A,
                     GAUSS, EleOp>::iNtegrate(EntitiesFieldData::EntData
                                                  &row_data) {
  MoFEMFunctionBegin;
  FTensor::Index<'m', 2> m;
  FTensor::Tensor1<double, 3> t_z{0., 0., 1.};

  const size_t nb_base_functions = row_data.getN().size2() / 3;
  // get element volume
  const double vol = OpBase::getMeasure();
  // get integration weights
  auto t_w = OpBase::getFTensor0IntegrationWeight();
  // get base function gradient on rows
  auto t_row_base = row_data.getFTensor1N<3>();
  // get coordinate at integration points
  auto t_coords = OpBase::getFTensor1CoordsAtGaussPts();

  // get tangent
  auto t_tangent = OpBase::getFTensor1TangentAtGaussPts();

  // loop over integration points
  for (int gg = 0; gg != OpBase::nbIntegrationPts; gg++) {
    // take into account Jacobian
    const double alpha = t_w * rhsScale * vol;

    // get normal
    FTensor::Tensor1<double, 3> t_normal;
    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    FTensor::Index<'k', 3> k;
    t_normal(i) = FTensor::levi_civita(i, j, k) * t_tangent(j) * t_z(k);
    // get normal vector length
    double length_normal = sqrt(t_normal(i) * t_normal(i));

    // source function
    auto source = sourceFun(t_coords(0), t_coords(1), t_coords(2));

    // loop over rows base functions
    int rr = 0;
    for (; rr != OpBase::nbRows; ++rr) {
      // add to Rhs
      OpBase::locF[rr] += alpha * t_row_base(m) * t_normal(m) * blockScale *
                          source / length_normal;
      ++t_row_base;
    }

    for (; rr < nb_base_functions; ++rr)
      ++t_row_base;
    ++t_coords;
    ++t_tangent;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <CubitBC BCTYPE, int BASE_DIM, int FIELD_DIM, AssemblyType A,
          IntegrationType I, typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<DDconstrains::FluxFunBcType<BCTYPE>, BASE_DIM, FIELD_DIM, A,
                  I, OpBase>,
    A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;

  static MoFEMErrorCode add(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, VectorFunc source_fun,
      double rhs_scale, std::string block_name, Sev sev

  ) {
    MoFEMFunctionBegin;

    using OP = OpFluxRhsImpl<DDconstrains::FluxFunBcType<BLOCKSET>, BASE_DIM,
                             FIELD_DIM, PETSC, GAUSS, OpBase>;

    auto add_op = [&](auto &&meshset_vec_ptr) {
      for (auto m : meshset_vec_ptr) {
        MOFEM_TAG_AND_LOG("WORLD", sev, "OpSourceRhs") << "Add " << *m;
        pipeline.push_back(new OP(m_field, m->getMeshsetId(), field_name,
                                  source_fun, rhs_scale));
      }
      MOFEM_LOG_CHANNEL("WORLD");
    };

    switch (BCTYPE) {
    case BLOCKSET:
      add_op(

          m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(
              std::regex(

                  (boost::format("%s(.*)") % block_name).str()

                      ))

      );

      break;
    default:
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED,
              "Handling of bc type not implemented");
      break;
    }
    MoFEMFunctionReturn(0);
  }
};

template <CubitBC BCTYPE, int BASE_DIM, int FIELD_DIM, AssemblyType A,
          IntegrationType I, typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BCTYPE>, BASE_DIM, FIELD_DIM, A,
                  I, OpBase>,
    A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;

  static MoFEMErrorCode add(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, ScalarFunc source_fun,
      double rhs_scale, std::string block_name, Sev sev

  ) {
    MoFEMFunctionBegin;

    using OP = OpFluxRhsImpl<DDconstrains::ScalarFunBcType<BLOCKSET>, BASE_DIM,
                             FIELD_DIM, PETSC, GAUSS, OpBase>;

    auto add_op = [&](auto &&meshset_vec_ptr) {
      for (auto m : meshset_vec_ptr) {
        MOFEM_TAG_AND_LOG("WORLD", sev, "OpSourceRhs") << "Add " << *m;
        pipeline.push_back(new OP(m_field, m->getMeshsetId(), field_name,
                                  source_fun, rhs_scale));
      }
      MOFEM_LOG_CHANNEL("WORLD");
    };

    switch (BCTYPE) {
    case BLOCKSET:
      add_op(

          m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(
              std::regex(

                  (boost::format("%s(.*)") % block_name).str()

                      ))

      );

      break;
    default:
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED,
              "Handling of bc type not implemented");
      break;
    }
    MoFEMFunctionReturn(0);
  }
};

template <int BASE_DIM, int FIELD_DIM, AssemblyType A, IntegrationType I,
          typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<BoundaryBCs, BASE_DIM, FIELD_DIM, A, I, OpBase>, A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;

  using T =
      typename NaturalBC<OpBase>::template Assembly<A>::template LinearForm<I>;

  using OpFluxConstant =
      typename T::template OpFlux<NaturalMeshsetType<BLOCKSET>, BASE_DIM,
                                  FIELD_DIM>;

  using OpEdgeVecFunTimesNormal =
      typename T::template OpFlux<DDconstrains::FluxFunBcType<BLOCKSET>,
                                  BASE_DIM, FIELD_DIM>;

  using OpEdgeScalarFun =
      typename T::template OpFlux<DDconstrains::ScalarFunBcType<BLOCKSET>,
                                  BASE_DIM, FIELD_DIM>;

  static MoFEMErrorCode add(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, double rhs_scale,
      Sev sev

  ) {
    MoFEMFunctionBegin;

    auto flux_Lshape = [](const double x, const double y, const double z) {
      VectorDouble res;
      res.resize(2);

      double numerator = 2. * (y * cos((M_PI + 2. * atan2(y, x)) / 3.) -
                               x * sin((M_PI + 2. * atan2(y, x)) / 3.));
      double denominator = -3. * pow(pow(x, 2) + pow(y, 2), 2.0 / 3.0);
      res[0] = numerator / denominator;
      numerator = -2. * (x * cos((M_PI + 2. * atan2(y, x)) / 3.) +
                         y * sin((M_PI + 2. * atan2(y, x)) / 3.));
      res[1] = numerator / denominator;
      return res;
    };

    auto flux_nonlinear_Lshape = [](const double x, const double y,
                                    const double z) {
      VectorDouble res;
      res.resize(2);
      double a = 1.;
      double b = 1.;
      double k = -(a + b * pow(x * x + y * y, 1. / 3.) *
                           sin(2. / 3. * (M_PI / 2. + atan2(y, x))));
      res[0] = ((-2. * y * cos((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
                    (3. * pow(x * x + y * y, 2. / 3.)) +
                (2. * x * sin((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
                    (3. * pow(x * x + y * y, 2. / 3.))) *
               k;
      res[1] = ((2. * x * cos((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
                    (3. * pow(x * x + y * y, 2. / 3.)) +
                (2. * y * sin((2. * (M_PI / 2. + atan2(y, x))) / 3.)) /
                    (3. * pow(x * x + y * y, 2. / 3.))) *
               k;
      return res;
    };

    auto flux_uniform = [](const double x, const double y, const double z) {
      return 1.0;
    };

    // CHKERR T::template AddFluxToPipeline<OpFluxConstant>::add(
    //     pipeline, m_field, field_name, {}, "FLUX_UNIFORM", sev);

    CHKERR T::template AddFluxToPipeline<OpEdgeScalarFun>::add(
        pipeline, m_field, field_name, flux_uniform, rhs_scale, "FLUX_UNIFORM",
        sev);

    CHKERR T::template AddFluxToPipeline<OpEdgeVecFunTimesNormal>::add(
        pipeline, m_field, field_name, flux_Lshape, rhs_scale, "FLUX_L", sev);

    CHKERR T::template AddFluxToPipeline<OpEdgeVecFunTimesNormal>::add(
        pipeline, m_field, field_name, flux_nonlinear_Lshape, rhs_scale,
        "FLUX_NL", sev);

    auto flux_octopus = [](const double x, const double y, const double z) {
      VectorDouble res;
      res.resize(2);
      double a = 1.;
      double b = 1.;
      double ee = -10 * x * x * y * y;
      res[0] = 20 * exp(ee) * (a + b * exp(ee)) * x * y * y;
      res[1] = 20 * exp(ee) * (a + b * exp(ee)) * x * x * y;
      return res;
    };

    CHKERR T::template AddFluxToPipeline<OpEdgeVecFunTimesNormal>::add(
        pipeline, m_field, field_name, flux_octopus, rhs_scale, "FLUX_OCTOPUS",
        sev);

    auto flux_square_sincos = [](const double x, const double y,
                                 const double z) {
      VectorDouble res;
      res.resize(2);
      double a = 2.;
      res[0] = -a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y);
      res[1] = a * M_PI * sin(a * M_PI * x) * sin(a * M_PI * y);
      return res;
    };

    CHKERR T::template AddFluxToPipeline<OpEdgeVecFunTimesNormal>::add(
        pipeline, m_field, field_name, flux_square_sincos, rhs_scale,
        "FLUX_SQUARE_SINCOS", sev);

    auto flux_square_nonlinear_sincos = [](const double x, const double y,
                                           const double z) {
      double a = 2.;
      double aa = 1.;
      double bb = 1.;
      double cc = 1.;

      VectorDouble res;
      res.resize(2);
      res[0] = -a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y) *
               (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                         (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x)));
      res[1] = a * M_PI * sin(a * M_PI * x) *
               (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                         (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x))) *
               sin(a * M_PI * y);
      return res;
    };

    CHKERR T::template AddFluxToPipeline<OpEdgeVecFunTimesNormal>::add(
        pipeline, m_field, field_name, flux_square_nonlinear_sincos, rhs_scale,
        "FLUX_SQUARE_NONLINEAR_SINCOS", sev);

    auto flux_square_top = [&](const double x, const double y, const double z) {
      return sin(x * M_PI) * cosh(M_PI) * M_PI / sinh(M_PI);
    };

    CHKERR T::template AddFluxToPipeline<OpEdgeScalarFun>::add(
        pipeline, m_field, field_name, flux_square_top, rhs_scale,
        "FLUX_SQUARE_TOP", sev);

    MoFEMFunctionReturn(0);
  }
};

#endif //__NATURAL_BOUNDARY_DD_BCS__
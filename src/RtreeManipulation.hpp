#ifndef __RTREE_MANIPULATION_HPP__
#define __RTREE_MANIPULATION_HPP__

#include <iostream>
#include <MoFEM.hpp>
#include <alt_boost_geometry_includes.hpp>
#include <cholesky.hpp>

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;
typedef bg::model::point<double, 4, bg::cs::cartesian> point4D;
typedef bg::model::point<double, 5, bg::cs::cartesian> point5D;
typedef bg::model::multi_point<point4D> mpoint_t;

using namespace MoFEM;

struct RtreeManipulation {

  // set point
  static point4D convertToPoint(const double v0, const double v1,
                                const double v2, const double v3);
  static point5D convertToPoint(const double v0, const double v1,
                                const double v2, const double v3,
                                const double v4);

  static void convertToPoint(point4D &poi, const VectorDouble vector);
  static void convertToPoint(point5D &poi, const VectorDouble vector);

  static std::vector<double> convertToVector(point4D poi);
  static std::vector<double> convertToVector(point5D poi);
  static FTensor::Tensor1<double, 4> convertToFTensor1(point4D poi);
  static FTensor::Tensor1<double, 5> convertToFTensor1(point5D poi);

  static MoFEMErrorCode
  loadFileData(const std::string filename,
               bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree, bool pack = true);
  static MoFEMErrorCode
  loadFileData(const std::string filename,
               bgi::rtree<point5D, bgi::rstar<16, 4>> &rtree, bool pack = true);

  static std::vector<double> loadScalingData(const std::string filename);

  static point4D returnNearestAndDistance(
      const point4D &finding,
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>> &distance,
      const bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree_in);
  static point5D returnNearestAndDistance(
      const point5D &finding,
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>> &distance,
      const bgi::rtree<point5D, bgi::rstar<16, 4>> &rtree_in);

  static point4D returnAveAndDistance(
      const point4D &finding,
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>> &distance,
      const bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree_in,
      const int number_points);

  static std::vector<point4D>
  returnNearestPoints(const point4D &finding,
                      const bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree_in,
                      const int number_points);
  static std::vector<point5D>
  returnNearestPoints(const point5D &finding,
                      const bgi::rtree<point5D, bgi::rstar<16, 4>> &rtree_in,
                      const int number_points);

  static double getGradient(const std::vector<double> v_x,
                            const std::vector<double> v_y);

  static MoFEMErrorCode
  getGradient_new(const std::vector<point4D> &points_vector,
                  MatrixDouble &tangent);
  static MoFEMErrorCode
  getGradient_new(const std::vector<point5D> &points_vector,
                  MatrixDouble &tangent);

  static double getCorrelation(const std::vector<double> v_x,
                               const std::vector<double> v_y);

  static void calcKstar(const std::vector<point4D> &points_vector,
                        std::vector<double> &tangent,
                        std::vector<double> &correlation);
  static void calcKstar(const std::vector<point5D> &points_vector,
                        std::vector<double> &tangent,
                        std::vector<double> &correlation);
};

#endif //__RTREE_MANIPULATION_HPP__
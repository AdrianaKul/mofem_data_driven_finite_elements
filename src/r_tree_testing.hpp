#ifndef __R_TREE_TESTING_HPP__
#define __R_TREE_TESTING_HPP__

// Rtree boost 
#include <alt_boost_geometry_includes.hpp>
#include <pOint_setter.hpp>
#include <boost/foreach.hpp>
#include <RtreeManipulation.hpp>

// Random boost requirements
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/normal_distribution.hpp>

// using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

struct RTreeDummy {
public:
  point nearest;

  // create rtree with continuous points
  void create_4D_rtree(const double k, const double range = 10,
                       const int n_points = 100, const double noise_level = 0.1,
                       PetscBool skip_vtk = PETSC_FALSE,
                       PetscBool rand_seed = PETSC_TRUE,
                       const int meterial_type = 0);

  // get rtree
  bgi::rtree<point, bgi::rstar<16, 4>> getRTreeDummy();
  // set rtree from elsewhere
  void setRTreeDummy(bgi::rtree<point, bgi::rstar<16, 4>> rtree_in);

  // mostly not used functions for finding
  point returnNearest(const point &finding);
  point returnNearest(const point &finding,
                      bgi::rtree<point, bgi::rstar<16, 4>> rtree_in);
  void findNearest(const point &finding, const int n = 1);
  void showNearest(const point &finding, const int n = 1);
  void showWithinD(const point &finding, const double max_distance,
                   const int max_n = 20);

  // private:
  bgi::rtree<point, bgi::rstar<16, 4>> rtree;
};

void RTreeDummy::create_4D_rtree(const double k, const double range,
                                 const int n_points, const double noise_level,
                                 PetscBool skip_vtk, PetscBool rand_seed,
                                 const int meterial_type) {

  // boost random
  boost::random::mt11213b gen; // generator
  if (rand_seed == PETSC_TRUE) {
    gen.seed(static_cast<unsigned int>(std::time(0)));
  }
  boost::random::uniform_real_distribution<> dist(
      -range, range); // distribution uniform real
  boost::random::normal_distribution<> noise(
      0, noise_level); // distribution normal

  double gradx, grady, fluxx, fluxy;

  // start writing dummy tree file
  std::ofstream myfile;
  myfile.open("dummy_tree.csv");

  myfile << "gradx, grady, fluxx, fluxy \n"; // headers

  std::vector<point> cloud; // with packing
  cloud.resize(n_points);

  // set point
  auto convertToPoint = [](const double v0, const double v1, const double v2,
                           const double v3) {
    boost::geometry::model::point<double, 4, boost::geometry::cs::cartesian>
        poi;
    poi.set<0>(v0);
    poi.set<1>(v1);
    poi.set<2>(v2);
    poi.set<3>(v3);
    return poi;
  };

  auto add_point = [&]() {
    if (meterial_type == 0) {
      gradx = dist(gen);
      fluxx = (k + noise(gen)) * gradx;
      grady = dist(gen);
      fluxy = (k + noise(gen)) * grady;
      // // old noise [start]
      // gradx = dist(gen);
      // fluxx = k * gradx + noise(gen);
      // grady = dist(gen);
      // fluxy = k * grady + noise(gen);
      // // old noise [end]
    }

    if (meterial_type == 1) {
      gradx = dist(gen);
      fluxx = -range * (1 - exp(-gradx)) / (1 + exp(-gradx));
      grady = dist(gen);
      fluxy = -range * (1 - exp(-grady)) / (1 + exp(-grady));
    }

    if (skip_vtk != PETSC_TRUE) {
      // populate the file with points
      myfile << gradx << ", " << grady << ", " << fluxx << ", " << fluxy
             << "\n";
    }

    return convertToPoint(gradx, grady, fluxx, fluxy);
  };

  for (unsigned i = 0; i < n_points; ++i) {
    cloud[i] = add_point();
  }

  // to initialise packing, rtree needs all data with constructor
  bgi::rtree<point, bgi::rstar<16, 4>> tempRtree(cloud); // with packing
  rtree.swap(tempRtree);

  // close writing into file (file created)
  myfile.close();
}

bgi::rtree<point, bgi::rstar<16, 4>> RTreeDummy::getRTreeDummy() {
  return rtree;
}

void RTreeDummy::setRTreeDummy(bgi::rtree<point, bgi::rstar<16, 4>> rtree_in) {
  rtree = rtree_in;
}

// return nearest point from the dummy tree
point RTreeDummy::returnNearest(const point &finding) {
  std::vector<point> result_n;
  rtree.query(bgi::nearest(finding, 1), std::back_inserter(result_n));
  return result_n[0];
}

void RTreeDummy::findNearest(const point &finding, const int n) {
  // this is for setting finding and setting the nearest point
  std::vector<point> result_n;
  rtree.query(bgi::nearest(finding, n), std::back_inserter(result_n));
  BOOST_FOREACH (point const &v, result_n) {
    double d = bg::distance(finding, v);
    std::cout << bg::wkt<point>(v) << ", distance= " << d << std::endl;
  }
  if (n == 1) {
    nearest = result_n[0];
  }
}

//  showing points around finding but doesn't do anything with them
void RTreeDummy::showNearest(const point &finding, const int n) {
  std::vector<point> result_n;
  rtree.query(bgi::nearest(finding, n), std::back_inserter(result_n));
  BOOST_FOREACH (point const &v, result_n) {
    double d = bg::distance(finding, v);
    std::cout << bg::wkt<point>(v) << ", distance= " << d << std::endl;
  }
}

// shows points within distance D of the point searched for in the dummy tree
void RTreeDummy::showWithinD(const point &finding, const double max_distance,
                             const int max_n) {
  for (bgi::rtree<point, bgi::rstar<16, 4>>::const_query_iterator it =
           rtree.qbegin(bgi::nearest(finding, max_n));
       it != rtree.qend(); ++it) {
    double d = bg::distance(finding, *it);

    std::cout << bg::wkt(*it) << ", distance= " << d << std::endl;

    // break if the distance is too big
    if (d > max_distance) {
      std::cout << "break!" << std::endl;
      break;
    }
  }
}

#endif //__R_TREE_TESTING_HPP__
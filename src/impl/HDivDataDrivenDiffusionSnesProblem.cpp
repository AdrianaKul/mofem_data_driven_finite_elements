#include <HDivDataDrivenDiffusionSnesProblem.hpp>

using OpHdivHdiv = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<3, 3>;

using OpHdivHdivEdge = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<3, 3>;

using OpMassMat = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<1, 2>;

// Scalars
using OpHdivU = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixDivTimesScalar<2>;

using OpPressureBC = FormsIntegrators<OpEdgeEle>::Assembly<PETSC>::LinearForm<
    GAUSS>::OpNormalMixVecTimesScalar<2>;

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpBaseTimeScalarRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpBaseTimesScalarField<1>;

using OpHDivTimesScalarRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpMixDivTimesU<3, 1, 2>;

using OpBoundaryHdivHdiv = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<3, 3>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 3, 2>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 3, 2>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

MoFEMErrorCode DiffusionHDivSnesDD::runProblem() {
  MoFEMFunctionBegin;

  CHKERR setParamValues();
  CHKERR SetParamDDValues();
  CHKERR readMesh();
  CHKERR setupProblem();
  CHKERR orderUpdateSetUp();
  CHKERR setIntegrationRules();
  CHKERR createClassicCommonData();
  CHKERR createDDCommonData();
  CHKERR createDDsnesCommonData();
  CHKERR createHdivCommonData();
  CHKERR createDDHdivCommonData();
  CHKERR createDDHdivsnesCommonData();
  CHKERR boundaryCondition();
  CHKERR removeLambda();
  CHKERR assembleSystem();
  CHKERR getSnesInitial();
  CHKERR setRandomInitialFields();
  CHKERR errorFileNameSetUp();
  CHKERR setSnesMonitors();
  CHKERR solveSnesSystem();
  if (refinementStyle) {
    CHKERR refineOrderLoop();
    CHKERR checkRefinement();
  } else {
    CHKERR checkError(1000, true);
    CHKERR saveSumanalys();
  }
  CHKERR checkResults();
  CHKERR outputHdivVtkFields(commonDataPtr->counter);
  CHKERR outputFarPointsCsv();
  CHKERR setParamMonteCarlo();
  CHKERR runMonteCarlo();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::setupProblem() {
  MoFEMFunctionBegin;

  int nb_quads = 0;
  CHKERR mField.get_moab().get_number_entities_by_type(0, MBQUAD, nb_quads);
  auto base = AINSWORTH_LEGENDRE_BASE;
  if (nb_quads) {
    // AINSWORTH_LEGENDRE_BASE is not implemented for HDIV/HCURL space on quads
    base = DEMKOWICZ_JACOBI_BASE;
  }

  // domain fields
  CHKERR simpleInterface->addDomainField(valueField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDomainField(gradField, L2, AINSWORTH_LEGENDRE_BASE,
                                         2);
  CHKERR simpleInterface->addDomainField(fluxField, HCURL, base, 1);
  CHKERR simpleInterface->addDomainField(tauField, HCURL, base, 1);
  CHKERR simpleInterface->addDomainField(lagrangeField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 1);

  // boundary fields
  CHKERR simpleInterface->addBoundaryField(fluxField, HCURL, base, 1);
  CHKERR simpleInterface->addBoundaryField(tauField, HCURL, base, 1);

  // skeleton fields
  CHKERR simpleInterface->addSkeletonField(fluxField, HCURL, base, 1);

  //  data fields
  CHKERR simpleInterface->addDataField("T_ave", L2, AINSWORTH_LEGENDRE_BASE, 1);

  // TODO: check orders
  CHKERR simpleInterface->setFieldOrder(valueField, oRder);
  CHKERR simpleInterface->setFieldOrder(gradField, oRder + 1);
  CHKERR simpleInterface->setFieldOrder(fluxField, oRder + 1);
  CHKERR simpleInterface->setFieldOrder(tauField, oRder + 1);
  CHKERR simpleInterface->setFieldOrder(lagrangeField, oRder);

  CHKERR simpleInterface->setFieldOrder("T_ave", oRder);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataHDivSnesDD>();
  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;
  HDivDiffusionProblem::commonDataPtr = commonDataPtr;
  DiffusionDD::commonDataPtr = commonDataPtr;
  DiffusionHDivDD::commonDataPtr = commonDataPtr;
  DiffusionSnesDD::commonDataPtr = commonDataPtr;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::createDDHdivsnesCommonData() {
  MoFEMFunctionBegin;

  commonDataPtr->mLagrangePtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mTauPtr = boost::make_shared<MatrixDouble>();

  commonDataPtr->mDivFluxPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mDivTauPtr = boost::make_shared<VectorDouble>();

  DDLogTag;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-p_ref_all",
                             &flg_refine_order_all, PETSC_NULL);
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-skip_all_vtk", &flg_skip_all_vtk,
                             PETSC_NULL);
  if (flg_refine_order_all)
    MOFEM_LOG("WORLD", Sev::inform)
        << "-p_ref_all used (order refines at all of the elements)";
  if (flg_skip_all_vtk)
    MOFEM_LOG("WORLD", Sev::inform)
        << "-skip_all_vtk used (no .h5m files produced)";

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::boundaryCondition() {
  MoFEMFunctionBegin;

  auto bc_mng = mField.getInterface<BcManager>();
  CHKERR bc_mng->pushMarkDOFsOnEntities(simpleInterface->getProblemName(),
                                        "FLUX", fluxField, 0, 0);

  auto &bc_map = bc_mng->getBcMapByBlockName();
  boundaryMarker = boost::make_shared<std::vector<char unsigned>>();
  for (auto b : bc_map) {
    if (std::regex_match(b.first, std::regex("(.*)FLUX(.*)"))) {
      boundaryMarker->resize(b.second->bcMarkers.size(), 0);
      for (int i = 0; i != b.second->bcMarkers.size(); ++i) {
        (*boundaryMarker)[i] |= b.second->bcMarkers[i];
      }
      fluxBcRange.merge(*(b.second->getBcEntsPtr()));
    }
  }

  Range boundary_vertices;
  CHKERR mField.get_moab().get_connectivity(fluxBcRange, boundary_vertices,
                                            true);
  fluxBcRange.merge(boundary_vertices);

  // Store entities for fieldsplit (block) solver
  boundaryEntitiesForFieldsplit = fluxBcRange;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::assembleSystem() {
  MoFEMFunctionBegin;

  // clearing previous settings
  feDomainLhsPtr->getOpPtrVector().clear();
  feDomainRhsPtr->getOpPtrVector().clear();
  feBoundaryLhsPtr->getOpPtrVector().clear();
  feBoundaryRhsPtr->getOpPtrVector().clear();

  // calculating jacobian and other general necessary parts
  CHKERR calculateHdivJacobian(feDomainLhsPtr);
  CHKERR calculateHdivJacobian(feDomainRhsPtr);
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());

  auto unity = []() { return 1.0; };

  { //  LHS domain elements

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivHdiv(fluxField, fluxField, s_q_Function));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpMassMat(gradField, gradField, s_p_Function));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpMixHdivTimesVecLhs(tauField, gradField,
                                                 oneFunction));

    feDomainLhsPtr->getOpPtrVector().push_back(new OpHdivU(
        tauField, valueField, [&]() { return 1. / commonDataPtr->scaleV; },
        true));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivU(fluxField, lagrangeField, unity, true));

    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  if (flg_part_star == true) { // partial derivatives for J(p,g,q) Rhs

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpFindKstar<3>(valueField, searchData));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpMixHdivTangentStarLhs(
            fluxField, gradField, searchData.mKStarPtr, oneFunction));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpMixHdivTimesHdivTangentStarLhs(
            fluxField, fluxField, searchData.mKStarPtr, oneFunction));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpMassTangentStarLhs(
            gradField, gradField, searchData.mKStarPtr, oneFunction));

    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  { // RHS boundary elements - neuman bc (pressure)
    // using tauField in boundary markers to avoid flux boundary conditions
    // repeating
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(tauField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, tauField, -1., Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(tauField));
  }

  { // Rhs domain (source)
    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, lagrangeField,
        -commonDataPtr->scaleQ, Sev::inform);
  }

  {   // Rhs DD domain
    { // get values from fields
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateVectorFieldValues<2>(gradField,
                                              commonDataPtr->mGradPtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateHVecVectorField<3>(fluxField,
                                            commonDataPtr->mFluxPtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues(lagrangeField,
                                           commonDataPtr->mLagrangePtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateHVecVectorField<3>(tauField, commonDataPtr->mTauPtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateHdivVectorDivergence<3, 2>(
              fluxField, commonDataPtr->mDivFluxPtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateHdivVectorDivergence<3, 2>(tauField,
                                                    commonDataPtr->mDivTauPtr));
    }

    { // find g* and q* in the data set and get point distance errors
      if (!flg_use_line) {
        switch (datasetDim) {
        case 4:
          feDomainRhsPtr->getOpPtrVector().push_back(
              new OperatorsForDD::OpFindData4D<3>(valueField, searchData));
          break;
        case 5:
          feDomainRhsPtr->getOpPtrVector().push_back(
              new OperatorsForDD::OpFindData5D<3>(valueField, searchData));
          if (dd_use_data_value) {
            feDomainRhsPtr->getOpPtrVector().push_back(
                new OperatorsForDD::Op1dStarRhs(
                    valueField, commonDataPtr->mValStarPtr, minusFunction));
            feDomainRhsPtr->getOpPtrVector().push_back(
                new OperatorsForDD::Op1dStarRhs(
                    valueField, commonDataPtr->mValPtr, oneFunction));
          }
          break;
        }
      }
      if (flg_use_line)
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindClosest<3>(valueField, searchData));
      if (flg_part_star == true)
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindKstar<3>(valueField, searchData));

      // feDomainRhsPtr->getOpPtrVector().push_back(
      //     new OpPointError(valueField, commonDataPtr));
    }

    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));
    { // RHS of domain elements - g* and q*
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpL2StarRhs<2>(
              gradField, commonDataPtr->mGradStarPtr, minusFunction));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpHdivStarRhs<2>(
              fluxField, commonDataPtr->mFluxStarPtr, minusFunction));
    }

    { // more residual operators due to snes solver

      auto scale_v_func = [&](const double x, const double y, const double z) {
        return 1. / commonDataPtr->scaleV;
      };

      feDomainRhsPtr->getOpPtrVector().push_back(new OpBaseTimeScalarRhs(
          valueField, commonDataPtr->mDivTauPtr, scale_v_func));
      feDomainRhsPtr->getOpPtrVector().push_back(new OpBaseTimeScalarRhs(
          lagrangeField, commonDataPtr->mDivFluxPtr, oneFunction));

      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpL2StarRhs<2>(gradField, commonDataPtr->mGradPtr,
                                             oneFunction));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpHdivStarRhs<3>(
              fluxField, commonDataPtr->mFluxPtr, oneFunction));

      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpL2StarRhs<3>(gradField, commonDataPtr->mTauPtr,
                                             oneFunction));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpHdivStarRhs<2>(
              tauField, commonDataPtr->mGradPtr, oneFunction));

      feDomainRhsPtr->getOpPtrVector().push_back(new OpHDivTimesScalarRhs(
          fluxField, commonDataPtr->mLagrangePtr, oneFunction));
      feDomainRhsPtr->getOpPtrVector().push_back(new OpHDivTimesScalarRhs(
          tauField, commonDataPtr->mValPtr, scale_v_func));
    }
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  // Hdiv essential bsc - flux
  CHKERR assembleEssentialBoundary();

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    feDomainRhsPtr->getOpPtrVector().push_back(new OpPrintStarIntegPts<3>(
        valueField, commonDataPtr, mField.get_moab()));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::assembleEssentialBoundary() {
  MoFEMFunctionBegin;
  // Hdiv essential bsc - flux

  // setting Rhs essential bc
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetBc(fluxField, false, boundaryMarker));
  CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
      feBoundaryRhsPtr->getOpPtrVector(), mField, fluxField,
      commonDataPtr->scaleQ, Sev::inform);
  // snes Rhs part
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpCalculateHVecVectorField<3>(fluxField, commonDataPtr->mFluxPtr));
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OperatorsForDD::OpHdivNormalBoundaryStarRhs<3>(
          fluxField, commonDataPtr->mFluxPtr, oneFunction));
  feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));

  // setting Lhs essential bc
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpSetBc(fluxField, false, boundaryMarker));
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpBoundaryHdivHdiv(fluxField, fluxField, oneFunction));
  feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::setRandomInitialFields() {
  MoFEMFunctionBegin;

  if (flg_rand_ini) {

    MOFEM_LOG("WORLD", Sev::inform) << "Setting random initial fields";

    // random
    PetscRandom rctx;
    PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
    PetscRandomSetSeed(rctx, (unsigned long)std::time(0));
    PetscRandomSeed(rctx);

    // make scale an input
    double scale = 10;
    CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-rand_ini_scale", &scale,
                               PETSC_NULL);

    if (dummy_k < 1)
      scale *= dummy_k;

    // set random G field (L2)
    for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, gradField, it)) {
      if (it->get()->getEntType() == MBTRI || MBQUAD) {
        auto field_data = (*it)->getEntFieldData();
        double value;

        PetscRandomGetValueReal(rctx, &value);
        field_data[0] = (value - 0.5) * scale; // value * scale
        PetscRandomGetValueReal(rctx, &value);
        field_data[1] = (value - 0.5) * scale;
      }
    }

    auto vector_times_scalar_field =
        [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
          MoFEMFunctionBeginHot;
          auto x_data = ent_ptr_x->getEntFieldData();
          double value;
          PetscRandomGetValueReal(rctx, &value);

          for (auto &v : x_data)
            v = (value - 0.5) * scale;

          MoFEMFunctionReturnHot(0);
        };

    CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(
        vector_times_scalar_field, fluxField);

    CHKERR outputHdivVtkFields(1000, "out_after_perturb_");
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::setSnesMonitors() {
  MoFEMFunctionBegin;

  // ctx for monitor containing the discrete manager
  auto snes_ctx_ptr = smartGetDMSnesCtx(simpleInterface->getDM());

  combinedPointers = boost::make_shared<CombinedPointers>();

  combinedPointers->commonDataPtr = commonDataPtr;
  combinedPointers->snesCtxPtr = snes_ctx_ptr;

  // define monitors which are executed at the begining of each iteration
  // postProc on integration points
  auto monitor_vtk_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                  void *combined_ctx) {
    MoFEMFunctionBegin;

    auto combined_ptr =
        boost::static_pointer_cast<CombinedPointers>(combined_ctx);

    auto common_data_ptr = combined_ptr->commonDataPtr;

    auto snes_ptr = combined_ptr->snesCtxPtr;

    auto &m_field = snes_ptr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();

    // scale fields
    CHKERR m_field.getInterface<FieldBlas>()->fieldScale(
        1. / common_data_ptr->scaleQ, "Q");
    CHKERR m_field.getInterface<FieldBlas>()->fieldScale(
        1. / common_data_ptr->scaleV, "T");

    auto post_proc_fe =
        boost::make_shared<PostProcBrokenMeshInMoab<FaceEle>>(m_field);

    // CHKERR AddHOOps<2, 2, 2>::add(post_proc_fe->getOpPtrVector(), {L2,
    // HCURL});

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
    post_proc_fe->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(
        new OpSetInvJacHcurlFace(inv_jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(
        new OpSetInvJacL2ForFace(inv_jac_ptr));

    using OpPPMap2 = OpPostProcMapInMoab<2, 2>;
    using OpPPMap3 = OpPostProcMapInMoab<3, 3>;

    std::string valueField = "T";
    std::string gradField = "G";
    std::string fluxField = "Q";
    std::string tauField = "tau";
    std::string lagrangeField = "xi";

    auto temperature_vec = boost::make_shared<VectorDouble>();
    auto gradient_mat = boost::make_shared<MatrixDouble>();
    auto flux_mat = boost::make_shared<MatrixDouble>();
    auto lagrange_vec = boost::make_shared<VectorDouble>();
    auto tau_mat = boost::make_shared<MatrixDouble>();

    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, temperature_vec));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<2>(gradField, gradient_mat));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(fluxField, flux_mat));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(lagrangeField, lagrange_vec));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(tauField, tau_mat));

    post_proc_fe->getOpPtrVector().push_back(

        new OpPPMap2(

            post_proc_fe->getPostProcMesh(), post_proc_fe->getMapGaussPts(),

            {{valueField, temperature_vec}, {lagrangeField, lagrange_vec}},

            {{gradField, gradient_mat}},

            {}, {})

    );

    post_proc_fe->getOpPtrVector().push_back(

        new OpPPMap3(

            post_proc_fe->getPostProcMesh(), post_proc_fe->getMapGaussPts(),

            {},

            {{fluxField, flux_mat}, {tauField, tau_mat}},

            {}, {})

    );

    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), post_proc_fe);

    // write output
    CHKERR post_proc_fe->writeFile(
        "out_iteration_" + boost::lexical_cast<std::string>(its) + ".h5m");

    // scale fields
    CHKERR m_field.getInterface<FieldBlas>()->fieldScale(
        common_data_ptr->scaleQ, "Q");
    CHKERR m_field.getInterface<FieldBlas>()->fieldScale(
        common_data_ptr->scaleV, "T");

    MoFEMFunctionReturn(0);
  };

  auto monitor_print_moab = [](SNES snes, PetscInt its, PetscReal fgnorm,
                               void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    string gauss_name;
    std::ostringstream strma;
    strma << "out_moab_" << its << ".h5m";
    gauss_name = strma.str();
    CHKERR snes_ptr->mField.get_moab().write_file(gauss_name.c_str(), "MOAB",
                                                  "PARALLEL=WRITE_PART");

    MoFEMFunctionReturn(0);
  };

  auto monitor_errors_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                     void *combined_ctx) {
    MoFEMFunctionBegin;
    auto combined_ptr =
        boost::static_pointer_cast<CombinedPointers>(combined_ctx);

    auto common_data_ptr = combined_ptr->commonDataPtr;

    SearchData monitorSearchData;

    monitorSearchData.mValPtr = common_data_ptr->mValPtr;
    monitorSearchData.mGradPtr = common_data_ptr->mGradPtr;
    monitorSearchData.mFluxPtr = common_data_ptr->mFluxPtr;
    monitorSearchData.mValStarPtr = common_data_ptr->mValStarPtr;
    monitorSearchData.mGradStarPtr = common_data_ptr->mGradStarPtr;
    monitorSearchData.mFluxStarPtr = common_data_ptr->mFluxStarPtr;
    monitorSearchData.pointDistance = common_data_ptr->pointDistance;
    monitorSearchData.rtree4D = common_data_ptr->rtree4D;
    monitorSearchData.rtree5D = common_data_ptr->rtree5D;
    monitorSearchData.flgPointAverage = common_data_ptr->flgPointAverage;
    monitorSearchData.pointAverage = common_data_ptr->pointAverage;
    monitorSearchData.linear_k = common_data_ptr->linear_k;
    monitorSearchData.mKStarPtr = common_data_ptr->mKStarPtr;
    monitorSearchData.mKStarCorrelationPtr =
        common_data_ptr->mKStarCorrelationPtr;

    auto &m_field = combined_ptr->snesCtxPtr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();
    int patchIntegNum = 3;
    boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
        new PatchIntegFaceEle(m_field, patchIntegNum));

    auto post_proc_rule_hook = [](int, int, int p) -> int { return 2 * p; };
    PostProcGauss->getRuleHook = post_proc_rule_hook;

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
    PostProcGauss->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpSetInvJacHcurlFace(inv_jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpSetInvJacL2ForFace(inv_jac_ptr));

    std::string valueField = "T";
    std::string gradField = "G";
    std::string fluxField = "Q";
    std::string tauField = "tau";
    std::string lagrangeField = "xi";

    // temperature: T
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, common_data_ptr->mValPtr));
    // gradient: g
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<2>(gradField,
                                            common_data_ptr->mGradPtr));
    // flux: q
    PostProcGauss->getOpPtrVector().push_back(new OpCalculateHVecVectorField<3>(
        fluxField, common_data_ptr->mFluxPtr));
    // gradient of temperature: grad(T)
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>("T",
                                              common_data_ptr->mValGradPtr));

    bool flgAnalytical = false;

    if (common_data_ptr->pReference) {
      PostProcGauss->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues("P_reference",
                                           common_data_ptr->mValRefPtr));
      PostProcGauss->getOpPtrVector().push_back(
          new OpPostProcRefErrorIntegPts("T", common_data_ptr));
    } else {

      PetscBool flg_ana_square_top = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_top",
                                 &flg_ana_square_top, PETSC_NULL);

      if (flg_ana_square_top) {
        flgAnalytical = true;
        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_squareTop));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mGradFuncPtr,
                                           analyticalFunctionGrad_squareTop));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          res[0] = -common_data_ptr->linear_k * M_PI * cos(M_PI * (x)) *
                   sinh(M_PI * (y)) / sinh(M_PI);
          res[1] = -common_data_ptr->linear_k * M_PI * sin(M_PI * (x)) *
                   cosh(M_PI * (y)) / sinh(M_PI);
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 3>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_square_sincos = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_sincos",
                                 &flg_ana_square_sincos, PETSC_NULL);

      if (flg_ana_square_sincos) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_squareSinCos));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(
                common_data_ptr->mGradFuncPtr,
                analyticalFunctionGrad_squareSinCos));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          double a = 2.;
          res[0] = -common_data_ptr->linear_k * a * M_PI * cos(a * M_PI * x) *
                   cos(a * M_PI * y);
          res[1] = common_data_ptr->linear_k * a * M_PI * sin(a * M_PI * x) *
                   sin(a * M_PI * y);
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 3>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_square_nonlinear_sincos = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_nonlinear_sincos",
                                 &flg_ana_square_nonlinear_sincos, PETSC_NULL);

      if (flg_ana_square_nonlinear_sincos) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_squareSinCos));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(
                common_data_ptr->mGradFuncPtr,
                analyticalFunctionGrad_squareSinCos));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          double a = 2.;
          double aa = 1.;
          double bb = 1.;
          double cc = 1.;

          VectorDouble res;
          res.resize(2);
          res[0] = -a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y) *
                   (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                             (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x)));
          res[1] =
              a * M_PI * sin(a * M_PI * x) *
              (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                        (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x))) *
              sin(a * M_PI * y);
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 3>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_mexi_hat = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_mexi_hat",
                                 &flg_ana_mexi_hat, PETSC_NULL);
      if (flg_ana_mexi_hat) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_mexiHat));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mGradFuncPtr,
                                           analyticalFunctionGrad_mexiHat));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          res[0] = -common_data_ptr->linear_k *
                   (-exp(-100. * (square(x) + square(y))) *
                    (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) *
                    cos(M_PI * y));
          res[1] = -common_data_ptr->linear_k *
                   (-exp(-100. * (square(x) + square(y))) *
                    (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) *
                    cos(M_PI * x));
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 3>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_L_shape = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_L_shape",
                                 &flg_ana_L_shape, PETSC_NULL);
      if (flg_ana_L_shape) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_LShape));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mGradFuncPtr,
                                           analyticalFunctionGrad_LShape));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          double numerator = 2 * common_data_ptr->linear_k *
                             (y * cos((M_PI + 2. * atan2(y, x)) / 3.) -
                              x * sin((M_PI + 2. * atan2(y, x)) / 3.));
          double denominator = 3. * pow(pow(x, 2) + pow(y, 2), 2.0 / 3.0);
          res[0] = numerator / denominator;
          numerator = -2 * common_data_ptr->linear_k *
                      (x * cos((M_PI + 2. * atan2(y, x)) / 3.) +
                       y * sin((M_PI + 2. * atan2(y, x)) / 3.));
          res[1] = numerator / denominator;
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 3>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }
    }

    PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
        common_data_ptr->petscVec, CommonDataSnesDD::GAUSS_COUNT));
    PostProcGauss->getOpPtrVector().push_back(
        new OpGetArea(common_data_ptr->petscVec, CommonDataClassic::VOL_E));

    if (flgAnalytical) {

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
          common_data_ptr->mValPtr, common_data_ptr->petscVec,
          CommonDataClassic::VALUE_ERROR, common_data_ptr->mValFuncPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
          common_data_ptr->mValGradPtr, common_data_ptr->petscVec,
          CommonDataClassic::GRAD_ERROR, common_data_ptr->mGradFuncPtr));

      // TODO: add grad grad error

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<3>(
          common_data_ptr->mFluxPtr, common_data_ptr->petscVec,
          CommonDataClassic::FLUX_ERROR, common_data_ptr->mFluxFuncPtr));
    }

    moab::Core mb_post;

    if (common_data_ptr->rtree4D.size() > 1)
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindData4D<3>("T", monitorSearchData, 1));
    else if (common_data_ptr->rtree5D.size() > 1) {
      // PostProcGauss->getOpPtrVector().push_back(
      //     new OpCalculateScalarFieldValues("T", common_data_ptr->mValPtr));
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindData5D<3>("T", monitorSearchData));
    } else
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindClosest<3>("T", monitorSearchData));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpScalarVarTags(common_data_ptr->pointDistance,
                                            "DD_DISTANCE_AVE",
                                            "DD_DISTANCE_VAR", m_field));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpFindKstar<3>("T", monitorSearchData));

    PostProcGauss->getOpPtrVector().push_back(
        new OpPointError("T", common_data_ptr));

    if (common_data_ptr->flgPrintInteg)
      PostProcGauss->getOpPtrVector().push_back(
          new OpPostProcPrintingIntegPts<3>("T", common_data_ptr, mb_post,
                                            analyticalFunction));

    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), PostProcGauss);

    if (common_data_ptr->flgPrintInteg) {
      string out_file_name;
      std::ostringstream strm;
      strm << "out_integ_pts_" << its << ".h5m";
      out_file_name = strm.str();
      CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                "PARALLEL=WRITE_PART");
    }

    mb_post.delete_mesh();

    MoFEMFunctionReturn(0);
  };

  auto monitor_edge_normal_post_proc = [](SNES snes, PetscInt its,
                                          PetscReal fgnorm, void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    auto &m_field = snes_ptr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();

    boost::shared_ptr<EdgeEle> PostProcGauss(new EdgeEle(m_field));

    auto post_proc_rule_hook = [](int, int, int p) -> int { return 2 * p; };
    PostProcGauss->getRuleHook = post_proc_rule_hook;

    PostProcGauss->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnEdge2D());

    moab::Core mb_post;

    PostProcGauss->getOpPtrVector().push_back(
        new OpPostProcPrintNormalPts("tau", mb_post));

    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getBoundaryFEName().c_str(), PostProcGauss);

    string out_file_name;
    std::ostringstream strm;
    strm << "out_edge_integ_pts_" << its << ".h5m";
    out_file_name = strm.str();
    CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                              "PARALLEL=WRITE_PART");

    mb_post.delete_mesh();

    MoFEMFunctionReturn(0);
  };

  // collect data across all integration points for the global vector
  auto monitor_combine_global_vector = [](SNES, PetscInt its, PetscReal,
                                          void *ctx) {
    MoFEMFunctionBegin;

    if (std::abs(its) < 1)
      MoFEMFunctionReturnHot(0);

    auto common_data_ptr =
        boost::static_pointer_cast<CommonDataHDivSnesDD>(ctx);

    CHKERR VecAssemblyBegin(common_data_ptr->petscVec);
    CHKERR VecAssemblyEnd(common_data_ptr->petscVec);

    const double *array;
    CHKERR VecGetArrayRead(common_data_ptr->petscVec, &array);

    common_data_ptr->vOlume = array[CommonDataHDivSnesDD::VOL_E];

    common_data_ptr->rmsPointDistErr.push_back(
        sqrt((array[CommonDataHDivSnesDD::POINT_ERROR]) /
             array[CommonDataHDivSnesDD::VOL_E]));
    common_data_ptr->rmsErr.push_back(
        sqrt((array[CommonDataHDivSnesDD::VALUE_ERROR]) /
             array[CommonDataHDivSnesDD::VOL_E]) /
        common_data_ptr->scaleV);
    common_data_ptr->gradErr.push_back(
        sqrt((array[CommonDataHDivSnesDD::GRAD_ERROR]) /
             array[CommonDataHDivSnesDD::VOL_E]));
    common_data_ptr->fluxErr.push_back(
        sqrt((array[CommonDataHDivSnesDD::FLUX_ERROR]) /
             array[CommonDataHDivSnesDD::VOL_E]) /
        common_data_ptr->scaleQ);

    std::cout << "Average point_distance: "
              << array[CommonDataHDivSnesDD::ACCUM_POINT_ERROR] /
                     array[CommonDataHDivSnesDD::GAUSS_COUNT]
              << std::endl;
    std::cout << "J = " << array[CommonDataHDivSnesDD::ACCUM_POINT_ERROR]
              << std::endl;
    std::cout << "# of gauss points: "
              << array[CommonDataHDivSnesDD::GAUSS_COUNT] << std::endl;

    std::ofstream error_file;
    error_file.open("errors_long_file.csv", ios::app);
    // populate the file with points
    error_file << common_data_ptr->rmsErr.back() << ","
               << common_data_ptr->gradErr.back() << ","
               << common_data_ptr->fluxErr.back() << ","
               << common_data_ptr->rmsPointDistErr.back() << "\n";
    // close writing into file
    error_file.close();

    CHKERR VecRestoreArrayRead(common_data_ptr->petscVec, &array);

    CHKERR VecZeroEntries(common_data_ptr->petscVec);

    MoFEMFunctionReturn(0);
  };

  bool print_edge_normals = false;
  // print_edge_normals = true;

  if (!print_edge_normals && !print_moab)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))MoFEMSNESMonitorFields,
                          (void *)(snes_ctx_ptr.get()), nullptr);

  if (!skip_vtk && !print_moab)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_vtk_post_proc,
                          (void *)(combinedPointers.get()), nullptr);

  if (print_moab) { // if whole moab output is desired
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_print_moab,
                          (void *)(snes_ctx_ptr.get()), nullptr);
  }

  CHKERR SNESMonitorSet(snesSolver,
                        (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                           void *))monitor_errors_post_proc,
                        (void *)(combinedPointers.get()), nullptr);

  if (!skip_vtk && print_edge_normals)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal, void *))
                              monitor_edge_normal_post_proc,
                          (void *)(snes_ctx_ptr.get()), nullptr);

  CHKERR SNESMonitorSet(snesSolver,
                        (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal, void *))
                            monitor_combine_global_vector,
                        (void *)(commonDataPtr.get()), nullptr);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::addMeshRefForDistanceVar() {
  MoFEMFunctionBegin;

  // remove everything from meshRefEntAdditional
  meshRefEntAdditional.clear();

  double tol_distance_var_ref = 0.0;
  // get tolerance for distance refinement
  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-tol_distance_var_ref",
                             &tol_distance_var_ref, PETSC_NULL);

  double tol_distance_var_ave = 0.0;
  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-tol_distance_var_ave",
                             &tol_distance_var_ave, PETSC_NULL);

  // if tolerance is defined and not zero
  if ((tol_distance_var_ref + tol_distance_var_ave) > 1e-10) {
    Tag th_distance_var, th_distance_ave;
    CHKERR getTagHandle(mField, "DD_DISTANCE_VAR", MB_TYPE_DOUBLE,
                        th_distance_var);
    CHKERR getTagHandle(mField, "DD_DISTANCE_AVE", MB_TYPE_DOUBLE,
                        th_distance_ave);

    // use within the elements which would have their order increased if they
    // could
    Tag th_error_ind;
    CHKERR getTagHandle(mField, "ERROR_ESTIMATOR", MB_TYPE_DOUBLE,
                        th_error_ind);

    for (auto &ent : domainEntities) {
      double dist_var = 0;
      double dist_ave = 0;
      CHKERR mField.get_moab().tag_get_data(th_distance_var, &ent, 1,
                                            &dist_var);
      CHKERR mField.get_moab().tag_get_data(th_distance_ave, &ent, 1,
                                            &dist_ave);

      double err_indic = 0;
      CHKERR mField.get_moab().tag_get_data(th_error_ind, &ent, 1, &err_indic);

      if ((dist_var >
           tol_distance_var_ref * 0.01 * commonDataPtr->normFields) &&
          (dist_var > tol_distance_var_ave * dist_ave) &&
          (err_indic >
           refControl * sqrt(errorIndicatorIntegral / totalElementNumber))) {

        meshRefEntAdditional.insert(ent);
        Range adj;
        CHKERR mField.get_moab().get_adjacencies(&ent, 1, 1, false, adj,
                                                 moab::Interface::UNION);
        meshRefEntAdditional.merge(adj);
      }
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::refineOrderLoop() {
  MoFEMFunctionBegin;

  CHKERR checkError(0, true);
  CHKERR outputHdivVtkFields(0, "out_result_refined_");
  CHKERR saveSumanalys();

  auto zero_fields = [&]() { // set G and Q to zero
    MoFEMFunctionBeginHot;
    for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, gradField, it)) {
      if (it->get()->getEntType() == MBTRI || MBQUAD) {
        auto field_data = (*it)->getEntFieldData();
        for (auto &v : field_data) {
          v = 0.;
        }
      }
    }

    for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, valueField, it)) {
      if (it->get()->getEntType() == MBTRI || MBQUAD) {
        auto field_data = (*it)->getEntFieldData();
        for (auto &v : field_data) {
          v = 0.;
        }
      }
    }

    auto zero_filed = [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
      MoFEMFunctionBeginHot;
      auto field_data = ent_ptr_x->getEntFieldData();
      for (auto &v : field_data) {
        v = 0.;
      }
      MoFEMFunctionReturnHot(0);
    };

    CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(zero_filed,
                                                                   fluxField);
    MoFEMFunctionReturnHot(0);
  };

  auto reset_error_indicators = [&]() { // reset error indicators
    MoFEMFunctionBeginHot;
    Tag th_error_ind, th_jump_l2;
    CHKERR getTagHandle(mField, "ERROR_INDICATOR_GRAD", MB_TYPE_DOUBLE,
                        th_error_ind);
    CHKERR getTagHandle(mField, "JUMP_L2", MB_TYPE_DOUBLE, th_jump_l2);
    for (auto &ent : domainEntities) {
      double zero = 0;
      CHKERR mField.get_moab().tag_set_data(th_error_ind, &ent, 1, &zero);
      CHKERR mField.get_moab().tag_set_data(th_jump_l2, &ent, 1, &zero);
    }
    MoFEMFunctionReturnHot(0);
  };

  switch (refinementStyle) {
  case 1:
    while (orderRefinementCounter < refIterNum) {
      CHKERR refineOrder();
      orderRefinementCounter += 1;

      CHKERR boundaryCondition();
      CHKERR removeLambda();
      CHKERR assembleSystem();
      CHKERR getSnesInitial();
      CHKERR setSnesMonitors();
      CHKERR zero_fields();
      CHKERR solveSnesSystem();

      CHKERR reset_error_indicators();

      CHKERR checkError(orderRefinementCounter, true);
      CHKERR saveSumanalys();

      CHKERR outputHdivVtkFields(orderRefinementCounter, "out_result_refined_");
    }
    break;
  case 2:
    while (meshRefinementCounter < refIterNum) {
      CHKERR addMeshRefForDistanceVar();
      CHKERR refineMesh();

      CHKERR boundaryCondition();
      CHKERR removeLambda();
      CHKERR assembleSystem();
      CHKERR getSnesInitial();
      simpleInterface->getBitRefLevel() =
          BitRefLevel().set(meshRefinementCounter);
      simpleInterface->getBitRefLevelMask() = BitRefLevel().set();
      CHKERR setSnesMonitors();
      CHKERR zero_fields();
      CHKERR solveSnesSystem();

      CHKERR reset_error_indicators();

      CHKERR checkError(meshRefinementCounter, true);
      CHKERR saveSumanalys();

      CHKERR outputHdivVtkFields(meshRefinementCounter, "out_result_refined_");
    }
    break;
  case 3:
    while (orderRefinementCounter < refIterNum) {
      CHKERR addMeshRefForDistanceVar();
      CHKERR refineOrder();
      CHKERR refineMesh();
      CHKERR recheckOrder();
      orderRefinementCounter += 1;
      CHKERR boundaryCondition();
      CHKERR removeLambda();
      CHKERR assembleSystem();
      CHKERR getSnesInitial();
      simpleInterface->getBitRefLevel() =
          BitRefLevel().set(meshRefinementCounter);
      simpleInterface->getBitRefLevelMask() = BitRefLevel().set();
      CHKERR setSnesMonitors();
      CHKERR zero_fields();
      CHKERR solveSnesSystem();

      CHKERR reset_error_indicators();

      CHKERR checkError(orderRefinementCounter, true);
      CHKERR saveSumanalys();

      CHKERR outputHdivVtkFields(orderRefinementCounter, "out_result_refined_");
    }
    break;
  case 4:
    while ((orderRefinementCounter + meshRefinementCounter) / 2 < refIterNum) {

      // alternate between order and mesh refinement
      if (orderRefinementCounter > meshRefinementCounter) {
        MOFEM_LOG("WORLD", Sev::inform) << ".. mesh refinement";
        CHKERR addMeshRefForDistanceVar();
        CHKERR refineMesh();
      } else {
        MOFEM_LOG("WORLD", Sev::inform) << ".. order refinement";
        CHKERR addMeshRefForDistanceVar();
        CHKERR refineOrder();
        orderRefinementCounter += 1;
      }

      CHKERR recheckOrder();

      CHKERR boundaryCondition();
      CHKERR removeLambda();
      CHKERR assembleSystem();
      CHKERR getSnesInitial();
      simpleInterface->getBitRefLevel() =
          BitRefLevel().set(meshRefinementCounter);
      simpleInterface->getBitRefLevelMask() = BitRefLevel().set();
      CHKERR setSnesMonitors();
      CHKERR zero_fields();
      CHKERR solveSnesSystem();

      CHKERR reset_error_indicators();

      CHKERR checkError(orderRefinementCounter, true);
      CHKERR saveSumanalys();

      CHKERR outputHdivVtkFields(orderRefinementCounter, "out_result_refined_");
    }
    break;
  default:
    MOFEM_LOG("WORLD", Sev::inform) << ".. no refinement";
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::recheckOrder() {
  MoFEMFunctionBegin;

  simpleInterface->getBitRefLevel() = BitRefLevel().set(meshRefinementCounter);
  simpleInterface->getBitRefLevelMask() = BitRefLevel().set();

  Tag th_order;
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, th_order);

  std::vector<Range> refinement_levels;
  refinement_levels.resize(refIterNum + 1);

  for (auto ent : domainEntities) {
    int order;
    CHKERR mField.get_moab().tag_get_data(th_order, &ent, 1, &order);
    refinement_levels[order - oRder].insert(ent);
  }

  for (int ll = 1; ll < refinement_levels.size(); ll++) {
    CHKERR mField.getInterface<CommInterface>()->synchroniseEntities(
        refinement_levels[ll]);
    CHKERR mField.set_field_order(refinement_levels[ll], fluxField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], gradField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], tauField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], valueField,
                                  oRder + ll);
    CHKERR mField.set_field_order(refinement_levels[ll], lagrangeField,
                                  oRder + ll);
  }

  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      fluxField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      gradField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      tauField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      valueField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      lagrangeField);
  CHKERR mField.build_fields();
  CHKERR mField.build_finite_elements();
  CHKERR mField.build_adjacencies(simpleInterface->getBitRefLevel());
  mField.getInterface<ProblemsManager>()->buildProblemFromFields = PETSC_TRUE;

  simpleInterface->getBitRefLevel() = BitRefLevel().set(meshRefinementCounter);
  simpleInterface->getBitRefLevelMask() = BitRefLevel().set();

  CHKERR DMSetUp_MoFEM(simpleInterface->getDM());

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::outputFarPointsCsv(std::string vtk_name) {
  MoFEMFunctionBegin;

  std::ofstream myfile;
  myfile.open(vtk_name);
  myfile << "gradx,grady,fluxx,fluxy,value\n"; // headers
  myfile.close();

  std::ofstream file_for_the_rest;
  file_for_the_rest.open("not_" + vtk_name);
  file_for_the_rest << "gradx,grady,fluxx,fluxy,value\n"; // headers
  file_for_the_rest.close();

  fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
      new PostProcFaceOnRefinedMesh(mField));
  fePostProcPtr->getOpPtrVector().clear();

  // Generate post-processing mesh
  CHKERR fePostProcPtr->generateReferenceElementMesh();

  CHKERR calculateHdivJacobian(fePostProcPtr);

  // temperature: T
  fePostProcPtr->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
  // gradient: g
  fePostProcPtr->getOpPtrVector().push_back(
      new OpCalculateVectorFieldValues<2>(gradField, commonDataPtr->mGradPtr));
  // flux: q
  fePostProcPtr->getOpPtrVector().push_back(
      new OpCalculateHVecVectorField<3>(fluxField, commonDataPtr->mFluxPtr));

  if (commonDataPtr->rtree4D.size() > 1)
    fePostProcPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpFindData4D<3>(valueField, searchData));
  else if (commonDataPtr->rtree5D.size() > 1)
    fePostProcPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpFindData5D<3>(valueField, searchData));

  fePostProcPtr->getOpPtrVector().push_back(new OpPostProcPrintFarIntegPts<3>(
      valueField, commonDataPtr, commonDataPtr->rmsPointDistErr.back(),
      vtk_name));

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), fePostProcPtr);

  fePostProcPtr->getOpPtrVector().clear();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::runMonteCarlo() {
  MoFEMFunctionBegin;

  if (!flg_monte_carlo)
    MoFEMFunctionReturnHot(0);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "I'm trying to run Monte Carlo...";

  CHKERR saveSumanalys("sumanalys_monte.csv");

  mField.getInterface<FieldBlas>()->fieldCopy(1., valueField, "T_ave");

  //   CHKERR getKSPinitial();

  // restart the counter
  commonDataPtr->counter = 0;

  {
    // Create RHS and solution vectors
    SmartPetscObj<Vec> global_rhs;
    CHKERR DMCreateGlobalVector_MoFEM(dM, global_rhs);
    CHKERR VecGetSize(global_rhs, &solution_size);
  }

  CHKERR MatSetSizes(matGlobalResults, commonDataPtr->numberMonteCarlo,
                     solution_size, commonDataPtr->numberMonteCarlo,
                     solution_size);
  // CHKERR MatSetType(matGlobalResults, MATSEQAIJ);

  PetscScalar *a;
  PetscMalloc1(commonDataPtr->numberMonteCarlo * solution_size, &a);

  MatSetType(matGlobalResults, MATSEQDENSE);
  MatSeqDenseSetPreallocation(matGlobalResults, a);

  // Assemble MPI vector
  CHKERR MatAssemblyBegin(matGlobalResults, MAT_FINAL_ASSEMBLY);
  CHKERR MatAssemblyEnd(matGlobalResults, MAT_FINAL_ASSEMBLY);

  CHKERR MatZeroEntries(matGlobalResults);

  // MatView(matGlobalResults, PETSC_VIEWER_STDOUT_WORLD);

  { // set sizes for tags matrices

    int patch_integ_num = commonDataPtr->monte_patch_number;
    boost::shared_ptr<PatchIntegFaceEle> TagGauss(
        new PatchIntegFaceEle(mField, patch_integ_num));

    auto domain_rule_tag = [](int, int, int p) -> int { return -1; };
    TagGauss->getRuleHook = domain_rule_tag;

    TagGauss->getOpPtrVector().push_back(new OpResizeMonteMatrix(
        valueField, commonDataPtr->numberMonteCarlo, mField));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), TagGauss);
  }

  { // set sizes for tags matrices in postproc

    fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
        new PostProcFaceOnRefinedMesh(mField));

    // Generate post-processing mesh
    CHKERR fePostProcPtr->generateReferenceElementMesh();

    CHKERR calculateHdivJacobian(fePostProcPtr);

    fePostProcPtr->getOpPtrVector().push_back(new OpResizeMonteMatrix(
        valueField, commonDataPtr->numberMonteCarlo, mField));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), fePostProcPtr);
  }

  // create the file and put in the headers
  if (mField.get_comm_rank() == 0 && flg_long_error_file) {
    std::ofstream error_file;
    error_file.open("standard_deviation_max.csv");
    error_file << "integ_sigma_max_p, " << "integ_sigma_max_grad_x, "
               << "integ_sigma_max_grad_y, " << "integ_sigma_max_flux_x, "
               << "integ_sigma_max_flux_y" << std::endl; // headers
    error_file.close();
  }

  // CHKERR assembleSystem();

  for (int i = 0; i < commonDataPtr->numberMonteCarlo; i++) {

    CHKERR assembleSystem();

    if (!skip_vtk)
      CHKERR outputHdivVtkFields(commonDataPtr->monte_carlo_counter,
                                 "out_before_perturb_");

    // CHKERR getKSPinitial();
    // CHKERR perturbate();

    // random
    PetscRandom rctx;
    PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
    PetscRandomSetSeed(rctx, (unsigned long)std::time(0));
    PetscRandomSeed(rctx);

    // make scale an input
    double scale = 10;
    CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-rand_ini_scale", &scale,
                               PETSC_NULL);

    if (scale < 10e-4)
      scale = commonDataPtr->rmsPointDistErr.back();

    // set random G field (L2)
    for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, gradField, it)) {
      if (it->get()->getEntType() == MBTRI || MBQUAD) {
        auto field_data = (*it)->getEntFieldData();
        double value;
        // double scale = 1.0;
        PetscRandomGetValueReal(rctx, &value);
        field_data[0] += (value - 0.5) * scale * 5; // value * scale
        PetscRandomGetValueReal(rctx, &value);
        field_data[1] += (value - 0.5) * scale * 5;
      }
    }

    auto vector_times_scalar_field =
        [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
          MoFEMFunctionBeginHot;
          auto x_data = ent_ptr_x->getEntFieldData();
          double value;
          PetscRandomGetValueReal(rctx, &value);

          for (auto &v : x_data) {
            PetscRandomGetValueReal(rctx, &value);
            v += (value - 0.5) * scale;
          }

          MoFEMFunctionReturnHot(0);
        };

    CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(
        vector_times_scalar_field, fluxField);

    if (!skip_vtk)
      CHKERR outputHdivVtkFields(commonDataPtr->monte_carlo_counter,
                                 "out_after_perturb_");

    // add to the counter
    commonDataPtr->counter++;

    CHKERR solveSnesSystem();

    { // restart max_sigmas
      // max sigma for for integ point
      commonDataPtr->integ_sigma_max_p = 0;
      commonDataPtr->integ_sigma_max_grad_x = 0;
      commonDataPtr->integ_sigma_max_grad_y = 0;
      commonDataPtr->integ_sigma_max_flux_x = 0;
      commonDataPtr->integ_sigma_max_flux_y = 0;
    }

    // calculation with TAGS
    { // populate the tag matrices

      moab::Core mb_post;                   // create database
      moab::Interface &moab_proc = mb_post; // create interface to database

      int patch_integ_num = commonDataPtr->monte_patch_number;
      boost::shared_ptr<PatchIntegFaceEle> TagGauss(
          new PatchIntegFaceEle(mField, patch_integ_num));

      auto domain_rule_tag = [](int, int, int p) -> int { return -1; };

      TagGauss->getRuleHook = domain_rule_tag;

      CHKERR calculateHdivJacobian(TagGauss);

      TagGauss->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
      TagGauss->getOpPtrVector().push_back(new OpCalculateVectorFieldValues<2>(
          gradField, commonDataPtr->mGradPtr));

      TagGauss->getOpPtrVector().push_back(new OpCalculateHVecVectorField<3>(
          fluxField, commonDataPtr->mFluxPtr));

      TagGauss->getOpPtrVector().push_back(
          new OpAddResultToMonteMatrix<3>(valueField, commonDataPtr, mField));

      TagGauss->getOpPtrVector().push_back(
          new OpGetMeanVar(commonDataPtr, mField, mb_post));

      TagGauss->getOpPtrVector().push_back(new OperatorsForDD::OpScalarToField(
          "T_ave", commonDataPtr->mValAvePtr));

      TagGauss->getOpPtrVector().push_back(
          new OpTagsMeanVar(valueField, commonDataPtr, mField, mb_post));

      CHKERR DMoFEMLoopFiniteElements(
          dM, simpleInterface->getDomainFEName().c_str(), TagGauss);

      if (flg_skip_all_vtk == false) {
        string out_file_name;
        std::ostringstream strm;
        strm << "out_integ_pts_mean_var_" << commonDataPtr->monte_carlo_counter
             << ".h5m";
        out_file_name = strm.str();
        CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                  "PARALLEL=WRITE_PART");
      }

      mb_post.delete_mesh();
    }

    if (flg_skip_all_vtk == false)
      CHKERR checkError(
          commonDataPtr->monte_carlo_counter, true,
          "out_monte_ele_errors_"); // post processing errors for Hdiv bits

    CHKERR saveSumanalys("sumanalys_monte.csv");

    CHKERR outputResults(
        commonDataPtr->monte_carlo_counter, true,
        "out_monte_integ_star_"); // also post processing errors

    if (flg_skip_all_vtk == false) {
      std::vector<Tag> tag_handles;
      tag_handles.resize(5);
      CHKERR getTagHandle(mField, "SIGMA_T", MB_TYPE_DOUBLE, tag_handles[0]);
      CHKERR getTagHandle(mField, "SIGMA_GRAD_X", MB_TYPE_DOUBLE,
                          tag_handles[1]);
      CHKERR getTagHandle(mField, "SIGMA_GRAD_Y", MB_TYPE_DOUBLE,
                          tag_handles[2]);
      CHKERR getTagHandle(mField, "SIGMA_FLUX_X", MB_TYPE_DOUBLE,
                          tag_handles[3]);
      CHKERR getTagHandle(mField, "SIGMA_FLUX_Y", MB_TYPE_DOUBLE,
                          tag_handles[4]);

      ParallelComm *pcomm =
          ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
      if (pcomm == NULL)
        SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
                "Communicator not set");

      auto bit_level1 = BitRefLevel().set(meshRefinementCounter);
      auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
      CHKERR
      mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
          bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);
      tag_handles.push_back(pcomm->part_tag());
      std::ostringstream strm;
      strm << "out_sigma_ele_" << commonDataPtr->monte_carlo_counter << ".h5m";
      CHKERR mField.get_moab().write_file(
          strm.str().c_str(), "MOAB", "PARALLEL=WRITE_PART",
          out_meshset_tri_ptr->get_ptr(), 1, tag_handles.data(),
          tag_handles.size());
    }

    commonDataPtr->monte_carlo_counter++;

    if (mField.get_comm_rank() == 0 && flg_long_error_file) {
      // open the error file with appending to the file
      std::ofstream error_file;
      error_file.open("standard_deviation_max.csv", ios::app);
      // populate the file with points
      error_file << commonDataPtr->integ_sigma_max_p << ", "
                 << commonDataPtr->integ_sigma_max_grad_x << ", "
                 << commonDataPtr->integ_sigma_max_grad_y << ", "
                 << commonDataPtr->integ_sigma_max_flux_x << ", "
                 << commonDataPtr->integ_sigma_max_flux_y << "\n";
      // close writing into file
      error_file.close();
    }

    // CHKERR checkError(commonDataPtr->monte_carlo_counter, false);
    // CHKERR saveSumanalys("sumanalys_monte.csv");
  }

  // TODO: check
  {

    auto post_proc_fe =
        boost::make_shared<PostProcBrokenMeshInMoab<FaceEle>>(mField);

    // CHKERR AddHOOps<2, 2, 2>::add(post_proc_fe->getOpPtrVector(), {L2,
    // HCURL});

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
    post_proc_fe->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(
        new OpSetInvJacHcurlFace(inv_jac_ptr));
    post_proc_fe->getOpPtrVector().push_back(
        new OpSetInvJacL2ForFace(inv_jac_ptr));

    using OpPPMap2 = OpPostProcMapInMoab<2, 2>;
    using OpPPMap3 = OpPostProcMapInMoab<3, 3>;

    std::string valueField = "T";
    std::string gradField = "G";
    std::string fluxField = "Q";
    std::string tauField = "tau";
    std::string lagrangeField = "xi";

    auto temperature_vec = boost::make_shared<VectorDouble>();
    auto gradient_mat = boost::make_shared<MatrixDouble>();
    auto flux_mat = boost::make_shared<MatrixDouble>();
    auto lagrange_vec = boost::make_shared<VectorDouble>();
    auto tau_mat = boost::make_shared<MatrixDouble>();

    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, temperature_vec));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<2>(gradField, gradient_mat));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(fluxField, flux_mat));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(lagrangeField, lagrange_vec));
    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(tauField, tau_mat));

    auto temperature_ave_vec = boost::make_shared<VectorDouble>();

    post_proc_fe->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("T_ave", temperature_ave_vec));

    post_proc_fe->getOpPtrVector().push_back(

        new OpPPMap2(

            post_proc_fe->getPostProcMesh(), post_proc_fe->getMapGaussPts(),

            {{valueField, temperature_vec},
             {lagrangeField, lagrange_vec},
             {"T_ave", temperature_ave_vec}},

            {{gradField, gradient_mat}},

            {}, {})

    );

    post_proc_fe->getOpPtrVector().push_back(

        new OpPPMap3(

            post_proc_fe->getPostProcMesh(), post_proc_fe->getMapGaussPts(),

            {},

            {{fluxField, flux_mat}, {tauField, tau_mat}},

            {}, {})

    );

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), post_proc_fe);

    // write output
    CHKERR post_proc_fe->writeFile("out_monte_result.h5m");
  }

  // add to the file capturing the end of the analysis
  // "sumanalys_monte_sigma.csv"
  if (mField.get_comm_rank() == 0) {
    std::ofstream sumanalysis;
    sumanalysis.open("sumanalys_monte_sigma.csv",
                     std::ofstream::out | std::ofstream::app);

    sumanalysis << commonDataPtr->integ_sigma_max_p << ", "
                << commonDataPtr->integ_sigma_max_grad_x << ", "
                << commonDataPtr->integ_sigma_max_grad_y << ", "
                << commonDataPtr->integ_sigma_max_flux_x << ", "
                << commonDataPtr->integ_sigma_max_flux_y << "\n";

    sumanalysis.close();

    CHKERR checkError(commonDataPtr->monte_carlo_counter, false);
    CHKERR saveSumanalys("sumanalys_monte.csv");
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivSnesDD::saveSumanalys(std::string csv_name) {
  MoFEMFunctionBegin;

  // add to the file capturing the end of the analysis "sumanalys.csv"
  if (mField.get_comm_rank() == 0) {
    std::ofstream sumanalysis;
    sumanalysis.open(csv_name, std::ofstream::out | std::ofstream::app);

    sumanalysis << oRder << ", "
                << commonDataPtr->numberIntegrationPoints.back() << ", "
                << commonDataPtr->rmsErr.size() << ", " << commonDataPtr->vOlume
                << ", " << dummy_count << ", "
                << commonDataPtr->rmsPointDistErr.back() << ", "
                << errorEstimator.back() << ", " << commonDataPtr->rmsErr.back()
                << ", " << commonDataPtr->gradErr.back() << ", "
                << commonDataPtr->fluxErr.back() << ", "
                << orderRefinementCounter << ", " << errorIndicatorGrad.back()
                << ", " << errorIndicatorDiv.back() << ", " << jumpL2.back()
                << ", " << jumpHdiv.back() << ", " << totalElementNumber
                << std::endl;

    sumanalysis.close();
  }

  MoFEMFunctionReturn(0);
}

// operators

// post processing
MoFEMErrorCode
DiffusionHDivSnesDD::OpPostProcPrintNormalPts::doWork(int side, EntityType type,
                                                      EntData &data) {
  MoFEMFunctionBegin;

  const int nb_integration_pts = data.getN().size1();

  int tag_length = 3;
  double def_VAL[tag_length];
  bzero(def_VAL, tag_length * sizeof(double));

  // normal vector
  Tag th_normal;
  CHKERR outputMesh.tag_get_handle("NORMAL", tag_length, MB_TYPE_DOUBLE,
                                   th_normal, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);
  // get tangent
  FTensor::Tensor1<double, 3> t_z{0., 0., 1.};
  auto t_tangent = UserDataOperator::getFTensor1TangentAtGaussPts<3>();

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    // create new mapping for gauss points
    EntityHandle new_vertex;
    const double *coords_ptr = &(getCoordsAtGaussPts()(gg, 0));
    CHKERR outputMesh.create_vertex(coords_ptr, new_vertex);

    // get normal
    FTensor::Tensor1<double, 3> t_normal;
    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    FTensor::Index<'k', 3> k;
    t_normal(i) = FTensor::levi_civita(i, j, k) * t_tangent(j) * t_z(k);
    // get normal vector length
    double length_normal = sqrt(t_normal(i) * t_normal(i));

    CHKERR outputMesh.tag_set_data(th_normal, &new_vertex, 1, &t_normal(0));

    ++t_tangent;
  }

  MoFEMFunctionReturn(0);
}
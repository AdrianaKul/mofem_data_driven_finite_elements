#include <DataDrivenDiffusionSnesProblem.hpp>
#include <OperatorsForDD.hpp>

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpDomainGradGrad = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpGradGrad<1, 1, 2>;

using OpDomainMass = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<1, 2>;

using OpBaseGradH = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixVectorTimesGrad<1, 2, 2>;

using OpBoundaryMass = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<1, 1>;

using OpBoundarySourceRhs = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpBoundaryTimesScalarField = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpBaseTimesScalarField<1>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 1, 1>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 1, 1>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

MoFEMErrorCode DiffusionSnesDD::runProblem() {
  MoFEMFunctionBegin;

  CHKERR setParamValues();
  CHKERR SetParamDDValues();
  CHKERR readMesh();
  CHKERR setupProblem();
  CHKERR setIntegrationRules();
  CHKERR createClassicCommonData();
  CHKERR createDDCommonData();
  CHKERR createDDsnesCommonData();
  CHKERR boundaryCondition();
  CHKERR markPressureBc();
  CHKERR setRandomInitialFields();
  CHKERR removeLambda();
  CHKERR assembleSystem();
  CHKERR getSnesInitial();
  CHKERR errorFileNameSetUp();
  CHKERR setSnesMonitors();
  CHKERR solveSnesSystem();
  CHKERR saveSumanalys();
  CHKERR outputHVtkFields(commonDataPtr->counter);
  CHKERR checkErrorDD(1000, true);
  CHKERR setParamMonteCarlo();
  CHKERR runMonteCarlo();
  CHKERR checkResults();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::setupProblem() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField(valueField, H1,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDomainField(fluxField, L2, AINSWORTH_LEGENDRE_BASE,
                                         2);
  CHKERR simpleInterface->addDomainField(lagrangeField, H1,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(valueField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(lagrangeField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->setFieldOrder(valueField, oRder);
  CHKERR simpleInterface->setFieldOrder(fluxField, oRder - 1);
  CHKERR simpleInterface->setFieldOrder(lagrangeField, oRder);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataSnesDD>();

  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;
  DiffusionDD::commonDataPtr = commonDataPtr;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::createDDsnesCommonData() {
  MoFEMFunctionBegin;

  commonDataPtr->mGradLagrangeScalarPtr = boost::make_shared<MatrixDouble>();

  DDLogTag;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-use_part_star", &flg_part_star,
                             PETSC_NULL);

  if (flg_part_star) {
    MOFEM_LOG("WORLD", Sev::inform) << "-use_part_star used";
    commonDataPtr->flgPrintK = true;

    CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-partial_part", &partial_part_a,
                               PETSC_NULL);
    if (abs(partial_part_a - 1.) > 1e-6) {
      if (abs(partial_part_a) > 1) {
        SETERRQ(PETSC_COMM_WORLD, MOFEM_NOT_IMPLEMENTED,
                "-partial_part out of range (choose value between 0.1 and 1)");
      }

      flg_partial_part = true;
      partial_part_b = 1. - partial_part_a;
      MOFEM_LOG("WORLD", Sev::inform)
          << "-partial_part used: " << partial_part_a << ": " << partial_part_b;
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::assembleSystem() {
  MoFEMFunctionBegin;

  CHKERR calculateJacobian(feDomainLhsPtr);
  CHKERR calculateJacobian(feDomainRhsPtr);

  //   feDomainLhsPtr->getOpPtrVector().push_back(new OpTestLhs(fluxField));

  { // LHS of domain elements - Push operators to the Pipeline
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    { // parts corresponding to * points
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpDomainGradGrad(valueField, valueField, s_p_Function));
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpDomainMass(fluxField, fluxField, s_q_Function));
    }
    { // Lagrange multiplier parts
      auto unity = []() { return 1.0; };
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpBaseGradH(fluxField, lagrangeField, unity, true));
    }
    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  if (flg_part_star == true) { // partial derivatives for J(p,g,q) Rhs

    // auto partial_part_b_function = [&](const double x, const double y,
    //                                    const double z) {
    //   return -partial_part_b;
    // };

    // auto partial_part_a_function = [&](const double x, const double y,
    //                                    const double z) {
    //   return partial_part_a;
    // };

    // auto q_star_g_function = [&]() { return partial_part_a * dummy_k; };
    // auto g_star_q_function = [&]() { return partial_part_a / dummy_k; };

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));

    // find k*

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpFindKstar<2>(valueField, searchData,
                                           commonDataPtr->scaleV));

    feDomainLhsPtr->getOpPtrVector().push_back(new OperatorsForDD::OpGstarQLhs(
        fluxField, valueField, searchData.mKStarPtr, oneFunction));

    feDomainLhsPtr->getOpPtrVector().push_back(new OperatorsForDD::OpQstarQLhs(
        fluxField, fluxField, searchData.mKStarPtr, oneFunction));

    feDomainLhsPtr->getOpPtrVector().push_back(new OperatorsForDD::OpGstarGLhs(
        valueField, fluxField, searchData.mKStarPtr, oneFunction));

    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // Rhs domain (source)
    // (l,f)
    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, lagrangeField,
        commonDataPtr->scaleQ * commonDataPtr->linear_k, Sev::inform);
  }

  { // Push operators to the Pipeline that is responsible for calculating LHS of
    // boundary elements
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpBoundaryMass(valueField, valueField, oneFunction,
                           boost::make_shared<Range>(pressureBcRange)));
    feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // Push operators to the Pipeline that is responsible for calculating RHS of
    // boundary elements
    // (u, u_bar)
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, -1.,
        Sev::inform);

    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpBoundaryTimesScalarField(
        valueField, commonDataPtr->mValPtr, oneFunction,
        boost::make_shared<Range>(pressureBcRange)));

    // (l, q_bar)
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(lagrangeField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, lagrangeField,
        commonDataPtr->scaleQ * commonDataPtr->linear_k, Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(lagrangeField));
  }

  { // get gradients of pressure and flux to find in the data set
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField,
                                              commonDataPtr->mGradPtr));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<2>(fluxField,
                                            commonDataPtr->mFluxPtr));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(
            lagrangeField, commonDataPtr->mGradLagrangeScalarPtr));
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // find grad p* and q* in the data set and get point distance errors
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    if (!flg_use_line) {
      switch (datasetDim) {
      case 4:
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData4D<2>(valueField, searchData));
        break;
      case 5:
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OpCalculateScalarFieldValues(valueField,
                                             commonDataPtr->mValPtr));
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData5D<2>(valueField, searchData,
                                                commonDataPtr->scaleV));
        if (dd_use_data_value) {
          auto scale_v_func = [&](const double x, const double y,
                                  const double z) {
            return -1. / commonDataPtr->scaleV;
          };
          feDomainRhsPtr->getOpPtrVector().push_back(
              new OperatorsForDD::Op1dStarRhs(
                  valueField, commonDataPtr->mValStarPtr, scale_v_func));
          feDomainRhsPtr->getOpPtrVector().push_back(
              new OperatorsForDD::Op1dStarRhs(
                  valueField, commonDataPtr->mValPtr, oneFunction));
        }
        break;
      }
    }
    if (flg_use_line)
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindClosest<2>(valueField, searchData));
    // feDomainRhsPtr->getOpPtrVector().push_back(
    //     new OperatorsForDD::OpFindKstar<2>(valueField, searchData));
    // feDomainRhsPtr->getOpPtrVector().push_back(
    //     new OpPointError(valueField, commonDataPtr));
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // RHS of domain elements - Push operators to the Pipeline
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpGradStarRhs(
            valueField, commonDataPtr->mGradStarPtr, minusFunction));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpL2StarRhs<2>(
            fluxField, commonDataPtr->mFluxStarPtr, minusFunction));
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // more residual operators due to snes solver
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpGradStarRhs(valueField, commonDataPtr->mGradPtr,
                                          oneFunction));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpL2StarRhs<2>(fluxField, commonDataPtr->mFluxPtr,
                                           oneFunction));

    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpGradStarRhs(
            lagrangeField, commonDataPtr->mFluxPtr, oneFunction));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpL2StarRhs<2>(
            fluxField, commonDataPtr->mGradLagrangeScalarPtr, oneFunction));
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    feDomainRhsPtr->getOpPtrVector().push_back(new OpPrintStarIntegPts<2>(
        valueField, commonDataPtr, mField.get_moab()));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::setRandomInitialFields() {
  MoFEMFunctionBegin;

  MOFEM_LOG("WORLD", Sev::inform) << "Setting random initial fields";

  if (flg_rand_ini) {
    // random
    PetscRandom rctx;
    PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
    PetscRandomSetSeed(rctx, (unsigned long)std::time(0));
    PetscRandomSeed(rctx);

    // make scale an input
    double scale = 10;
    CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-rand_ini_scale", &scale,
                               PETSC_NULL);

    // set random G field (L2)
    for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, fluxField, it)) {
      if (it->get()->getEntType() == MBTRI || MBQUAD) {
        auto field_data = (*it)->getEntFieldData();
        double value;
        // double scale = 1.0;
        PetscRandomGetValueReal(rctx, &value);
        field_data[0] = value * scale - scale / 2; // value * scale
        PetscRandomGetValueReal(rctx, &value);
        field_data[1] = value * scale - scale / 2;
      }
    }

    auto vector_times_scalar_field =
        [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
          MoFEMFunctionBeginHot;
          auto x_data = ent_ptr_x->getEntFieldData();
          double value;
          PetscRandomGetValueReal(rctx, &value);

          for (auto &v : x_data)
            v = (value - 0.5) * scale;

          MoFEMFunctionReturnHot(0);
        };

    CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(
        vector_times_scalar_field, valueField);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::getSnesInitial() {
  MoFEMFunctionBegin;

  // get Discrete Manager (SmartPetscObj)
  dM = simpleInterface->getDM();

  // Create nonlinear solver (SNES)
  snesSolver = createSNES(mField.get_comm());

  CHKERR SNESSetDM(snesSolver, dM);

  { // Set operators for nonlinear equations solver (SNES) from MoFEM Pipelines

    // Set operators for calculation of LHS and RHS of domain elements
    boost::shared_ptr<FaceEle> null_face;
    CHKERR DMMoFEMSNESSetJacobian(dM, simpleInterface->getDomainFEName(),
                                  feDomainLhsPtr, null_face, null_face);
    CHKERR DMMoFEMSNESSetFunction(dM, simpleInterface->getDomainFEName(),
                                  feDomainRhsPtr, null_face, null_face);

    // Set operators for calculation of LHS and RHS of boundary elements
    boost::shared_ptr<EdgeEle> null_edge;
    CHKERR DMMoFEMSNESSetJacobian(dM, simpleInterface->getBoundaryFEName(),
                                  feBoundaryLhsPtr, null_edge, null_edge);
    CHKERR DMMoFEMSNESSetFunction(dM, simpleInterface->getBoundaryFEName(),
                                  feBoundaryRhsPtr, null_edge, null_edge);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::solveSnesSystem() {
  MoFEMFunctionBegin;

  // Create RHS and solution vectors
  SmartPetscObj<Vec> global_rhs, global_solution;
  CHKERR DMCreateGlobalVector_MoFEM(dM, global_rhs);
  global_solution = smartVectorDuplicate(global_rhs);

  CHKERR DMoFEMMeshToLocalVector(dM, global_solution, INSERT_VALUES,
                                 SCATTER_FORWARD);

  // Assemble MPI vector
  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);
  CHKERR VecZeroEntries(commonDataPtr->petscVec);

  CHKERR SNESSetFromOptions(snesSolver);

  { // Setup fieldsplit (block) solver - optional: yes/no
    KSP ksp;
    CHKERR SNESGetKSP(snesSolver, &ksp);
    PC pc;
    CHKERR KSPGetPC(ksp, &pc);
    PetscBool is_pcfs = PETSC_FALSE;
    PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);

    // Set up FIELDSPLIT, only when user set -pc_type fieldsplit
    // Identify the index for boundary entities, remaining will be for domain
    // Then split the fields for boundary and domain for solving
    if (is_pcfs == PETSC_TRUE) {
      IS is_domain, is_boundary;
      cerr << "Running FIELDSPLIT..." << endl;
      const MoFEM::Problem *problem_ptr;
      CHKERR DMMoFEMGetProblemPtr(dM, &problem_ptr);
      CHKERR mField.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, valueField, 0, 1, &is_boundary,
          &boundaryEntitiesForFieldsplit);
      // CHKERR ISView(is_boundary, PETSC_VIEWER_STDOUT_SELF);

      CHKERR PCFieldSplitSetIS(pc, NULL, is_boundary);

      CHKERR ISDestroy(&is_boundary);
    }
  }

  CHKERR SNESSetUp(snesSolver);

  commonDataPtr->tolPoiDist = tol_poi_dist;
  std::cout << "tol_poi_dist: " << commonDataPtr->tolPoiDist << std::endl;

  // Postprocess only field values
  const Field_multiIndex *fields;
  mField.get_fields(&fields);

  // if there is "Q" in fields
  if (mField.check_field("Q"))
    CHKERR SNESSetConvergenceTest(snesSolver, MyConvergenceTest,
                                  (void *)(commonDataPtr.get()), NULL);

  // Solve the system
  CHKERR SNESSolve(snesSolver, global_rhs, global_solution);
  // VecView(global_rhs, PETSC_VIEWER_STDOUT_SELF);

  // Scatter result data on the mesh
  CHKERR DMoFEMMeshToLocalVector(dM, global_solution, INSERT_VALUES,
                                 SCATTER_REVERSE);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::MyConvergenceTest(SNES snes, PetscInt it,
                                                  PetscReal xnorm,
                                                  PetscReal gnorm, PetscReal f,
                                                  SNESConvergedReason *reason,
                                                  void *ctx) {

  MoFEMFunctionBegin;

  auto common_data_ptr = boost::static_pointer_cast<CommonDataSnesDD>(ctx);
  std::vector<double> &a = common_data_ptr->rmsPointDistErr;
  double tolerance = common_data_ptr->tolPoiDist;

  if (it < 2)
    MoFEMFunctionReturnHot(0);

  if (!a.size())
    MoFEMFunctionReturnHot(0);

  double diff = std::abs(a[a.size() - 1] - a[a.size() - 2]);

  if (diff < tolerance)
    *reason = SNES_CONVERGED_SNORM_RELATIVE;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionSnesDD::setSnesMonitors() {
  MoFEMFunctionBegin;

  // ctx for monitor containing the discrete manager
  auto snes_ctx_ptr = smartGetDMSnesCtx(simpleInterface->getDM());

  combinedPointers = boost::make_shared<CombinedPointers>();

  combinedPointers->commonDataPtr = commonDataPtr;
  combinedPointers->snesCtxPtr = snes_ctx_ptr;

  // define monitors which are executed at the begining of each iteration
  // postProc on integration points
  auto monitor_vtk_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                  void *combined_ctx) {
    MoFEMFunctionBegin;

    auto combined_ptr =
        boost::static_pointer_cast<CombinedPointers>(combined_ctx);

    auto common_data_ptr = combined_ptr->commonDataPtr;
    auto snes_ptr = combined_ptr->snesCtxPtr;

    auto &m_field = snes_ptr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();

    // scale fields
    CHKERR m_field.getInterface<FieldBlas>()->fieldScale(
        1. / common_data_ptr->scaleQ, "Q");

    boost::shared_ptr<PostProcFaceOnRefinedMesh> fePostProcPtr;
    fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
        new PostProcFaceOnRefinedMesh(m_field));

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    fePostProcPtr->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpSetInvJacH1ForFace(inv_jac_ptr));

    // Generate post-processing mesh
    CHKERR fePostProcPtr->generateReferenceElementMesh();
    // Postprocess only field values
    const Field_multiIndex *fields;
    m_field.get_fields(&fields);
    for (auto &it : *fields) {
      fePostProcPtr->addFieldValuesPostProc(it->getName());
    }
    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), fePostProcPtr);

    // write output
    CHKERR fePostProcPtr->writeFile(
        "out_iteration_" + boost::lexical_cast<std::string>(its) + ".h5m");

    // scale fields
    CHKERR m_field.getInterface<FieldBlas>()->fieldScale(
        common_data_ptr->scaleQ, "Q");

    MoFEMFunctionReturn(0);
  };

  auto monitor_print_moab = [](SNES snes, PetscInt its, PetscReal fgnorm,
                               void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    string gauss_name;
    std::ostringstream strma;
    strma << "out_moab_" << its << ".h5m";
    gauss_name = strma.str();
    CHKERR snes_ptr->mField.get_moab().write_file(gauss_name.c_str(), "MOAB",
                                                  "PARALLEL=WRITE_PART");

    MoFEMFunctionReturn(0);
  };

  auto monitor_errors_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                     void *combined_ctx) {
    MoFEMFunctionBegin;
    auto combined_ptr =
        boost::static_pointer_cast<CombinedPointers>(combined_ctx);

    auto common_data_ptr = combined_ptr->commonDataPtr;

    SearchData monitorSearchData;

    monitorSearchData.mValPtr = common_data_ptr->mValPtr;
    monitorSearchData.mGradPtr = common_data_ptr->mGradPtr;
    monitorSearchData.mFluxPtr = common_data_ptr->mFluxPtr;
    monitorSearchData.mValStarPtr = common_data_ptr->mValStarPtr;
    monitorSearchData.mGradStarPtr = common_data_ptr->mGradStarPtr;
    monitorSearchData.mFluxStarPtr = common_data_ptr->mFluxStarPtr;
    monitorSearchData.pointDistance = common_data_ptr->pointDistance;
    monitorSearchData.mKStarPtr = common_data_ptr->mKStarPtr;
    monitorSearchData.mKStarCorrelationPtr =
        common_data_ptr->mKStarCorrelationPtr;
    monitorSearchData.rtree4D = common_data_ptr->rtree4D;
    monitorSearchData.rtree5D = common_data_ptr->rtree5D;
    monitorSearchData.flgPointAverage = common_data_ptr->flgPointAverage;
    monitorSearchData.pointAverage = common_data_ptr->pointAverage;
    monitorSearchData.linear_k = common_data_ptr->linear_k;

    auto &m_field = combined_ptr->snesCtxPtr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();
    int patchIntegNum = 3;
    boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
        new PatchIntegFaceEle(m_field, patchIntegNum));

    auto post_proc_rule_hook = [](int, int, int p) -> int { return 2 * p; };
    PostProcGauss->getRuleHook = post_proc_rule_hook;

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpSetInvJacH1ForFace(inv_jac_ptr));

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("T", common_data_ptr->mValPtr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>("T", common_data_ptr->mGradPtr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<2>("Q", common_data_ptr->mFluxPtr));

    bool flgAnalytical = false;

    if (common_data_ptr->pReference) {
      PostProcGauss->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues("P_reference",
                                           common_data_ptr->mValRefPtr));
      PostProcGauss->getOpPtrVector().push_back(
          new OpPostProcRefErrorIntegPts("T", common_data_ptr));
    } else {

      PetscBool flg_ana_square_top = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_top",
                                 &flg_ana_square_top, PETSC_NULL);

      if (flg_ana_square_top) {
        flgAnalytical = true;
        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_squareTop));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mGradFuncPtr,
                                           analyticalFunctionGrad_squareTop));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          res[0] = -common_data_ptr->linear_k * M_PI * cos(M_PI * (x)) *
                   sinh(M_PI * (y)) / sinh(M_PI);
          res[1] = -common_data_ptr->linear_k * M_PI * sin(M_PI * (x)) *
                   cosh(M_PI * (y)) / sinh(M_PI);
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_square_sincos = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_sincos",
                                 &flg_ana_square_sincos, PETSC_NULL);

      if (flg_ana_square_sincos) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_squareSinCos));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(
                common_data_ptr->mGradFuncPtr,
                analyticalFunctionGrad_squareSinCos));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          double a = 2.;
          res[0] = -common_data_ptr->linear_k * a * M_PI * cos(a * M_PI * x) *
                   cos(a * M_PI * y);
          res[1] = common_data_ptr->linear_k * a * M_PI * sin(a * M_PI * x) *
                   sin(a * M_PI * y);
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_square_nonlinear_sincos = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_nonlinear_sincos",
                                 &flg_ana_square_nonlinear_sincos, PETSC_NULL);

      if (flg_ana_square_nonlinear_sincos) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_squareSinCos));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(
                common_data_ptr->mGradFuncPtr,
                analyticalFunctionGrad_squareSinCos));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          double a = 2.;
          double aa = 1.;
          double bb = 1.;
          double cc = 1.;

          VectorDouble res;
          res.resize(2);
          res[0] = -a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y) *
                   (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                             (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x)));
          res[1] =
              a * M_PI * sin(a * M_PI * x) *
              (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                        (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x))) *
              sin(a * M_PI * y);
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_mexi_hat = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_mexi_hat",
                                 &flg_ana_mexi_hat, PETSC_NULL);
      if (flg_ana_mexi_hat) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_mexiHat));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mGradFuncPtr,
                                           analyticalFunctionGrad_mexiHat));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          res[0] = -common_data_ptr->linear_k *
                   (-exp(-100. * (square(x) + square(y))) *
                    (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) *
                    cos(M_PI * y));
          res[1] = -common_data_ptr->linear_k *
                   (-exp(-100. * (square(x) + square(y))) *
                    (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) *
                    cos(M_PI * x));
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }

      PetscBool flg_ana_L_shape = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_L_shape",
                                 &flg_ana_L_shape, PETSC_NULL);
      if (flg_ana_L_shape) {
        flgAnalytical = true;

        PostProcGauss->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
            common_data_ptr->mValFuncPtr, analyticalFunction_LShape));

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mGradFuncPtr,
                                           analyticalFunctionGrad_LShape));

        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          VectorDouble res;
          res.resize(2);
          double numerator = 2 * common_data_ptr->linear_k *
                             (y * cos((M_PI + 2 * atan2(y, x)) / 3) -
                              x * sin((M_PI + 2 * atan2(y, x)) / 3));
          double denominator = 3 * pow(pow(x, 2) + pow(y, 2), 2.0 / 3.0);
          res[0] = numerator / denominator;
          numerator = -2 * common_data_ptr->linear_k *
                      (x * cos((M_PI + 2 * atan2(y, x)) / 3) +
                       y * sin((M_PI + 2 * atan2(y, x)) / 3));
          res[1] = numerator / denominator;
          return res;
        };

        PostProcGauss->getOpPtrVector().push_back(
            new OpGetTensor1fromFunc<2, 2>(common_data_ptr->mFluxFuncPtr,
                                           analyticalFunctionFlux));
      }
    }

    PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
        common_data_ptr->petscVec, CommonDataSnesDD::GAUSS_COUNT));
    PostProcGauss->getOpPtrVector().push_back(
        new OpGetArea(common_data_ptr->petscVec, CommonDataClassic::VOL_E));

    if (flgAnalytical) {

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
          common_data_ptr->mValPtr, common_data_ptr->petscVec,
          CommonDataClassic::VALUE_ERROR, common_data_ptr->mValFuncPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
          common_data_ptr->mGradPtr, common_data_ptr->petscVec,
          CommonDataClassic::GRAD_ERROR, common_data_ptr->mGradFuncPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
          common_data_ptr->mFluxPtr, common_data_ptr->petscVec,
          CommonDataClassic::FLUX_ERROR, common_data_ptr->mFluxFuncPtr));
    }

    moab::Core mb_post;

    if (common_data_ptr->rtree4D.size() > 1)
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindData4D<2>("T", monitorSearchData));
    else if (common_data_ptr->rtree5D.size() > 1) {
      PostProcGauss->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues("T", common_data_ptr->mValPtr));
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindData5D<2>("T", monitorSearchData,
                                              common_data_ptr->scaleV));
    } else
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindClosest<2>("T", monitorSearchData));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpFindKstar<2>("T", monitorSearchData,
                                           common_data_ptr->scaleV));

    PostProcGauss->getOpPtrVector().push_back(
        new OpPointError("T", common_data_ptr));

    if (common_data_ptr->flgPrintInteg)
      PostProcGauss->getOpPtrVector().push_back(
          new OpPostProcPrintingIntegPts<2>("Q", common_data_ptr, mb_post,
                                            analyticalFunction));

    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), PostProcGauss);

    if (common_data_ptr->flgPrintInteg) {
      string out_file_name;
      std::ostringstream strm;
      strm << "out_integ_pts_" << its << ".h5m";
      out_file_name = strm.str();
      CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                "PARALLEL=WRITE_PART");
    }

    mb_post.delete_mesh();

    MoFEMFunctionReturn(0);
  };

  // collect data across all integration points for the global vector
  auto monitor_combine_global_vector = [](SNES, PetscInt its, PetscReal,
                                          void *ctx) {
    MoFEMFunctionBegin;

    if (std::abs(its) < 1)
      MoFEMFunctionReturnHot(0);

    auto common_data_ptr = boost::static_pointer_cast<CommonDataSnesDD>(ctx);

    CHKERR VecAssemblyBegin(common_data_ptr->petscVec);
    CHKERR VecAssemblyEnd(common_data_ptr->petscVec);

    const double *array;
    CHKERR VecGetArrayRead(common_data_ptr->petscVec, &array);

    double volume = array[CommonDataSnesDD::VOL_E]; // TODO: fix this value
    common_data_ptr->vOlume = array[CommonDataSnesDD::VOL_E];

    common_data_ptr->rmsPointDistErr.push_back(
        sqrt((array[CommonDataSnesDD::POINT_ERROR]) / volume));

    common_data_ptr->rmsErr.push_back(
        sqrt((array[CommonDataSnesDD::VALUE_ERROR]) / volume));

    common_data_ptr->gradErr.push_back(
        sqrt((array[CommonDataSnesDD::GRAD_ERROR]) / volume));

    common_data_ptr->fluxErr.push_back(
        sqrt((array[CommonDataSnesDD::FLUX_ERROR]) / volume));

    std::cout << "rmsErr = " << common_data_ptr->rmsErr.back() << std::endl;

    std::cout << "J = " << array[CommonDataSnesDD::ACCUM_POINT_ERROR]
              << std::endl;
    std::cout << "# of gauss points: " << array[CommonDataSnesDD::GAUSS_COUNT]
              << std::endl;
    common_data_ptr->numberIntegrationPoints.push_back(
        array[CommonDataSnesDD::GAUSS_COUNT]);

    std::ofstream error_file;
    error_file.open("errors_long_file.csv", ios::app);
    // populate the file with points
    error_file << common_data_ptr->rmsErr.back() << ","
               << common_data_ptr->gradErr.back() << ","
               << common_data_ptr->fluxErr.back() << ","
               << common_data_ptr->rmsPointDistErr.back() << "\n";
    // close writing into file
    error_file.close();

    CHKERR VecRestoreArrayRead(common_data_ptr->petscVec, &array);

    CHKERR VecZeroEntries(common_data_ptr->petscVec);

    MoFEMFunctionReturn(0);
  };

  if (print_moab == PETSC_FALSE)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))MoFEMSNESMonitorFields,
                          (void *)(snes_ctx_ptr.get()), nullptr);

  if (!skip_vtk)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_vtk_post_proc,
                          (void *)(combinedPointers.get()), nullptr);

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_print_moab,
                          (void *)(snes_ctx_ptr.get()), nullptr);
  }

  if (!skip_vtk)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_errors_post_proc,
                          (void *)(combinedPointers.get()), nullptr);

  if (!skip_vtk)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal, void *))
                              monitor_combine_global_vector,
                          (void *)(commonDataPtr.get()), nullptr);

  MoFEMFunctionReturn(0);
}

//! [Check error]
MoFEMErrorCode DiffusionSnesDD::checkErrorDD(int iter_num, bool print,
                                             std::string vtk_name) {
  MoFEMFunctionBegin;

  boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
      new PatchIntegFaceEle(mField, patchIntegNum));
  PostProcGauss->getRuleHook =
      feDomainRhsPtr->getRuleHook; // use the same integration rule as domainRhs

  CHKERR calculateJacobian(PostProcGauss);

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>(valueField,
                                            commonDataPtr->mGradPtr));
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateVectorFieldValues<2>(fluxField, commonDataPtr->mFluxPtr));

  // data-related errors
  {
    if (!flg_use_line) {
      switch (datasetDim) {
      case 4:
        PostProcGauss->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData4D<2>(valueField, searchData));
        break;
      case 5:
        PostProcGauss->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData5D<2>(valueField, searchData,
                                                commonDataPtr->scaleV));
        break;
      }
    }
    if (flg_use_line)
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindClosest<2>(valueField, searchData));

    PostProcGauss->getOpPtrVector().push_back(new OperatorsForDD::OpScalarTags(
        commonDataPtr->pointDistance, "DD_DISTANCE", mField));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpScalarVarTags(commonDataPtr->pointDistance,
                                            "DD_DISTANCE_AVE",
                                            "DD_DISTANCE_VAR", mField));

    if (commonDataPtr->rtree5D.size())
      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
          commonDataPtr->mValPtr, commonDataPtr->petscVec,
          CommonDataClassic::DISTANCE_T, commonDataPtr->mValStarPtr));

    // PostProcGauss->getOpPtrVector().push_back(new
    // OperatorsForDD::OpScalarTags(
    //     commonDataPtr->mValStarPtr, "DISTANCE_T", mField));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
        commonDataPtr->mGradPtr, commonDataPtr->petscVec,
        CommonDataClassic::DISTANCE_G, commonDataPtr->mGradStarPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpVectorNormTags<2>(commonDataPtr->mGradStarPtr,
                                                "DISTANCE_G", mField));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
        commonDataPtr->mFluxPtr, commonDataPtr->petscVec,
        CommonDataClassic::DISTANCE_F, commonDataPtr->mFluxStarPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpVectorNormTags<2>(commonDataPtr->mFluxStarPtr,
                                                "DISTANCE_F", mField));
  }

  // PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
  //     commonDataPtr->petscVec, CommonDataClassic::GAUSS_COUNT));
  PostProcGauss->getOpPtrVector().push_back(
      new OpGetArea(commonDataPtr->petscVec, CommonDataClassic::VOL_E));

  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);

  // CHKERR VecAssemblyBegin(commonDataPtr->petscRefineErrorVec);
  // CHKERR VecAssemblyEnd(commonDataPtr->petscRefineErrorVec);

  CHKERR VecZeroEntries(commonDataPtr->petscVec);
  // CHKERR VecZeroEntries(commonDataPtr->petscRefineErrorVec);

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

  // CHKERR getJumps(iter_num);

  std::vector<Tag> tag_handles_calculated;
  enum tag_handles_calculated_enum {
    DD_DISTANCE_AVE = 0,
    DD_DISTANCE_VAR,
    // DISTANCE_T,
    DISTANCE_G,
    DISTANCE_F,
    LAST_ELEMENT
  };

  tag_handles_calculated.resize(LAST_ELEMENT);
  CHKERR getTagHandle(mField, "DISTANCE_G", MB_TYPE_DOUBLE,
                      tag_handles_calculated[DISTANCE_G]);
  CHKERR getTagHandle(mField, "DISTANCE_F", MB_TYPE_DOUBLE,
                      tag_handles_calculated[DISTANCE_F]);

  CHKERR getTagHandle(mField, "DD_DISTANCE_AVE", MB_TYPE_DOUBLE,
                      tag_handles_calculated[DD_DISTANCE_AVE]);
  CHKERR getTagHandle(mField, "DD_DISTANCE_VAR", MB_TYPE_DOUBLE,
                      tag_handles_calculated[DD_DISTANCE_VAR]); // variance

  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);

  const double *array_classical;
  CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array_classical);

  if (mField.get_comm_rank() == 0) {
    if (commonDataPtr->rtree5D.size())
      MOFEM_LOG("WORLD", Sev::inform)
          << "Global Temperature distance from the dataset: "
          << std::sqrt(array_classical[CommonDataClassic::DISTANCE_T]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global Gradient distance from the dataset: "
        << std::sqrt(array_classical[CommonDataClassic::DISTANCE_G]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global Flux distance from the dataset: "
        << std::sqrt(array_classical[CommonDataClassic::DISTANCE_F]);
  }

  // CHKERR VecRestoreArrayRead(commonDataPtr->petscRefineErrorVec, &array);
  CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array_classical);

  if (print) {
    ParallelComm *pcomm =
        ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
    if (pcomm == NULL)
      SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
              "Communicator not set");

    auto bit_level1 = BitRefLevel().set(0);
    auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
    CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
        bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);
    tag_handles_calculated.push_back(pcomm->part_tag());
    std::ostringstream strm;
    strm << vtk_name << iter_num << ".h5m";
    CHKERR mField.get_moab().write_file(
        strm.str().c_str(), "MOAB", "PARALLEL=WRITE_PART",
        out_meshset_tri_ptr->get_ptr(), 1, tag_handles_calculated.data(),
        tag_handles_calculated.size());
  }

  MoFEMFunctionReturn(0);
}
//! [Check error]

MoFEMErrorCode DiffusionSnesDD::runMonteCarlo() {
  MoFEMFunctionBegin;

  if (!flg_monte_carlo)
    MoFEMFunctionReturnHot(0);

  for (int i = 0; i < commonDataPtr->numberMonteCarlo; i++) {

    { // restart max_sigmas
      // max sigma for for integ point
      commonDataPtr->integ_sigma_max_p = 0;
      commonDataPtr->integ_sigma_max_grad_x = 0;
      commonDataPtr->integ_sigma_max_grad_y = 0;
      commonDataPtr->integ_sigma_max_flux_x = 0;
      commonDataPtr->integ_sigma_max_flux_y = 0;
    }

    CHKERR outputHVtkFields(commonDataPtr->monte_carlo_counter,
                            "out_before_perturb_");

    // Perturb fields relevant to material data search
    {
      std::cout << "rmsPointDistErr used for perturbation = "
                << commonDataPtr->rmsPointDistErr.back() << std::endl;

      // random
      PetscRandom rctx;
      PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
      PetscRandomSetSeed(rctx, (unsigned long)std::time(0));
      PetscRandomSeed(rctx);

      // make scale an input
      double scale = 10;
      CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-rand_ini_scale", &scale,
                                 PETSC_NULL);

      if (scale < 10e-4)
        scale = commonDataPtr->rmsPointDistErr.back();

      // set random Q field (L2)
      for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, fluxField, it)) {
        if (it->get()->getEntType() == MBTRI || MBQUAD) {
          auto field_data = (*it)->getEntFieldData();
          double value;
          // double scale = 1.0;
          PetscRandomGetValueReal(rctx, &value);
          field_data[0] += (value - 0.5) * scale * 5; // value * scale
          PetscRandomGetValueReal(rctx, &value);
          field_data[1] += (value - 0.5) * scale * 5;
        }
      }

      auto vector_times_scalar_field =
          [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
            MoFEMFunctionBeginHot;
            auto x_data = ent_ptr_x->getEntFieldData();
            double value;
            PetscRandomGetValueReal(rctx, &value);

            for (auto &v : x_data) {
              PetscRandomGetValueReal(rctx, &value);
              v += (value - 0.5) * scale;
            }

            MoFEMFunctionReturnHot(0);
          };

      CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(
          vector_times_scalar_field, valueField);
    }

    CHKERR outputHVtkFields(commonDataPtr->monte_carlo_counter,
                            "out_after_perturb_");

    CHKERR solveSnesSystem();

    CHKERR monteSaveHResults();

    // if (flg_skip_all_vtk == false) {
    CHKERR outputHVtkFields(commonDataPtr->monte_carlo_counter,
                            "out_converged_after_monte_");
    // }

    // if (flg_skip_all_vtk == false) {
    std::vector<Tag> tag_handles;
    tag_handles.resize(5);
    CHKERR getTagHandle(mField, "SIGMA_T", MB_TYPE_DOUBLE, tag_handles[0]);
    CHKERR getTagHandle(mField, "SIGMA_GRAD_X", MB_TYPE_DOUBLE, tag_handles[1]);
    CHKERR getTagHandle(mField, "SIGMA_GRAD_Y", MB_TYPE_DOUBLE, tag_handles[2]);
    CHKERR getTagHandle(mField, "SIGMA_FLUX_X", MB_TYPE_DOUBLE, tag_handles[3]);
    CHKERR getTagHandle(mField, "SIGMA_FLUX_Y", MB_TYPE_DOUBLE, tag_handles[4]);

    ParallelComm *pcomm =
        ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
    if (pcomm == NULL)
      SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
              "Communicator not set");

    // auto bit_level1 = BitRefLevel().set(meshRefinementCounter);
    auto bit_level1 = BitRefLevel().set(0);
    auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
    CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
        bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);
    tag_handles.push_back(pcomm->part_tag());
    std::ostringstream strm;
    strm << "out_sigma_ele_" << commonDataPtr->monte_carlo_counter << ".h5m";
    CHKERR mField.get_moab().write_file(strm.str().c_str(), "MOAB",
                                        "PARALLEL=WRITE_PART",
                                        out_meshset_tri_ptr->get_ptr(), 1,
                                        tag_handles.data(), tag_handles.size());
    // }

    commonDataPtr->monte_carlo_counter++;

    if (mField.get_comm_rank() == 0 && flg_long_error_file) {
      // open the error file with appending to the file
      std::ofstream error_file;
      error_file.open("standard_deviation_max.csv", ios::app);
      // populate the file with points
      error_file << commonDataPtr->integ_sigma_max_p << ", "
                 << commonDataPtr->integ_sigma_max_grad_x << ", "
                 << commonDataPtr->integ_sigma_max_grad_y << ", "
                 << commonDataPtr->integ_sigma_max_flux_x << ", "
                 << commonDataPtr->integ_sigma_max_flux_y << "\n";
      // close writing into file
      error_file.close();
    }
  }

  // add to the file capturing the end of the analysis
  // "sumanalys_monte_sigma.csv"
  if (mField.get_comm_rank() == 0) {
    std::ofstream sumanalysis;
    sumanalysis.open("sumanalys_monte_sigma.csv",
                     std::ofstream::out | std::ofstream::app);

    sumanalysis << commonDataPtr->integ_sigma_max_p << ", "
                << commonDataPtr->integ_sigma_max_grad_x << ", "
                << commonDataPtr->integ_sigma_max_grad_y << ", "
                << commonDataPtr->integ_sigma_max_flux_x << ", "
                << commonDataPtr->integ_sigma_max_flux_y << "\n";

    sumanalysis.close();
  }

  MoFEMFunctionReturn(0);
}
#include <ClassicDiffusionProblem.hpp>

using namespace MoFEM;

using OpDomainGradGrad = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpGradGrad<1, 1, 2>;

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpBoundaryMass = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<1, 1>;

using OpBoundarySourceRhs = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 1, 1>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 1, 1>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

ClassicDiffusionProblem::ClassicDiffusionProblem(MoFEM::Interface &m_field)
    : valueField("P_reference"), mField(m_field), mpiComm(mField.get_comm()),
      mpiRank(mField.get_comm_rank()) {
  feDomainLhsPtr = boost::shared_ptr<FaceEle>(new FaceEle(mField));
  feDomainRhsPtr = boost::shared_ptr<PatchIntegFaceEle>(
      new PatchIntegFaceEle(mField, patchIntegNum));
  feBoundaryLhsPtr = boost::shared_ptr<EdgeEle>(new EdgeEle(mField));
  feBoundaryRhsPtr = boost::shared_ptr<EdgeEle>(new EdgeEle(mField));
}

MoFEMErrorCode ClassicDiffusionProblem::runProblem() {
  MoFEMFunctionBegin;

  // Set values from param file
  CHKERR setParamValues();
  CHKERR readMesh();
  CHKERR setupProblem();
  CHKERR setIntegrationRules();
  CHKERR createClassicCommonData();
  CHKERR boundaryCondition();
  CHKERR markPressureBc();
  CHKERR assembleSystem();
  CHKERR getKSPinitial();
  CHKERR solveSystem();
  CHKERR outputHVtkFields();
  CHKERR postProcessErrors();
  CHKERR checkResults();

  // Switch off fields
  auto off_field = [&](auto fe_name, auto field_name) {
    CHKERR mField.modify_finite_element_off_field_row(fe_name, field_name);
    CHKERR mField.modify_finite_element_off_field_col(fe_name, field_name);
  };

  off_field(simpleInterface->getDomainFEName(), "P_reference");
  off_field(simpleInterface->getBoundaryFEName(), "P_reference");

  // produce .h5m file to be compared to poisson_dd_H data driven results
  mField.get_moab().write_file("analysis_mesh.h5m", "MOAB",
                               "PARALLEL=WRITE_PART");

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::setParamValues() {
  MoFEMFunctionBegin;

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "DD Config", "none");
  // Set values from param file
  CHKERR PetscOptionsInt("-my_order",
                         "approximation order of spatial positions", "", 1,
                         &oRder, PETSC_NULL);
  CHKERR PetscOptionsReal("-my_dummy_k", "k for dummy rtree", "", 3, &dummy_k,
                          PETSC_NULL);

  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-patch_integ", &flgPatchInteg,
                             PETSC_NULL);
  CHKERR PetscOptionsInt(
      "-patch_integ_num",
      "number of integratioon points used for patch integration", "", 3,
      &patchIntegNum, PETSC_NULL);

  // mumps settings (leave be)
  CHKERR PetscOptionsInsertString(
      NULL, "-mat_mumps_icntl_14 1200 -mat_mumps_icntl_24 1 "
            "-mat_mumps_icntl_13 1 -mat_mumps_icntl_20 0");

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "-my_order " << oRder;
  MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_k " << dummy_k;

  if (flgPatchInteg) {
    MOFEM_LOG("WORLD", Sev::inform) << "-patch_integ used";
    MOFEM_LOG("WORLD", Sev::inform) << "-patch_integ_num " << patchIntegNum;
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::readMesh() {
  MoFEMFunctionBegin;

  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  simpleInterface->getAddSkeletonFE() = true;
  CHKERR simpleInterface->loadFile();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::setupProblem() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField(valueField, H1,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(valueField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->setFieldOrder(valueField, oRder);

  CHKERR simpleInterface->setUp();

  // create common data pointers
  commonDataPtr = boost::make_shared<CommonDataClassic>();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::setIntegrationRules() {
  MoFEMFunctionBegin;

  auto domain_rule_lhs = [](int, int, int p) -> int { return 2 * p; };
  auto domain_rule_rhs = [](int, int, int p) -> int { return 2 * p; };
  feDomainLhsPtr->getRuleHook = domain_rule_lhs;
  feDomainRhsPtr->getRuleHook = domain_rule_rhs;

  if (flgPatchInteg) {
    auto domain_rule_rhs = [](int, int, int p) -> int { return -1; };
    feDomainRhsPtr->getRuleHook = domain_rule_rhs;
  }

  auto boundary_rule_lhs = [](int, int, int p) -> int { return 2 * p; };
  auto boundary_rule_rhs = [](int, int, int p) -> int { return 2 * p; };
  feBoundaryLhsPtr->getRuleHook = boundary_rule_lhs;
  feBoundaryRhsPtr->getRuleHook = boundary_rule_rhs;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::createClassicCommonData() {
  MoFEMFunctionBegin;

  // initialise main field pointers
  commonDataPtr->mValPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mValGradPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mGradPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mFluxPtr = boost::make_shared<MatrixDouble>();

  // initialise pointers for values from analytical functions
  commonDataPtr->mValFuncPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mGradFuncPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mFluxFuncPtr = boost::make_shared<MatrixDouble>();

  int local_size;
  if (mField.get_comm_rank() == 0) // get_comm_rank() gets processor number
    // processor 0
    local_size =
        CommonDataClassic::LAST_ELEMENT; // last element gives size of vector
  else
    // other processors (e.g. 1, 2, 3, etc.)
    local_size = 0; // local size of vector is zero on other processors

  commonDataPtr->petscVec = createSmartVectorMPI(
      mField.get_comm(), local_size, CommonDataClassic::LAST_ELEMENT);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::boundaryCondition() {
  MoFEMFunctionBegin;

  // Get boundary edges marked in block named "BOUNDARY_CONDITION"
  auto essential_bc = [&]() {
    Range boundary_entities;
    auto bc_mng = mField.getInterface<BcManager>();
    CHKERR bc_mng->pushMarkDOFsOnEntities(simpleInterface->getProblemName(),
                                          "PRESSURE", valueField, 0, 0);

    auto &bc_map = bc_mng->getBcMapByBlockName();
    // boundaryMarker = boost::make_shared<std::vector<char unsigned>>();
    for (auto b : bc_map) {
      if (std::regex_match(b.first, std::regex("(.*)PRESSURE(.*)"))) {
        boundary_entities.merge(*(b.second->getBcEntsPtr()));
      }
    }
    // Add vertices to boundary entities
    Range boundary_vertices;
    CHKERR mField.get_moab().get_connectivity(boundary_entities,
                                              boundary_vertices, true);
    boundary_entities.merge(boundary_vertices);

    return boundary_entities;
  };

  // Get boundary edges marked in block named "BOUNDARY_CONDITION"
  auto get_q_bar_on_mesh = [&]() {
    Range boundary_entities;
    auto bc_mng = mField.getInterface<BcManager>();
    CHKERR bc_mng->pushMarkDOFsOnEntities(simpleInterface->getProblemName(),
                                          "FLUX", valueField, 0, 0);

    auto &bc_map = bc_mng->getBcMapByBlockName();
    for (auto b : bc_map) {
      if (std::regex_match(b.first, std::regex("(.*)FLUX(.*)"))) {
        boundary_entities.merge(*(b.second->getBcEntsPtr()));
      }
    }
    // Add vertices to boundary entities
    Range boundary_vertices;
    CHKERR mField.get_moab().get_connectivity(boundary_entities,
                                              boundary_vertices, true);
    boundary_entities.merge(boundary_vertices);

    return boundary_entities;
  };

  fluxBcRange = get_q_bar_on_mesh();
  pressureBcRange = essential_bc();

  Range common_boundary = intersect(fluxBcRange, pressureBcRange);
  fluxBcRange = subtract(fluxBcRange, common_boundary);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::markPressureBc() {
  MoFEMFunctionBegin;

  auto mark_boundary_dofs = [&](Range &skin_edges) {
    auto problem_manager = mField.getInterface<ProblemsManager>();
    auto marker_ptr = boost::make_shared<std::vector<unsigned char>>();
    problem_manager->markDofs(simpleInterface->getProblemName(), ROW,
                              skin_edges, *marker_ptr);
    return marker_ptr;
  };

  boundaryMarker = mark_boundary_dofs(pressureBcRange);

  // Store entities for fieldsplit (block) solver
  boundaryEntitiesForFieldsplit = pressureBcRange;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
ClassicDiffusionProblem::calculateJacobian(boost::shared_ptr<FaceEle> fe_ptr) {
  MoFEMFunctionBegin;

  auto det_ptr = boost::make_shared<VectorDouble>();
  auto jac_ptr = boost::make_shared<MatrixDouble>();
  auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

  fe_ptr->getOpPtrVector().push_back(new OpCalculateHOJacForFace(jac_ptr));
  fe_ptr->getOpPtrVector().push_back(
      new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpSetInvJacH1ForFace(inv_jac_ptr));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::assembleSystem() {
  MoFEMFunctionBegin;

  CHKERR calculateJacobian(feDomainLhsPtr);
  CHKERR calculateJacobian(feDomainRhsPtr);

  auto kFunction = [&](const double, const double, const double) {
    return -dummy_k;
  };
  auto beta = [](const double, const double, const double) { return 1.0; };

  { // Push operators to the Pipeline that is responsible for calculating LHS of
    // domain elements

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpDomainGradGrad(valueField, valueField, kFunction));
    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // source added to the domain Rhs
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    // Body forces
    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, valueField, -dummy_k,
        Sev::inform);
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // Push operators to the Pipeline that is responsible for calculating LHS of
    // boundary elements
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpBoundaryMass(valueField, valueField, beta,
                           boost::make_shared<Range>(pressureBcRange)));
    feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // Push operators to the Pipeline that is responsible for calculating RHS of
    // boundary elements
    // (u,u_bar)
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, 1.0,
        Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

    // (u, q_bar)
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, -1.0,
        Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::getKSPinitial() {
  MoFEMFunctionBegin;

  // get Discrete Manager (SmartPetscObj)
  dM = simpleInterface->getDM();

  { // Set operators for linear equations solver (KSP) from MoFEM Pipelines

    // Set operators for calculation of LHS and RHS of domain elements
    boost::shared_ptr<FaceEle> null_face;
    CHKERR DMMoFEMKSPSetComputeOperators(dM, simpleInterface->getDomainFEName(),
                                         feDomainLhsPtr, null_face, null_face);
    CHKERR DMMoFEMKSPSetComputeRHS(dM, simpleInterface->getDomainFEName(),
                                   feDomainRhsPtr, null_face, null_face);

    // Set operators for calculation of LHS and RHS of domain elements
    boost::shared_ptr<EdgeEle> null_edge;
    CHKERR DMMoFEMKSPSetComputeOperators(
        dM, simpleInterface->getBoundaryFEName(), feBoundaryLhsPtr, null_edge,
        null_edge);
    CHKERR DMMoFEMKSPSetComputeRHS(dM, simpleInterface->getBoundaryFEName(),
                                   feBoundaryRhsPtr, null_edge, null_edge);
  }
  // Setup KSP solver
  kspSolver = createKSP(mField.get_comm());
  CHKERR KSPSetFromOptions(kspSolver);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::solveSystem() {
  MoFEMFunctionBegin;

  // Create RHS and solution vectors
  SmartPetscObj<Vec> global_rhs, global_solution;
  CHKERR DMCreateGlobalVector_MoFEM(dM, global_rhs);
  global_solution = smartVectorDuplicate(global_rhs);

  CHKERR KSPSetDM(kspSolver, dM);

  { // Setup fieldsplit (block) solver - optional: yes/no
    PC pc;
    CHKERR KSPGetPC(kspSolver, &pc);
    PetscBool is_pcfs = PETSC_FALSE;
    PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);

    // Set up FIELDSPLIT, only when user set -pc_type fieldsplit
    // Identify the index for boundary entities, remaining will be for domain
    // Then split the fields for boundary and domain for solving
    if (is_pcfs == PETSC_TRUE) {
      IS is_domain, is_boundary;
      cerr << "Running FIELDSPLIT..." << endl;
      const MoFEM::Problem *problem_ptr;
      CHKERR DMMoFEMGetProblemPtr(dM, &problem_ptr);
      CHKERR mField.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, valueField, 0, 1, &is_boundary,
          &boundaryEntitiesForFieldsplit);
      // CHKERR ISView(is_boundary, PETSC_VIEWER_STDOUT_SELF);

      CHKERR PCFieldSplitSetIS(pc, NULL, is_boundary);

      CHKERR ISDestroy(&is_boundary);
    }
  }

  CHKERR KSPSetUp(kspSolver);

  // Solve the system
  CHKERR KSPSolve(kspSolver, global_rhs, global_solution);
  // VecView(global_rhs, PETSC_VIEWER_STDOUT_SELF);

  // Scatter result data on the mesh
  CHKERR DMoFEMMeshToGlobalVector(dM, global_solution, INSERT_VALUES,
                                  SCATTER_REVERSE);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::outputHVtkFields() {
  MoFEMFunctionBegin;

  fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
      new PostProcFaceOnRefinedMesh(mField));

  CHKERR calculateJacobian(fePostProcPtr);

  // Generate post-processing mesh
  CHKERR fePostProcPtr->generateReferenceElementMesh();
  // Postprocess only field values
  fePostProcPtr->addFieldValuesPostProc(valueField);

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), fePostProcPtr);

  // write output
  CHKERR fePostProcPtr->writeFile("out_ori_result.h5m");

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::postGetAnalyticalValues(
    boost::shared_ptr<FaceEle> fe_ptr) {
  MoFEMFunctionBegin;

  PetscBool flg_ana_square_top = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_top",
                             &flg_ana_square_top, PETSC_NULL);

  if (flg_ana_square_top) {
    flgAnalytical = true;
    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_squareTop));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_squareTop));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);
      res[0] =
          -dummy_k * M_PI * cos(M_PI * (x)) * sinh(M_PI * (y)) / sinh(M_PI);
      res[1] =
          -dummy_k * M_PI * sin(M_PI * (x)) * cosh(M_PI * (y)) / sinh(M_PI);
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_square_sincos = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_sincos",
                             &flg_ana_square_sincos, PETSC_NULL);

  if (flg_ana_square_sincos) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_squareSinCos));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_squareSinCos));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);
      double a = 2.;
      res[0] = -dummy_k * a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y);
      res[1] = dummy_k * a * M_PI * sin(a * M_PI * x) * sin(a * M_PI * y);
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_square_nonlinear_sincos = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_nonlinear_sincos",
                             &flg_ana_square_nonlinear_sincos, PETSC_NULL);

  if (flg_ana_square_nonlinear_sincos) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_squareSinCos));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_squareSinCos));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      double a = 2.;
      double aa = 1.;
      double bb = 1.;
      double cc = 1.;

      VectorDouble res;
      res.resize(2);
      res[0] = -a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y) *
               (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                         (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x)));
      res[1] = a * M_PI * sin(a * M_PI * x) *
               (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                         (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x))) *
               sin(a * M_PI * y);
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_mexi_hat = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_mexi_hat", &flg_ana_mexi_hat,
                             PETSC_NULL);
  if (flg_ana_mexi_hat) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_mexiHat));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_mexiHat));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);
      res[0] = -dummy_k * (-exp(-100. * (square(x) + square(y))) *
                           (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) *
                           cos(M_PI * y));
      res[1] = -dummy_k * (-exp(-100. * (square(x) + square(y))) *
                           (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) *
                           cos(M_PI * x));
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_L_shape = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_L_shape", &flg_ana_L_shape,
                             PETSC_NULL);
  if (flg_ana_L_shape) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_LShape));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_LShape));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);
      double numerator = 2. * dummy_k *
                         (y * cos((M_PI + 2. * atan2(y, x)) / 3.) -
                          x * sin((M_PI + 2. * atan2(y, x)) / 3.));
      double denominator = 3. * pow(pow(x, 2) + pow(y, 2), 2.0 / 3.0);
      res[0] = numerator / denominator;
      numerator = -2. * dummy_k *
                  (x * cos((M_PI + 2. * atan2(y, x)) / 3.) +
                   y * sin((M_PI + 2. * atan2(y, x)) / 3.));
      res[1] = numerator / denominator;
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::postProcessErrors() {
  MoFEMFunctionBegin;
  // post processing on integration points
  {

    boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
        new PatchIntegFaceEle(mField, patchIntegNum));
    PostProcGauss->getRuleHook =
        feDomainRhsPtr
            ->getRuleHook; // use the same integration rule as domainRhs

    CHKERR calculateJacobian(PostProcGauss);

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField,
                                              commonDataPtr->mGradPtr));

    CHKERR postGetAnalyticalValues(PostProcGauss);

    if (flgAnalytical) {

      auto kFunction = [&](const double, const double, const double) {
        return dummy_k;
      };

      PostProcGauss->getOpPtrVector().push_back(new OpGetFluxFromGrad<2>(
          commonDataPtr->mGradPtr, kFunction, commonDataPtr->mFluxPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
          commonDataPtr->mValPtr, commonDataPtr->petscVec,
          CommonDataClassic::VALUE_ERROR, commonDataPtr->mValFuncPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
          commonDataPtr->mGradPtr, commonDataPtr->petscVec,
          CommonDataClassic::GRAD_ERROR, commonDataPtr->mGradFuncPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
          commonDataPtr->mFluxPtr, commonDataPtr->petscVec,
          CommonDataClassic::FLUX_ERROR, commonDataPtr->mFluxFuncPtr));
    }

    PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
        commonDataPtr->petscVec, CommonDataClassic::GAUSS_COUNT));
    PostProcGauss->getOpPtrVector().push_back(
        new OpGetArea(commonDataPtr->petscVec, CommonDataClassic::VOL_E));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);
  }

  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);

  const double *array;
  CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array);

  if (mField.get_comm_rank() == 0) {

    commonDataPtr->vOlume = array[CommonDataClassic::VOL_E];

    commonDataPtr->rmsErr.push_back(
        sqrt((array[CommonDataClassic::VALUE_ERROR]) /
             array[CommonDataClassic::VOL_E]));
    commonDataPtr->gradErr.push_back(
        sqrt((array[CommonDataClassic::GRAD_ERROR]) /
             array[CommonDataClassic::VOL_E]));
    commonDataPtr->fluxErr.push_back(
        sqrt((array[CommonDataClassic::FLUX_ERROR]) /
             array[CommonDataClassic::VOL_E]));
    commonDataPtr->numberIntegrationPoints.push_back(
        array[CommonDataClassic::GAUSS_COUNT]);

    // open the error file with appending to the file
    std::ofstream error_file;
    error_file.open("FEM_errors.csv", ios::app);
    // populate the file with points
    error_file << oRder << ", " << commonDataPtr->numberIntegrationPoints.back()
               << ", " << commonDataPtr->rmsErr.size() << ", "
               << commonDataPtr->vOlume << ", " << "-1" << ", " << "-1" << ", "
               << "-1" << ", " << commonDataPtr->rmsErr.back() << ", "
               << commonDataPtr->gradErr.back() << ", "
               << commonDataPtr->fluxErr.back() << ", " << -1.0 << "\n";

    DDLogTag;
    MOFEM_LOG("WORLD", Sev::inform)
        << "order, gaussnum, iterations, volume, datanum, rmsPoiErr, "
           "errorIndicatorGrad, L2norm, H1seminorm, fluxErr, "
           "orderRefinementCounter";

    MOFEM_LOG("WORLD", Sev::inform)
        << oRder << ", " << commonDataPtr->numberIntegrationPoints.back()
        << ", " << commonDataPtr->rmsErr.size() << ", " << commonDataPtr->vOlume
        << ", -1, -1, -1, " << commonDataPtr->rmsErr.back() << ", "
        << commonDataPtr->gradErr.back() << ", "
        << commonDataPtr->fluxErr.back() << ", " << -1.0;

    MOFEM_LOG("WORLD", Sev::inform) << "order: " << oRder;
    MOFEM_LOG("WORLD", Sev::inform)
        << "gaussnum: " << commonDataPtr->numberIntegrationPoints.back();
    MOFEM_LOG("WORLD", Sev::inform)
        << "iterations: " << commonDataPtr->rmsErr.size();
    MOFEM_LOG("WORLD", Sev::inform) << "volume: " << commonDataPtr->vOlume;
    MOFEM_LOG("WORLD", Sev::inform)
        << "L2norm: " << commonDataPtr->rmsErr.back();
    MOFEM_LOG("WORLD", Sev::inform)
        << "H1seminorm: " << commonDataPtr->gradErr.back();
    MOFEM_LOG("WORLD", Sev::inform)
        << "fluxErr: " << commonDataPtr->fluxErr.back();
    // close writing into file
    error_file.close();
  }

  CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array);

  MoFEMFunctionReturn(0);
}

//! [Test example]
MoFEMErrorCode ClassicDiffusionProblem::checkResults() {
  MoFEMFunctionBegin;
  const double *array;
  CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array);

  PetscBool test = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-test", &test, PETSC_NULL);
  if (mField.get_comm_rank() == 0 && test) {
    constexpr double tolerance_value = 1e-2;
    constexpr double tolerance_flux = 1e-1;
    constexpr double expected_volume = 1.;

    // if (std::abs(commonDataPtr->vOlume - expected_volume) > tolerance) {
    //   std::cout << "Volume: " << commonDataPtr->vOlume << std::endl;
    //   SETERRQ(PETSC_COMM_SELF, 1, "Test failed: wrong volume");
    // }

    if (commonDataPtr->rmsErr.back() > tolerance_value) {
      std::cout << "Value error: " << commonDataPtr->rmsErr.back() << std::endl;
      SETERRQ(PETSC_COMM_SELF, 1, "Test failed: wrong value error");
    }

    if (commonDataPtr->fluxErr.back() > tolerance_flux) {
      std::cout << "Flux error: " << commonDataPtr->fluxErr.back() << std::endl;
      SETERRQ(PETSC_COMM_SELF, 1, "Test failed: wrong flux error");
    }
  }
  CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array);

  MoFEMFunctionReturn(0);
}
//! [Test example]

// Operators functions

MoFEMErrorCode ClassicDiffusionProblem::OpPostProcErrorPressureIntegPts::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  FTensor::Index<'i', 2> i;

  const int nb_integration_pts = data.getN().size1();

  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();

  auto t_val = getFTensor0FromVec(*(commonDataPtr->mValPtr));
  auto t_grad = getFTensor1FromMat<2>(*(commonDataPtr->mGradPtr));

  // get coordinates at integration point
  auto t_coords = getFTensor1CoordsAtGaussPts();

  // Create storage for the local errors and area
  std::array<double, 3> element_local_value;
  std::fill(element_local_value.begin(), element_local_value.end(), 0.0);

  FTensor::Tensor1<double, 2> t_grad_diff;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    double analytical_val =
        analyticalFunc(t_coords(0), t_coords(1), t_coords(2));

    VectorDouble vec_grad =
        analyticalGrad(t_coords(0), t_coords(1), t_coords(2));
    auto t_analytical_grad = getFTensor1FromArray<2, 1>(vec_grad);

    // find error on gauss points
    double error = analytical_val - t_val;
    t_grad_diff(i) = t_grad(i) - t_analytical_grad(i);

    element_local_value[0] += error * error * area * t_w; // RMS error
    element_local_value[1] +=
        t_grad_diff(i) * t_grad_diff(i) * area * t_w; // RMS flux error
    element_local_value[2] += t_w * area;

    ++t_val;
    ++t_grad;
    ++t_w;
    ++t_coords;
  }

  // Set array of indices
  constexpr std::array<int, 3> indices = {CommonDataClassic::VALUE_ERROR,
                                          CommonDataClassic::GRAD_ERROR,
                                          CommonDataClassic::VOL_E};

  // Assemble first moment of inertia
  CHKERR ::VecSetValues(commonDataPtr->petscVec, indices.size(), indices.data(),
                        &element_local_value[0], ADD_VALUES);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
ClassicDiffusionProblem::OpPostProcErrorFluxDerivedIntegPts::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  FTensor::Index<'i', 2> i;

  const int nb_integration_pts = data.getN().size1();

  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();

  auto t_grad = getFTensor1FromMat<2>(*(commonDataPtr->mGradPtr));
  auto t_val = getFTensor0FromVec(*(commonDataPtr->mValPtr));

  // get coordinates at integration point
  auto t_coords = getFTensor1CoordsAtGaussPts();

  // Create storage for the local errors and area
  std::array<double, 1> element_local_value;
  std::fill(element_local_value.begin(), element_local_value.end(), 0.0);

  FTensor::Tensor1<double, 2> t_flux_diff;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    double k = kFunction(t_val, 0, 0);

    VectorDouble vec_flux =
        analyticalFlux(t_coords(0), t_coords(1), t_coords(2));
    auto t_analytical_flux = getFTensor1FromArray<2, 1>(vec_flux);

    // find error on gauss points
    t_flux_diff(i) = -k * t_grad(i) - t_analytical_flux(i);

    element_local_value[0] +=
        t_flux_diff(i) * t_flux_diff(i) * area * t_w; // RMS flux error

    ++t_grad;
    ++t_val;
    ++t_w;
    ++t_coords;
  }

  // Set array of indices
  constexpr std::array<int, 3> indices = {CommonDataClassic::FLUX_ERROR};

  // Assemble first moment of inertia
  CHKERR ::VecSetValues(commonDataPtr->petscVec, indices.size(), indices.data(),
                        &element_local_value[0], ADD_VALUES);

  MoFEMFunctionReturn(0);
}

template <int BASE_DIM>
MoFEMErrorCode ClassicDiffusionProblem::OpGetFluxFromGrad<BASE_DIM>::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;

  FTensor::Index<'i', 2> i;

  const int nb_integration_pts = getGaussPts().size2();
  fluxPtr->resize(BASE_DIM, nb_integration_pts);
  auto t_grad = getFTensor1FromMat<2>(*(gradPtr));

  double fi[nb_integration_pts];

  FTensor::Tensor0<double *> t_val(fi);
  if (valuePtr)
    t_val = getFTensor0FromVec(*valuePtr);
  auto t_flux = getFTensor1FromMat<BASE_DIM>(*(fluxPtr));

  // get coordinates at integration point
  auto t_coords = getFTensor1CoordsAtGaussPts();
  double k = sFunc(0, 0, 0);

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    k = sFunc(t_val, 0, 0);

    t_flux(i) = -k * t_grad(i);

    ++t_val;
    ++t_grad;
    ++t_flux;
    ++t_coords;
  }

  MoFEMFunctionReturn(0);
}

template struct ClassicDiffusionProblem::OpGetFluxFromGrad<2>;
template struct ClassicDiffusionProblem::OpGetFluxFromGrad<3>;

MoFEMErrorCode ClassicDiffusionProblem::OpGetArea::doWork(int side,
                                                          EntityType type,
                                                          EntData &data) {
  MoFEMFunctionBegin;
  // get area of the element
  const double area = getMeasure();

  // add to dataVec at iNdex position
  CHKERR VecSetValue(dataVec, iNdex, area, ADD_VALUES);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ClassicDiffusionProblem::OpGetGaussCount::doWork(int side,
                                                                EntityType type,
                                                                EntData &data) {
  MoFEMFunctionBegin;

  // get number of gauss points
  auto nb_gauss_pts = getGaussPts().size2();

  // add to dataVec at iNdex position
  CHKERR VecSetValue(dataVec, iNdex, nb_gauss_pts, ADD_VALUES);
  MoFEMFunctionReturn(0);
}
#include <DiffusionNonlinearSnesProblem.hpp>
#include <OperatorsForDD.hpp>

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpDomainGradGrad = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpGradGrad<1, 1, 2>;

using OpDomainMass = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<1, 2>;

using OpBaseGradH = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixVectorTimesGrad<1, 2, 2>;

using OpBoundaryMass = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<1, 1>;

using OpBoundarySourceRhs = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpBoundaryTimesScalarField = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpBaseTimesScalarField<1>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 1, 1>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 1, 1>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

MoFEMErrorCode DiffusionNonlinearSnes::runProblem() {
  MoFEMFunctionBegin;

  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-nonlinear_G", &flgNonlinearToG,
                             PETSC_NULL);
  if (flgNonlinearToG)
    flgNonlinearToP = PETSC_FALSE;

  CHKERR setParamValues();
  CHKERR SetParamDDValues();
  CHKERR readMesh();
  CHKERR setupProblem();
  CHKERR setIntegrationRules();
  CHKERR createClassicCommonData();
  CHKERR createDDCommonData();
  CHKERR boundaryCondition();
  CHKERR markPressureBc();
  CHKERR assembleSystem();
  CHKERR getSnesInitial();
  CHKERR setSnesMonitors();
  CHKERR solveSnesSystem();
  CHKERR outputHVtkFields(commonDataPtr->counter);
  CHKERR outputCsvDataset();

  // Switch off fields
  auto off_field = [&](auto fe_name, auto field_name) {
    CHKERR mField.modify_finite_element_off_field_row(fe_name, field_name);
    CHKERR mField.modify_finite_element_off_field_col(fe_name, field_name);
  };

  off_field(simpleInterface->getDomainFEName(), "P_reference");
  off_field(simpleInterface->getBoundaryFEName(), "P_reference");

  // produce .h5m file to be compared to poisson_dd_H data driven results
  mField.get_moab().write_file("analysis_mesh.h5m", "MOAB",
                               "PARALLEL=WRITE_PART");

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearSnes::setupProblem() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField(valueField, H1,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(valueField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->setFieldOrder(valueField, oRder);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataSnesDD>();

  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;
  DiffusionDD::commonDataPtr = commonDataPtr;

  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-nonlinear_alpha",
                             &nonlinear_alpha, PETSC_NULL);
  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-nonlinear_k", &nonlinear_k,
                             PETSC_NULL);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "-nonlinear_alpha " << nonlinear_alpha;
  MOFEM_LOG("WORLD", Sev::inform) << "-nonlinear_k " << nonlinear_k;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearSnes::assembleSystem() {
  MoFEMFunctionBegin;

  CHKERR calculateJacobian(feDomainLhsPtr);
  CHKERR calculateJacobian(feDomainRhsPtr);

  auto alpha_function = [&](const double x, const double y, const double z) {
    return nonlinear_alpha;
  };

  // start of Lhs

  feDomainLhsPtr->getOpPtrVector().push_back(
      new OpSetBc(valueField, true, boundaryMarker));

  if (flgNonlinearToG) {
    auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
    feDomainLhsPtr->getOpPtrVector().push_back(new OpGradGradNonlinearLhs(
        valueField, valueField, mat_grad_ptr, nonlinear_alpha, nonlinear_k));
  }

  if (flgNonlinearToP) {
    nonlinear_alpha = 2.;
    nonlinear_k = 1.;
    auto vec_val_ptr = boost::make_shared<VectorDouble>();
    auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
    feDomainLhsPtr->getOpPtrVector().push_back(new OpGradGradNonlinearPLhs(
        valueField, valueField, vec_val_ptr, mat_grad_ptr, nonlinear_alpha,
        nonlinear_k));
  }

  feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpSetBc(valueField, false, boundaryMarker));

  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpBoundaryMass(valueField, valueField, oneFunction,
                         boost::make_shared<Range>(pressureBcRange)));

  feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

  // end of Lhs

  //  start of Rhs

  feDomainRhsPtr->getOpPtrVector().push_back(
      new OpSetBc(valueField, true, boundaryMarker));

  if (flgNonlinearToG) {
    auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
    feDomainRhsPtr->getOpPtrVector().push_back(new OpGradNonlinearRhs(
        valueField, mat_grad_ptr, nonlinear_alpha, nonlinear_k));
  }

  if (flgNonlinearToP) {
    auto vec_val_ptr = boost::make_shared<VectorDouble>();
    auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
    feDomainRhsPtr->getOpPtrVector().push_back(new OpGradNonlinearPRhs(
        valueField, vec_val_ptr, mat_grad_ptr, nonlinear_alpha, nonlinear_k));
  }

  { // source added to the domain Rhs
    auto sourceFunction = [&](const double x, const double y, const double z) {
      // minus for snes
      return -exp(-100. * (square(x) + square(y))) *
             (400. * M_PI *
                  (x * cos(M_PI * y) * sin(M_PI * x) +
                   y * cos(M_PI * x) * sin(M_PI * y)) +
              2. * (20000. * (square(x) + square(y)) - 200. - square(M_PI)) *
                  cos(M_PI * x) * cos(M_PI * y));
      // return 0;
      // return -2. * square(M_PI) * cos(M_PI * (x - 0.5)) * cos(M_PI * (y -
      // 0.5));
    };

    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, valueField, dummy_k,
        Sev::inform);
    // feDomainRhsPtr->getOpPtrVector().push_back(
    //     new OpDomainSourceRhs(valueField, sourceFunction));
  }

  feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

  // boundary conditions Rhs

  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetBc(valueField, true, boundaryMarker));
  // feBoundaryRhsPtr->getOpPtrVector().push_back(new OpBoundarySourceRhs(
  //     valueField, fluxBcFunction, boost::make_shared<Range>(fluxBcRange)));
  CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
      feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, dummy_k,
      Sev::inform);
  feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

  // (u, u_bar)
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetBc(valueField, false, boundaryMarker));
  CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
      feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, -dummy_k,
      Sev::inform);

  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
  feBoundaryRhsPtr->getOpPtrVector().push_back(new OpBoundaryTimesScalarField(
      valueField, commonDataPtr->mValPtr, oneFunction,
      boost::make_shared<Range>(pressureBcRange)));

  feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

  // end of Rhs

  if (flg_rand_ini) {
    // random
    PetscRandom rctx;
    PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
    PetscRandomSetSeed(rctx, (unsigned long)std::time(0));
    PetscRandomSeed(rctx);

    // make scale an input
    double scale = 10;
    CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-rand_ini_scale", &scale,
                               PETSC_NULL);

    auto vector_times_scalar_field =
        [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
          MoFEMFunctionBeginHot;
          auto x_data = ent_ptr_x->getEntFieldData();
          double value;
          PetscRandomGetValueReal(rctx, &value);

          for (auto &v : x_data)
            v = (value - 0.5) * scale;

          MoFEMFunctionReturnHot(0);
        };

    CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(
        vector_times_scalar_field, valueField);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearSnes::setSnesMonitors() {
  MoFEMFunctionBegin;

  // ctx for monitor containing the discrete manager
  auto snes_ctx_ptr = smartGetDMSnesCtx(simpleInterface->getDM());

  combinedPointers = boost::make_shared<CombinedPointers>();

  combinedPointers->commonDataPtr = commonDataPtr;
  combinedPointers->snesCtxPtr = snes_ctx_ptr;

  // define monitors which are executed at the begining of each iteration
  // postProc on integration points
  auto monitor_vtk_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                  void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    auto &m_field = snes_ptr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();

    boost::shared_ptr<PostProcFaceOnRefinedMesh> fePostProcPtr;
    fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
        new PostProcFaceOnRefinedMesh(m_field));

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    fePostProcPtr->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpSetInvJacH1ForFace(inv_jac_ptr));

    // Generate post-processing mesh
    CHKERR fePostProcPtr->generateReferenceElementMesh();
    // Postprocess only field values
    const Field_multiIndex *fields;
    m_field.get_fields(&fields);
    for (auto &it : *fields) {
      fePostProcPtr->addFieldValuesPostProc(it->getName());
    }
    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), fePostProcPtr);

    // write output
    CHKERR fePostProcPtr->writeFile(
        "out_iteration_" + boost::lexical_cast<std::string>(its) + ".h5m");

    MoFEMFunctionReturn(0);
  };

  auto monitor_print_moab = [](SNES snes, PetscInt its, PetscReal fgnorm,
                               void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    string gauss_name;
    std::ostringstream strma;
    strma << "out_moab_" << its << ".h5m";
    gauss_name = strma.str();
    CHKERR snes_ptr->mField.get_moab().write_file(gauss_name.c_str(), "MOAB",
                                                  "PARALLEL=WRITE_PART");

    MoFEMFunctionReturn(0);
  };

  CHKERR SNESMonitorSet(snesSolver,
                        (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                           void *))MoFEMSNESMonitorFields,
                        (void *)(snes_ctx_ptr.get()), nullptr);

  if (!skip_vtk)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_vtk_post_proc,
                          (void *)(snes_ctx_ptr.get()), nullptr);

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_print_moab,
                          (void *)(snes_ctx_ptr.get()), nullptr);
  }

  MoFEMFunctionReturn(0);
}

// printing material dataset

MoFEMErrorCode DiffusionNonlinearSnes::outputCsvDataset(bool header) {
  MoFEMFunctionBegin;

  std::string csv_GQ = "data_GQ_generated.csv";
  std::string csv_VGQ = "data_VGQ_generated.csv";

  if (header) {
    std::ofstream myfile;
    myfile.open(csv_GQ);
    myfile << "gradx, grady, fluxx, fluxy\n"; // headers
    myfile.close();

    myfile.open(csv_VGQ);
    myfile << "gradx, grady, fluxx, fluxy, value\n"; // headers
    myfile.close();
  }

  moab::Core mb_post;                   // create database
  moab::Interface &moab_proc = mb_post; // create interface to database

  boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
      new PatchIntegFaceEle(mField, patchIntegNum));

  auto domain_post_proc_rule = [](int, int, int p) -> int { return -1; };
  PostProcGauss->getRuleHook = domain_post_proc_rule;

  CHKERR calculateJacobian(PostProcGauss);

  auto vec_val_ptr = boost::make_shared<VectorDouble>();
  auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
  auto mat_flux_ptr = boost::make_shared<MatrixDouble>();

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, vec_val_ptr));

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));

  auto get_k_from_g_function =
      [&](const FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 2> grad) {
        return 1 + nonlinear_k * pow(grad(0) * grad(0) + grad(1) * grad(1),
                                     nonlinear_alpha / 2.);
      };

  if (flgNonlinearToG)
    PostProcGauss->getOpPtrVector().push_back(new OpGetFluxFromGrad(
        valueField, mat_grad_ptr, mat_flux_ptr, get_k_from_g_function));

  auto get_k_from_val_function = [&](const double val, const double,
                                     const double) {
    return 1 + nonlinear_k * pow(abs(val), nonlinear_alpha);
  };

  if (flgNonlinearToP)
    PostProcGauss->getOpPtrVector().push_back(
        new OpGetFluxFromVal(valueField, vec_val_ptr, mat_grad_ptr,
                             mat_flux_ptr, get_k_from_val_function));

  PostProcGauss->getOpPtrVector().push_back(
      new OperatorsForDD::OpPostProcSaveCsvDatasetGQ<2, 2>(
          valueField, mat_grad_ptr, mat_flux_ptr, csv_GQ));

  PostProcGauss->getOpPtrVector().push_back(
      new OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<2, 2>(
          valueField, vec_val_ptr, mat_grad_ptr, mat_flux_ptr, csv_VGQ));

  mb_post.delete_mesh();

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

  MoFEMFunctionReturn(0);
}

// Operators

MoFEMErrorCode DiffusionNonlinearSnes::OpGradGradNonlinearLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    FTensor::Index<'k', 2> k;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    const int nb_base_functions = row_data.getN().size2();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));

    // get derivatives of base functions on row
    auto t_row_diff_base = row_data.getFTensor1DiffN<2>();

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_row_dofs; ++rr) {
        // get derivatives of base functions on column
        auto t_col_diff_base = col_data.getFTensor1DiffN<2>(gg, 0);

        for (int cc = 0; cc != nb_col_dofs; cc++) {

          locLhs(rr, cc) +=
              -nonlinearK * a *
              ((1 / nonlinearK +
                pow(t_grad(j) * t_grad(j), nonlinearAlpha / 2.)) *
                   t_row_diff_base(i) * t_col_diff_base(i) +
               nonlinearAlpha *
                   (pow(t_grad(j) * t_grad(j), (nonlinearAlpha - 2.) / 2.)) *
                   t_grad(i) * t_row_diff_base(i) * t_col_diff_base(k) *
                   t_grad(k));

          // locLhs(rr, cc) +=
          //     -t_row_diff_base(i) * t_col_diff_base(i) * a *
          //     nonlinearK * (1 + (s_term + 1) * pow(sqrt(t_grad(i) *
          //     t_grad(i)), s_term));

          // move to the derivatives of the next base functions on column
          ++t_col_diff_base;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_diff_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_diff_base;

      // move to the weight of the next integration point
      ++t_w;
      ++t_grad;
    }

    CHKERR MatSetValues<EssentialBcStorage>(
        getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearSnes::OpGradGradNonlinearPLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    FTensor::Index<'k', 2> k;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    const int nb_base_functions = row_data.getN().size2();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    auto t_val = getFTensor0FromVec(*(valPtr));
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));

    // get base functions on row
    auto t_row_base = row_data.getFTensor0N();
    // get derivatives of base functions on row
    auto t_row_diff_base = row_data.getFTensor1DiffN<2>();

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {
      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_row_dofs; ++rr) {
        // get base functions on column
        auto t_col_base = col_data.getFTensor0N(gg, 0);
        // get derivatives of base functions on column
        auto t_col_diff_base = col_data.getFTensor1DiffN<2>(gg, 0);

        for (int cc = 0; cc != nb_col_dofs; cc++) {
          locLhs(rr, cc) -=
              (((1 + nonlinearK * pow(t_val, nonlinearAlpha)) *
                t_row_diff_base(i) * t_col_diff_base(i)) +
               nonlinearK * (nonlinearAlpha * pow(t_val, nonlinearAlpha - 1) *
                             t_grad(i) * t_row_diff_base(i) * t_col_base)) *
              a;

          // move to the next base functions on column
          ++t_col_base;
          // move to the derivatives of the next base function on column
          ++t_col_diff_base;
        }

        // move to the next base function on row
        ++t_row_base;
        // move to the derivatives of the next base function on row
        ++t_row_diff_base;
      }

      for (; rr != nb_base_functions; ++rr) {
        ++t_row_base;
        ++t_row_diff_base;
      }

      // move to the weight of the next integration point
      ++t_w;
      // move to the solution (field value) at the next integration point
      ++t_val;
      // move to the gradient of field value at the next integration point
      ++t_grad;
    }

    CHKERR MatSetValues<EssentialBcStorage>(
        getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
DiffusionNonlinearSnes::OpGradNonlinearRhs::doWork(int side, EntityType type,
                                                   EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = data.getN().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get base functions
    auto t_diff_base = data.getFTensor1DiffN<2>();
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_dofs; rr++) {
        locRhs[rr] +=
            -t_diff_base(i) * a * t_grad(i) * nonlinearK *
            (1 / nonlinearK + pow(t_grad(i) * t_grad(i), nonlinearAlpha / 2.));

        // move to the next base function
        ++t_diff_base;
      }
      for (; rr != nb_base_functions; ++rr)
        ++t_diff_base;

      ++t_grad;
      // move to the weight of the next integration point
      ++t_w;
    }

    CHKERR VecSetValues<EssentialBcStorage>(getKSPf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
DiffusionNonlinearSnes::OpGradNonlinearPRhs::doWork(int side, EntityType type,
                                                    EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = data.getN().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    auto t_val = getFTensor0FromVec(*(valPtr));
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));

    // get base functions
    auto t_diff_base = data.getFTensor1DiffN<2>();

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_dofs; rr++) {
        locRhs[rr] -= t_diff_base(i) * a * t_grad(i) *
                      (1 + nonlinearK * pow(t_val, nonlinearAlpha));

        // move to the next base function
        ++t_diff_base;
      }
      for (; rr != nb_base_functions; ++rr)
        ++t_diff_base;

      ++t_val;
      ++t_grad;
      // move to the weight of the next integration point
      ++t_w;
    }

    CHKERR VecSetValues<EssentialBcStorage>(getKSPf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
DiffusionNonlinearSnes::OpGetFluxFromGrad::doWork(int side, EntityType type,
                                                  EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();

    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));
    fluxPtr->resize(2, nb_integration_points, false);
    fluxPtr->clear();
    auto t_flux = getFTensor1FromMat<2>(*(fluxPtr));

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      auto k_f = kFunc(t_grad);

      t_flux(i) = -k_f * t_grad(i);

      ++t_grad;
      ++t_flux;
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearSnes::OpGetFluxFromVal::doWork(int side,
                                                                EntityType type,
                                                                EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();

    auto t_val = getFTensor0FromVec(*(valPtr));
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));
    fluxPtr->resize(2, nb_integration_points, false);
    fluxPtr->clear();
    auto t_flux = getFTensor1FromMat<2>(*(fluxPtr));

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      auto k_f = kFunc(t_val, 0, 0);

      t_flux(i) = -k_f * t_grad(i);

      ++t_val;
      ++t_grad;
      ++t_flux;
    }
  }

  MoFEMFunctionReturn(0);
}
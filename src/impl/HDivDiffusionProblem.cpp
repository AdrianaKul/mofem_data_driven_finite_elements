#include <HDivDiffusionProblem.hpp>
using OpHdivHdiv = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<3, 2>;

using OpBoundaryHdivHdiv = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<3, 2>;

// Scalars
using OpHdivU = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixDivTimesScalar<2>;
using OpUHdiv = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixScalarTimesDiv<2>;

using OpPressureBC = FormsIntegrators<OpEdgeEle>::Assembly<PETSC>::LinearForm<
    GAUSS>::OpNormalMixVecTimesScalar<2>;

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

template struct HDivDiffusionProblem::OpErrorIndGrad<2>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 3, 2>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 3, 2>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

MoFEMErrorCode HDivDiffusionProblem::setupProblem() {
  MoFEMFunctionBegin;

  // domain fields
  CHKERR simpleInterface->addDomainField(valueField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 1);

  int nb_quads = 0;
  CHKERR mField.get_moab().get_number_entities_by_type(0, MBQUAD, nb_quads);
  auto base = AINSWORTH_LEGENDRE_BASE;
  if (nb_quads) {
    // AINSWORTH_LEGENDRE_BASE is not implemented for HDIV/HCURL space on quads
    base = DEMKOWICZ_JACOBI_BASE;
  }
  CHKERR simpleInterface->addDomainField(fluxField, HCURL, base, 1);

  // boundary fields
  CHKERR simpleInterface->addBoundaryField(fluxField, HCURL, base, 1);
  // skeleton fields
  CHKERR simpleInterface->addSkeletonField(fluxField, HCURL, base, 1);

  // set orders
  CHKERR simpleInterface->setFieldOrder(valueField, oRder);
  CHKERR simpleInterface->setFieldOrder(fluxField, oRder + 1);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataHDiv>();
  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::orderUpdateSetUp() {
  MoFEMFunctionBegin;

  refIterNum = 0;
  orderRefinementCounter = 0;
  meshRefinementCounter = 0;
  refinementStyle = 0;

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "DD Config", "none");
  // Set values from param file
  CHKERR PetscOptionsInt(
      "-refinement_style",
      "0 = no refinement; 1 = order refinement; 2 = mesh refinement", "", 0,
      &refinementStyle, PETSC_NULL);
  CHKERR PetscOptionsReal("-ref_control",
                          "number multiplying error indicator average to "
                          "determine elements to refine",
                          "", 1., &refControl, PETSC_NULL);

  CHKERR PetscOptionsReal("-ref_control_mesh",
                          "number multiplying error indicator average to "
                          "determine elements to refine for mesh refinement",
                          "", 1., &refControlMesh, PETSC_NULL);

  if (refControlMesh < 1e-7)
    refControlMesh = refControl;

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-ref_iter_num", &refIterNum,
                            PETSC_NULL);

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "-refinement_style " << refinementStyle;
  switch (refinementStyle) {
  case 1:
    MOFEM_LOG("WORLD", Sev::inform) << "  .: using ORDER refinement ";
    break;
  case 2:
    MOFEM_LOG("WORLD", Sev::inform) << "  .: using MESH refinement ";
    break;
  case 3:
    MOFEM_LOG("WORLD", Sev::inform) << "h-p refinement in development";
    break;
  case 4:
    MOFEM_LOG("WORLD", Sev::inform)
        << "h-p alternating refinement in development";
    break;
  default:
    MOFEM_LOG("WORLD", Sev::inform) << "  .: no refinement";
    refIterNum = 0;
  }

  MOFEM_LOG("WORLD", Sev::inform) << "-ref_iter_num " << refIterNum;

  CHKERR mField.get_moab().get_entities_by_dimension(0, 2, domainEntities,
                                                     false);
  Tag th_order;
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, th_order);
  for (auto &ent : domainEntities) {
    CHKERR mField.get_moab().tag_set_data(th_order, &ent, 1, &oRder);
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::createHdivCommonData() {
  MoFEMFunctionBegin;
  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "DD Config", "none");
  CHKERR PetscOptionsBool(
      "-print_error",
      "set if element errors should be printed to out_error_*.h5m", "",
      PETSC_TRUE, &flg_out_error, PETSC_NULL);
  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  int local_size;
  if (mField.get_comm_rank() == 0) // get_comm_rank() gets processor number
    // processor 0
    local_size = CommonDataHDiv::LAST_REFINE_ELEMENT; // last element gives size
                                                      // of vector
  else
    // other processors (e.g. 1, 2, 3, etc.)
    local_size = 0; // local size of vector is zero on other processors

  commonDataPtr->petscRefineErrorVec = createSmartVectorMPI(
      mField.get_comm(), local_size, CommonDataHDiv::LAST_REFINE_ELEMENT);

  // set conductivity
  commonDataPtr->k_minus = -dummy_k;
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::markFluxBc() {
  MoFEMFunctionBegin;

  auto mark_boundary_dofs = [&](Range &skin_edges) {
    auto problem_manager = mField.getInterface<ProblemsManager>();
    auto marker_ptr = boost::make_shared<std::vector<unsigned char>>();
    problem_manager->markDofs(simpleInterface->getProblemName(), ROW,
                              skin_edges, *marker_ptr);
    return marker_ptr;
  };

  boundaryMarker.reset();
  boundaryMarker = mark_boundary_dofs(fluxBcRange);

  // Store entities for fieldsplit (block) solver
  boundaryEntitiesForFieldsplit = fluxBcRange;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
HDivDiffusionProblem::calculateHdivJacobian(boost::shared_ptr<FaceEle> fe_ptr) {
  MoFEMFunctionBegin;

  auto det_ptr = boost::make_shared<VectorDouble>();
  auto jac_ptr = boost::make_shared<MatrixDouble>();
  auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

  fe_ptr->getOpPtrVector().push_back(new OpCalculateHOJacForFace(jac_ptr));
  fe_ptr->getOpPtrVector().push_back(
      new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
  fe_ptr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpSetInvJacHcurlFace(inv_jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpSetInvJacL2ForFace(inv_jac_ptr));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::recheckOrder() {
  MoFEMFunctionBegin;

  simpleInterface->getBitRefLevel() = BitRefLevel().set(meshRefinementCounter);
  simpleInterface->getBitRefLevelMask() = BitRefLevel().set();

  Tag th_order;
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, th_order);

  std::vector<Range> refinement_levels;
  refinement_levels.resize(refIterNum + 1);

  for (auto ent : domainEntities) {
    int order;
    CHKERR mField.get_moab().tag_get_data(th_order, &ent, 1, &order);
    refinement_levels[order - oRder].insert(ent);
  }

  for (int ll = 1; ll < refinement_levels.size(); ll++) {
    CHKERR mField.getInterface<CommInterface>()->synchroniseEntities(
        refinement_levels[ll]);
    CHKERR mField.set_field_order(refinement_levels[ll], fluxField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], valueField,
                                  oRder + ll);
  }

  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      fluxField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      valueField);
  CHKERR mField.build_fields();
  CHKERR mField.build_finite_elements();
  CHKERR mField.build_adjacencies(simpleInterface->getBitRefLevel());
  mField.getInterface<ProblemsManager>()->buildProblemFromFields = PETSC_TRUE;

  simpleInterface->getBitRefLevel() = BitRefLevel().set(meshRefinementCounter);
  simpleInterface->getBitRefLevelMask() = BitRefLevel().set();

  CHKERR DMSetUp_MoFEM(simpleInterface->getDM());

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::runLoop() {
  MoFEMFunctionBegin;
  CHKERR assembleSystem();
  CHKERR getKSPinitial();
  CHKERR solveSystem();
  CHKERR outputResults(0);

  for (int ii = 0; ii != refIterNum; ++ii) {
    switch (refinementStyle) {
    case 1:
      CHKERR refineOrder();
      break;
    case 2:
      CHKERR refineMesh();
      break;
    case 3:
      if (ii > 0)
        CHKERR refineMesh();
      CHKERR refineOrder();
      CHKERR recheckOrder();
      break;
    default:
      MOFEM_LOG("WORLD", Sev::inform) << ".. no refinement";
    }
    CHKERR boundaryCondition();
    CHKERR markFluxBc();
    // reset error indicators
    Tag th_error_ind, th_jump_l2;
    CHKERR getTagHandle(mField, "ERROR_INDICATOR_GRAD", MB_TYPE_DOUBLE,
                        th_error_ind);
    CHKERR getTagHandle(mField, "JUMP_L2", MB_TYPE_DOUBLE, th_jump_l2);
    for (auto &ent : domainEntities) {
      double zero = 0;
      CHKERR mField.get_moab().tag_set_data(th_error_ind, &ent, 1, &zero);
      CHKERR mField.get_moab().tag_set_data(th_jump_l2, &ent, 1, &zero);
    }
    CHKERR assembleSystem();
    CHKERR getKSPinitial();
    CHKERR solveSystem();
    CHKERR outputResults(ii + 1);
    CHKERR checkRefinement();
  }
  MoFEMFunctionReturn(0);
}

//! [Assemble system]
MoFEMErrorCode HDivDiffusionProblem::assembleSystem() {
  MoFEMFunctionBegin;

  // clearing previous settings
  feDomainLhsPtr->getOpPtrVector().clear();
  feDomainRhsPtr->getOpPtrVector().clear();
  feBoundaryLhsPtr->getOpPtrVector().clear();
  feBoundaryRhsPtr->getOpPtrVector().clear();

  // calculating jacobian and other general necessary parts
  CHKERR calculateHdivJacobian(feDomainLhsPtr);
  CHKERR calculateHdivJacobian(feDomainRhsPtr);
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());

  { // Lhs domain operators

    auto minus_unity = []() { return -1.0; };
    auto one_over_k_function = [&](double, double, double) {
      return 1. / dummy_k;
    };

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivHdiv(fluxField, fluxField, one_over_k_function));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivU(fluxField, valueField, minus_unity, true));

    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  { // RHS boundary elements - neuman bc (pressure)
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, fluxField, -1.0,
        Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  { // Rhs domain (source)
    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, valueField, -dummy_k,
        Sev::inform);
  }

  // Hdiv essential bsc - flux
  CHKERR assembleEssentialBoundary();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::assembleEssentialBoundary() {
  MoFEMFunctionBegin;
  // Hdiv essential bsc - flux

  // setting Rhs essential bc
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetBc(fluxField, false, boundaryMarker));
  CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
      feBoundaryRhsPtr->getOpPtrVector(), mField, fluxField, -1.0, Sev::inform);
  feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));

  // setting Rhs essential bc
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpSetBc(fluxField, false, boundaryMarker));
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpBoundaryHdivHdiv(fluxField, fluxField, oneFunction,
                             boost::make_shared<Range>(fluxBcRange)));
  feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));

  MoFEMFunctionReturn(0);
}
//! [Assemble system]

MoFEMErrorCode HDivDiffusionProblem::refineMesh() {
  MoFEMFunctionBegin;

  auto &moab = mField.get_moab();

  CHKERR mField.get_moab().get_entities_by_dimension(0, 2, domainEntities,
                                                     false);

  auto bit_level0 = BitRefLevel().set(meshRefinementCounter);
  meshRefinementCounter++;
  auto bit_level1 = BitRefLevel().set(meshRefinementCounter);

  Tag th_error_ind;
  CHKERR getTagHandle(mField, "ERROR_ESTIMATOR", MB_TYPE_DOUBLE, th_error_ind);
  Tag th_order_tag;
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, th_order_tag);

  Range refined_ents;

  for (auto &ent : domainEntities) {
    double err_indic = 0;
    CHKERR mField.get_moab().tag_get_data(th_error_ind, &ent, 1, &err_indic);

    if (err_indic >
        refControlMesh * sqrt(errorIndicatorIntegral / totalElementNumber)) {
      refined_ents.insert(ent);
      Range adj;
      CHKERR mField.get_moab().get_adjacencies(&ent, 1, 1, false, adj,
                                               moab::Interface::UNION);
      refined_ents.merge(adj);
    }
  }

  PetscBool flg_refine_h_boundary_only = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-refine_h_boundary_only",
                             &flg_refine_h_boundary_only, PETSC_NULL);

  // Range to_intersect = domainEntities;
  if (flg_refine_h_boundary_only) {
    Skinner skin(&mField.get_moab());
    Range skin_edges; // skin edges from 2d ents
    CHKERR skin.find_skin(0, domainEntities, true, skin_edges);

    Range skin_nodes;
    // CHKERR mField.get_moab().get_connectivity(skin_edges, skin_nodes, true);
    CHKERR mField.get_moab().get_adjacencies(skin_edges, 0, false, skin_nodes,
                                             moab::Interface::UNION);

    Range skin_faces;
    CHKERR mField.get_moab().get_adjacencies(skin_nodes, 2, false, skin_faces,
                                             moab::Interface::UNION);

    CHKERR mField.get_moab().get_adjacencies(skin_faces, 1, false, skin_edges,
                                             moab::Interface::UNION);

    // refined_ents.intersect(skin_faces);
    // Range common_boundary = intersect(fluxBcRange, pressureBcRange);
    refined_ents = intersect(refined_ents, skin_edges);
  }

  auto refine = mField.getInterface<MeshRefinement>();

  auto meshset_level0_ptr = get_temp_meshset_ptr(moab);
  CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
      bit_level0, BitRefLevel().set(), simpleInterface->getDim(),
      *meshset_level0_ptr);

  // mesh refinement
  auto meshset_ref_edges_ptr = get_temp_meshset_ptr(moab);
  Range edges_to_refine;
  CHKERR mField.getInterface<BitRefManager>()->getEntitiesByTypeAndRefLevel(
      bit_level0, BitRefLevel().set(), MBEDGE, edges_to_refine);

  // add meshRefEntAdditional to refined_ents
  refined_ents.merge(meshRefEntAdditional);

  edges_to_refine = intersect(refined_ents, edges_to_refine);

  CHKERR refine->addVerticesInTheMiddleOfEdges(edges_to_refine, bit_level1,
                                               VERBOSE);
  if (simpleInterface->getDim() == 2) {
    CHKERR refine->refineTris(*meshset_level0_ptr, bit_level1, VERBOSE);
  } else {
    SETERRQ(PETSC_COMM_WORLD, MOFEM_ATOM_TEST_INVALID,
            "Dimension not handled by test");
  }

  // update tags
  auto bit_ref_mng = mField.getInterface<BitRefManager>();
  // get tag handle for parent entities handle
  auto th_parent_ent = mField.get_basic_entity_data_ptr()->th_RefParentHandle;

  CHKERR bit_ref_mng->getEntitiesByDimAndRefLevel(
      bit_level1, BitRefLevel().set(), 2, domainEntities);

  auto update_tags_by_parent_double =
      [&](const char *name, const DataType MB_type,
          const EntityHandle *child_ent, const EntityHandle *parent_ent) {
        Tag tag;
        CHKERR getTagHandle(mField, name, MB_type, tag);
        double data;
        CHKERR mField.get_moab().tag_get_data(tag, parent_ent, 1, &data);
        CHKERR mField.get_moab().tag_set_data(tag, child_ent, 1, &data);
      };

  auto update_tags_by_parent_int = [&](const char *name, const DataType MB_type,
                                       const EntityHandle *child_ent,
                                       const EntityHandle *parent_ent) {
    Tag tag;
    CHKERR getTagHandle(mField, name, MB_type, tag);
    int data;
    CHKERR mField.get_moab().tag_get_data(tag, parent_ent, 1, &data);
    CHKERR mField.get_moab().tag_set_data(tag, child_ent, 1, &data);
  };

  Range refined_only_ents;
  // fix faces
  {
    CHKERR bit_ref_mng->getEntitiesByDimAndRefLevel(bit_level1, bit_level1, 2,
                                                    refined_only_ents);
    for (auto &ent : refined_only_ents) {
      EntityHandle parent_ent;
      CHKERR mField.get_moab().tag_get_data(th_parent_ent, &ent, 1,
                                            &parent_ent);

      update_tags_by_parent_double("ERROR_INDICATOR_GRAD", MB_TYPE_DOUBLE, &ent,
                                   &parent_ent);
      update_tags_by_parent_double("ERROR_ESTIMATOR", MB_TYPE_DOUBLE, &ent,
                                   &parent_ent);
      update_tags_by_parent_int("ORDER", MB_TYPE_INTEGER, &ent, &parent_ent);
      update_tags_by_parent_double("DD_DISTANCE_AVE", MB_TYPE_DOUBLE, &ent,
                                   &parent_ent);
    }
  }
  // fix edges
  {
    CHKERR bit_ref_mng->getEntitiesByDimAndRefLevel(bit_level1, bit_level1, 1,
                                                    refined_only_ents);
    for (auto &ent : refined_only_ents) {
      EntityHandle parent_ent;
      CHKERR mField.get_moab().tag_get_data(th_parent_ent, &ent, 1,
                                            &parent_ent);
      update_tags_by_parent_int("_App_Order_Q", MB_TYPE_INTEGER, &ent,
                                &parent_ent);
    }
  }

  Range meshsets;
  CHKERR moab.get_entities_by_type(0, MBENTITYSET, meshsets, true);
  for (auto m : meshsets) {
    CHKERR mField.getInterface<BitRefManager>()
        ->updateMeshsetByEntitiesChildren(m, bit_level1, m, MBMAXTYPE, false);
  }

  auto meshset_level1_ptr = get_temp_meshset_ptr(moab);
  CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
      bit_level1, BitRefLevel().set(), simpleInterface->getDim(),
      *meshset_level1_ptr);

  simpleInterface->getBitRefLevel() = bit_level1;
  simpleInterface->getBitRefLevelMask() = BitRefLevel().set();

  CHKERR simpleInterface->reSetUp();

  auto out_meshset_tri_ptr = get_temp_meshset_ptr(moab);
  CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
      bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);

  Range tris;
  CHKERR moab.get_entities_by_type(*out_meshset_tri_ptr, MBTRI, tris, true);

  // create vtk file name
  std::ostringstream out_vtk;
  out_vtk << "out_mesh_refine_" << meshRefinementCounter << ".vtk";

  CHKERR moab.write_file(out_vtk.str().c_str(), "VTK", "",
                         out_meshset_tri_ptr->get_ptr(), 1);

  // create vtk file name
  std::ostringstream out_meshset_vtk;
  out_meshset_vtk << "out_blockset_" << meshRefinementCounter << ".vtk";

  // // get all the fields
  // const Field_multiIndex *fields;
  // mField.get_fields(&fields);
  // for (auto &it : *fields)
  //   CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
  //       simpleInterface->getProblemName(), it->getName(),
  //       BitRefLevel().set(), bit_level0);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::getJumps(int iter_num) {
  MoFEMFunctionBegin;

  // FIXME: getJumps does NOT work for multiprocessing
  PetscBool flg_get_jumps = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-get_jumps", &flg_get_jumps,
                             PETSC_NULL);

  if (!flg_get_jumps)
    MoFEMFunctionReturnHot(0);

  boost::shared_ptr<EdgeEle> feBoundaryJumpsPtr;
  feBoundaryJumpsPtr = boost::shared_ptr<EdgeEle>(new EdgeEle(mField));

  auto boundary_rule_jump = [](int, int, int p) -> int { return 2 * p; };
  feBoundaryJumpsPtr->getRuleHook = boundary_rule_jump;

  constexpr int SPACE_DIM = 2;

  auto op_loop_side = new OpLoopSide<SideEle>(
      mField, simpleInterface->getDomainFEName(), SPACE_DIM);

  auto det_ptr = boost::make_shared<VectorDouble>();
  auto jac_ptr = boost::make_shared<MatrixDouble>();
  auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

  op_loop_side->getOpPtrVector().push_back(
      new OpCalculateHOJacForFace(jac_ptr));
  op_loop_side->getOpPtrVector().push_back(
      new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
  op_loop_side->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
  op_loop_side->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
  op_loop_side->getOpPtrVector().push_back(
      new OpSetInvJacHcurlFace(inv_jac_ptr));
  op_loop_side->getOpPtrVector().push_back(
      new OpSetInvJacL2ForFace(inv_jac_ptr));

  op_loop_side->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));

  op_loop_side->getOpPtrVector().push_back(
      new OpCalculateHVecVectorField<3>(fluxField, commonDataPtr->mFluxPtr));

  auto side_data_ptr = boost::make_shared<SideData>();
  op_loop_side->getOpPtrVector().push_back(
      new OpSideGetData<2, 3>(fluxField, commonDataPtr, side_data_ptr));

  feBoundaryJumpsPtr->getOpPtrVector().push_back(op_loop_side);
  feBoundaryJumpsPtr->getOpPtrVector().push_back(
      new OpSkeletonJump(side_data_ptr, commonDataPtr));

  feBoundaryJumpsPtr->getOpPtrVector().push_back(
      new OpJumpTags(side_data_ptr, commonDataPtr, mField));

  CHKERR DMoFEMLoopFiniteElementsUpAndLowRank(
      dM, simpleInterface->getSkeletonFEName(), feBoundaryJumpsPtr,
      mField.get_comm_rank(), mField.get_comm_rank());

  // CHKERR DMoFEMLoopFiniteElements(dM, simpleInterface->getSkeletonFEName(),
  //                                 feBoundaryRhsPtr);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode HDivDiffusionProblem::postGetAnalyticalValuesHdiv(
    boost::shared_ptr<FaceEle> fe_ptr) {
  MoFEMFunctionBegin;

  PetscBool flg_ana_square_top = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_top",
                             &flg_ana_square_top, PETSC_NULL);

  if (flg_ana_square_top) {
    flgAnalytical = true;
    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_squareTop));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_squareTop));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);
      res[0] =
          -dummy_k * M_PI * cos(M_PI * (x)) * sinh(M_PI * (y)) / sinh(M_PI);
      res[1] =
          -dummy_k * M_PI * sin(M_PI * (x)) * cosh(M_PI * (y)) / sinh(M_PI);
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 3>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_square_sincos = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_sincos",
                             &flg_ana_square_sincos, PETSC_NULL);

  if (flg_ana_square_sincos) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_squareSinCos));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_squareSinCos));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);
      double a = 2.;
      res[0] = -dummy_k * a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y);
      res[1] = dummy_k * a * M_PI * sin(a * M_PI * x) * sin(a * M_PI * y);
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 3>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_square_nonlinear_sincos = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_nonlinear_sincos",
                             &flg_ana_square_nonlinear_sincos, PETSC_NULL);

  if (flg_ana_square_nonlinear_sincos) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_squareSinCos));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_squareSinCos));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      double a = 2.;
      double aa = 1.;
      double bb = 1.;
      double cc = 1.;

      VectorDouble res;
      res.resize(2);
      res[0] = -a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y) *
               (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                         (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x)));
      res[1] = a * M_PI * sin(a * M_PI * x) *
               (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                         (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x))) *
               sin(a * M_PI * y);
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 3>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_mexi_hat = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_mexi_hat", &flg_ana_mexi_hat,
                             PETSC_NULL);
  if (flg_ana_mexi_hat) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_mexiHat));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_mexiHat));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);
      res[0] = -dummy_k * (-exp(-100. * (square(x) + square(y))) *
                           (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) *
                           cos(M_PI * y));
      res[1] = -dummy_k * (-exp(-100. * (square(x) + square(y))) *
                           (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) *
                           cos(M_PI * x));
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 3>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  PetscBool flg_ana_L_shape = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_L_shape", &flg_ana_L_shape,
                             PETSC_NULL);
  if (flg_ana_L_shape) {
    flgAnalytical = true;

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor0fromFunc(
        commonDataPtr->mValFuncPtr, analyticalFunction_LShape));

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 2>(
        commonDataPtr->mGradFuncPtr, analyticalFunctionGrad_LShape));

    auto analyticalFunctionFlux = [&](const double x, const double y,
                                      const double z) {
      VectorDouble res;
      res.resize(2);

      double numerator = 2 * dummy_k *
                         (y * cos((M_PI + 2 * atan2(y, x)) / 3) -
                          x * sin((M_PI + 2 * atan2(y, x)) / 3));
      double denominator = 3 * pow(pow(x, 2) + pow(y, 2), 2.0 / 3.0);
      res[0] = numerator / denominator;
      numerator = -2 * dummy_k *
                  (x * cos((M_PI + 2 * atan2(y, x)) / 3) +
                   y * sin((M_PI + 2 * atan2(y, x)) / 3));
      res[1] = numerator / denominator;
      return res;
    };

    fe_ptr->getOpPtrVector().push_back(new OpGetTensor1fromFunc<2, 3>(
        commonDataPtr->mFluxFuncPtr, analyticalFunctionFlux));
  }

  MoFEMFunctionReturn(0);
}

//! [Check error]
MoFEMErrorCode HDivDiffusionProblem::checkError(int iter_num) {
  MoFEMFunctionBegin;

  boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
      new PatchIntegFaceEle(mField, patchIntegNum));
  PostProcGauss->getRuleHook =
      feDomainRhsPtr->getRuleHook; // use the same integration rule as domainRhs

  CHKERR calculateHdivJacobian(PostProcGauss);

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>(valueField,
                                            commonDataPtr->mGradPtr));

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateHVecVectorField<3>(fluxField, commonDataPtr->mFluxPtr));
  PostProcGauss->getOpPtrVector().push_back(new OpErrorIndGrad<3>(
      commonDataPtr->mGradPtr, commonDataPtr->mFluxPtr,
      commonDataPtr->petscRefineErrorVec, CommonDataHDiv::ERROR_INDICATOR_GRAD,
      mField, "ERROR_INDICATOR_GRAD", -dummy_k));

  auto source_val_ptr = boost::make_shared<VectorDouble>();
  CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::get_val(
      PostProcGauss->getOpPtrVector(), mField, fluxField, source_val_ptr,
      dummy_k, Sev::inform);

  auto div_flux_ptr = boost::make_shared<VectorDouble>();
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateHdivVectorDivergence<3, 2>(fluxField, div_flux_ptr));

  PostProcGauss->getOpPtrVector().push_back(new OpErrorIndDiv(
      div_flux_ptr, source_val_ptr, commonDataPtr->petscRefineErrorVec,
      CommonDataHDiv::ERROR_INDICATOR_DIV, mField, "ERROR_INDICATOR_DIV"));

  // PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
  //     source_val_ptr, commonDataPtr->petscRefineErrorVec,
  //     CommonDataHDiv::ERROR_INDICATOR_DIV, div_flux_ptr));

  // PostProcGauss->getOpPtrVector().push_back(
  //     new OpScalarTags(div_flux_ptr, "ERROR_INDICATOR_DIV", mField));

  CHKERR postGetAnalyticalValuesHdiv(PostProcGauss);

  if (flgAnalytical) {

    auto kFunction = [&](const double, const double, const double) {
      return dummy_k;
    };

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(fluxField, commonDataPtr->mFluxPtr));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
        commonDataPtr->mValPtr, commonDataPtr->petscVec,
        CommonDataClassic::VALUE_ERROR, commonDataPtr->mValFuncPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OpScalarTags(commonDataPtr->mValFuncPtr, "ERROR_L2_NORM", mField));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
        commonDataPtr->mGradPtr, commonDataPtr->petscVec,
        CommonDataClassic::GRAD_ERROR, commonDataPtr->mGradFuncPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpVectorNormTags<2>(commonDataPtr->mGradFuncPtr,
                                                "ERROR_H1_SEMINORM", mField));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<3>(
        commonDataPtr->mFluxPtr, commonDataPtr->petscVec,
        CommonDataClassic::FLUX_ERROR, commonDataPtr->mFluxFuncPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpVectorNormTags<3>(commonDataPtr->mFluxFuncPtr,
                                                "ERROR_FLUX", mField));
  }

  PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
      commonDataPtr->petscVec, CommonDataClassic::GAUSS_COUNT));
  PostProcGauss->getOpPtrVector().push_back(
      new OpGetArea(commonDataPtr->petscVec, CommonDataClassic::VOL_E));

  CHKERR VecZeroEntries(commonDataPtr->petscVec);
  CHKERR VecZeroEntries(commonDataPtr->petscRefineErrorVec);

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

  CHKERR getJumps(iter_num);

  std::vector<Tag> tag_handles_calculated;
  enum tag_handles_calculated_enum {
    ORDER = 0,
    ERROR_INDICATOR_GRAD,
    ERROR_INDICATOR_DIV,
    ERROR_FLUX,
    JUMP_L2,
    ERROR_L2_NORM,
    ERROR_H1_SEMINORM,
    ERROR_ESTIMATOR,
    LAST_ELEMENT
  };

  tag_handles_calculated.resize(LAST_ELEMENT);
  CHKERR getTagHandle(mField, "ERROR_INDICATOR_GRAD", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_INDICATOR_GRAD]);
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER,
                      tag_handles_calculated[ORDER]);
  CHKERR getTagHandle(mField, "JUMP_L2", MB_TYPE_DOUBLE,
                      tag_handles_calculated[JUMP_L2]);
  CHKERR getTagHandle(mField, "ERROR_INDICATOR_DIV", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_INDICATOR_DIV]);

  CHKERR getTagHandle(mField, "ERROR_L2_NORM", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_L2_NORM]);
  CHKERR getTagHandle(mField, "ERROR_H1_SEMINORM", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_H1_SEMINORM]);
  CHKERR getTagHandle(mField, "ERROR_FLUX", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_FLUX]);

  CHKERR getTagHandle(mField, "ERROR_ESTIMATOR", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_ESTIMATOR]);

  double test_area = 0.;
  double error_estimator_iter = 0.;
  double err_indic_grad = 0.;
  double err_indic_div = 0.;
  double jump_l2 = 0.;

  for (auto ent : domainEntities) {

    CHKERR mField.get_moab().tag_get_data(
        tag_handles_calculated[ERROR_INDICATOR_GRAD], &ent, 1, &err_indic_grad);

    CHKERR mField.get_moab().tag_get_data(
        tag_handles_calculated[ERROR_INDICATOR_DIV], &ent, 1, &err_indic_div);

    CHKERR mField.get_moab().tag_get_data(tag_handles_calculated[JUMP_L2], &ent,
                                          1, &jump_l2);

    double error_estimator_ele =
        sqrt(err_indic_grad * err_indic_grad + err_indic_div * err_indic_div +
             jump_l2 * jump_l2);

    // CHKERR VecSetValue(commonDataPtr->petscRefineErrorVec,
    //                    CommonDataHDiv::ERROR_ESTIMATOR, error_estimator_ele,
    //                    ADD_VALUES);

    CHKERR mField.get_moab().tag_set_data(
        tag_handles_calculated[ERROR_ESTIMATOR], &ent, 1, &error_estimator_ele);
  }

  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);
  CHKERR VecAssemblyBegin(commonDataPtr->petscRefineErrorVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscRefineErrorVec);

  const double *array;
  CHKERR VecGetArrayRead(commonDataPtr->petscRefineErrorVec, &array);

  const double *array_classical;
  CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array_classical);

  if (mField.get_comm_rank() == 0) {

    jumpL2.push_back(std::sqrt(array[CommonDataHDiv::JUMP_L2]));
    jumpHdiv.push_back(std::sqrt(array[CommonDataHDiv::JUMP_HDIV]));
    errorIndicatorGrad.push_back(
        std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_GRAD]));
    errorIndicatorDiv.push_back(
        std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_DIV]));
    errorEstimator.push_back(std::sqrt(square(errorIndicatorGrad.back()) +
                                       square(errorIndicatorDiv.back()) +
                                       square(jumpL2.back())));

    MOFEM_LOG("WORLD", Sev::inform)
        << "Global error indicator grad: "
        << std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_GRAD]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global error indicator div: "
        << std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_DIV]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global jump of values: "
        << std::sqrt(array[CommonDataHDiv::JUMP_L2]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global jump of fluxes: "
        << std::sqrt(array[CommonDataHDiv::JUMP_HDIV]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global error estimators: " << errorEstimator.back();
    MOFEM_LOG("WORLD", Sev::inform) << "Total number of elements: "
                                    << (int)array[CommonDataHDiv::TOTAL_NUMBER];

    // errorIndicatorIntegral = array[CommonDataHDiv::ERROR_INDICATOR_GRAD];
    errorIndicatorIntegral = array[CommonDataHDiv::ERROR_INDICATOR_GRAD] +
                             array[CommonDataHDiv::ERROR_INDICATOR_DIV] +
                             array[CommonDataHDiv::JUMP_L2];
    totalElementNumber = (int)array[CommonDataHDiv::TOTAL_NUMBER];

    commonDataPtr->vOlume = array_classical[CommonDataClassic::VOL_E];
    commonDataPtr->rmsErr.push_back(
        sqrt((array_classical[CommonDataClassic::VALUE_ERROR]) /
             array_classical[CommonDataClassic::VOL_E]));
    commonDataPtr->gradErr.push_back(
        sqrt((array_classical[CommonDataClassic::GRAD_ERROR]) /
             array_classical[CommonDataClassic::VOL_E]));
    commonDataPtr->fluxErr.push_back(
        sqrt((array_classical[CommonDataClassic::FLUX_ERROR]) /
             array_classical[CommonDataClassic::VOL_E]));
    commonDataPtr->numberIntegrationPoints.push_back(
        array_classical[CommonDataClassic::GAUSS_COUNT]);

    MOFEM_LOG("WORLD", Sev::inform) << "order: " << oRder;
    MOFEM_LOG("WORLD", Sev::inform)
        << "gaussnum: " << array_classical[CommonDataClassic::GAUSS_COUNT];
    MOFEM_LOG("WORLD", Sev::inform)
        << "volume: " << array_classical[CommonDataClassic::VOL_E];
    MOFEM_LOG("WORLD", Sev::inform)
        << "L2norm: "
        << sqrt((array_classical[CommonDataClassic::VALUE_ERROR]) /
                array_classical[CommonDataClassic::VOL_E]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "H1seminorm: "
        << sqrt((array_classical[CommonDataClassic::GRAD_ERROR]) /
                array_classical[CommonDataClassic::VOL_E]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "fluxErr: "
        << sqrt((array_classical[CommonDataClassic::FLUX_ERROR]) /
                array_classical[CommonDataClassic::VOL_E]);
  }

  CHKERR VecRestoreArrayRead(commonDataPtr->petscRefineErrorVec, &array);
  CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array_classical);

  // if (flg_out_error == PETSC_TRUE) {
  ParallelComm *pcomm =
      ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
  if (pcomm == NULL)
    SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY, "Communicator not set");

  auto bit_level1 = BitRefLevel().set(meshRefinementCounter);
  auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
  CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
      bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);

  tag_handles_calculated.push_back(pcomm->part_tag());
  std::ostringstream strm;
  strm << "out_error_" << iter_num << ".h5m";
  CHKERR mField.get_moab().write_file(
      strm.str().c_str(), "MOAB", "PARALLEL=WRITE_PART",
      out_meshset_tri_ptr->get_ptr(), 1, tag_handles_calculated.data(),
      tag_handles_calculated.size());
  // }
  MoFEMFunctionReturn(0);
}
//! [Check error]

MoFEMErrorCode HDivDiffusionProblem::outputHdivVtkFields(int ii,
                                                         std::string vtk_name) {
  // default vtk_name = "out_result_"
  MoFEMFunctionBegin;

  // scale fields
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(
      1. / commonDataPtr->scaleQ, "Q");
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(
      1. / commonDataPtr->scaleV, "T");

  fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
      new PostProcFaceOnRefinedMesh(mField));

  // Generate post-processing mesh
  CHKERR fePostProcPtr->generateReferenceElementMesh();

  CHKERR calculateHdivJacobian(fePostProcPtr);
  // Postprocess only field values
  const Field_multiIndex *fields;
  mField.get_fields(&fields);
  for (auto &it : *fields)
    fePostProcPtr->addFieldValuesPostProc(it->getName());

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), fePostProcPtr);

  // write output
  CHKERR fePostProcPtr->writeFile(
      vtk_name + boost::lexical_cast<std::string>(ii) + ".h5m");

  // scale fields
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(commonDataPtr->scaleQ,
                                                      "Q");
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(commonDataPtr->scaleV,
                                                      "T");
  MoFEMFunctionReturn(0);
}

//! [Output results]
MoFEMErrorCode HDivDiffusionProblem::outputResults(int ii) {
  MoFEMFunctionBegin;

  CHKERR outputHdivVtkFields(ii);
  CHKERR checkError(ii);

  // add to the file capturing the end of the analysis "sumanalys.csv"
  if (mField.get_comm_rank() == 0) {
    std::ofstream sumanalysis;
    sumanalysis.open("sumanalys.csv", std::ofstream::out | std::ofstream::app);

    sumanalysis << oRder << ", "
                << commonDataPtr->numberIntegrationPoints.back() << ", "
                << commonDataPtr->rmsErr.size() << ", " << commonDataPtr->vOlume
                << ", " << 0.0 << ", " << 0.0 << ", " << errorEstimator.back()
                << ", " << commonDataPtr->rmsErr.back() << ", "
                << commonDataPtr->gradErr.back() << ", "
                << commonDataPtr->fluxErr.back() << ", "
                << orderRefinementCounter << ", " << errorIndicatorGrad.back()
                << ", " << errorIndicatorDiv.back() << ", " << jumpL2.back()
                << ", " << jumpHdiv.back() << ", " << totalElementNumber
                << std::endl;

    sumanalysis.close();
  }

  MoFEMFunctionReturn(0);
}
//! [Output results]

//! [Refine]
MoFEMErrorCode HDivDiffusionProblem::refineOrder() {
  MoFEMFunctionBegin;
  Tag th_error_ind, th_order;
  CHKERR getTagHandle(mField, "ERROR_ESTIMATOR", MB_TYPE_DOUBLE, th_error_ind);
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, th_order);

  // include all of entities with refined mesh

  auto bit_ref_mng = mField.getInterface<BitRefManager>();
  auto bit_level1 = BitRefLevel().set(meshRefinementCounter);
  Range refined_only_ents;
  if (refinementStyle == 3 && meshRefinementCounter > 0)
    CHKERR bit_ref_mng->getEntitiesByDimAndRefLevel(bit_level1, bit_level1, 2,
                                                    refined_only_ents);

  std::vector<Range> refinement_levels;
  refinement_levels.resize(refIterNum + 1);
  for (auto &ent : domainEntities) {
    double err_indic = 0;
    CHKERR mField.get_moab().tag_get_data(th_error_ind, &ent, 1, &err_indic);
    int order, new_order;
    CHKERR mField.get_moab().tag_get_data(th_order, &ent, 1, &order);
    new_order = order + 1;
    Range refined_ents;
    if ((err_indic >
         refControl * sqrt(errorIndicatorIntegral / totalElementNumber))) {
      refined_ents.insert(ent);
      Range adj;
      CHKERR mField.get_moab().get_adjacencies(&ent, 1, 1, false, adj,
                                               moab::Interface::UNION);
      refined_ents.merge(adj);
      refinement_levels[new_order - oRder].merge(refined_ents);
      CHKERR mField.get_moab().tag_set_data(th_order, &ent, 1, &new_order);
    }
  }

  Range entities_to_check;
  Range surrounding_edges;
  // for (int ll = 1; ll < refinement_levels.size(); ll++) {
  for (int ll = refinement_levels.size() - 1; ll > 0; ll--) {
    int order_to_compare = oRder + ll;
    int new_order = order_to_compare - 1;
    surrounding_edges.merge(refinement_levels[ll]);
    Range adj_faces;
    CHKERR mField.get_moab().get_adjacencies(surrounding_edges, 2, false,
                                             adj_faces, moab::Interface::UNION);
    entities_to_check.merge(adj_faces);
    Range adj_edges;
    CHKERR mField.get_moab().get_adjacencies(entities_to_check, 1, false,
                                             adj_edges, moab::Interface::UNION);
    surrounding_edges.merge(adj_edges);
    entities_to_check.merge(adj_faces);

    for (auto &ent : entities_to_check) {
      int order;
      CHKERR mField.get_moab().tag_get_data(th_order, &ent, 1, &order);

      if (order < order_to_compare - 1) {
        refinement_levels[ll - 1].insert(ent);
        CHKERR mField.get_moab().tag_set_data(th_order, &ent, 1, &new_order);
      }
    }
  }

  for (int ll = 1; ll < refinement_levels.size(); ll++) {
    CHKERR mField.getInterface<CommInterface>()->synchroniseEntities(
        refinement_levels[ll]);
    CHKERR mField.set_field_order(refinement_levels[ll], fluxField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], valueField,
                                  oRder + ll);
  }

  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      fluxField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      valueField);
  CHKERR mField.build_fields();
  CHKERR mField.build_finite_elements();
  CHKERR mField.build_adjacencies(simpleInterface->getBitRefLevel());
  mField.getInterface<ProblemsManager>()->buildProblemFromFields = PETSC_TRUE;
  CHKERR DMSetUp_MoFEM(simpleInterface->getDM());
  orderRefinementCounter += 1;

  MoFEMFunctionReturn(0);
}
//! [Refine]

MoFEMErrorCode HDivDiffusionProblem::checkRefinement() {
  MoFEMFunctionBegin;

  if (errorEstimator.size() < 2)
    MoFEMFunctionReturnHot(0);

  PetscBool test = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-test", &test, PETSC_NULL);
  if (mField.get_comm_rank() == 0 && test) {

    if (errorIndicatorGrad.back() > errorIndicatorGrad[0]) {
      std::cout << "Error estimator did not decrease with refinement: "
                << errorIndicatorGrad.back() << " > " << errorIndicatorGrad[0]
                << std::endl;
      SETERRQ(PETSC_COMM_SELF, 1,
              "Test failed: error estimator did not decrease with refinement");
    }

    if (commonDataPtr->rmsErr.back() > commonDataPtr->rmsErr[0]) {
      std::cout << "Value error did not decrease with refinement: "
                << commonDataPtr->rmsErr.back() << " > "
                << commonDataPtr->rmsErr[0] << std::endl;
      SETERRQ(PETSC_COMM_SELF, 1,
              "Test failed: value error did not decrease with refinement");
    }

    if (commonDataPtr->fluxErr.back() > commonDataPtr->fluxErr[0]) {
      std::cout << "Flux error did not decrease with refinement: "
                << commonDataPtr->fluxErr.back() << " > "
                << commonDataPtr->fluxErr[0] << std::endl;
      SETERRQ(PETSC_COMM_SELF, 1,
              "Test failed: flux error did not decrease with refinement");
    }
  }

  MoFEMFunctionReturn(0);
}

//! [OpErrorIndGrad]
template <int N>
MoFEMErrorCode HDivDiffusionProblem::OpErrorIndGrad<N>::doWork(int side,
                                                               EntityType type,
                                                               EntData &data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();
  auto t_grad = getFTensor1FromMat<2>(*gradPtr);
  auto t_field = getFTensor1FromMat<N>(*fieldPtr);
  FTensor::Tensor1<double, 2> t_diff;
  FTensor::Index<'i', 2> i;

  double error_ind = 0;
  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    const double alpha = t_w * area;

    t_diff(i) = t_grad(i) - 1 / K * t_field(i);
    error_ind += alpha * t_diff(i) * t_diff(i);

    ++t_w;
    ++t_grad;
    ++t_field;
  }

  double sqrt_error_ind = sqrt(error_ind);

  const EntityHandle ent = getFEEntityHandle();
  Tag th_error_ind;
  CHKERR ClassicDiffusionProblem::getTagHandle(mField, tagName.c_str(),
                                               MB_TYPE_DOUBLE, th_error_ind);
  CHKERR mField.get_moab().tag_set_data(th_error_ind, &ent, 1, &sqrt_error_ind);

  // add to dataVec at iNdex position
  CHKERR VecSetValue(dataVec, iNdex, error_ind, ADD_VALUES);

  // add to dataVec at iNdex position
  CHKERR VecSetValue(dataVec, CommonDataHDiv::TOTAL_NUMBER, 1., ADD_VALUES);

  MoFEMFunctionReturn(0);
}
//! [OpErrorIndGrad]

//! [OpErrorIndDiv]
MoFEMErrorCode HDivDiffusionProblem::OpErrorIndDiv::doWork(int side,
                                                           EntityType type,
                                                           EntData &data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();
  // representative length of the element
  double length_repr = sqrt(area);

  auto t_div = getFTensor0FromVec(*divPtr);
  auto t_source = getFTensor0FromVec(*sourcePtr);

  double error_ind_div = 0;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    const double alpha = t_w * area;

    // // if t_source is a pointer to nothing, set to 0
    // if (t_source == nullptr) {
    //   t_source = new double(0);
    // }

    // $h ||s - \nabla \cdot \mathbf q||_{\Omega_e}^2$
    error_ind_div += alpha * square(t_div - t_source) * length_repr;

    ++t_w;
    ++t_div;
    ++t_source;
  }

  double sqrt_error_ind = sqrt(error_ind_div);

  const EntityHandle ent = getFEEntityHandle();
  Tag th_error_ind;
  CHKERR ClassicDiffusionProblem::getTagHandle(mField, tagName.c_str(),
                                               MB_TYPE_DOUBLE, th_error_ind);
  CHKERR mField.get_moab().tag_set_data(th_error_ind, &ent, 1, &sqrt_error_ind);

  // add to dataVec at iNdex position
  CHKERR VecSetValue(dataVec, iNdex, error_ind_div, ADD_VALUES);

  MoFEMFunctionReturn(0);
}
//! [OpErrorIndDiv]
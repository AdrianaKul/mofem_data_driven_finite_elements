#include <OperatorsForDD.hpp>

template struct OperatorsForDD::OpVectorNormTags<2>;
template struct OperatorsForDD::OpVectorNormTags<3>;
template struct OperatorsForDD::OpL2StarRhs<2>;
template struct OperatorsForDD::OpL2StarRhs<3>;
template struct OperatorsForDD::OpHdivStarRhs<2>;
template struct OperatorsForDD::OpHdivStarRhs<3>;
template struct OperatorsForDD::OpHdivNormalBoundaryStarRhs<2>;
template struct OperatorsForDD::OpHdivNormalBoundaryStarRhs<3>;
template struct OperatorsForDD::PerturbateField<2>;
template struct OperatorsForDD::OpFindData4D<2>;
template struct OperatorsForDD::OpFindData4D<3>;
template struct OperatorsForDD::OpFindData5D<2>;
template struct OperatorsForDD::OpFindData5D<3>;
template struct OperatorsForDD::OpFindClosest<2>;
template struct OperatorsForDD::OpFindClosest<3>;
template struct OperatorsForDD::OpFindKstar<2>;
template struct OperatorsForDD::OpFindKstar<3>;
template struct OperatorsForDD::OpPostProcSaveCsvDatasetGQ<2, 2>;
template struct OperatorsForDD::OpPostProcSaveCsvDatasetGQ<2, 3>;
template struct OperatorsForDD::OpPostProcSaveCsvDatasetGQ<3, 3>;
template struct OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<2, 2>;
template struct OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<2, 3>;
template struct OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<3, 3>;

MoFEMErrorCode
OperatorsForDD::OpScalarToField::doWork(int side, EntityType type,
                                        EntitiesFieldData::EntData &data) {
  MoFEMFunctionBegin;

  const size_t nb_dofs = data.getIndices().size();

  // if (data.getFieldData().size()) {
  if (type == MBVERTEX) {
    // const EntityHandle ent = getFEEntityHandle();
    const int nb_integration_pts = getGaussPts().size2();
    // const double area = getMeasure();
    // auto t_w = getFTensor0IntegrationWeight();

    data.getFieldData() = *dataPtr;

    // auto t_val = getFTensor0FromVec(*dataPtr);

    // auto test = data.getFieldData();

    // for (int gg = 0; gg != nb_integration_pts; ++gg) {

    //   // val_norm += t_val * t_val * t_w;

    //   ++t_val;
    // }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
OperatorsForDD::OpScalarVarTags::doWork(int side, EntityType type,
                                        EntitiesFieldData::EntData &data) {
  MoFEMFunctionBegin;

  const EntityHandle ent = getFEEntityHandle();
  const int nb_integration_pts = getGaussPts().size2();
  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();

  auto t_val = getFTensor0FromVec(*dataPtr);

  auto val_ave = sum(*dataPtr) / (nb_integration_pts);

  double difference_squared = 0;
  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    difference_squared += square(t_val - val_ave);
    ++t_val;
  }
  auto val_variance = sqrt(difference_squared / nb_integration_pts);

  Tag th_data_ave, th_data_var;
  double val_norm = 0.0;
  CHKERR mField.get_moab().tag_get_handle(
      tagAveName.c_str(), 1, MB_TYPE_DOUBLE, th_data_ave,
      MB_TAG_CREAT | MB_TAG_SPARSE, &val_norm);

  CHKERR mField.get_moab().tag_get_handle(
      tagVarName.c_str(), 1, MB_TYPE_DOUBLE, th_data_var,
      MB_TAG_CREAT | MB_TAG_SPARSE, &val_norm);

  CHKERR mField.get_moab().tag_set_data(th_data_ave, &ent, 1, &val_ave);
  CHKERR mField.get_moab().tag_set_data(th_data_var, &ent, 1, &val_variance);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
OperatorsForDD::OpScalarTags::doWork(int side, EntityType type,
                                     EntitiesFieldData::EntData &data) {
  MoFEMFunctionBegin;

  const EntityHandle ent = getFEEntityHandle();
  const int nb_integration_pts = getGaussPts().size2();
  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();

  auto t_val = getFTensor0FromVec(*dataPtr);

  Tag th_data;
  double val_norm = 0.0;
  CHKERR mField.get_moab().tag_get_handle(tagName.c_str(), 1, MB_TYPE_DOUBLE,
                                          th_data, MB_TAG_CREAT | MB_TAG_SPARSE,
                                          &val_norm);
  val_norm = 0.0;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    val_norm += t_val * t_val * t_w;

    ++t_val;
    ++t_w;
  }

  val_norm = sqrt(val_norm * area);

  CHKERR mField.get_moab().tag_set_data(th_data, &ent, 1, &val_norm);

  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode
OperatorsForDD::OpVectorNormTags<N>::doWork(int side, EntityType type,
                                            EntitiesFieldData::EntData &data) {
  MoFEMFunctionBegin;

  FTensor::Index<'i', N> i;

  const EntityHandle ent = getFEEntityHandle();
  const int nb_integration_pts = getGaussPts().size2();
  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();

  // get vector values
  auto t_data = getFTensor1FromMat<N>(*dataPtr);

  Tag th_data;
  double val_norm = 0.0;
  CHKERR mField.get_moab().tag_get_handle(tagName.c_str(), 1, MB_TYPE_DOUBLE,
                                          th_data, MB_TAG_CREAT | MB_TAG_SPARSE,
                                          &val_norm);
  val_norm = 0.0;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    val_norm += t_w * (t_data(i) * t_data(i));
    ++t_data;
    ++t_w;
  }

  val_norm = sqrt(val_norm * area);

  CHKERR mField.get_moab().tag_set_data(th_data, &ent, 1, &val_norm);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpGradStarRhs::doWork(int side, EntityType type,
                                                     EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = data.getN().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();
    // get coordinates of the integration point
    auto t_coords = getFTensor1CoordsAtGaussPts();

    // get base functions
    auto t_diff_base = data.getFTensor1DiffN<2>();
    auto t_star = getFTensor1FromMat<2>(*(starPtr));

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      const double a = t_w * area;

      auto s_f = sFunc(t_coords(0), t_coords(1), t_coords(2));

      int rr = 0;
      for (; rr != nb_dofs; rr++) {
        locRhs[rr] += t_diff_base(i) * a * s_f * t_star(i);
        // move to the next base function
        ++t_diff_base;
      }
      for (; rr != nb_base_functions; ++rr)
        ++t_diff_base;

      ++t_star;
      // move to the weight of the next integration point
      ++t_w;
      // move to the coordinates of the next integration point
      ++t_coords;
    }

    CHKERR VecSetValues<EssentialBcStorage>(getKSPf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode OperatorsForDD::OpL2StarRhs<N>::doWork(int side, EntityType type,
                                                      EntData &data) {
  MoFEMFunctionBegin;

  const size_t nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    const size_t nb_base_functions = data.getN().size2();
    if (2 * nb_base_functions < nb_dofs)
      SETERRQ(
          PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
          "Number of DOFs is larger than number of base functions on entity");

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    const auto nb_integration_points = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    // get coordinates of the integration point
    auto t_coords = getFTensor1CoordsAtGaussPts();
    auto t_base = data.getFTensor0N();
    auto t_star = getFTensor1FromMat<N>(*(starPtr));

    for (size_t gg = 0; gg != nb_integration_points; ++gg) {

      double alpha = getMeasure() * t_w;
      auto s_f = sFunc(t_coords(0), t_coords(1), t_coords(2));

      FTensor::Tensor1<FTensor::PackPtr<double *, 2>, 2> t_nf{&locRhs[0],
                                                              &locRhs[1]};

      size_t bb = 0;
      for (; bb != nb_dofs / 2; ++bb) {
        t_nf(i) += alpha * t_base * s_f * t_star(i);
        ++t_nf;
        ++t_base;
      }

      for (; bb != nb_base_functions; ++bb)
        ++t_base;

      ++t_star;
      ++t_w;
      // move to the coordinates of the next integration point
      ++t_coords;
    }

    // FILL VALUES OF THE GLOBAL VECTOR ENTRIES FROM THE LOCAL ONES
    CHKERR VecSetValues<EssentialBcStorage>(getKSPf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::Op1dStarRhs::doWork(int side, EntityType type,
                                                   EntData &data) {
  MoFEMFunctionBegin;

  const size_t nb_dofs = data.getIndices().size();

  if (nb_dofs) {

    const size_t nb_base_functions = data.getN().size2();

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    const auto nb_integration_points = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    // get coordinates of the integration point
    auto t_coords = getFTensor1CoordsAtGaussPts();
    auto t_base = data.getFTensor0N();

    auto t_star = getFTensor0FromVec(*(starPtr));

    for (size_t gg = 0; gg != nb_integration_points; ++gg) {

      double alpha = getMeasure() * t_w;
      auto s_f = sFunc(t_coords(0), t_coords(1), t_coords(2));

      size_t bb = 0;
      for (; bb != nb_dofs; ++bb) {
        locRhs[bb] += alpha * t_base * s_f * t_star;
        ++t_base;
      }

      for (; bb != nb_base_functions; ++bb)
        ++t_base;

      ++t_star;
      ++t_w;
      // move to the coordinates of the next integration point
      ++t_coords;
    }

    // FILL VALUES OF THE GLOBAL VECTOR ENTRIES FROM THE LOCAL ONES
    CHKERR VecSetValues<EssentialBcStorage>(getKSPf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode OperatorsForDD::OpHdivStarRhs<N>::doWork(int side,
                                                        EntityType type,
                                                        EntData &data) {
  MoFEMFunctionBegin;

  const size_t nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    const size_t nb_base_functions = data.getN().size2() / 3;
    if (2 * nb_base_functions < nb_dofs)
      SETERRQ(
          PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
          "Number of DOFs is larger than number of base functions on entity");

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    const auto nb_integration_points = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    // get coordinates of the integration point
    auto t_coords = getFTensor1CoordsAtGaussPts();
    auto t_base = data.getFTensor1N<3>();
    auto t_star = getFTensor1FromMat<N>(*(starPtr));

    for (size_t gg = 0; gg != nb_integration_points; ++gg) {

      double alpha = getMeasure() * t_w;
      auto s_f = sFunc(t_coords(0), t_coords(1), t_coords(2));

      size_t bb = 0;
      for (; bb != nb_dofs; ++bb) {
        locRhs[bb] += alpha * s_f * t_base(i) * t_star(i);
        ++t_base;
      }

      for (; bb != nb_base_functions; ++bb)
        ++t_base;

      ++t_star;
      ++t_w;
      // move to the coordinates of the next integration point
      ++t_coords;
    }

    // FILL VALUES OF THE GLOBAL VECTOR ENTRIES FROM THE LOCAL ONES
    CHKERR VecSetValues<EssentialBcStorage>(getKSPf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpMixHdivTimesVecLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = row_data.getN().size2() / 3;
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on row
    auto t_row_base = row_data.getFTensor1N<3>();

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;

      auto s_f = sFunc(0, 0, 0);

      int rr = 0;
      for (; rr != nb_row_dofs; ++rr) {
        // get derivatives of base functions on column
        auto t_col_base = col_data.getFTensor0N(gg, 0);

        for (int cc = 0; cc != nb_col_dofs / 2; ++cc) {

          FTensor::Tensor1<double *, 2> t_a{&locLhs(rr, 2 * cc),
                                            &locLhs(rr, 2 * cc + 1)};

          t_a(i) += t_row_base(i) * t_col_base * a * s_f;

          // move to the derivatives of the next base functions on column
          ++t_col_base;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_base;

      // move to the weight of the next integration point
      ++t_w;
    }
    // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX
    if (!onlyTransform)
      CHKERR MatSetValues<EssentialBcStorage>(
          getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
    if (tRansform) {
      MatrixDouble a_loc_trans = trans(locLhs);
      CHKERR MatSetValues<EssentialBcStorage>(getKSPB(), col_data, row_data,
                                              &*a_loc_trans.data().begin(),
                                              ADD_VALUES);
    }
  }

  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode OperatorsForDD::OpHdivNormalBoundaryStarRhs<N>::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;

  const size_t nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'m', N> m;
    FTensor::Tensor1<double, 3> t_z{0., 0., 1.};

    const size_t nb_base_functions = data.getN().size2() / 3;
    if (2 * nb_base_functions < nb_dofs)
      SETERRQ(
          PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
          "Number of DOFs is larger than number of base functions on entity");

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    const auto nb_integration_points = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    // get coordinates of the integration point
    auto t_coords = getFTensor1CoordsAtGaussPts();
    auto t_base = data.getFTensor1N<3>();
    auto t_star = getFTensor1FromMat<N>(*(starPtr));

    // get tangent
    auto t_tangent = getFTensor1TangentAtGaussPts();

    for (size_t gg = 0; gg != nb_integration_points; ++gg) {

      FTensor::Tensor1<double, 3> t_normal;
      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Index<'k', 3> k;
      t_normal(i) = FTensor::levi_civita(i, j, k) * t_tangent(j) * t_z(k);

      auto length_normal = sqrt(t_normal(i) * t_normal(i));

      double alpha = getMeasure() * t_w;
      auto s_f = sFunc(t_coords(0), t_coords(1), t_coords(2));

      size_t bb = 0;
      for (; bb != nb_dofs; ++bb) {
        locRhs[bb] += alpha * s_f * t_base(m) * t_star(m);
        ++t_base;
      }

      for (; bb != nb_base_functions; ++bb)
        ++t_base;

      ++t_star;
      ++t_w;
      // move to the coordinates of the next integration point
      ++t_coords;
      ++t_tangent;
    }

    // FILL VALUES OF THE GLOBAL VECTOR ENTRIES FROM THE LOCAL ONES
    CHKERR VecSetValues<EssentialBcStorage>(getKSPf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

// peturbating fields
template <int N>
MoFEMErrorCode OperatorsForDD::PerturbateField<N>::doWork(int side,
                                                          EntityType type,
                                                          EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {
    FTensor::Index<'i', 2> i;

    const size_t nb_gauss_pts = getGaussPts().size2();
    starPtr->resize(2, nb_gauss_pts);
    starPtr->clear();

    auto t_peturbation = getFTensor1FromMat<2>(*(starPtr));
    auto t_point_distance = getFTensor0FromVec(*(pointDistancePtr));

    // boost random
    boost::random::mt11213b gen; // generator
    gen.seed(static_cast<unsigned int>(std::time(0)));
    boost::random::normal_distribution<> noise(
        0, t_point_distance); // distribution normal

    for (size_t gg = 0; gg != nb_gauss_pts; ++gg) {

      t_peturbation(0) = perturbMultiplier * noise(gen);
      t_peturbation(1) = perturbMultiplier * noise(gen);

      ++t_peturbation;
      ++t_point_distance;
    }
  }
  MoFEMFunctionReturn(0);
}

// searching the data set
template <int N>
MoFEMErrorCode OperatorsForDD::OpFindData4D<N>::doWork(int side,
                                                       EntityType type,
                                                       EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    const size_t nb_gauss_pts = getGaussPts().size2();
    searchData.mGradStarPtr->resize(2, nb_gauss_pts);
    searchData.mGradStarPtr->clear();
    searchData.mFluxStarPtr->resize(2, nb_gauss_pts);
    searchData.mFluxStarPtr->clear();
    searchData.pointDistance->resize(nb_gauss_pts);
    searchData.pointDistance->clear();

    auto t_grad = getFTensor1FromMat<2>(*(searchData.mGradPtr));
    auto t_flux = getFTensor1FromMat<N>(*(searchData.mFluxPtr));
    auto t_grad_star = getFTensor1FromMat<2>(*(searchData.mGradStarPtr));
    auto t_flux_star = getFTensor1FromMat<2>(*(searchData.mFluxStarPtr));
    auto t_point_distance = getFTensor0FromVec(*(searchData.pointDistance));

    for (size_t gg = 0; gg != nb_gauss_pts; ++gg) {

      // data search and allocation
      point4D point_found;
      if (!searchData.flgPointAverage)
        point_found = RtreeManipulation::returnNearestAndDistance(
            RtreeManipulation::convertToPoint(
                t_grad(0), t_grad(1), t_flux(0) / sCale, t_flux(1) / sCale),
            t_point_distance, searchData.rtree4D);
      if (searchData.flgPointAverage)
        point_found = RtreeManipulation::returnAveAndDistance(
            RtreeManipulation::convertToPoint(
                t_grad(0), t_grad(1), t_flux(0) / sCale, t_flux(1) / sCale),
            t_point_distance, searchData.rtree4D, searchData.pointAverage);

      t_grad_star(0) = point_found.get<0>();
      t_grad_star(1) = point_found.get<1>();
      t_flux_star(0) = point_found.get<2>();
      t_flux_star(1) = point_found.get<3>();

      ++t_grad;
      ++t_flux;
      ++t_grad_star;
      ++t_flux_star;
      ++t_point_distance;
    }
  }
  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode OperatorsForDD::OpFindData5D<N>::doWork(int side,
                                                       EntityType type,
                                                       EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    const size_t nb_gauss_pts = getGaussPts().size2();
    searchData.mValStarPtr->resize(nb_gauss_pts);
    searchData.mGradStarPtr->resize(2, nb_gauss_pts);
    searchData.mFluxStarPtr->resize(2, nb_gauss_pts);
    searchData.pointDistance->resize(nb_gauss_pts);

    auto t_val = getFTensor0FromVec(*(searchData.mValPtr));
    auto t_grad = getFTensor1FromMat<2>(*(searchData.mGradPtr));
    auto t_flux = getFTensor1FromMat<N>(*(searchData.mFluxPtr));
    auto t_val_star = getFTensor0FromVec(*(searchData.mValStarPtr));
    auto t_grad_star = getFTensor1FromMat<2>(*(searchData.mGradStarPtr));
    auto t_flux_star = getFTensor1FromMat<2>(*(searchData.mFluxStarPtr));
    auto t_point_distance = getFTensor0FromVec(*(searchData.pointDistance));

    for (size_t gg = 0; gg != nb_gauss_pts; ++gg) {

      // data search and allocation
      point5D point_found;
      // if (!searchData.flgPointAverage)
      point_found = RtreeManipulation::returnNearestAndDistance(
          RtreeManipulation::convertToPoint(
              t_grad(0), t_grad(1), t_flux(0) * scaleFlux,
              t_flux(1) * scaleFlux, t_val * scaleVal),
          t_point_distance, searchData.rtree5D);
      // if (searchData.flgPointAverage)
      //   point_found = RtreeManipulation::returnAveAndDistance(
      //       RtreeManipulation::convertToPoint(
      //           t_grad(0), t_grad(1), t_flux(0) / sCale, t_flux(1) / sCale),
      //       t_point_distance, searchData.rtree4D, searchData.pointAverage);

      t_grad_star(0) = point_found.get<0>();
      t_grad_star(1) = point_found.get<1>();
      t_flux_star(0) = point_found.get<2>();
      t_flux_star(1) = point_found.get<3>();
      t_val_star = point_found.get<4>();

      ++t_val;
      ++t_grad;
      ++t_flux;
      ++t_val_star;
      ++t_grad_star;
      ++t_flux_star;
      ++t_point_distance;
    }
  }
  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode OperatorsForDD::OpFindClosest<N>::doWork(int side,
                                                        EntityType type,
                                                        EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;

    const size_t nb_gauss_pts = getGaussPts().size2();
    searchData.mGradStarPtr->resize(2, nb_gauss_pts);
    searchData.mGradStarPtr->clear();
    searchData.mFluxStarPtr->resize(2, nb_gauss_pts);
    searchData.mFluxStarPtr->clear();
    searchData.pointDistance->resize(nb_gauss_pts);
    searchData.pointDistance->clear();

    auto t_grad = getFTensor1FromMat<2>(*(searchData.mGradPtr));
    auto t_flux = getFTensor1FromMat<N>(*(searchData.mFluxPtr));
    auto t_grad_star = getFTensor1FromMat<2>(*(searchData.mGradStarPtr));
    auto t_flux_star = getFTensor1FromMat<2>(*(searchData.mFluxStarPtr));
    auto t_point_distance = getFTensor0FromVec(*(searchData.pointDistance));

    for (size_t gg = 0; gg != nb_gauss_pts; ++gg) {

      double k = searchData.linear_k;

      t_grad_star(0) = (t_grad(0) - k * t_flux(0)) / (square(k) + 1.);
      t_grad_star(1) = (t_grad(1) - k * t_flux(1)) / (square(k) + 1.);

      t_flux_star(0) = k * (k * t_flux(0) - t_grad(0)) / (square(k) + 1.);
      t_flux_star(1) = k * (k * t_flux(1) - t_grad(1)) / (square(k) + 1.);

      double test_dist = sqrt(
          square(std::abs(k * t_grad(0) + t_flux(0)) / sqrt(square(k) + 1.)) +
          square(std::abs(k * t_grad(1) + t_flux(1)) / sqrt(square(k) + 1.)));

      t_point_distance = sqrt(square(t_grad_star(0) - t_grad(0)) +
                              square(t_grad_star(1) - t_grad(1)) +
                              square(t_flux_star(0) - t_flux(0)) +
                              square(t_flux_star(1) - t_flux(1)));

      // double t_dist_1 = t_point_distance;

      // FTensor::Tensor1<double, 2> t_a{k, k};

      // // create FTensor poiters to the common data sorted by x and y
      // FTensor::Tensor1<double, 2> t_x{t_grad(0), t_flux(0) / sCale};
      // FTensor::Tensor1<FTensor::PackPtr<double *, 2>, 2> t_x_star{
      //     &t_grad_star(0), &t_flux_star(0)};

      // FTensor::Tensor1<double, 2> t_y{t_grad(1), t_flux(1) / sCale};
      // FTensor::Tensor1<FTensor::PackPtr<double *, 2>, 2> t_y_star{
      //     &t_grad_star(1), &t_flux_star(1)};

      // // calculate closest points
      // t_x_star(j) = t_x(j) - (t_x(i) * t_a(i) / (t_a(i) * t_a(i))) * t_a(j);
      // t_y_star(j) = t_y(j) - (t_y(i) * t_a(i) / (t_a(i) * t_a(i))) * t_a(j);

      // t_point_distance = (square(t_grad_star(0) - t_grad(0)) +
      //                         square(t_grad_star(1) - t_grad(1)) +
      //                         square(t_flux_star(0) - t_flux(0)) +
      //                         square(t_flux_star(1) - t_flux(1)));

      // double t_dist_2 = t_point_distance;

      // if (std::abs(t_dist_1 - t_dist_2) > 0.0000000001)
      //   std::cout << "something's wrong with the distance calcs" <<
      //   std::endl;

      if (std::abs(test_dist - t_point_distance) > 0.000000001)
        std::cout << "something's wrong with the distance calcs" << std::endl;

      ++t_grad;
      ++t_flux;
      ++t_grad_star;
      ++t_flux_star;
      ++t_point_distance;
    }
  }
  MoFEMFunctionReturn(0);
}

// searching the data set
template <int N>
MoFEMErrorCode OperatorsForDD::OpFindKstar<N>::doWork(int side, EntityType type,
                                                      EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;

    int number_of_search_points = 20;
    std::vector<double> k_star;
    std::vector<double> correlation;
    k_star.resize(2);
    correlation.resize(2);

    const size_t nb_gauss_pts = getGaussPts().size2();
    searchData.mKStarPtr->resize(3 * 3, nb_gauss_pts);
    searchData.mKStarPtr->clear();
    searchData.mKStarCorrelationPtr->resize(2, nb_gauss_pts);
    searchData.mKStarCorrelationPtr->clear();

    auto t_val = getFTensor0FromVec(*(searchData.mValPtr));
    auto t_grad = getFTensor1FromMat<2>(*(searchData.mGradPtr));
    auto t_flux = getFTensor1FromMat<N>(*(searchData.mFluxPtr));
    auto t_k_star = getFTensor2FromMat<3, 3>(*(searchData.mKStarPtr));
    // auto t_k_correlation =
    //     getFTensor1FromMat<2>(*(searchData.mKStarCorrelationPtr));

    for (size_t gg = 0; gg != nb_gauss_pts; ++gg) {

      MatrixDouble test(2, 2);

      if (searchData.rtree4D.size()) {
        std::vector<point4D> n_points = RtreeManipulation::returnNearestPoints(
            RtreeManipulation::convertToPoint(t_grad(0), t_grad(1), t_flux(0),
                                              t_flux(1)),
            searchData.rtree4D, number_of_search_points);
        // RtreeManipulation::calcKstar(n_points, k_star, correlation);
        RtreeManipulation::getGradient_new(n_points, test);
      } else if (searchData.rtree5D.size()) {
        std::vector<point5D> n_points = RtreeManipulation::returnNearestPoints(
            RtreeManipulation::convertToPoint(t_grad(0), t_grad(1), t_flux(0),
                                              t_flux(1), t_val * sCale),
            searchData.rtree5D, number_of_search_points);
        // RtreeManipulation::calcKstar(n_points, k_star, correlation);
        RtreeManipulation::getGradient_new(n_points, test);
      } else {
        // std::cout << "no rtree found" << std::endl;
        test(0, 0) = -1.0;
        test(1, 1) = -1.0;
        test(1, 0) = 0.0;
        test(0, 1) = 0.0;
      }
      FTensor::Tensor2<double, 2, 2> t_tangent{test(0, 0), test(1, 0),
                                               test(0, 1), test(1, 1)};

      t_k_star(i, j) = t_tangent(i, j);
      // std::cout << t_k_star << std::endl;

      ++t_val;
      ++t_grad;
      ++t_flux;
      ++t_k_star;
      // ++t_k_correlation;
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpGstarQLhs::doWork(int row_side, int col_side,
                                                   EntityType row_type,
                                                   EntityType col_type,
                                                   EntData &row_data,
                                                   EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  FTensor::Index<'i', 2> i;
  FTensor::Index<'j', 2> j;
  FTensor::Index<'k', 2> k;

  if (nb_row_dofs && nb_col_dofs) {

    const size_t nb_base_functions = row_data.getN().size2();
    if (2 * nb_base_functions < nb_row_dofs)
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Number of DOFs is larger than number of base functions on "
              "entity");

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on row
    auto t_row_base = row_data.getFTensor0N();

    // get t_k_star
    auto t_k_star = getFTensor2FromMat<3, 3>(*(kStarPtr));

    auto s_f = sFunc(0, 0, 0);

    FTensor::Tensor2<double, 2, 2> t_identity{1.0, 0.0, 0.0, 1.0};

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;
      auto k_star_squared = t_k_star(i, j) * t_k_star(j, i);

      int rr = 0;
      for (; rr != nb_row_dofs / 2; ++rr) {

        // get derivatives of base functions on column
        auto t_col_diff_base = col_data.getFTensor1DiffN<2>(gg, 0);

        // get derivatives of base functions on row
        auto t_col_base = col_data.getFTensor0N(gg, 0);

        FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 2> t_a{
            &locLhs(2 * rr, 0), &locLhs(2 * rr + 1, 0)};

        for (int cc = 0; cc != nb_col_dofs; cc++) {

          // std::cout << -t_k_star(i, j) * t_identity(j, i) /
          //                  (k_star_squared + 2.)
          //           << std::endl;

          t_a(k) += t_k_star(i, j) * t_identity(j, i) / (k_star_squared + 2.) *
                    t_row_base * t_col_diff_base(k) * a * s_f;

          // move to the derivatives of the next base functions on column
          ++t_col_diff_base;
          ++t_a;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_base;

      // move to the weight of the next integration point
      ++t_w;
      ++t_k_star;
    }

    if (!onlyTransform)
      CHKERR MatSetValues<EssentialBcStorage>(
          getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
    if (tRansform) {
      MatrixDouble a_loc_trans = trans(locLhs);
      CHKERR MatSetValues<EssentialBcStorage>(getKSPB(), col_data, row_data,
                                              &*a_loc_trans.data().begin(),
                                              ADD_VALUES);
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpQstarQLhs::doWork(int row_side, int col_side,
                                                   EntityType row_type,
                                                   EntityType col_type,
                                                   EntData &row_data,
                                                   EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  FTensor::Index<'i', 2> i;
  FTensor::Index<'j', 2> j;
  const int FIELD_DIM = 2;

  if (nb_row_dofs && nb_col_dofs) {

    const size_t nb_base_functions = row_data.getN().size2();
    if (2 * nb_base_functions < nb_row_dofs)
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Number of DOFs is larger than number of base functions on "
              "entity");

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on row
    auto t_row_base = row_data.getFTensor0N();

    // get t_k_star
    auto t_k_star = getFTensor2FromMat<3, 3>(*(kStarPtr));

    auto s_f = sFunc(0, 0, 0);

    auto get_t_vec = [&](const int rr) {
      std::array<double *, FIELD_DIM> ptrs;
      for (auto i = 0; i != FIELD_DIM; ++i)
        ptrs[i] = &locLhs(rr + i, i);
      return FTensor::Tensor1<FTensor::PackPtr<double *, FIELD_DIM>, FIELD_DIM>(
          ptrs);
    };

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;
      auto k_star_squared = t_k_star(i, j) * t_k_star(j, i);

      int rr = 0;
      for (; rr != nb_row_dofs / 2; ++rr) {

        // get derivatives of base functions on column
        auto t_col_base = col_data.getFTensor0N(gg, 0);

        auto t_vec = get_t_vec(FIELD_DIM * rr);

        for (int cc = 0; cc != nb_col_dofs / 2; cc++) {

          // std::cout << -k_star_squared / (k_star_squared + 2.) << std::endl;

          t_vec(i) += k_star_squared / (k_star_squared + 2.) * t_row_base *
                      t_col_base * a * s_f;

          // move to the derivatives of the next base functions on column
          ++t_col_base;
          ++t_vec;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_base;

      // move to the weight of the next integration point
      ++t_w;
      ++t_k_star;
    }

    CHKERR MatSetValues<EssentialBcStorage>(
        getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpGstarGLhs::doWork(int row_side, int col_side,
                                                   EntityType row_type,
                                                   EntityType col_type,
                                                   EntData &row_data,
                                                   EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  FTensor::Index<'i', 2> i;
  FTensor::Index<'j', 2> j;
  const int FIELD_DIM = 2;

  if (nb_row_dofs && nb_col_dofs) {

    const size_t nb_base_functions = row_data.getN().size2();
    if (2 * nb_base_functions < nb_row_dofs)
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Number of DOFs is larger than number of base functions on "
              "entity");

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on row
    auto t_row_diff_base = row_data.getFTensor1DiffN<2>();

    // get t_k_star
    auto t_k_star = getFTensor2FromMat<3, 3>(*(kStarPtr));

    auto s_f = sFunc(0, 0, 0);

    auto get_t_vec = [&](const int rr) {
      std::array<double *, FIELD_DIM> ptrs;
      for (auto i = 0; i != FIELD_DIM; ++i)
        ptrs[i] = &locLhs(rr + i, i);
      return FTensor::Tensor1<FTensor::PackPtr<double *, FIELD_DIM>, FIELD_DIM>(
          ptrs);
    };

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;
      auto k_star_squared = t_k_star(i, j) * t_k_star(j, i);

      int rr = 0;
      for (; rr != nb_row_dofs / FIELD_DIM; ++rr) {

        // get derivatives of base functions on column
        auto t_col_diff_base = col_data.getFTensor1DiffN<2>(gg, 0);

        auto t_vec = get_t_vec(rr * FIELD_DIM);

        for (int cc = 0; cc != nb_col_dofs / FIELD_DIM; cc++) {

          // std::cout << "GG: " << -2. / (k_star_squared + 2.) << std::endl;

          t_vec(j) += 2. / (k_star_squared + 2.) * t_row_diff_base(i) *
                      t_col_diff_base(i) * a * s_f;

          // move to the derivatives of the next base functions on column
          ++t_col_diff_base;
          ++t_vec;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_diff_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_diff_base;

      // move to the weight of the next integration point
      ++t_w;
      ++t_k_star;
    }

    CHKERR MatSetValues<EssentialBcStorage>(
        getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpMixHdivTangentStarLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    FTensor::Index<'k', 2> k;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = row_data.getN().size2() / 3;
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on row
    auto t_row_base = row_data.getFTensor1N<3>();

    // get t_k_star
    auto t_k_star = getFTensor2FromMat<3, 3>(*(kStarPtr));

    FTensor::Tensor2<double, 2, 2> t_identity{1.0, 0.0, 0.0, 1.0};

    auto s_f = sFunc(0, 0, 0);

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;

      auto k_star_squared = t_k_star(i, j) * t_k_star(j, i);

      int rr = 0;
      for (; rr != nb_row_dofs; ++rr) {
        // get derivatives of base functions on column
        auto t_col_base = col_data.getFTensor0N(gg, 0);

        for (int cc = 0; cc != nb_col_dofs / 2; cc++) {

          FTensor::Tensor1<double *, 2> t_a{&locLhs(rr, 2 * cc),
                                            &locLhs(rr, 2 * cc + 1)};

          t_a(k) += t_k_star(i, j) * t_identity(j, i) / (k_star_squared + 2.) *
                    t_row_base(k) * t_col_base * a * s_f;

          // move to the derivatives of the next base functions on column
          ++t_col_base;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_base;

      // move to the weight of the next integration point
      ++t_w;
      ++t_k_star;
    }
    // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX

    CHKERR MatSetValues<EssentialBcStorage>(
        getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
    MatrixDouble a_loc_trans = trans(locLhs);
    CHKERR MatSetValues<EssentialBcStorage>(getKSPB(), col_data, row_data,
                                            &*a_loc_trans.data().begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpMixHdivTimesHdivTangentStarLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = row_data.getN().size2() / 3;
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on row
    auto t_row_base = row_data.getFTensor1N<3>();

    // get t_k_star
    auto t_k_star = getFTensor2FromMat<3, 3>(*(kStarPtr));

    auto s_f = sFunc(0, 0, 0);

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;
      auto k_star_squared = t_k_star(i, j) * t_k_star(j, i);

      int rr = 0;
      for (; rr != nb_row_dofs; ++rr) {
        // get derivatives of base functions on column
        auto t_col_base = col_data.getFTensor1N<3>(gg, 0);

        for (int cc = 0; cc != nb_col_dofs; ++cc) {

          locLhs(rr, cc) += k_star_squared / (k_star_squared + 2.) *
                            t_row_base(i) * t_col_base(i) * a * s_f;
          ++t_col_base;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_base;

      // move to the weight of the next integration point
      ++t_w;
      ++t_k_star;
    }
    // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX

    CHKERR MatSetValues<EssentialBcStorage>(
        getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode OperatorsForDD::OpMassTangentStarLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    const int FIELD_DIM = 2;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = row_data.getN().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on row
    auto t_row_base = row_data.getFTensor0N();
    // auto t_row_base = row_data.getFTensor1N<3>();

    // get t_k_star
    auto t_k_star = getFTensor2FromMat<3, 3>(*(kStarPtr));

    auto s_f = sFunc(0, 0, 0);

    auto get_t_vec = [&](const int rr) {
      std::array<double *, FIELD_DIM> ptrs;
      for (auto i = 0; i != FIELD_DIM; ++i)
        ptrs[i] = &locLhs(rr + i, i);
      return FTensor::Tensor1<FTensor::PackPtr<double *, FIELD_DIM>, FIELD_DIM>(
          ptrs);
    };

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {

      const double a = t_w * area;
      auto k_star_squared = t_k_star(i, j) * t_k_star(j, i);

      int rr = 0;
      for (; rr != nb_row_dofs / FIELD_DIM; ++rr) {
        // get derivatives of base functions on column
        auto t_col_base = col_data.getFTensor0N(gg, 0);

        auto t_vec = get_t_vec(FIELD_DIM * rr);

        for (int cc = 0; cc != nb_col_dofs / FIELD_DIM; ++cc) {

          t_vec(i) +=
              2. / (k_star_squared + 2.) * t_row_base * t_col_base * a * s_f;

          ++t_col_base;
          ++t_vec;
        }

        // move to the derivatives of the next base functions on row
        ++t_row_base;
      }

      for (; rr != nb_base_functions; ++rr)
        ++t_row_base;

      // move to the weight of the next integration point
      ++t_w;
      ++t_k_star;
    }
    // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX

    CHKERR MatSetValues<EssentialBcStorage>(
        getKSPB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

template <int DIM_GRAD, int DIM_FLUX>
MoFEMErrorCode
OperatorsForDD::OpPostProcSaveCsvDatasetGQ<DIM_GRAD, DIM_FLUX>::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  const int nb_integration_pts = data.getN().size1();

  auto t_grad = getFTensor1FromMat<DIM_GRAD>(*(mGradPtr));
  auto t_flux = getFTensor1FromMat<DIM_FLUX>(*(mFluxPtr));

  std::ofstream myfile;
  myfile.open(csvName, std::ofstream::out | std::ofstream::app);
  myfile.precision(17);

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    myfile << t_grad(0) << ", " << t_grad(1) << ", " << t_flux(0) * sFlux
           << ", " << t_flux(1) * sFlux << "\n";

    ++t_grad;
    ++t_flux;
  }

  myfile.close();

  MoFEMFunctionReturn(0);
}

template <int DIM_GRAD, int DIM_FLUX>
MoFEMErrorCode
OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<DIM_GRAD, DIM_FLUX>::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  const int nb_integration_pts = data.getN().size1();

  auto t_val = getFTensor0FromVec(*(mValPtr));
  auto t_grad = getFTensor1FromMat<DIM_GRAD>(*(mGradPtr));
  auto t_flux = getFTensor1FromMat<DIM_FLUX>(*(mFluxPtr));

  std::ofstream myfile;
  myfile.open(csvName, std::ofstream::out | std::ofstream::app);
  myfile.precision(17);

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    myfile << t_grad(0) << ", " << t_grad(1) << ", " << t_flux(0) * sFlux
           << ", " << t_flux(1) * sFlux << ", " << t_val * sVal << "\n";

    ++t_val;
    ++t_grad;
    ++t_flux;
  }

  myfile.close();

  MoFEMFunctionReturn(0);
}
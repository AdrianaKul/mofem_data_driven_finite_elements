#include <CreateCsvDataset.hpp>

using namespace MoFEM;

MoFEMErrorCode CreatingCsvMaterialModel::setParamValues() {
  MoFEMFunctionBegin;
  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "DD Config", "none");
  // Set values from param file
  CHKERR PetscOptionsString("-output_file", "output csv file name", "",
                            "dummy_tree.csv", csv_out_file, 255, PETSC_NULL);
  CHKERR PetscOptionsInt("-my_csv_dim",
                         "dimenstions of the material dataset (4 or 5)", "", 4,
                         &csvDim, PETSC_NULL);
  CHKERR PetscOptionsReal("-my_dummy_k", "k for dummy rtree", "", 3, &dummy_k,
                          PETSC_NULL);
  // ranges
  CHKERR PetscOptionsReal("-my_dummy_range_dp",
                          "range of grad(p) for dummy rtree", "", 4.,
                          &dummy_range_dp, PETSC_NULL);
  CHKERR PetscOptionsReal("-my_dummy_range_q",
                          "range of grad(p) for dummy rtree", "", 12.,
                          &dummy_range_q, PETSC_NULL);
  // number of points created
  CHKERR PetscOptionsInt("-my_dummy_count",
                         "ammount of points in dummy rtree data set", "",
                         1000000, &dummy_count, PETSC_NULL);
  // noise
  CHKERR PetscOptionsReal("-my_dummy_noise_k", "noise on k for dummy rtree", "",
                          0.1, &dummy_noise_k, PETSC_NULL);
  CHKERR PetscOptionsReal("-my_dummy_noise_dp",
                          "noise on grad(p) for dummy rtree", "", 0.,
                          &dummy_noise_dp, PETSC_NULL);
  CHKERR PetscOptionsReal("-my_dummy_noise_q", "noise on q for dummy rtree", "",
                          0., &dummy_noise_q, PETSC_NULL);
  // random seed for point generation
  CHKERR PetscOptionsBool(
      "-rand_seed",
      "if set to 0 the created datasets will be the same each time", "",
      PETSC_TRUE, &rand_seed, PETSC_NULL);

  // basic nonlinear material dataset to compare to FEM
  CHKERR PetscOptionsReal("-nonlinear_alpha", "0", "", 0., &nonlinear_alpha,
                          &flg_bnmd);

  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-forchheirmer", &flg_forch,
                             PETSC_NULL);
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-scaling", &flg_scaling,
                             PETSC_NULL);
  CHKERR PetscOptionsReal("-forch_coeff_2",
                          "dp = - 1/k q - forch_coeff_2 |q| q ", "", 0.,
                          &forch_coeff_2, PETSC_NULL);

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "-output_file " << csv_out_file;

  if (csvDim != 4 && csvDim != 5) {
    MOFEM_LOG("WORLD", Sev::inform)
        << "-my_csv_dim " << csvDim
        << " not supported... reseting to default value";
    csvDim = 4;
  }
  MOFEM_LOG("WORLD", Sev::inform) << "-my_csv_dim " << csvDim;

  MOFEM_LOG("WORLD", Sev::inform) << "-rand_seed " << rand_seed;

  MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_count " << dummy_count;

  if (!flg_scaling)
    MOFEM_LOG("WORLD", Sev::inform) << "-scaling OFF";

  if (!flg_forch) {
    MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_k " << dummy_k;
    MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_range_dp " << dummy_range_dp;
  }
  if (flg_forch) {
    MOFEM_LOG("WORLD", Sev::inform) << "-forchheirmer used";
    MOFEM_LOG("WORLD", Sev::inform)
        << "Using Forchheirmer equation: dp = - 1/k q - forch_coeff_2 |q| q";
    MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_range_q " << dummy_range_q;
    MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_k " << dummy_k;
    MOFEM_LOG("WORLD", Sev::inform) << "-forch_coeff_2 " << forch_coeff_2;
  }
  MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_noise_k " << dummy_noise_k;
  MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_noise_dp " << dummy_noise_dp;
  MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_noise_q " << dummy_noise_q;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CreatingCsvMaterialModel::startCsvFile() {
  MoFEMFunctionBegin;
  // start writing dummy tree file
  std::ofstream myfile;
  myfile.open(csv_out_file);
  if (csvDim == 4)
    myfile << "gradx,grady,fluxx,fluxy\n"; // headers
  if (csvDim == 5)
    myfile << "gradx,grady,fluxx,fluxy,value\n"; // headers
  myfile.close();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CreatingCsvMaterialModel::createBasicMaterialModelData() {
  MoFEMFunctionBegin;

  if (flg_scaling) {
    scaling = dummy_k;
    dummy_k = 1.;
  }

  boost::random::uniform_real_distribution<> dist(
      -dummy_range_dp, dummy_range_dp); // distribution uniform real
  boost::random::normal_distribution<> noise_k(
      0, dummy_noise_k); // distribution normal
  boost::random::normal_distribution<> noise_q(
      0, dummy_noise_q); // distribution normal

  double gradx, grady, fluxx, fluxy;

  std::ofstream myfile;
  myfile.open(csv_out_file, std::ofstream::out | std::ofstream::app);
  myfile.precision(17);

  auto add_point = [&]() {
    MoFEMFunctionBegin;
    gradx = dist(gen);
    fluxx = -(dummy_k + noise_k(gen)) * gradx + noise_q(gen);
    grady = dist(gen);
    fluxy = -(dummy_k + noise_k(gen)) * grady + noise_q(gen);

    switch (csvDim) {
    case 4:
      // populate the file with points
      // if (!((gradx > 0.5) && (gradx < 0.7) && (grady > 0.15) && (grady <
      // 0.4)))
      myfile << gradx << ", " << grady << ", " << fluxx << ", " << fluxy
             << "\n";
      break;
    case 5:
      // populate the file with points
      myfile << gradx << ", " << grady << ", " << fluxx << ", " << fluxy
             << ", 0\n";
    }
    MoFEMFunctionReturn(0);
  };

  for (unsigned i = 0; i < dummy_count; ++i) {
    add_point();
  }

  // close writing into file (file created)
  myfile.close();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
CreatingCsvMaterialModel::createBasicNonlinearMaterialModelData() {
  MoFEMFunctionBegin;

  MOFEM_LOG("WORLD", Sev::inform) << "-nonlinear_alpha " << nonlinear_alpha;

  if (flg_scaling) {
    scaling = dummy_k;
    dummy_k = 1.;
  }

  boost::random::uniform_real_distribution<> dist(
      -dummy_range_dp, dummy_range_dp); // distribution uniform real
  boost::random::normal_distribution<> noise_k(
      0, dummy_noise_k); // distribution normal
  boost::random::normal_distribution<> noise_q(
      0, dummy_noise_q); // distribution normal

  double gradx, grady, fluxx, fluxy;

  std::ofstream myfile;
  myfile.open(csv_out_file, std::ofstream::out | std::ofstream::app);
  myfile.precision(17);

  auto add_point = [&]() {
    MoFEMFunctionBegin;
    gradx = dist(gen);
    grady = dist(gen);
    double k =
        1 + dummy_k * pow(gradx * gradx + grady * grady, nonlinear_alpha / 2);
    fluxx = -k * gradx;
    fluxy = -k * grady;

    // fluxx = -(dummy_k + noise_k(gen)) * gradx + noise_q(gen);
    // fluxy = -(dummy_k + noise_k(gen)) * grady + noise_q(gen);

    switch (csvDim) {
    case 4:
      // populate the file with points
      myfile << gradx << ", " << grady << ", " << fluxx << ", " << fluxy
             << "\n";
      break;
    case 5:
      // populate the file with points
      myfile << gradx << ", " << grady << ", " << fluxx << ", " << fluxy
             << ", 0\n";
    }
    MoFEMFunctionReturn(0);
  };

  for (unsigned i = 0; i < dummy_count; ++i) {
    add_point();
  }

  // close writing into file (file created)
  myfile.close();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CreatingCsvMaterialModel::createForchheirmerMaterialModelData() {
  MoFEMFunctionBegin;
  boost::random::uniform_real_distribution<> dist(
      -dummy_range_q, dummy_range_q); // distribution uniform real
  boost::random::normal_distribution<> noise_k(
      0, dummy_noise_k); // distribution normal
  boost::random::normal_distribution<> noise_dp(
      0, dummy_noise_dp); // distribution normal
  boost::random::normal_distribution<> noise_q(
      0, dummy_noise_q); // distribution normal

  double gradx, grady, fluxx, fluxy;

  std::ofstream myfile;
  myfile.open(csv_out_file, std::ofstream::out | std::ofstream::app);

  auto add_point = [&]() {
    MoFEMFunctionBegin;
    fluxx = dist(gen);
    double gradx1 = -(viscosity / (K * porosity)) * fluxx;
    double gradx2 = -(drag_const * density_fluid * sqrt(K) / square(porosity)) *
                    std::abs(fluxx) * fluxx;
    gradx = -1 / dummy_k * fluxx - forch_coeff_2 * std::abs(fluxx) * fluxx;
    // gradx = -(viscosity / (K * porosity)) * fluxx -
    //         (drag_const * density_fluid * sqrt(K) / square(porosity)) *
    //             std::abs(fluxx) * fluxx;
    fluxy = dist(gen);
    grady = -1 / dummy_k * fluxy - forch_coeff_2 * std::abs(fluxy) * fluxy;
    // grady = -(viscosity / (K * porosity)) * fluxy -
    //         (drag_const * density_fluid * sqrt(K) / square(porosity)) *
    //             std::abs(fluxy) * fluxy;

    switch (csvDim) {
    case 4:
      // populate the file with points
      myfile << gradx << ", " << grady << ", " << fluxx << ", " << fluxy
             << "\n";
      break;
    case 5:
      // populate the file with points
      myfile << gradx << ", " << grady << ", " << fluxx << ", " << fluxy
             << ", 0\n";
    }
    MoFEMFunctionReturn(0);
  };

  for (unsigned i = 0; i < dummy_count; ++i) {
    add_point();
  }

  // close writing into file (file created)
  myfile.close();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CreatingCsvMaterialModel::saveScalingFactor() {
  MoFEMFunctionBegin;
  // create a scaling file and save the scale factor
  std::ofstream myfile;
  myfile.open("scaling.in");
  myfile << scaling << std::endl;
  myfile << 1.0 << std::endl;
  myfile.close();

  MoFEMFunctionReturn(0);
}
#include <DdiDiffusionHdivProblem.hpp>

using namespace MoFEM;

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpHdivHdiv = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<3, 3>;

using OpMassMat = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<1, 2>;

// Scalars
using OpHdivU = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixDivTimesScalar<2>;

using OpBoundaryHdivHdiv = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<3, 3>;

using OpPressureBC = FormsIntegrators<OpEdgeEle>::Assembly<PETSC>::LinearForm<
    GAUSS>::OpNormalMixVecTimesScalar<2>;

DdiDiffusionHdiv::DdiDiffusionHdiv(MoFEM::Interface &m_field)
    : gStarField("Gstar"), qStarField("Qstar"), fluxField("Q"),
      lagrangeField("L"), mField(m_field), mpiComm(mField.get_comm()),
      mpiRank(mField.get_comm_rank()) {
  feDomainLhsPtr = boost::shared_ptr<FaceEle>(new FaceEle(mField));
  feDomainRhsPtr = boost::shared_ptr<FaceEle>(new FaceEle(mField));
  feBoundaryLhsPtr = boost::shared_ptr<EdgeEle>(new EdgeEle(mField));
  feBoundaryRhsPtr = boost::shared_ptr<EdgeEle>(new EdgeEle(mField));
}

MoFEMErrorCode DdiDiffusionHdiv::runProblem() {
  MoFEMFunctionBegin;

  CHKERR setParamValues();
  CHKERR readMesh();
  CHKERR setupProblem();
  CHKERR setIntegrationRules();
  CHKERR createClassicCommonData();
  CHKERR boundaryCondition();
  CHKERR markFluxBc();
  CHKERR assembleSystem();
  CHKERR getKSPinitial();
  CHKERR solveSystem();
  CHKERR outputHdivVtkFields(0);
  CHKERR outputCsvDataset();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::setParamValues() {
  MoFEMFunctionBegin;

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "DD Config", "none");
  // Set values from param file
  CHKERR PetscOptionsInt("-my_order",
                         "approximation order of spatial positions", "", 1,
                         &oRder, PETSC_NULL);
  CHKERR PetscOptionsReal("-my_dummy_k", "k for dummy rtree", "", 3, &dummy_k,
                          PETSC_NULL);

  CHKERR PetscOptionsInt(
      "-patch_integ_num",
      "number of integratioon points used for patch integration", "", 1,
      &patchIntegNum, PETSC_NULL);

  // mumps settings (leave be)
  CHKERR PetscOptionsInsertString(
      NULL, "-mat_mumps_icntl_14 1200 -mat_mumps_icntl_24 1 "
            "-mat_mumps_icntl_13 1 -mat_mumps_icntl_20 0");

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "-my_order " << oRder;
  MOFEM_LOG("WORLD", Sev::inform) << "-my_dummy_k " << dummy_k;
  MOFEM_LOG("WORLD", Sev::inform) << "-patch_integ_num " << patchIntegNum;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::readMesh() {
  MoFEMFunctionBegin;

  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::setupProblem() {
  MoFEMFunctionBegin;

  // domain fields
  CHKERR simpleInterface->addDomainField(gStarField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 2);
  // CHKERR simpleInterface->addDomainField(qStarField, L2,
  //                                        AINSWORTH_LEGENDRE_BASE, 2);
  CHKERR simpleInterface->addDomainField(lagrangeField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDomainField(fluxField, HCURL,
                                         DEMKOWICZ_JACOBI_BASE, 1);

  // boundary fields
  CHKERR simpleInterface->addBoundaryField(fluxField, HCURL,
                                           DEMKOWICZ_JACOBI_BASE, 1);

  CHKERR simpleInterface->setFieldOrder(gStarField, oRder);
  // CHKERR simpleInterface->setFieldOrder(qStarField, oRder);
  CHKERR simpleInterface->setFieldOrder(fluxField, oRder);
  CHKERR simpleInterface->setFieldOrder(lagrangeField, oRder - 1);

  CHKERR simpleInterface->setUp();

  // create common data pointers
  commonDataPtr = boost::make_shared<CommonDataDDI>();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::setIntegrationRules() {
  MoFEMFunctionBegin;

  auto domain_rule_lhs = [](int, int, int p) -> int { return 2 * p; };
  auto domain_rule_rhs = [](int, int, int p) -> int { return 2 * p; };
  feDomainLhsPtr->getRuleHook = domain_rule_lhs;
  feDomainRhsPtr->getRuleHook = domain_rule_rhs;

  auto boundary_rule_lhs = [](int, int, int p) -> int { return 2 * p; };
  auto boundary_rule_rhs = [](int, int, int p) -> int { return 2 * p; };
  feBoundaryLhsPtr->getRuleHook = boundary_rule_lhs;
  feBoundaryRhsPtr->getRuleHook = boundary_rule_rhs;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::createClassicCommonData() {
  MoFEMFunctionBegin;

  commonDataPtr->mValRefPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mGradRefPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mGradPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mFluxPtr = boost::make_shared<MatrixDouble>();

  int local_size;
  if (mField.get_comm_rank() == 0) // get_comm_rank() gets processor number
    // processor 0
    local_size =
        CommonDataDDI::LAST_ELEMENT; // last element gives size of vector
  else
    // other processors (e.g. 1, 2, 3, etc.)
    local_size = 0; // local size of vector is zero on other processors

  commonDataPtr->petscVec = createSmartVectorMPI(mField.get_comm(), local_size,
                                                 CommonDataDDI::LAST_ELEMENT);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::boundaryCondition() {
  MoFEMFunctionBegin;

  // Get boundary edges marked in block named "BOUNDARY_CONDITION"
  auto essential_bc = [&]() {
    Range boundary_entities;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
      std::string entity_name = it->getName();
      if (entity_name.compare(0, 18, "BOUNDARY_CONDITION") == 0) {
        CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 1,
                                                   boundary_entities, true);
      }
    }
    // Add vertices to boundary entities
    Range boundary_vertices;
    CHKERR mField.get_moab().get_connectivity(boundary_entities,
                                              boundary_vertices, true);
    boundary_entities.merge(boundary_vertices);

    return boundary_entities;
  };

  // Get boundary edges marked in block named "BOUNDARY_CONDITION"
  auto get_q_bar_on_mesh = [&]() {
    Range boundary_entities;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
      std::string entity_name = it->getName();
      if (entity_name.compare(0, 14, "BOUNDARY_Q_BAR") == 0) {
        CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 1,
                                                   boundary_entities, true);
      }
    }
    // Add vertices to boundary entities
    Range boundary_vertices;
    CHKERR mField.get_moab().get_connectivity(boundary_entities,
                                              boundary_vertices, true);
    boundary_entities.merge(boundary_vertices);

    return boundary_entities;
  };

  fluxBcRange = get_q_bar_on_mesh();
  pressureBcRange = essential_bc();

  Range common_boundary = intersect(fluxBcRange, pressureBcRange);
  fluxBcRange = subtract(fluxBcRange, common_boundary);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::markFluxBc() {
  MoFEMFunctionBegin;

  auto mark_boundary_dofs = [&](Range &skin_edges) {
    auto problem_manager = mField.getInterface<ProblemsManager>();
    auto marker_ptr = boost::make_shared<std::vector<unsigned char>>();
    problem_manager->markDofs(simpleInterface->getProblemName(), ROW,
                              skin_edges, *marker_ptr);
    return marker_ptr;
  };

  boundaryMarker.reset();
  boundaryMarker = mark_boundary_dofs(fluxBcRange);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
DdiDiffusionHdiv::calculateHdivJacobian(boost::shared_ptr<FaceEle> fe_ptr) {
  MoFEMFunctionBegin;

  auto det_ptr = boost::make_shared<VectorDouble>();
  auto jac_ptr = boost::make_shared<MatrixDouble>();
  auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

  fe_ptr->getOpPtrVector().push_back(new OpCalculateHOJacForFace(jac_ptr));
  fe_ptr->getOpPtrVector().push_back(
      new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
  fe_ptr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpSetInvJacHcurlFace(inv_jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpSetInvJacL2ForFace(inv_jac_ptr));
  fe_ptr->getOpPtrVector().push_back(new OpSetInvJacH1ForFace(inv_jac_ptr));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::assembleSystem() {
  MoFEMFunctionBegin;

  // checking if the previous fields are here
  if (mField.check_field("P_reference")) {
    std::cout << "The P_reference field is here :D\n";
    commonDataPtr->pReference = true;
  }

  // checking if the previous fields are here
  if (mField.check_field("G_reference")) {
    std::cout << "The G_reference field is here :D\n";
    commonDataPtr->gReference = true;
  }

  CHKERR calculateHdivJacobian(feDomainLhsPtr);
  CHKERR calculateHdivJacobian(feDomainRhsPtr);

  { // Push operators to the Pipeline that is responsible for calculating LHS of
    // domain elements
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivHdiv(fluxField, fluxField, oneFunction));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpMassMat(gStarField, gStarField, oneFunction));
    // feDomainLhsPtr->getOpPtrVector().push_back(
    //     new OpMassMat(qStarField, qStarField, oneFunction));

    // feDomainLhsPtr->getOpPtrVector().push_back(
    //     new OperatorsForDD::OpMixHdivTimesVecLhs(fluxField, qStarField,
    //                                              minusFunction));

    auto unity = []() { return 1.0; };

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivU(fluxField, lagrangeField, unity, true));

    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  { // source added to the domain Rhs
    auto sourceFunction = [&](const double x, const double y, const double z) {
      return -dummy_k * exp(-100. * (square(x) + square(y))) *
             (400. * M_PI *
                  (x * cos(M_PI * y) * sin(M_PI * x) +
                   y * cos(M_PI * x) * sin(M_PI * y)) +
              2. * (20000. * (square(x) + square(y)) - 200. - square(M_PI)) *
                  cos(M_PI * x) * cos(M_PI * y));
    };

    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpDomainSourceRhs(lagrangeField, sourceFunction));
  }

  {
    // S_G(G*,g) to Rhs domain
    if (commonDataPtr->pReference)
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldGradient<2>("P_reference",
                                                commonDataPtr->mGradRefPtr));

    else if (commonDataPtr->gReference)
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateVectorFieldValues<2>("G_reference",
                                              commonDataPtr->mGradRefPtr));

    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpL2StarRhs<2>(
            gStarField, commonDataPtr->mGradRefPtr, oneFunction));
  }

  { // LHS boundary elements

    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnEdge2D());

    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, false, boundaryMarker));
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpBoundaryHdivHdiv(fluxField, fluxField, oneFunction,
                               boost::make_shared<Range>(fluxBcRange)));
    feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  { // Rhs boundary elements

    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnEdge2D());

    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, false, boundaryMarker));
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpPressureBC(
        fluxField, fluxBcFunction, boost::make_shared<Range>(fluxBcRange)));
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::getKSPinitial() {
  MoFEMFunctionBegin;

  // get Discrete Manager (SmartPetscObj)
  dM = simpleInterface->getDM();

  { // Set operators for linear equations solver (KSP) from MoFEM Pipelines

    // Set operators for calculation of LHS and RHS of domain elements
    boost::shared_ptr<FaceEle> null_face;
    CHKERR DMMoFEMKSPSetComputeOperators(dM, simpleInterface->getDomainFEName(),
                                         feDomainLhsPtr, null_face, null_face);
    CHKERR DMMoFEMKSPSetComputeRHS(dM, simpleInterface->getDomainFEName(),
                                   feDomainRhsPtr, null_face, null_face);

    // Set operators for calculation of LHS and RHS of domain elements
    boost::shared_ptr<EdgeEle> null_edge;
    CHKERR DMMoFEMKSPSetComputeOperators(
        dM, simpleInterface->getBoundaryFEName(), feBoundaryLhsPtr, null_edge,
        null_edge);
    CHKERR DMMoFEMKSPSetComputeRHS(dM, simpleInterface->getBoundaryFEName(),
                                   feBoundaryRhsPtr, null_edge, null_edge);
  }
  // Setup KSP solver
  kspSolver = createKSP(mField.get_comm());
  CHKERR KSPSetFromOptions(kspSolver);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::solveSystem() {
  MoFEMFunctionBegin;

  // Create RHS and solution vectors
  SmartPetscObj<Vec> global_rhs, global_solution;
  CHKERR DMCreateGlobalVector_MoFEM(dM, global_rhs);
  global_solution = smartVectorDuplicate(global_rhs);

  CHKERR KSPSetDM(kspSolver, dM);
  CHKERR KSPSetUp(kspSolver);

  // Solve the system
  CHKERR KSPSolve(kspSolver, global_rhs, global_solution);
  // VecView(global_rhs, PETSC_VIEWER_STDOUT_SELF);

  // Scatter result data on the mesh
  CHKERR DMoFEMMeshToGlobalVector(dM, global_solution, INSERT_VALUES,
                                  SCATTER_REVERSE);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::outputHdivVtkFields(int ii,
                                                     std::string vtk_name) {
  // default vtk_name = "out_result_"
  MoFEMFunctionBegin;

  fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
      new PostProcFaceOnRefinedMesh(mField));

  // Generate post-processing mesh
  CHKERR fePostProcPtr->generateReferenceElementMesh();

  CHKERR calculateHdivJacobian(fePostProcPtr);
  // Postprocess only field values
  const Field_multiIndex *fields;
  mField.get_fields(&fields);
  for (auto &it : *fields)
    fePostProcPtr->addFieldValuesPostProc(it->getName());

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), fePostProcPtr);

  // write output
  CHKERR fePostProcPtr->writeFile(
      vtk_name + boost::lexical_cast<std::string>(ii) + ".h5m");

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DdiDiffusionHdiv::outputCsvDataset(std::string csv_name) {
  MoFEMFunctionBegin;

  std::string csv_GQ = "data_GQ_generated.csv";
  std::string csv_VGQ = "data_VGQ_generated.csv";

  std::ofstream myfile;
  myfile.open(csv_GQ);
  myfile << "gradx, grady, fluxx, fluxy\n"; // headers
  myfile.close();

  myfile.open(csv_VGQ);
  myfile << "gradx, grady, fluxx, fluxy, value\n"; // headers
  myfile.close();

  moab::Core mb_post;                   // create database
  moab::Interface &moab_proc = mb_post; // create interface to database

  boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
      new PatchIntegFaceEle(mField, patchIntegNum));

  auto domain_post_proc_rule = [](int, int, int p) -> int { return -1; };
  PostProcGauss->getRuleHook = domain_post_proc_rule;

  CHKERR calculateHdivJacobian(PostProcGauss);

  auto vec_val_ptr = boost::make_shared<VectorDouble>();
  auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
  auto mat_flux_ptr = boost::make_shared<MatrixDouble>();

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("P_reference", vec_val_ptr));

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateVectorFieldValues<2>(gStarField, mat_grad_ptr));

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateHVecVectorField<3>(fluxField, mat_flux_ptr));

  PostProcGauss->getOpPtrVector().push_back(
      new OperatorsForDD::OpPostProcSaveCsvDatasetGQ<2, 3>(
          gStarField, mat_grad_ptr, mat_flux_ptr, csv_GQ));

  PostProcGauss->getOpPtrVector().push_back(
      new OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<2, 3>(
          gStarField, vec_val_ptr, mat_grad_ptr, mat_flux_ptr, csv_VGQ));

  mb_post.delete_mesh();

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

  MoFEMFunctionReturn(0);
}

#include <DiffusionNonlinearHdivGraphiteProblem.hpp>
#include <OperatorsForDD.hpp>

using OpHdivHdiv = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<3, 3>;

using OpHdivU = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixDivTimesScalar<2>;

using OpBoundaryHdivHdiv = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<3, 3>;

using OpBaseTimeScalarRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpBaseTimesScalarField<1>;

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarNonlinearBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 3, 2>;
using OpBoundaryScalarRhsBCs =
    BoundaryRhsBCs::OpFlux<BoundaryScalarNonlinearBCs, 3, 2>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
// #include <ScalarBoundaryBC.hpp>
#include <ScalarBoundaryNonlinearBC.hpp>
// boundary conditions (end)

MoFEMErrorCode DiffusionNonlinearHdivGraphite::runProblem() {
  MoFEMFunctionBegin;

  CHKERR setParamValues();
  CHKERR readMesh();
  CHKERR setupProblem();
  CHKERR setIntegrationRules();
  CHKERR createClassicCommonData();
  CHKERR boundaryCondition();
  CHKERR assembleSystem();
  CHKERR getSnesInitial();
  CHKERR setSnesMonitors();
  CHKERR solveSnesSystem();
  // CHKERR outputHdivVtkFields(commonDataPtr->counter);
  orderRefinementCounter = 0;
  refIterNum = 0;
  CHKERR getErrorIndicators();
  for (int ii = 0; ii != refIterNum; ++ii) {
    CHKERR refineOrder();
    CHKERR boundaryCondition();
    CHKERR assembleSystem();
    CHKERR getSnesInitial();
    CHKERR setSnesMonitors();
    CHKERR solveSnesSystem();
    CHKERR getErrorIndicators();
  }
  CHKERR outputCsvDataset();

  // Switch off fields
  auto off_field = [&](auto fe_name, auto field_name) {
    CHKERR mField.modify_finite_element_off_field_row(fe_name, field_name);
    CHKERR mField.modify_finite_element_off_field_col(fe_name, field_name);
  };

  off_field(simpleInterface->getDomainFEName(), valueField);
  off_field(simpleInterface->getBoundaryFEName(), valueField);

  // produce .h5m file to be compared to poisson_dd_H data driven results
  mField.get_moab().write_file("analysis_mesh.h5m", "MOAB",
                               "PARALLEL=WRITE_PART");

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearHdivGraphite::setupProblem() {
  MoFEMFunctionBegin;

  // domain fields
  CHKERR simpleInterface->addDomainField(valueField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDomainField(fluxField, HCURL,
                                         DEMKOWICZ_JACOBI_BASE, 1);

  // boundary fields
  CHKERR simpleInterface->addBoundaryField(fluxField, HCURL,
                                           DEMKOWICZ_JACOBI_BASE, 1);
  // check orders
  CHKERR simpleInterface->setFieldOrder(valueField, oRder);
  CHKERR simpleInterface->setFieldOrder(fluxField, oRder + 1);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataGraphiteHDiv>();

  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;
  DiffusionDD::commonDataPtr = commonDataPtr;
  DiffusionSnesDD::commonDataPtr = commonDataPtr;
  HDivDiffusionProblem::commonDataPtr = commonDataPtr;

  { // establish petscRefineErrorVec
    int local_size;
    if (mField.get_comm_rank() == 0) // get_comm_rank() gets processor number
      // processor 0
      local_size =
          CommonDataGraphiteHDiv::LAST_REFINE_ELEMENT; // last element gives
                                                       // size of vector
    else
      // other processors (e.g. 1, 2, 3, etc.)
      local_size = 0; // local size of vector is zero on other processors
    commonDataPtr->petscRefineErrorVec =
        createSmartVectorMPI(mField.get_comm(), local_size,
                             CommonDataGraphiteHDiv::LAST_REFINE_ELEMENT);
  }

  CHKERR mField.get_moab().get_entities_by_dimension(0, 2, domainEntities,
                                                     false);
  Tag th_order;
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, th_order);
  for (auto &ent : domainEntities) {
    CHKERR mField.get_moab().tag_set_data(th_order, &ent, 1, &oRder);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearHdivGraphite::boundaryCondition() {
  MoFEMFunctionBegin;

  auto bc_mng = mField.getInterface<BcManager>();
  CHKERR bc_mng->pushMarkDOFsOnEntities(simpleInterface->getProblemName(),
                                        "FLUX", fluxField, 0, 0);

  auto &bc_map = bc_mng->getBcMapByBlockName();
  boundaryMarker = boost::make_shared<std::vector<char unsigned>>();
  for (auto b : bc_map) {
    if (std::regex_match(b.first, std::regex("(.*)FLUX(.*)"))) {
      boundaryMarker->resize(b.second->bcMarkers.size(), 0);
      for (int i = 0; i != b.second->bcMarkers.size(); ++i) {
        (*boundaryMarker)[i] |= b.second->bcMarkers[i];
      }
      fluxBcRange.merge(*(b.second->getBcEntsPtr()));
    }
  }

  Range boundary_vertices;
  CHKERR mField.get_moab().get_connectivity(fluxBcRange, boundary_vertices,
                                            true);
  fluxBcRange.merge(boundary_vertices);

  // Store entities for fieldsplit (block) solver
  boundaryEntitiesForFieldsplit = fluxBcRange;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearHdivGraphite::assembleSystem() {
  MoFEMFunctionBegin;

  // clearing previous settings
  feDomainLhsPtr->getOpPtrVector().clear();
  feDomainRhsPtr->getOpPtrVector().clear();
  feBoundaryLhsPtr->getOpPtrVector().clear();
  feBoundaryRhsPtr->getOpPtrVector().clear();

  auto calculateHdivJacobian = [&](boost::shared_ptr<FaceEle> fe_ptr) {
    MoFEMFunctionBegin;
    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    fe_ptr->getOpPtrVector().push_back(new OpCalculateHOJacForFace(jac_ptr));
    fe_ptr->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    fe_ptr->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
    fe_ptr->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
    fe_ptr->getOpPtrVector().push_back(new OpSetInvJacHcurlFace(inv_jac_ptr));
    fe_ptr->getOpPtrVector().push_back(new OpSetInvJacL2ForFace(inv_jac_ptr));
    fe_ptr->getOpPtrVector().push_back(new OpSetInvJacH1ForFace(inv_jac_ptr));
    MoFEMFunctionReturn(0);
  };

  // calculating jacobian and other general necessary parts
  CHKERR calculateHdivJacobian(feDomainLhsPtr);
  CHKERR calculateHdivJacobian(feDomainRhsPtr);
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());

  { // domain Lhs
    auto vec_val_ptr = boost::make_shared<VectorDouble>();
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));
    // (Q,Q)
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivHdiv(fluxField, fluxField, oneFunction));

    // (U,div(Q))
    feDomainLhsPtr->getOpPtrVector().push_back(new OpHdivU(
        fluxField, valueField, []() { return -1.; }, true, true));

    // (div(Q), -k U)
    // k = a + b u + c u^2
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpNonlinearLhs(fluxField, valueField, vec_val_ptr, nonlinear_a,
                           nonlinear_b, nonlinear_c));
    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }
  { // domain Rhs
    auto vec_val_ptr = boost::make_shared<VectorDouble>();
    auto mat_flux_ptr = boost::make_shared<MatrixDouble>();
    auto mat_div_flux_ptr = boost::make_shared<VectorDouble>();
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(fluxField, mat_flux_ptr));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateHdivVectorDivergence<3, 2>(fluxField, mat_div_flux_ptr));
    // -(div(Q), a u + b u^2 + c u^3)
    feDomainRhsPtr->getOpPtrVector().push_back(new OpNonlinearRhs(
        fluxField, vec_val_ptr, nonlinear_a, nonlinear_b, nonlinear_c));
    // (Q,q)
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpHdivStarRhs<3>(fluxField, mat_flux_ptr,
                                             oneFunction));
    // (U,div(q))
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpBaseTimeScalarRhs(valueField, mat_div_flux_ptr, minusFunction));

    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, valueField, 1., Sev::inform);

    // auto sourceFun = [&](const double x, const double y, const double z) {
    //   return 5.;
    // };

    // feDomainRhsPtr->getOpPtrVector().push_back(
    //     new OpDomainSourceRhs(valueField, sourceFun));

    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }
  { // boundary Lhs
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, false, boundaryMarker));

    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpBoundaryHdivHdiv(fluxField, fluxField, oneFunction,
                               boost::make_shared<Range>(fluxBcRange)));
    // feBoundaryLhsPtr->getOpPtrVector().push_back(new OpBoundaryHdivHdiv(
    //     fluxField, fluxField, oneFunction));

    feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }
  { // boundary Rhs
    // essential
    auto mat_flux_ptr = boost::make_shared<MatrixDouble>();

    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, false, boundaryMarker));

    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(fluxField, mat_flux_ptr));
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpHdivNormalBoundaryStarRhs<3>(
            fluxField, mat_flux_ptr, oneFunction));

    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, fluxField, 1., Sev::inform);

    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));

    // natural
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, fluxField, 1., nonlinear_a,
        nonlinear_b, nonlinear_c, Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearHdivGraphite::setSnesMonitors() {
  MoFEMFunctionBegin;

  // ctx for monitor containing the discrete manager
  auto snes_ctx_ptr = smartGetDMSnesCtx(simpleInterface->getDM());

  combinedPointers = boost::make_shared<CombinedPointers>();

  combinedPointers->commonDataPtr = commonDataPtr;
  combinedPointers->snesCtxPtr = snes_ctx_ptr;

  // define monitors which are executed at the begining of each iteration
  // postProc on integration points
  auto monitor_vtk_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                  void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    auto &m_field = snes_ptr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();

    boost::shared_ptr<PostProcFaceOnRefinedMesh> fePostProcPtr;
    fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
        new PostProcFaceOnRefinedMesh(m_field));

    // auto det_ptr = boost::make_shared<VectorDouble>();
    // auto jac_ptr = boost::make_shared<MatrixDouble>();
    // auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    // fePostProcPtr->getOpPtrVector().push_back(
    //     new OpCalculateHOJacForFace(jac_ptr));
    // fePostProcPtr->getOpPtrVector().push_back(
    //     new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    // fePostProcPtr->getOpPtrVector().push_back(
    //     new OpSetInvJacH1ForFace(inv_jac_ptr));

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    fePostProcPtr->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
    fePostProcPtr->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformOnFace2D(jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpSetInvJacHcurlFace(inv_jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpSetInvJacL2ForFace(inv_jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpSetInvJacH1ForFace(inv_jac_ptr));

    // Generate post-processing mesh
    CHKERR fePostProcPtr->generateReferenceElementMesh();
    // Postprocess only field values
    const Field_multiIndex *fields;
    m_field.get_fields(&fields);
    for (auto &it : *fields) {
      fePostProcPtr->addFieldValuesPostProc(it->getName());
    }
    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), fePostProcPtr);

    // write output
    CHKERR fePostProcPtr->writeFile(
        "out_iteration_" + boost::lexical_cast<std::string>(its) + ".h5m");

    MoFEMFunctionReturn(0);
  };

  auto monitor_print_moab = [](SNES snes, PetscInt its, PetscReal fgnorm,
                               void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    string gauss_name;
    std::ostringstream strma;
    strma << "out_moab_" << its << ".h5m";
    gauss_name = strma.str();
    CHKERR snes_ptr->mField.get_moab().write_file(gauss_name.c_str(), "MOAB",
                                                  "PARALLEL=WRITE_PART");

    MoFEMFunctionReturn(0);
  };

  CHKERR SNESMonitorSet(snesSolver,
                        (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                           void *))MoFEMSNESMonitorFields,
                        (void *)(snes_ctx_ptr.get()), nullptr);

  if (!skip_vtk)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_vtk_post_proc,
                          (void *)(snes_ctx_ptr.get()), nullptr);

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_print_moab,
                          (void *)(snes_ctx_ptr.get()), nullptr);
  }

  MoFEMFunctionReturn(0);
}

// get error indicators
MoFEMErrorCode DiffusionNonlinearHdivGraphite::getErrorIndicators() {
  MoFEMFunctionBegin;
  boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
      new PatchIntegFaceEle(mField, patchIntegNum));
  PostProcGauss->getRuleHook =
      feDomainRhsPtr->getRuleHook; // use the same integration rule as domainRhs

  CHKERR calculateHdivJacobian(PostProcGauss);

  auto vec_val_ptr = boost::make_shared<VectorDouble>();
  auto mat_flux_ptr = boost::make_shared<MatrixDouble>();
  auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateHVecVectorField<3>(fluxField, mat_flux_ptr));
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));

  auto getConductivity = [&](const double val, const double, const double) {
    return nonlinear_a + nonlinear_b * val + nonlinear_c * val * val;
  };

  PostProcGauss->getOpPtrVector().push_back(
      new OpErrorIndicator<3>(valueField, commonDataPtr, mField, mat_flux_ptr,
                              mat_grad_ptr, vec_val_ptr, getConductivity));

  CHKERR VecAssemblyBegin(commonDataPtr->petscRefineErrorVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscRefineErrorVec);
  CHKERR VecZeroEntries(commonDataPtr->petscRefineErrorVec);

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

  CHKERR VecAssemblyBegin(commonDataPtr->petscRefineErrorVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscRefineErrorVec);

  const double *array;
  CHKERR VecGetArrayRead(commonDataPtr->petscRefineErrorVec, &array);

  MOFEM_LOG("WORLD", Sev::inform)
      << "Global error indicator: "
      << std::sqrt(array[CommonDataGraphiteHDiv::ERROR_INDICATOR_GRAD]);
  MOFEM_LOG("WORLD", Sev::inform)
      << "Total number of elements: "
      << (int)array[CommonDataGraphiteHDiv::TOTAL_NUMBER];
  MOFEM_LOG("WORLD", Sev::inform)
      << "Total number of integration points: "
      << (int)array[CommonDataGraphiteHDiv::INTEG_COUNT];

  errorIndicatorIntegral = array[CommonDataHDiv::ERROR_INDICATOR_GRAD];
  totalElementNumber = (int)array[CommonDataHDiv::TOTAL_NUMBER];

  CHKERR VecRestoreArrayRead(commonDataPtr->petscRefineErrorVec, &array);

  int iter_num = 0;
  if (1) {
    std::vector<Tag> tag_handles;
    tag_handles.resize(4);
    CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, tag_handles[0]);
    CHKERR getTagHandle(mField, "ERROR_INDICATOR_GRAD", MB_TYPE_DOUBLE,
                        tag_handles[1]);
    CHKERR getTagHandle(mField, "AREA", MB_TYPE_DOUBLE, tag_handles[2]);
    CHKERR getTagHandle(mField, "K_AVERAGE", MB_TYPE_DOUBLE, tag_handles[3]);

    ParallelComm *pcomm =
        ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
    if (pcomm == NULL)
      SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
              "Communicator not set");

    auto bit_level1 = BitRefLevel().set(0);
    auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
    CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
        bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);

    tag_handles.push_back(pcomm->part_tag());
    std::ostringstream strm;
    strm << "out_error_" << orderRefinementCounter << ".h5m";
    CHKERR mField.get_moab().write_file(strm.str().c_str(), "MOAB",
                                        "PARALLEL=WRITE_PART",
                                        out_meshset_tri_ptr->get_ptr(), 1,
                                        tag_handles.data(), tag_handles.size());
  }

  MoFEMFunctionReturn(0);
}

// // printing material dataset

// MoFEMErrorCode DiffusionNonlinearHdivGraphite::outputCsvDataset(bool header)
// {
//   MoFEMFunctionBegin;

//   std::string csv_GQ = "data_GQ_generated.csv";
//   std::string csv_VGQ = "data_VGQ_generated.csv";

//   if (header) {
//     std::ofstream myfile;
//     myfile.open(csv_GQ);
//     myfile << "gradx, grady, fluxx, fluxy\n"; // headers
//     myfile.close();

//     myfile.open(csv_VGQ);
//     myfile << "gradx, grady, fluxx, fluxy, value\n"; // headers
//     myfile.close();
//   }

//   moab::Core mb_post;                   // create database
//   moab::Interface &moab_proc = mb_post; // create interface to database

//   boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
//       new PatchIntegFaceEle(mField, patchIntegNum));

//   auto domain_post_proc_rule = [](int, int, int p) -> int { return 2 * p; };
//   PostProcGauss->getRuleHook = domain_post_proc_rule;

//   CHKERR calculateJacobian(PostProcGauss);

//   auto vec_val_ptr = boost::make_shared<VectorDouble>();
//   auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
//   auto mat_flux_ptr = boost::make_shared<MatrixDouble>();

//   double max_value = 0.;
//   double max_grad = 0.;
//   double max_flux = 0.;
//   double s_t = 1.;
//   double s_q = 1.;

//   auto get_k_from_val_function = [&](const double val, const double,
//                                      const double) {
//     return nonlinear_a + nonlinear_b * val + nonlinear_c * square(val);
//   };

//   {
//     PostProcGauss->getOpPtrVector().push_back(
//         new OpCalculateScalarFieldValues(valueField, vec_val_ptr));

//     PostProcGauss->getOpPtrVector().push_back(
//         new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));

//     PostProcGauss->getOpPtrVector().push_back(
//         new OpGetFluxFromVal(valueField, vec_val_ptr, mat_grad_ptr,
//         mat_flux_ptr,
//                              get_k_from_val_function));

//     PostProcGauss->getOpPtrVector().push_back(
//         new OpGetMaxVals(valueField, vec_val_ptr, mat_grad_ptr, mat_flux_ptr,
//                          &max_value, &max_grad, &max_flux));

//     CHKERR DMoFEMLoopFiniteElements(
//         dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

//     std::cout << max_value << std::endl;
//     std::cout << max_grad << std::endl;
//     std::cout << max_flux << std::endl;

//     PostProcGauss->getOpPtrVector().clear();
//   }

//   if (max_value && max_grad && max_flux) {
//     s_t = max_grad / max_value;
//     s_q = max_grad / max_flux;
//   }

//   std::cout << s_t << std::endl;
//   std::cout << s_q << std::endl;

//   std::ofstream myfile;
//   myfile.open("scaling.in");
//   myfile << s_q << std::endl;
//   myfile << s_t << std::endl;
//   myfile.close();

//   CHKERR calculateJacobian(PostProcGauss);

//   PostProcGauss->getOpPtrVector().push_back(
//       new OpCalculateScalarFieldValues(valueField, vec_val_ptr));

//   PostProcGauss->getOpPtrVector().push_back(
//       new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));

//   // if (flgNonlinearToP)
//   PostProcGauss->getOpPtrVector().push_back(
//       new OpGetFluxFromVal(valueField, vec_val_ptr, mat_grad_ptr,
//       mat_flux_ptr,
//                            get_k_from_val_function));

//   PostProcGauss->getOpPtrVector().push_back(
//       new OperatorsForDD::OpPostProcSaveCsvDatasetGQ<2, 2>(
//           valueField, mat_grad_ptr, mat_flux_ptr, csv_GQ));

//   PostProcGauss->getOpPtrVector().push_back(
//       new OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<2, 2>(
//           valueField, vec_val_ptr, mat_grad_ptr, mat_flux_ptr, csv_VGQ, s_q,
//           s_t));

//   mb_post.delete_mesh();

//   CHKERR DMoFEMLoopFiniteElements(
//       dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

//   MoFEMFunctionReturn(0);
// }

// Operators

MoFEMErrorCode DiffusionNonlinearHdivGraphite::OpNonlinearLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    FTensor::Index<'k', 2> k;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    size_t nb_base_functions = row_data.getN().size2() / 3;
    auto t_row_diff_base = row_data.getFTensor2DiffN<3, 2>();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    auto t_val = getFTensor0FromVec(*(valPtr));

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {
      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_row_dofs; ++rr) {
        // get base functions on column
        auto t_col_base = col_data.getFTensor0N(gg, 0);

        for (int cc = 0; cc != nb_col_dofs; cc++) {
          const double t_row_div_base = t_row_diff_base(i, i);
          locLhs(rr, cc) -=
              a * t_row_div_base * t_col_base *
              (nonlinearA + nonlinearB * t_val + nonlinearC * square(t_val));

          // move to the next base functions on column
          ++t_col_base;
        }

        // move to the next base function on row
        ++t_row_diff_base;
      }
      for (; rr < nb_base_functions; ++rr)
        ++t_row_diff_base;

      // move to the weight of the next integration point
      ++t_w;
      // move to the solution (field value) at the next integration point
      ++t_val;
    }

    CHKERR MatSetValues<EssentialBcStorage>(
        getSNESB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearHdivGraphite::OpNonlinearRhs::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // const int nb_base_functions = data.getN().size2();
    size_t nb_base_functions = data.getN().size2() / 3;
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    auto t_val = getFTensor0FromVec(*(valPtr));

    // get base functions
    auto t_diff_base = data.getFTensor2DiffN<3, 2>();

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_dofs; rr++) {
        const double t_div_base = t_diff_base(i, i);
        locRhs[rr] -= t_div_base * a *
                      (nonlinearA * t_val + nonlinearB / 2 * square(t_val) +
                       nonlinearC / 3 * square(t_val) * t_val);

        // move to the next base function
        ++t_diff_base;
      }
      for (; rr != nb_base_functions; ++rr)
        ++t_diff_base;

      ++t_val;
      // move to the weight of the next integration point
      ++t_w;
    }

    CHKERR VecSetValues<EssentialBcStorage>(getSNESf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

//! [OpErrorIndGrad]
template <int N>
MoFEMErrorCode DiffusionNonlinearHdivGraphite::OpErrorIndicator<N>::doWork(
    int side, EntityType type, EntData &data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();
  auto t_val = getFTensor0FromVec(*(valPtr));
  auto t_grad = getFTensor1FromMat<2>(*(gradPtr));
  auto t_flux = getFTensor1FromMat<N>(*(fluxPtr));
  FTensor::Tensor1<double, 2> t_diff;
  FTensor::Index<'i', 2> i;

  double error_ind = 0;
  int integ_count = 0;
  double k_average = 0;
  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    const double alpha = t_w * area;

    double k = sFunc(t_val, sqrt(t_grad(i) * t_grad(i)), 0);
    k_average += k;

    t_diff(i) = t_flux(i) + k * t_grad(i);
    error_ind += alpha * t_diff(i) * t_diff(i);
    integ_count += 1;

    ++t_w;
    ++t_val;
    ++t_grad;
    ++t_flux;
  }
  k_average /= nb_integration_pts;
  error_ind = sqrt(error_ind);

  const EntityHandle ent = getFEEntityHandle();
  Tag th_error_ind;
  CHKERR ClassicDiffusionProblem::getTagHandle(mField, "ERROR_INDICATOR_GRAD",
                                               MB_TYPE_DOUBLE, th_error_ind);
  CHKERR mField.get_moab().tag_set_data(th_error_ind, &ent, 1, &error_ind);

  Tag th_area;
  CHKERR ClassicDiffusionProblem::getTagHandle(mField, "AREA", MB_TYPE_DOUBLE,
                                               th_area);
  CHKERR mField.get_moab().tag_set_data(th_area, &ent, 1, &area);

  Tag th_k;
  CHKERR ClassicDiffusionProblem::getTagHandle(mField, "K_AVERAGE",
                                               MB_TYPE_DOUBLE, th_k);
  CHKERR mField.get_moab().tag_set_data(th_k, &ent, 1, &k_average);

  constexpr std::array<int, 4> indices = {
      CommonDataHDiv::ERROR_INDICATOR_GRAD, CommonDataHDiv::TOTAL_NUMBER,
      CommonDataHDiv::INTEG_COUNT, CommonDataHDiv::VOLUME};
  std::array<double, 4> values;
  values[0] = error_ind;
  values[1] = 1.;
  values[2] = integ_count;
  values[3] = area;
  CHKERR VecSetValues(commonDataPtr->petscRefineErrorVec, 4, indices.data(),
                      values.data(), ADD_VALUES);
  MoFEMFunctionReturn(0);
}
//! [OpErrorIndGrad]
#include <RtreeManipulation.hpp>
using namespace MoFEM;

// set point
point4D RtreeManipulation::convertToPoint(const double v0, const double v1,
                                          const double v2, const double v3) {
  boost::geometry::model::point<double, 4, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  poi.set<1>(v1);
  poi.set<2>(v2);
  poi.set<3>(v3);
  return poi;
}

point5D RtreeManipulation::convertToPoint(const double v0, const double v1,
                                          const double v2, const double v3,
                                          const double v4) {
  boost::geometry::model::point<double, 5, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  poi.set<1>(v1);
  poi.set<2>(v2);
  poi.set<3>(v3);
  poi.set<4>(v4);
  return poi;
}

void RtreeManipulation::convertToPoint(point4D &poi,
                                       const VectorDouble vector) {
  poi.set<0>(vector[0]);
  poi.set<1>(vector[1]);
  poi.set<2>(vector[2]);
  poi.set<3>(vector[3]);
};

void RtreeManipulation::convertToPoint(point5D &poi,
                                       const VectorDouble vector) {
  poi.set<0>(vector[0]);
  poi.set<1>(vector[1]);
  poi.set<2>(vector[2]);
  poi.set<3>(vector[3]);
  poi.set<4>(vector[4]);
};

std::vector<double> RtreeManipulation::convertToVector(point4D poi) {
  std::vector<double> vector{poi.get<0>(), poi.get<1>(), poi.get<2>(),
                             poi.get<3>()};
  return vector;
};

std::vector<double> RtreeManipulation::convertToVector(point5D poi) {
  std::vector<double> vector{poi.get<0>(), poi.get<1>(), poi.get<2>(),
                             poi.get<3>(), poi.get<4>()};
  return vector;
};

FTensor::Tensor1<double, 4> RtreeManipulation::convertToFTensor1(point4D poi) {
  FTensor::Tensor1<double, 4> t_point{poi.get<0>(), poi.get<1>(), poi.get<2>(),
                                      poi.get<3>()};
  return t_point;
};

FTensor::Tensor1<double, 5> RtreeManipulation::convertToFTensor1(point5D poi) {
  FTensor::Tensor1<double, 5> t_point{poi.get<0>(), poi.get<1>(), poi.get<2>(),
                                      poi.get<3>(), poi.get<4>()};
  return t_point;
};

MoFEMErrorCode
RtreeManipulation::loadFileData(const std::string filename,
                                bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree,
                                bool pack) {
  MoFEMFunctionBegin;
  std::ifstream in(filename);
  if (!in.is_open())
    throw std::runtime_error("Could not open file");

  typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;

  std::string line;
  std::vector<point4D> cloud;

  while (getline(in, line)) {
    std::vector<double> vec;
    Tokenizer tok(line);
    point4D point_read;
    for (Tokenizer::iterator it(tok.begin()), end(tok.end()); it != end; ++it) {
      vec.push_back(atof(it->c_str()));
    }

    convertToPoint(point_read, vec);

    if (pack) {
      cloud.push_back(point_read);
    } else {
      rtree.insert(point_read);
    }
  }

  if (pack) {
    bgi::rtree<point4D, bgi::rstar<16, 4>> tempRtree(cloud);
    rtree.swap(tempRtree);
  }

  rtree.remove(convertToPoint(0, 0, 0, 0));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
RtreeManipulation::loadFileData(const std::string filename,
                                bgi::rtree<point5D, bgi::rstar<16, 4>> &rtree,
                                bool pack) {
  MoFEMFunctionBegin;
  std::ifstream in(filename);
  if (!in.is_open())
    throw std::runtime_error("Could not open file");

  typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;

  std::string line;
  std::vector<point5D> cloud;

  while (getline(in, line)) {
    std::vector<double> vec;
    Tokenizer tok(line);
    point5D point_read;
    for (Tokenizer::iterator it(tok.begin()), end(tok.end()); it != end; ++it) {
      vec.push_back(atof(it->c_str()));
    }

    while (vec.size() < 5)
      vec.push_back(0.);

    convertToPoint(point_read, vec);

    if (pack) {
      cloud.push_back(point_read);
    } else {
      rtree.insert(point_read);
    }
  }

  if (pack) {
    bgi::rtree<point5D, bgi::rstar<16, 4>> tempRtree(cloud);
    rtree.swap(tempRtree);
  }

  rtree.remove(convertToPoint(0, 0, 0, 0, 0));
  MoFEMFunctionReturn(0);
}

std::vector<double>
RtreeManipulation::loadScalingData(const std::string filename) {
  std::ifstream in(filename);
  if (!in.is_open())
    throw std::runtime_error("Could not open file");

  typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;

  std::string line;
  std::vector<double> data_loaded;

  while (getline(in, line)) {
    std::vector<double> vec;
    Tokenizer tok(line);
    for (Tokenizer::iterator it(tok.begin()), end(tok.end()); it != end; ++it) {
      vec.push_back(atof(it->c_str()));
    }

    data_loaded.push_back(vec[0]);
  }

  return data_loaded;
}

point4D RtreeManipulation::returnNearestAndDistance(
    const point4D &finding,
    FTensor::Tensor0<FTensor::PackPtr<double *, 1>> &distance,
    const bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree_in) {
  std::vector<point4D> result_n;
  rtree_in.query(bgi::nearest(finding, 1), std::back_inserter(result_n));
  distance = bg::distance(finding, result_n[0]);
  return result_n[0];
}
point5D RtreeManipulation::returnNearestAndDistance(
    const point5D &finding,
    FTensor::Tensor0<FTensor::PackPtr<double *, 1>> &distance,
    const bgi::rtree<point5D, bgi::rstar<16, 4>> &rtree_in) {
  std::vector<point5D> result_n;
  rtree_in.query(bgi::nearest(finding, 1), std::back_inserter(result_n));
  distance = bg::distance(finding, result_n[0]);
  return result_n[0];
}

point4D RtreeManipulation::returnAveAndDistance(
    const point4D &finding,
    FTensor::Tensor0<FTensor::PackPtr<double *, 1>> &distance,
    const bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree_in,
    const int number_points) {

  mpoint_t multi_point;

  std::vector<point4D> result_n;
  rtree_in.query(bgi::nearest(finding, number_points),
                 std::back_inserter(result_n));

  for (int i = 0; i < number_points; ++i) {
    bg::append(multi_point, result_n[i]);
  }

  point4D ave_point;
  bg::centroid(multi_point, ave_point);

  distance = bg::distance(finding, ave_point);

  return ave_point;
}

std::vector<point4D> RtreeManipulation::returnNearestPoints(
    const point4D &finding,
    const bgi::rtree<point4D, bgi::rstar<16, 4>> &rtree_in,
    const int number_points) {

  std::vector<point4D> result_n;
  rtree_in.query(bgi::nearest(finding, number_points),
                 std::back_inserter(result_n));

  return result_n;
}

std::vector<point5D> RtreeManipulation::returnNearestPoints(
    const point5D &finding,
    const bgi::rtree<point5D, bgi::rstar<16, 4>> &rtree_in,
    const int number_points) {

  std::vector<point5D> result_n;
  rtree_in.query(bgi::nearest(finding, number_points),
                 std::back_inserter(result_n));

  return result_n;
}

double RtreeManipulation::getGradient(const std::vector<double> v_x,
                                      const std::vector<double> v_y) {

  double gradient;

  int N = v_x.size();
  double sum_x;
  double sum_y;
  double sum_xy;
  double sum_x2;

  for (unsigned i = 0; i < v_x.size(); ++i) {
    double x = v_x[i];
    double y = v_y[i];
    sum_x += x;
    sum_y += y;
    sum_xy += x * y;
    sum_x2 += x * x;
  }

  // gradient = (N * sum_xy - sum_x * sum_y) / (N * sum_x2 - sum_x * sum_x);
  gradient = sum_xy / sum_x2;

  return gradient;
}

//

MoFEMErrorCode
RtreeManipulation::getGradient_new(const std::vector<point4D> &points_vector,
                                   MatrixDouble &tangent) {
  MoFEMFunctionBegin;
  FTensor::Index<'i', 2> i;
  FTensor::Index<'j', 2> j;

  MatrixDouble rhs(2, 2);
  MatrixDouble lhs(2, 2);

  FTensor::Tensor2<double *, 2, 2> t_rhs(&rhs(0), &rhs(1), &rhs(2), &rhs(3));
  FTensor::Tensor2<double *, 2, 2> t_lhs(&lhs(0), &lhs(1), &lhs(2), &lhs(3));

  for (point4D n : points_vector) {
    FTensor::Tensor1<double, 2> t_grad{n.get<0>(), n.get<1>()};
    FTensor::Tensor1<double, 2> t_flux{n.get<2>(), n.get<3>()};
    t_rhs(i, j) += t_grad(i) * t_flux(j);
    t_lhs(i, j) += t_grad(i) * t_grad(j);
  }

  cholesky_decompose(lhs);
  cholesky_solve(lhs, rhs, ublas::lower());
  tangent = rhs;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
RtreeManipulation::getGradient_new(const std::vector<point5D> &points_vector,
                                   MatrixDouble &tangent) {
  MoFEMFunctionBegin;
  FTensor::Index<'i', 2> i;
  FTensor::Index<'j', 2> j;

  MatrixDouble rhs(2, 2);
  MatrixDouble lhs(2, 2);

  FTensor::Tensor2<double *, 2, 2> t_rhs(&rhs(0), &rhs(1), &rhs(2), &rhs(3));
  FTensor::Tensor2<double *, 2, 2> t_lhs(&lhs(0), &lhs(1), &lhs(2), &lhs(3));

  for (point5D n : points_vector) {
    FTensor::Tensor1<double, 2> t_grad{n.get<0>(), n.get<1>()};
    FTensor::Tensor1<double, 2> t_flux{n.get<2>(), n.get<3>()};
    t_rhs(i, j) += t_grad(i) * t_flux(j);
    t_lhs(i, j) += t_grad(i) * t_grad(j);
  }

  cholesky_decompose(lhs);
  cholesky_solve(lhs, rhs, ublas::lower());
  tangent = rhs;

  MoFEMFunctionReturn(0);
}

double RtreeManipulation::getCorrelation(const std::vector<double> v_x,
                                         const std::vector<double> v_y) {

  double correlation;

  int N = v_x.size();
  double sum_x;
  double sum_y;
  double sum_xy;
  double sum_x2;
  double sum_y2;

  for (unsigned i = 0; i < v_x.size(); ++i) {
    double x = v_x[i];
    double y = v_y[i];
    sum_x += x;
    sum_y += y;
    sum_xy += x * y;
    sum_x2 += x * x;
    sum_y2 += y * y;
  }

  correlation =
      (N * sum_xy - sum_x * sum_y) /
      (sqrt(N * sum_x2 - sum_x * sum_x) * sqrt(N * sum_y2 - sum_y * sum_y));

  return correlation;
}

void RtreeManipulation::calcKstar(const std::vector<point4D> &points_vector,
                                  std::vector<double> &tangent,
                                  std::vector<double> &correlation) {

  std::vector<double> x_0;
  std::vector<double> x_1;
  std::vector<double> x_2;
  std::vector<double> x_3;

  for (point4D n : points_vector) {
    std::vector<double> temp = convertToVector(n);
    x_0.push_back(temp[0]);
    x_1.push_back(temp[1]);
    x_2.push_back(temp[2]);
    x_3.push_back(temp[3]);
  }

  correlation[0] = getCorrelation(x_0, x_2);
  correlation[1] = getCorrelation(x_1, x_3);
  tangent[0] = getGradient(x_0, x_2);
  tangent[1] = getGradient(x_1, x_3);
}

void RtreeManipulation::calcKstar(const std::vector<point5D> &points_vector,
                                  std::vector<double> &tangent,
                                  std::vector<double> &correlation) {

  std::vector<double> x_0;
  std::vector<double> x_1;
  std::vector<double> x_2;
  std::vector<double> x_3;

  for (point5D n : points_vector) {
    std::vector<double> temp = convertToVector(n);
    x_0.push_back(temp[0]);
    x_1.push_back(temp[1]);
    x_2.push_back(temp[2]);
    x_3.push_back(temp[3]);
  }

  correlation[0] = getCorrelation(x_0, x_2);
  correlation[1] = getCorrelation(x_1, x_3);
  tangent[0] = getGradient(x_0, x_2);
  tangent[1] = getGradient(x_1, x_3);
}

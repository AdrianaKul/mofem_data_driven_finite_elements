#include <DiffusionNonlinearGraphiteProblem.hpp>
#include <OperatorsForDD.hpp>

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpDomainGradGrad = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpGradGrad<1, 1, 2>;

using OpDomainMass = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<1, 2>;

using OpBaseGradH = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixVectorTimesGrad<1, 2, 2>;

using OpBoundaryMass = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<1, 1>;

using OpBoundarySourceRhs = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpBoundaryTimesScalarField = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpBaseTimesScalarField<1>;

// boundary conditions(start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 1, 1>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 1, 1>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

MoFEMErrorCode DiffusionNonlinearGraphite::runProblem() {
  MoFEMFunctionBegin;

  CHKERR setParamValues();
  CHKERR readMesh();
  CHKERR setupProblem();
  CHKERR setIntegrationRules();
  CHKERR createClassicCommonData();
  CHKERR boundaryCondition();
  CHKERR assembleSystem();
  CHKERR getSnesInitial();
  CHKERR setSnesMonitors();
  CHKERR solveSnesSystem();
  CHKERR outputHVtkFields(commonDataPtr->counter);
  CHKERR outputCsvDataset();
  PetscBool flg_ana_square_nonlinear_sincos = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_nonlinear_sincos",
                             &flg_ana_square_nonlinear_sincos, PETSC_NULL);
  if (flg_ana_square_nonlinear_sincos)
    CHKERR postProcessErrors();

  // Switch off fields
  auto off_field = [&](auto fe_name, auto field_name) {
    CHKERR mField.modify_finite_element_off_field_row(fe_name, field_name);
    CHKERR mField.modify_finite_element_off_field_col(fe_name, field_name);
  };

  off_field(simpleInterface->getDomainFEName(), valueField);
  off_field(simpleInterface->getBoundaryFEName(), valueField);

  // produce .h5m file to be compared to poisson_dd_H data driven results
  mField.get_moab().write_file("analysis_mesh.h5m", "MOAB",
                               "PARALLEL=WRITE_PART");

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearGraphite::setupProblem() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField(valueField, H1,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(valueField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->setFieldOrder(valueField, oRder);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataSnesDD>();

  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;
  DiffusionDD::commonDataPtr = commonDataPtr;

  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-nonlinear_a", &nonlinear_a,
                             PETSC_NULL);
  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-nonlinear_b", &nonlinear_b,
                             PETSC_NULL);
  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-nonlinear_c", &nonlinear_c,
                             PETSC_NULL);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearGraphite::boundaryCondition() {
  MoFEMFunctionBegin;

  auto bc_mng = mField.getInterface<BcManager>();
  CHKERR bc_mng->pushMarkDOFsOnEntities(simpleInterface->getProblemName(),
                                        "PRESSURE", valueField, 0, 0);

  auto &bc_map = bc_mng->getBcMapByBlockName();
  boundaryMarker = boost::make_shared<std::vector<char unsigned>>();
  for (auto b : bc_map) {
    if (std::regex_match(b.first, std::regex("(.*)PRESSURE(.*)"))) {
      boundaryMarker->resize(b.second->bcMarkers.size(), 0);
      for (int i = 0; i != b.second->bcMarkers.size(); ++i) {
        (*boundaryMarker)[i] |= b.second->bcMarkers[i];
      }
      pressureBcRange.merge(*(b.second->getBcEntsPtr()));
    }
  }

  Range boundary_vertices;
  CHKERR mField.get_moab().get_connectivity(pressureBcRange, boundary_vertices,
                                            true);
  pressureBcRange.merge(boundary_vertices);

  // Store entities for fieldsplit (block) solver
  boundaryEntitiesForFieldsplit = pressureBcRange;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearGraphite::assembleSystem() {
  MoFEMFunctionBegin;

  CHKERR calculateJacobian(feDomainLhsPtr);
  CHKERR calculateJacobian(feDomainRhsPtr);

  { // domain Lhs
    auto vec_val_ptr = boost::make_shared<VectorDouble>();
    auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpNonlinearLhs(valueField, valueField, vec_val_ptr, mat_grad_ptr,
                           nonlinear_a, nonlinear_b, nonlinear_c));
    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }
  { // domain Rhs
    auto vec_val_ptr = boost::make_shared<VectorDouble>();
    auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpNonlinearRhs(valueField, vec_val_ptr, mat_grad_ptr, nonlinear_a,
                           nonlinear_b, nonlinear_c));
    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, valueField, -1., Sev::inform);
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }
  { // boundary Lhs
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));

    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpBoundaryMass(valueField, valueField, oneFunction,
                           boost::make_shared<Range>(pressureBcRange)));

    feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }
  { // boundary Rhs
    auto vec_val_ptr = boost::make_shared<VectorDouble>();

    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));

    // (u,u_bar)
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, -1.,
        Sev::inform);

    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpBoundaryTimesScalarField(
        valueField, vec_val_ptr, oneFunction,
        boost::make_shared<Range>(pressureBcRange)));

    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

    // (u,q_bar)
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, -1.,
        Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearGraphite::setSnesMonitors() {
  MoFEMFunctionBegin;

  // ctx for monitor containing the discrete manager
  auto snes_ctx_ptr = smartGetDMSnesCtx(simpleInterface->getDM());

  combinedPointers = boost::make_shared<CombinedPointers>();

  combinedPointers->commonDataPtr = commonDataPtr;
  combinedPointers->snesCtxPtr = snes_ctx_ptr;

  // define monitors which are executed at the begining of each iteration
  // postProc on integration points
  auto monitor_vtk_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                  void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    auto &m_field = snes_ptr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();

    boost::shared_ptr<PostProcFaceOnRefinedMesh> fePostProcPtr;
    fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
        new PostProcFaceOnRefinedMesh(m_field));

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    fePostProcPtr->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    fePostProcPtr->getOpPtrVector().push_back(
        new OpSetInvJacH1ForFace(inv_jac_ptr));

    // Generate post-processing mesh
    CHKERR fePostProcPtr->generateReferenceElementMesh();
    // Postprocess only field values
    const Field_multiIndex *fields;
    m_field.get_fields(&fields);
    for (auto &it : *fields) {
      fePostProcPtr->addFieldValuesPostProc(it->getName());
    }
    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), fePostProcPtr);

    // write output
    CHKERR fePostProcPtr->writeFile(
        "out_iteration_" + boost::lexical_cast<std::string>(its) + ".h5m");

    MoFEMFunctionReturn(0);
  };

  auto monitor_print_moab = [](SNES snes, PetscInt its, PetscReal fgnorm,
                               void *snes_ctx) {
    MoFEMFunctionBegin;
    auto snes_ptr = boost::static_pointer_cast<SnesCtx>(snes_ctx);

    string gauss_name;
    std::ostringstream strma;
    strma << "out_moab_" << its << ".h5m";
    gauss_name = strma.str();
    CHKERR snes_ptr->mField.get_moab().write_file(gauss_name.c_str(), "MOAB",
                                                  "PARALLEL=WRITE_PART");

    MoFEMFunctionReturn(0);
  };

  auto monitor_errors_post_proc = [](SNES snes, PetscInt its, PetscReal fgnorm,
                                     void *combined_ctx) {
    MoFEMFunctionBegin;
    auto combined_ptr =
        boost::static_pointer_cast<CombinedPointers>(combined_ctx);

    auto common_data_ptr = combined_ptr->commonDataPtr;

    auto &m_field = combined_ptr->snesCtxPtr->mField;
    Simple *simple_interface = m_field.getInterface<Simple>();
    SmartPetscObj<DM> dm = simple_interface->getDM();
    int patchIntegNum = 3;
    boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
        new PatchIntegFaceEle(m_field, patchIntegNum));

    auto post_proc_rule_hook = [](int, int, int p) -> int { return 2 * p; };
    PostProcGauss->getRuleHook = post_proc_rule_hook;

    auto det_ptr = boost::make_shared<VectorDouble>();
    auto jac_ptr = boost::make_shared<MatrixDouble>();
    auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateHOJacForFace(jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpSetInvJacH1ForFace(inv_jac_ptr));

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("T", common_data_ptr->mValPtr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>("T", common_data_ptr->mGradPtr));

    auto k_function = [&](const double T, const double, const double) {
      double aa = 1.;
      double bb = 1.;
      double cc = 1.;
      return aa + bb * T + cc * square(T);
    };
    // analytical solutions
    {
      PetscBool flg_ana_square_nonlinear_sincos = PETSC_FALSE;
      CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_nonlinear_sincos",
                                 &flg_ana_square_nonlinear_sincos, PETSC_NULL);

      if (flg_ana_square_nonlinear_sincos) {
        PostProcGauss->getOpPtrVector().push_back(
            new OpPostProcErrorPressureIntegPts(
                "T", common_data_ptr, analyticalFunction_squareSinCos,
                analyticalFunctionGrad_squareSinCos));
        auto analyticalFunctionFlux = [&](const double x, const double y,
                                          const double z) {
          double a = 2.;
          double aa = 1.;
          double bb = 1.;
          double cc = 1.;

          VectorDouble res;
          res.resize(2);
          res[0] = -a * M_PI * cos(a * M_PI * x) * cos(a * M_PI * y) *
                   (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                             (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x)));
          res[1] =
              a * M_PI * sin(a * M_PI * x) *
              (aa + cos(a * M_PI * y) * sin(a * M_PI * x) *
                        (bb + cc * cos(a * M_PI * y) * sin(a * M_PI * x))) *
              sin(a * M_PI * y);
          return res;
        };
        PostProcGauss->getOpPtrVector().push_back(
            new OpPostProcErrorFluxDerivedIntegPts(
                "T", common_data_ptr, analyticalFunctionFlux, k_function));
      }
    }

    PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
        common_data_ptr->petscVec, CommonDataSnesDD::GAUSS_COUNT));

    CHKERR DMoFEMLoopFiniteElements(
        dm, simple_interface->getDomainFEName().c_str(), PostProcGauss);

    MoFEMFunctionReturn(0);
  };

  // collect data across all integration points for the global vector
  auto monitor_combine_global_vector = [](SNES, PetscInt its, PetscReal,
                                          void *combined_ctx) {
    MoFEMFunctionBegin;

    auto combined_ptr =
        boost::static_pointer_cast<CombinedPointers>(combined_ctx);
    auto common_data_ptr = combined_ptr->commonDataPtr;
    auto snes_ptr = combined_ptr->snesCtxPtr;

    auto &m_field = snes_ptr->mField;

    CHKERR VecAssemblyBegin(common_data_ptr->petscVec);
    CHKERR VecAssemblyEnd(common_data_ptr->petscVec);

    const double *array;
    CHKERR VecGetArrayRead(common_data_ptr->petscVec, &array);

    if (m_field.get_comm_rank() == 0) {

      common_data_ptr->vOlume = array[CommonDataSnesDD::VOL_E];

      common_data_ptr->rmsErr.push_back(
          sqrt((array[CommonDataSnesDD::VALUE_ERROR]) /
               array[CommonDataSnesDD::VOL_E]));
      common_data_ptr->gradErr.push_back(
          sqrt((array[CommonDataSnesDD::GRAD_ERROR]) /
               array[CommonDataSnesDD::VOL_E]));
      common_data_ptr->fluxErr.push_back(
          sqrt((array[CommonDataSnesDD::FLUX_ERROR]) /
               array[CommonDataSnesDD::VOL_E]));
      common_data_ptr->numberIntegrationPoints.push_back(
          array[CommonDataSnesDD::GAUSS_COUNT]);

      std::ofstream error_file;
      error_file.open("errors_long_file.csv", ios::app);
      // populate the file with points
      error_file << common_data_ptr->rmsErr.back() << ","
                 << common_data_ptr->gradErr.back() << ","
                 << common_data_ptr->fluxErr.back() << "\n";
      // close writing into file
      error_file.close();

      DDLogTag;
      MOFEM_LOG("WORLD", Sev::inform)
          << "Iteration: " << its << " Volume: " << common_data_ptr->vOlume
          << " RMS: " << common_data_ptr->rmsErr.back()
          << " Grad: " << common_data_ptr->gradErr.back()
          << " Flux: " << common_data_ptr->fluxErr.back()
          << " Gauss: " << common_data_ptr->numberIntegrationPoints.back();
    }

    CHKERR VecRestoreArrayRead(common_data_ptr->petscVec, &array);

    CHKERR VecZeroEntries(common_data_ptr->petscVec);

    MoFEMFunctionReturn(0);
  };

  CHKERR SNESMonitorSet(snesSolver,
                        (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                           void *))MoFEMSNESMonitorFields,
                        (void *)(snes_ctx_ptr.get()), nullptr);

  if (!skip_vtk)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_vtk_post_proc,
                          (void *)(snes_ctx_ptr.get()), nullptr);

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_print_moab,
                          (void *)(snes_ctx_ptr.get()), nullptr);
  }

  // check errors w.r.t analytical solution
  PetscBool flg_ana_square_nonlinear_sincos = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-ana_square_nonlinear_sincos",
                             &flg_ana_square_nonlinear_sincos, PETSC_NULL);
  if (flg_ana_square_nonlinear_sincos)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal,
                                             void *))monitor_errors_post_proc,
                          (void *)(combinedPointers.get()), nullptr);

  if (flg_ana_square_nonlinear_sincos)
    CHKERR SNESMonitorSet(snesSolver,
                          (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal, void *))
                              monitor_combine_global_vector,
                          (void *)(combinedPointers.get()), nullptr);

  MoFEMFunctionReturn(0);
}

// printing material dataset

MoFEMErrorCode DiffusionNonlinearGraphite::outputCsvDataset(bool header) {
  MoFEMFunctionBegin;

  std::cout << "Outputting csv dataset" << std::endl;
  std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
               "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            << std::endl;

  PetscBool add_to_csv = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-add_to_csv", &add_to_csv,
                             PETSC_NULL);

  std::string csv_GQ = "data_GQ_generated.csv";
  std::string csv_VGQ = "data_VGQ_generated.csv";

  if (!add_to_csv) {
    std::ofstream myfile;
    myfile.open(csv_GQ);
    myfile << "gradx,grady,fluxx,fluxy\n"; // headers
    myfile.close();

    myfile.open(csv_VGQ);
    myfile << "gradx,grady,fluxx,fluxy,value\n"; // headers
    myfile.close();
  }

  moab::Core mb_post;                   // create database
  moab::Interface &moab_proc = mb_post; // create interface to database

  boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
      new PatchIntegFaceEle(mField, patchIntegNum));

  auto domain_post_proc_rule = [](int, int, int p) -> int { return 2 * p; };
  PostProcGauss->getRuleHook = domain_post_proc_rule;

  auto vec_val_ptr = boost::make_shared<VectorDouble>();
  auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
  auto mat_flux_ptr = boost::make_shared<MatrixDouble>();

  double max_value = 0.;
  double max_grad = 0.;
  double max_flux = 0.;
  double s_t = 1.;
  double s_q = 1.;

  auto get_k_from_val_function = [&](const double val, const double,
                                     const double) {
    return nonlinear_a + nonlinear_b * val + nonlinear_c * square(val);
  };

  PetscBool scaling = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-scaling", &scaling, PETSC_NULL);

  if (scaling) {
    CHKERR calculateJacobian(PostProcGauss);

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, vec_val_ptr));

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));

    PostProcGauss->getOpPtrVector().push_back(
        new OpGetFluxFromVal(valueField, vec_val_ptr, mat_grad_ptr,
                             mat_flux_ptr, get_k_from_val_function));

    PostProcGauss->getOpPtrVector().push_back(
        new OpGetMaxVals(valueField, vec_val_ptr, mat_grad_ptr, mat_flux_ptr,
                         &max_value, &max_grad, &max_flux));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

    std::cout << max_value << std::endl;
    std::cout << max_grad << std::endl;
    std::cout << max_flux << std::endl;

    PostProcGauss->getOpPtrVector().clear();
  }

  if (max_value && max_grad && max_flux) {
    s_t = max_grad / max_value;
    s_q = max_grad / max_flux;
  }

  std::cout << s_t << std::endl;
  std::cout << s_q << std::endl;

  std::ofstream myfile;
  myfile.open("scaling.in");
  myfile << s_q << std::endl;
  myfile << s_t << std::endl;
  myfile.close();

  CHKERR calculateJacobian(PostProcGauss);

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, vec_val_ptr));

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));

  // if (flgNonlinearToP)
  PostProcGauss->getOpPtrVector().push_back(
      new OpGetFluxFromVal(valueField, vec_val_ptr, mat_grad_ptr, mat_flux_ptr,
                           get_k_from_val_function));

  PostProcGauss->getOpPtrVector().push_back(
      new OperatorsForDD::OpPostProcSaveCsvDatasetGQ<2, 2>(
          valueField, mat_grad_ptr, mat_flux_ptr, csv_GQ, s_q));

  PostProcGauss->getOpPtrVector().push_back(
      new OperatorsForDD::OpPostProcSaveCsvDatasetVGQ<2, 2>(
          valueField, vec_val_ptr, mat_grad_ptr, mat_flux_ptr, csv_VGQ, s_q,
          s_t));

  mb_post.delete_mesh();

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

  MoFEMFunctionReturn(0);
}

// Operators

MoFEMErrorCode DiffusionNonlinearGraphite::OpNonlinearLhs::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (nb_row_dofs && nb_col_dofs) {

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    FTensor::Index<'k', 2> k;

    locLhs.resize(nb_row_dofs, nb_col_dofs, false);
    locLhs.clear();

    const int nb_base_functions = row_data.getN().size2();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    auto t_val = getFTensor0FromVec(*(valPtr));
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));

    // get base functions on row
    auto t_row_base = row_data.getFTensor0N();
    // get derivatives of base functions on row
    auto t_row_diff_base = row_data.getFTensor1DiffN<2>();

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL MATRIX
    for (int gg = 0; gg != nb_integration_points; gg++) {
      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_row_dofs; ++rr) {
        // get base functions on column
        auto t_col_base = col_data.getFTensor0N(gg, 0);
        // get derivatives of base functions on column
        auto t_col_diff_base = col_data.getFTensor1DiffN<2>(gg, 0);

        for (int cc = 0; cc != nb_col_dofs; cc++) {
          locLhs(rr, cc) +=
              (((nonlinearA + nonlinearB * t_val + nonlinearC * square(t_val)) *
                t_row_diff_base(i) * t_col_diff_base(i)) +
               ((nonlinearB + 2 * nonlinearC * t_val) * t_grad(j) *
                t_row_diff_base(j) * t_col_base)) *
              a;

          // move to the next base functions on column
          ++t_col_base;
          // move to the derivatives of the next base function on column
          ++t_col_diff_base;
        }

        // move to the next base function on row
        ++t_row_base;
        // move to the derivatives of the next base function on row
        ++t_row_diff_base;
      }

      for (; rr != nb_base_functions; ++rr) {
        ++t_row_base;
        ++t_row_diff_base;
      }

      // move to the weight of the next integration point
      ++t_w;
      // move to the solution (field value) at the next integration point
      ++t_val;
      // move to the gradient of field value at the next integration point
      ++t_grad;
    }

    CHKERR MatSetValues<EssentialBcStorage>(
        getSNESB(), row_data, col_data, &*locLhs.data().begin(), ADD_VALUES);
  }

  // FILL VALUES OF LOCAL MATRIX ENTRIES TO THE GLOBAL MATRIX

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
DiffusionNonlinearGraphite::OpNonlinearRhs::doWork(int side, EntityType type,
                                                   EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    locRhs.resize(nb_dofs, false);
    locRhs.clear();

    // get element area
    const double area = getMeasure();

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();
    const int nb_base_functions = data.getN().size2();
    // get integration weights
    auto t_w = getFTensor0IntegrationWeight();

    auto t_val = getFTensor0FromVec(*(valPtr));
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));

    // get base functions
    auto t_diff_base = data.getFTensor1DiffN<2>();

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      const double a = t_w * area;

      int rr = 0;
      for (; rr != nb_dofs; rr++) {
        locRhs[rr] +=
            t_diff_base(i) * a * t_grad(i) *
            (nonlinearA + nonlinearB * t_val + nonlinearC * square(t_val));

        // move to the next base function
        ++t_diff_base;
      }
      for (; rr != nb_base_functions; ++rr)
        ++t_diff_base;

      ++t_val;
      ++t_grad;
      // move to the weight of the next integration point
      ++t_w;
    }

    CHKERR VecSetValues<EssentialBcStorage>(getSNESf(), data, &*locRhs.begin(),
                                            ADD_VALUES);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
DiffusionNonlinearGraphite::OpGetFluxFromVal::doWork(int side, EntityType type,
                                                     EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();

    auto t_val = getFTensor0FromVec(*(valPtr));
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));
    fluxPtr->resize(2, nb_integration_points, false);
    fluxPtr->clear();
    auto t_flux = getFTensor1FromMat<2>(*(fluxPtr));

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      auto k_f = kFunc(t_val, 0, 0);

      t_flux(i) = -k_f * t_grad(i);

      ++t_val;
      ++t_grad;
      ++t_flux;
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionNonlinearGraphite::OpGetMaxVals::doWork(int side,
                                                                EntityType type,
                                                                EntData &data) {
  MoFEMFunctionBegin;

  const int nb_dofs = data.getIndices().size();

  if (nb_dofs) {
    FTensor::Index<'i', 2> i;

    // get number of integration points
    const int nb_integration_points = getGaussPts().size2();

    auto t_val = getFTensor0FromVec(*(valPtr));
    auto t_grad = getFTensor1FromMat<2>(*(gradPtr));
    auto t_flux = getFTensor1FromMat<2>(*(fluxPtr));

    // START THE LOOP OVER INTEGRATION POINTS TO CALCULATE LOCAL VECTOR
    for (int gg = 0; gg != nb_integration_points; ++gg) {

      if (abs(t_val) > *maxValue)
        *maxValue = abs(t_val);

      if (abs(t_grad(0)) > *maxGrad)
        *maxGrad = abs(t_grad(0));
      if (abs(t_grad(1)) > *maxGrad)
        *maxGrad = abs(t_grad(1));
      if (abs(t_flux(0)) > *maxFlux)
        *maxFlux = abs(t_flux(0));
      if (abs(t_flux(1)) > *maxFlux)
        *maxFlux = abs(t_flux(1));

      ++t_val;
      ++t_grad;
      ++t_flux;
    }
  }

  MoFEMFunctionReturn(0);
}
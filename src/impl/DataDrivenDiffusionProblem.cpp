#include <DataDrivenDiffusionProblem.hpp>
#include <OperatorsForDD.hpp>

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

using OpDomainGradGrad = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpGradGrad<1, 1, 2>;

using OpDomainMass = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<1, 2>;

using OpDomainScalarMass = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<1, 1>;

using OpBaseGradH = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixVectorTimesGrad<1, 2, 2>;

using OpBoundaryMass = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<1, 1>;

using OpBoundarySourceRhs = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 1, 1>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 1, 1>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

template struct DiffusionDD::OpPostProcPrintingIntegPts<3>;
template struct DiffusionDD::OpPrintStarIntegPts<3>;
template struct DiffusionDD::OpAddResultToMonteMatrix<3>;
template struct DiffusionDD::OpPostProcErrorFluxIntegPts<3>;

template struct DiffusionDD::OpPostProcPrintFarIntegPts<2>;
template struct DiffusionDD::OpPostProcPrintFarIntegPts<3>;

MoFEMErrorCode DiffusionDD::SetParamDDValues() {
  MoFEMFunctionBegin;
  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "DD Config", "none");
  // Set values from param file
  CHKERR
  PetscOptionsString("-csv_tree_file", "name of a 4D material dataset file", "",
                     "dummy_tree.csv", csv_tree_file, 255, PETSC_NULL);
  CHKERR PetscOptionsInt("-data_dim", "dimensions taken from material dataset",
                         "", 4, &datasetDim, PETSC_NULL);
  CHKERR PetscOptionsBool("-dd_use_data_value",
                          "use value found from the dataset in FEM solution",
                          "", PETSC_FALSE, &dd_use_data_value, PETSC_NULL);
  CHKERR PetscOptionsBool("-skip_vtk", "set if vtk printing should be skipped",
                          "", PETSC_FALSE, &skip_vtk, PETSC_NULL);
  CHKERR PetscOptionsBool("-write_long_error_file",
                          "set if convergence of errors through iterations "
                          "wants to be seen in .csv file",
                          "", PETSC_FALSE, &flg_long_error_file, PETSC_NULL);
  CHKERR PetscOptionsBool(
      "-print_integ",
      "set if integration points should be printed to out_integ_*.h5m", "",
      PETSC_FALSE, &flg_out_integ, PETSC_NULL);
  CHKERR PetscOptionsBool("-print_moab", "set if whole moab prints to .h5m", "",
                          PETSC_FALSE, &print_moab, PETSC_NULL);
  CHKERR PetscOptionsReal("-point_dist_tol",
                          "tolerance for rms point distance error between "
                          "iterations to stop the search",
                          "", 1e-5, &tol_poi_dist, PETSC_NULL);
  CHKERR PetscOptionsInt("-max_iter", "set the maximum number of iterations",
                         "", 100, &max_iter, &flg_max_iter);
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-scaling", &flg_scaling,
                             PETSC_NULL);
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-use_line", &flg_use_line,
                             PETSC_NULL);
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-rand_ini", &flg_rand_ini,
                             PETSC_NULL);

  CHKERR PetscOptionsInt("-point_average",
                         "set if the data search requires averaging", "", 10,
                         &point_average, &flg_point_average);

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "-csv_tree_file " << csv_tree_file;
  MOFEM_LOG("WORLD", Sev::inform) << "-data_dim " << datasetDim;
  if (dd_use_data_value)
    MOFEM_LOG("WORLD", Sev::inform)
        << "-dd_use_data_value used .: using values from dataset in solution"
        << datasetDim;
  MOFEM_LOG("WORLD", Sev::inform) << "-skip_vtk " << skip_vtk;
  if (flg_long_error_file)
    MOFEM_LOG("WORLD", Sev::inform)
        << "-write_long_error_file used .: convergence of errors through "
           "iterations is saved in .csv file";
  if (flg_out_integ)
    MOFEM_LOG("WORLD", Sev::inform)
        << "-print_integ used .: printing values at integration points";
  MOFEM_LOG("WORLD", Sev::inform) << "-print_moab " << print_moab;
  MOFEM_LOG("WORLD", Sev::inform) << "-point_dist_tol " << tol_poi_dist;
  if (flg_max_iter)
    MOFEM_LOG("WORLD", Sev::inform) << "-max_iter " << max_iter;
  if (flg_scaling)
    MOFEM_LOG("WORLD", Sev::inform) << "-scaling used";
  if (flg_use_line)
    MOFEM_LOG("WORLD", Sev::inform) << "-use_line used (instead of dataset "
                                       "equation line will determine * values)";

  if (flg_point_average)
    MOFEM_LOG("WORLD", Sev::inform) << "-point_average " << point_average;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::setupProblem() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField(valueField, H1,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDomainField(fluxField, L2, AINSWORTH_LEGENDRE_BASE,
                                         2);
  CHKERR simpleInterface->addDomainField(lagrangeField, H1,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(valueField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(lagrangeField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->setFieldOrder(valueField, oRder);
  CHKERR simpleInterface->setFieldOrder(fluxField, oRder - 1);
  CHKERR simpleInterface->setFieldOrder(lagrangeField, oRder);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataDD>();

  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::createDDCommonData() {
  MoFEMFunctionBegin;

  // grad p* and q* found in the data set
  commonDataPtr->mValStarPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mGradStarPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mFluxStarPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mKStarPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mKStarCorrelationPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mValRefPtr = boost::make_shared<VectorDouble>();

  // storage for averages and standard deviations for monte-carlo
  commonDataPtr->mValAvePtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mGradAvePtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mFluxAvePtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mValSigmaPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->mGradSigmaPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mFluxSigmaPtr = boost::make_shared<MatrixDouble>();

  searchData.mKStarPtr = commonDataPtr->mKStarPtr;
  searchData.mKStarCorrelationPtr = commonDataPtr->mKStarCorrelationPtr;

  // monte-carlo
  MatCreate(PETSC_COMM_SELF, &matGlobalResults);

  // counter for iterations done
  commonDataPtr->counter = 0;

  commonDataPtr->flgScaling = flg_scaling;
  if (flg_scaling) {
    std::vector<double> scaling =
        RtreeManipulation::loadScalingData("scaling.in");

    commonDataPtr->scaleQ = scaling[0];
    if (scaling[1])
      commonDataPtr->scaleV = scaling[1];
    dummy_k = 1.;
  }
  commonDataPtr->linear_k = dummy_k;

  commonDataPtr->flgPointAverage = flg_point_average;
  commonDataPtr->pointAverage = point_average;
  commonDataPtr->skip_vtk = skip_vtk;
  commonDataPtr->flgPrintInteg = flg_out_integ;

  commonDataPtr->pointDistance = boost::make_shared<VectorDouble>();

  DDLogTag;
  if (!flg_use_line) { // create rtree
    BOOST_LOG_SCOPED_THREAD_ATTR("Timeline", attrs::timer());
    MOFEM_LOG("WORLD", Sev::inform) << "Reading rtree from " << csv_tree_file;
    switch (datasetDim) {
    case 4:
      CHKERR RtreeManipulation::loadFileData(csv_tree_file,
                                             commonDataPtr->rtree4D);
      dummy_count = commonDataPtr->rtree4D.size();
      break;
    case 5:
      CHKERR RtreeManipulation::loadFileData(csv_tree_file,
                                             commonDataPtr->rtree5D);
      dummy_count = commonDataPtr->rtree5D.size();
      break;
    default:
      MOFEM_LOG("WORLD", Sev::inform)
          << "Wrong data dimension " << datasetDim << ", defaulting to 4D";
      CHKERR RtreeManipulation::loadFileData(csv_tree_file,
                                             commonDataPtr->rtree4D);
      dummy_count = commonDataPtr->rtree4D.size();
      datasetDim = 4;
    }

    MOFEM_LOG("WORLD", Sev::inform)
        << "rtree with " << dummy_count << " points loaded";
  }
  if (flg_use_line)
    MOFEM_LOG("WORLD", Sev::inform)
        << "Using a line instead of dataset, therefore rtree is not created";

  searchData.mValPtr = commonDataPtr->mValPtr;
  searchData.mGradPtr = commonDataPtr->mGradPtr;
  searchData.mFluxPtr = commonDataPtr->mFluxPtr;
  searchData.mValStarPtr = commonDataPtr->mValStarPtr;
  searchData.mGradStarPtr = commonDataPtr->mGradStarPtr;
  searchData.mFluxStarPtr = commonDataPtr->mFluxStarPtr;
  searchData.pointDistance = commonDataPtr->pointDistance;
  switch (datasetDim) {
  case 4:
    searchData.rtree4D = commonDataPtr->rtree4D;
    break;
  case 5:
    searchData.rtree5D = commonDataPtr->rtree5D;
    break;
  }
  searchData.flgPointAverage = commonDataPtr->flgPointAverage;
  searchData.pointAverage = commonDataPtr->pointAverage;
  searchData.linear_k = commonDataPtr->linear_k;

  // checking if the previous fields are here
  if (mField.check_field("P_reference")) {
    std::cout << "The P_reference field is here :D\n";
    commonDataPtr->pReference = true;
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::removeLambda() {
  MoFEMFunctionBegin;
  auto remove_lambda = [&](auto &ents) {
    auto problem_manager = mField.getInterface<ProblemsManager>();
    CHKERR problem_manager->removeDofsOnEntities(
        simpleInterface->getProblemName(), lagrangeField, ents, 0, 1);
  };
  remove_lambda(pressureBcRange);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::assembleSystem() {
  MoFEMFunctionBegin;

  CHKERR calculateJacobian(feDomainLhsPtr);
  CHKERR calculateJacobian(feDomainRhsPtr);

  { // LHS of domain elements - Push operators to the Pipeline
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    { // parts corresponding to * points
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpDomainGradGrad(valueField, valueField, s_p_Function));
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpDomainMass(fluxField, fluxField, s_q_Function));
      if (datasetDim == 5)
        feDomainLhsPtr->getOpPtrVector().push_back(
            new OpDomainScalarMass(valueField, valueField, s_p_Function));
    }
    { // Lagrange multiplier parts
      auto unity = []() { return 1.0; };
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpBaseGradH(fluxField, lagrangeField, unity, true));
    }
    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // source added to the domain Rhs
    // (l,f)
    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, lagrangeField, -dummy_k,
        Sev::inform);
  }

  { // Push operators to the Pipeline that is responsible for calculating LHS of
    // boundary elements
    auto beta = [](const double, const double, const double) { return 1.0; };
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));
    feBoundaryLhsPtr->getOpPtrVector().push_back(
        new OpBoundaryMass(valueField, valueField, beta,
                           boost::make_shared<Range>(pressureBcRange)));
    feBoundaryLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  { // Push operators to the Pipeline that is responsible for calculating RHS of
    // boundary elements
    // (u, u_bar)
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, false, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, valueField, dummy_k,
        Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));

    // (l, q_bar)
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(lagrangeField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, lagrangeField,
        -commonDataPtr->scaleQ, Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(lagrangeField));
  }

  { // get gradients of pressure and flux to find in the data set
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField,
                                              commonDataPtr->mGradPtr));

    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<2>(fluxField,
                                            commonDataPtr->mFluxPtr));
  }

  { // find grad p* and q* in the data set and get point distance errors
    if (!flg_use_line) {
      switch (datasetDim) {
      case 4:
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData4D<2>(valueField, searchData));
        break;
      case 5:
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OpCalculateScalarFieldValues(valueField,
                                             commonDataPtr->mValPtr));
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData5D<2>(valueField, searchData));
        break;
      }
    }
    if (flg_use_line)
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindClosest<2>(valueField, searchData));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpPointError(valueField, commonDataPtr));
  }

  { // RHS of domain elements - Push operators to the Pipeline
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(valueField, true, boundaryMarker));
    if (datasetDim == 5)
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::Op1dStarRhs(valueField, commonDataPtr->mValPtr,
                                          s_p_Function));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpGradStarRhs(
            valueField, commonDataPtr->mGradStarPtr, s_p_Function));
    feDomainRhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpL2StarRhs<2>(
            fluxField, commonDataPtr->mFluxStarPtr, s_q_Function));
    feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(valueField));
  }

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    feDomainRhsPtr->getOpPtrVector().push_back(new OpPrintStarIntegPts<2>(
        valueField, commonDataPtr, mField.get_moab()));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::errorFileNameSetUp() {
  MoFEMFunctionBegin;

  // create appropriate errors file name
  std::ostringstream para;
  // para << "_ord_" << oRder << "_datapoints_" << dummy_count << "_k_"
  //      << commonDataPtr->scaleQ;
  para << "_long_file";

  string parameters;
  parameters = para.str();

  std::ostringstream strm;
  strm << "errors" << parameters << ".csv";
  csvErrorsName = strm.str();

  // create the file and put in the headers
  if (mField.get_comm_rank() == 0 && flg_long_error_file) {
    std::ofstream error_file;
    error_file.open(csvErrorsName);
    error_file << "rmsErr" << parameters << ", rmsGradErr" << parameters
               << ", rmsFluxErr" << parameters << ", rmsPointDistErr"
               << parameters << std::endl; // headers
    error_file.close();
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::errorCalculation(bool print_on_integ) {
  MoFEMFunctionBegin;

  // post processing on integration points
  {
    moab::Core mb_post;                   // create database
    moab::Interface &moab_proc = mb_post; // create interface to database

    boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
        new PatchIntegFaceEle(mField, patchIntegNum));
    PostProcGauss->getRuleHook =
        feDomainRhsPtr
            ->getRuleHook; // use the same integration rule as domainRhs

    CHKERR calculateJacobian(PostProcGauss);

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldGradient<2>(valueField,
                                              commonDataPtr->mGradPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateVectorFieldValues<2>(fluxField,
                                            commonDataPtr->mFluxPtr));

    CHKERR postGetAnalyticalValues(PostProcGauss);

    if (flgAnalytical) {

      auto kFunction = [&](const double, const double, const double) {
        return dummy_k;
      };

      PostProcGauss->getOpPtrVector().push_back(new OpGetFluxFromGrad<2>(
          commonDataPtr->mGradPtr, kFunction, commonDataPtr->mFluxPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
          commonDataPtr->mValPtr, commonDataPtr->petscVec,
          CommonDataClassic::VALUE_ERROR, commonDataPtr->mValFuncPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
          commonDataPtr->mGradPtr, commonDataPtr->petscVec,
          CommonDataClassic::GRAD_ERROR, commonDataPtr->mGradFuncPtr));

      PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
          commonDataPtr->mFluxPtr, commonDataPtr->petscVec,
          CommonDataClassic::FLUX_ERROR, commonDataPtr->mFluxFuncPtr));
    }

    PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
        commonDataPtr->petscVec, CommonDataClassic::GAUSS_COUNT));
    PostProcGauss->getOpPtrVector().push_back(
        new OpGetArea(commonDataPtr->petscVec, CommonDataClassic::VOL_E));

    // if (commonDataPtr->pReference) {
    //   PostProcGauss->getOpPtrVector().push_back(
    //       new OpCalculateScalarFieldValues("P_reference",
    //                                        commonDataPtr->mValRefPtr));
    //   PostProcGauss->getOpPtrVector().push_back(
    //       new OpPostProcRefErrorIntegPts(valueField, commonDataPtr));
    // }

    if (skip_vtk != PETSC_TRUE) {
      // get gradients of pressure and flux from previous step to find in the
      // data set
      if (!flg_use_line) {
        switch (datasetDim) {
        case 4:
          PostProcGauss->getOpPtrVector().push_back(
              new OperatorsForDD::OpFindData4D<2>(valueField, searchData,
                                                  commonDataPtr->scaleQ));
          break;
        case 5:
          PostProcGauss->getOpPtrVector().push_back(
              new OpCalculateScalarFieldValues(valueField,
                                               commonDataPtr->mValPtr));
          PostProcGauss->getOpPtrVector().push_back(
              new OperatorsForDD::OpFindData5D<2>(valueField, searchData));
          break;
        }
      }
      if (flg_use_line)
        PostProcGauss->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindClosest<2>(valueField, searchData,
                                                 commonDataPtr->scaleQ));
      if (print_on_integ)
        PostProcGauss->getOpPtrVector().push_back(
            new OpPostProcPrintingIntegPts<2>(fluxField, commonDataPtr, mb_post,
                                              analyticalFunction));
    }

    mb_post.delete_mesh();

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

    if (print_on_integ) {
      string out_file_name;
      std::ostringstream strm;
      strm << "out_integ_pts_" << commonDataPtr->counter << ".h5m";
      out_file_name = strm.str();
      CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                "PARALLEL=WRITE_PART");
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::errorHandlingWithinLoop(bool allowed = true) {
  MoFEMFunctionBegin;

  // Assemble MPI vector
  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);

  const double *array;
  CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array);

  if (mField.get_comm_rank() == 0) {

    commonDataPtr->distVec.push_back(array[CommonDataDD::ACCUM_POINT_ERROR]);

    commonDataPtr->vOlume = array[CommonDataDD::VOL_E]; // volume of the domain

    commonDataPtr->rmsPointDistErr.push_back(
        sqrt((array[CommonDataDD::POINT_ERROR]) / array[CommonDataDD::VOL_E]));
    commonDataPtr->rmsErr.push_back(
        sqrt((array[CommonDataDD::VALUE_ERROR]) / array[CommonDataDD::VOL_E]));
    commonDataPtr->gradErr.push_back(
        sqrt((array[CommonDataDD::GRAD_ERROR]) / array[CommonDataDD::VOL_E]));
    commonDataPtr->fluxErr.push_back(
        sqrt((array[CommonDataDD::FLUX_ERROR]) / array[CommonDataDD::VOL_E]));

    DDLogTag;
    MOFEM_LOG("WORLD", Sev::inform)
        << "accumulated distance: " << commonDataPtr->distVec.back();

    // assuming that the same accumulated distance means that the same data
    // points were chosen. If so, don"t do more iterations
    if (std::abs(std::abs(commonDataPtr->distVec.back()) -
                 std::abs(commonDataPtr->distVec.end()[-2])) < tol_poi_dist &&
        commonDataPtr->counter > 2 && allowed) {
      CHKERR VecSetValue(go_per_proc, mField.get_comm_rank(), 1, INSERT_VALUES);
    }
    // maximum number of iterations
    if (flg_max_iter && commonDataPtr->counter == max_iter && allowed) {
      CHKERR VecSetValue(go_per_proc, mField.get_comm_rank(), 1, INSERT_VALUES);
    }

    MOFEM_LOG("WORLD", Sev::inform) << "counter: " << commonDataPtr->counter;

    if (mField.get_comm_rank() == 0 && flg_long_error_file) {
      // open the error file with appending to the file
      std::ofstream error_file;
      error_file.open(csvErrorsName, ios::app);
      // populate the file with points
      error_file << commonDataPtr->rmsErr.back() << ", "
                 << commonDataPtr->gradErr.back() << ","
                 << commonDataPtr->fluxErr.back() << ","
                 << commonDataPtr->rmsPointDistErr.back() << "\n";
      // close writing into file
      error_file.close();
    }
  }

  CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::solveDDLoopSystem() {
  MoFEMFunctionBegin;

  CHKERR getKSPinitial();

  CHKERR errorFileNameSetUp();

  double go = 0.;
  // Vec go_per_proc;
  CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE, &go_per_proc);

  while (go <= 10e-5) {
    // Zero global vector
    CHKERR VecZeroEntries(commonDataPtr->petscVec);

    CHKERR solveSystem();

    CHKERR outputResults(); // also post processing errors

    // add to the counter
    commonDataPtr->counter = commonDataPtr->counter + 1;

    CHKERR errorHandlingWithinLoop();

    CHKERR VecAssemblyBegin(go_per_proc);
    CHKERR VecAssemblyEnd(go_per_proc);
    CHKERR VecSum(go_per_proc, &go);
  }

  if (mField.get_comm_rank() == 0) {

    const double *array;
    CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array);

    DDLogTag;
    MOFEM_LOG("WORLD", Sev::inform)
        << "Finished with " << commonDataPtr->distVec.size() << " iterations. "
        << "Convergence: ";

    // print the errors at the end of analysis
    for (int i = 0; i < commonDataPtr->distVec.size(); i++) {
      MOFEM_LOG("WORLD", Sev::inform)
          << commonDataPtr->fluxErr[i] << " <- fluxErr ; rmsErr -> "
          << commonDataPtr->rmsErr[i];
    }

    commonDataPtr->numberIntegrationPoints.push_back(
        array[CommonDataDD::GAUSS_COUNT]);

    CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array);
  }

  CHKERR saveSumanalys();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::saveSumanalys(std::string csv_name) {
  MoFEMFunctionBegin;

  if (mField.get_comm_rank() == 0) {

    std::ofstream sumanalysis;
    // add to the file capturing the end of the analysis "sumanalys.csv"
    sumanalysis.open(csv_name, std::ofstream::out | std::ofstream::app);

    sumanalysis << oRder << ", "
                << commonDataPtr->numberIntegrationPoints.back() << ", "
                << commonDataPtr->rmsErr.size() << ", " << commonDataPtr->vOlume
                << ", " << dummy_count << ", "
                << commonDataPtr->rmsPointDistErr.back() << ", " << 0.0 << ", "
                << commonDataPtr->rmsErr.back() << ", "
                << commonDataPtr->gradErr.back() << ", "
                << commonDataPtr->fluxErr.back() << ", " << -1.0 << "\n";

    sumanalysis.close();
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::outputHVtkFields(int ii, std::string vtk_name) {
  MoFEMFunctionBegin;

  // Postprocess only field values
  const Field_multiIndex *fields;
  mField.get_fields(&fields);

  // if there is "Q" in fields
  if (mField.check_field("Q")) {
    // scale fields
    CHKERR mField.getInterface<FieldBlas>()->fieldScale(
        1. / commonDataPtr->scaleQ, "Q");
  }

  fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
      new PostProcFaceOnRefinedMesh(mField));

  // Generate post-processing mesh
  CHKERR fePostProcPtr->generateReferenceElementMesh();

  CHKERR calculateJacobian(fePostProcPtr);
  // Postprocess only field values
  for (auto &it : *fields)
    fePostProcPtr->addFieldValuesPostProc(it->getName());

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), fePostProcPtr);

  // write output
  CHKERR fePostProcPtr->writeFile(
      vtk_name + boost::lexical_cast<std::string>(ii) + ".h5m");

  // if there is "Q" in fields
  if (mField.check_field("Q")) {
    // scale fields
    CHKERR mField.getInterface<FieldBlas>()->fieldScale(commonDataPtr->scaleQ,
                                                        "Q");
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::outputResults() {
  MoFEMFunctionBegin;

  // scale fields
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(commonDataPtr->scaleQ,
                                                      fluxField);

  if (!skip_vtk)
    CHKERR outputHVtkFields(commonDataPtr->counter);

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    string gauss_name;
    std::ostringstream strma;
    strma << "out_moab_" << commonDataPtr->counter << ".h5m";
    gauss_name = strma.str();
    CHKERR mField.get_moab().write_file(gauss_name.c_str(), "MOAB",
                                        "PARALLEL=WRITE_PART");
  }

  CHKERR errorCalculation(skip_vtk != PETSC_TRUE &&
                          flg_out_integ == PETSC_TRUE);

  // scale fields
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(
      1. / commonDataPtr->scaleQ, fluxField);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::setParamMonteCarlo() {
  MoFEMFunctionBegin;

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "DD Config", "none");
  CHKERR PetscOptionsInt("-monte_carlo", "Monte-Carlo statistics", "", 10,
                         &commonDataPtr->numberMonteCarlo, &flg_monte_carlo);
  CHKERR PetscOptionsReal("-perturb_multiplier",
                          "multiplier for the perturbation for monte_carlo", "",
                          10, &commonDataPtr->perturb_multiplier, PETSC_NULL);
  CHKERR PetscOptionsInt(
      "-monte_patch_number",
      "Monte-Carlo number of integration points for the statistics", "", 2,
      &commonDataPtr->monte_patch_number, PETSC_NULL);
  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  if (commonDataPtr->numberMonteCarlo < 0.5)
    flg_monte_carlo = PETSC_FALSE;
  if (!flg_monte_carlo)
    MoFEMFunctionReturnHot(0);

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform)
      << "-monte_carlo " << commonDataPtr->numberMonteCarlo;
  // MOFEM_LOG("WORLD", Sev::inform)
  //     << "-perturb_multiplier " << commonDataPtr->perturb_multiplier;
  MOFEM_LOG("WORLD", Sev::inform)
      << "-monte_patch_number " << commonDataPtr->monte_patch_number;

  DDLogTag;
  MOFEM_LOG("WORLD", Sev::inform) << "I'm trying to run Monte Carlo...";

  {
    // Create RHS and solution vectors
    SmartPetscObj<Vec> global_rhs;
    CHKERR DMCreateGlobalVector_MoFEM(dM, global_rhs);
    CHKERR VecGetSize(global_rhs, &solution_size);
  }

  CHKERR MatSetSizes(matGlobalResults, commonDataPtr->numberMonteCarlo,
                     solution_size, commonDataPtr->numberMonteCarlo,
                     solution_size);
  // CHKERR MatSetType(matGlobalResults, MATSEQAIJ);

  PetscScalar *a;
  PetscMalloc1(commonDataPtr->numberMonteCarlo * solution_size, &a);

  MatSetType(matGlobalResults, MATSEQDENSE);
  MatSeqDenseSetPreallocation(matGlobalResults, a);

  // Assemble MPI vector
  CHKERR MatAssemblyBegin(matGlobalResults, MAT_FINAL_ASSEMBLY);
  CHKERR MatAssemblyEnd(matGlobalResults, MAT_FINAL_ASSEMBLY);

  CHKERR MatZeroEntries(matGlobalResults);

  // MatView(matGlobalResults, PETSC_VIEWER_STDOUT_WORLD);

  { // set sizes for tags matrices

    int patch_integ_num = commonDataPtr->monte_patch_number;
    boost::shared_ptr<PatchIntegFaceEle> TagGauss(
        new PatchIntegFaceEle(mField, patch_integ_num));

    auto domain_rule_tag = [](int, int, int p) -> int { return -1; };
    TagGauss->getRuleHook = domain_rule_tag;

    TagGauss->getOpPtrVector().push_back(new OpResizeMonteMatrix(
        valueField, commonDataPtr->numberMonteCarlo, mField));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), TagGauss);
  }

  { // set sizes for tags matrices in postproc
    // MoFEMFunctionBegin;

    fePostProcPtr = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
        new PostProcFaceOnRefinedMesh(mField));

    // Generate post-processing mesh
    CHKERR fePostProcPtr->generateReferenceElementMesh();

    // CHKERR calculateJacobian(fePostProcPtr);

    fePostProcPtr->getOpPtrVector().push_back(new OpResizeMonteMatrix(
        valueField, commonDataPtr->numberMonteCarlo, mField));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), fePostProcPtr);
  }

  // create the file and put in the headers
  if (mField.get_comm_rank() == 0 && flg_long_error_file) {
    std::ofstream error_file;
    error_file.open("standard_deviation_max.csv");
    error_file << "integ_sigma_max_p, " << "integ_sigma_max_grad_x, "
               << "integ_sigma_max_grad_y, " << "integ_sigma_max_flux_x, "
               << "integ_sigma_max_flux_y" << std::endl; // headers
    error_file.close();
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::runMonteCarlo() {
  MoFEMFunctionBegin;

  if (!flg_monte_carlo)
    MoFEMFunctionReturnHot(0);

  // restart the counter
  commonDataPtr->counter = 0;
  double go = 0.;
  CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE, &go_per_proc);

  for (int i = 0; i < commonDataPtr->numberMonteCarlo; i++) {

    // restart the counter
    commonDataPtr->counter = 0;
    // restart the iterations
    go = 0.;
    CHKERR VecZeroEntries(go_per_proc);

    { // restart max_sigmas
      // max sigma for for integ point
      commonDataPtr->integ_sigma_max_p = 0;
      commonDataPtr->integ_sigma_max_grad_x = 0;
      commonDataPtr->integ_sigma_max_grad_y = 0;
      commonDataPtr->integ_sigma_max_flux_x = 0;
      commonDataPtr->integ_sigma_max_flux_y = 0;
    }

    if (!skip_vtk)
      CHKERR outputHVtkFields(commonDataPtr->monte_carlo_counter,
                              "out_before_perturb_");

    // Perturb fields relevant to material data search
    {
      std::cout << "rmsPointDistErr used for perturbation = "
                << commonDataPtr->rmsPointDistErr.back() << std::endl;

      // random
      PetscRandom rctx;
      PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
      PetscRandomSetSeed(rctx, (unsigned long)std::time(0));
      PetscRandomSeed(rctx);

      double scale = commonDataPtr->rmsPointDistErr.back();

      // set random Q field (L2)
      for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, fluxField, it)) {
        if (it->get()->getEntType() == MBTRI || MBQUAD) {
          auto field_data = (*it)->getEntFieldData();
          double value;
          // double scale = 1.0;
          PetscRandomGetValueReal(rctx, &value);
          field_data[0] += (value - 0.5) * scale * 5; // value * scale
          PetscRandomGetValueReal(rctx, &value);
          field_data[1] += (value - 0.5) * scale * 5;
        }
      }

      auto vector_times_scalar_field =
          [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
            MoFEMFunctionBeginHot;
            auto x_data = ent_ptr_x->getEntFieldData();
            double value;
            PetscRandomGetValueReal(rctx, &value);

            for (auto &v : x_data) {
              PetscRandomGetValueReal(rctx, &value);
              v += (value - 0.5) * scale;
            }

            MoFEMFunctionReturnHot(0);
          };

      CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(
          vector_times_scalar_field, valueField);
    }

    if (!skip_vtk)
      CHKERR outputHVtkFields(commonDataPtr->monte_carlo_counter,
                              "out_after_perturb_");

    while (go < 10e-5) {

      // Assemble MPI vector
      CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
      CHKERR VecAssemblyEnd(commonDataPtr->petscVec);

      // Zero global vector
      CHKERR VecZeroEntries(commonDataPtr->petscVec);

      CHKERR solveSystem();

      CHKERR errorCalculation(false);
      CHKERR errorHandlingWithinLoop(true);

      CHKERR outputHVtkFields(commonDataPtr->counter,
                              "out_result_during_monte_" +
                                  boost::lexical_cast<std::string>(
                                      commonDataPtr->monte_carlo_counter) +
                                  "_");

      // add to the counter
      commonDataPtr->counter = commonDataPtr->counter + 1;

      CHKERR VecAssemblyBegin(go_per_proc);
      CHKERR VecAssemblyEnd(go_per_proc);
      CHKERR VecSum(go_per_proc, &go);
    }

    CHKERR monteSaveHResults();

    // if (flg_skip_all_vtk == false) {
    CHKERR outputHVtkFields(commonDataPtr->monte_carlo_counter,
                            "out_converged_after_monte_");
    // }

    // if (flg_skip_all_vtk == false) {
    std::vector<Tag> tag_handles;
    tag_handles.resize(5);
    CHKERR getTagHandle(mField, "SIGMA_T", MB_TYPE_DOUBLE, tag_handles[0]);
    CHKERR getTagHandle(mField, "SIGMA_GRAD_X", MB_TYPE_DOUBLE, tag_handles[1]);
    CHKERR getTagHandle(mField, "SIGMA_GRAD_Y", MB_TYPE_DOUBLE, tag_handles[2]);
    CHKERR getTagHandle(mField, "SIGMA_FLUX_X", MB_TYPE_DOUBLE, tag_handles[3]);
    CHKERR getTagHandle(mField, "SIGMA_FLUX_Y", MB_TYPE_DOUBLE, tag_handles[4]);

    ParallelComm *pcomm =
        ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
    if (pcomm == NULL)
      SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
              "Communicator not set");

    // auto bit_level1 = BitRefLevel().set(meshRefinementCounter);
    auto bit_level1 = BitRefLevel().set(0);
    auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
    CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
        bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);
    tag_handles.push_back(pcomm->part_tag());
    std::ostringstream strm;
    strm << "out_sigma_ele_" << commonDataPtr->monte_carlo_counter << ".h5m";
    CHKERR mField.get_moab().write_file(strm.str().c_str(), "MOAB",
                                        "PARALLEL=WRITE_PART",
                                        out_meshset_tri_ptr->get_ptr(), 1,
                                        tag_handles.data(), tag_handles.size());
    // }

    commonDataPtr->monte_carlo_counter++;

    if (mField.get_comm_rank() == 0 && flg_long_error_file) {
      // open the error file with appending to the file
      std::ofstream error_file;
      error_file.open("standard_deviation_max.csv", ios::app);
      // populate the file with points
      error_file << commonDataPtr->integ_sigma_max_p << ", "
                 << commonDataPtr->integ_sigma_max_grad_x << ", "
                 << commonDataPtr->integ_sigma_max_grad_y << ", "
                 << commonDataPtr->integ_sigma_max_flux_x << ", "
                 << commonDataPtr->integ_sigma_max_flux_y << "\n";
      // close writing into file
      error_file.close();
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::monteSaveHResults() {
  MoFEMFunctionBegin;

  // calculation with TAGS
  { // populate the tag matrices

    moab::Core mb_post;                   // create database
    moab::Interface &moab_proc = mb_post; // create interface to database

    int patch_integ_num = commonDataPtr->monte_patch_number;
    boost::shared_ptr<PatchIntegFaceEle> TagGauss(
        new PatchIntegFaceEle(mField, patch_integ_num));

    auto domain_rule_tag = [](int, int, int p) -> int { return -1; };

    TagGauss->getRuleHook = domain_rule_tag;

    CHKERR calculateJacobian(TagGauss);

    TagGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
    TagGauss->getOpPtrVector().push_back(new OpCalculateScalarFieldGradient<2>(
        valueField, commonDataPtr->mGradPtr));

    TagGauss->getOpPtrVector().push_back(new OpCalculateVectorFieldValues<2>(
        fluxField, commonDataPtr->mFluxPtr));

    TagGauss->getOpPtrVector().push_back(
        new OpAddResultToMonteMatrix<2>(valueField, commonDataPtr, mField));

    TagGauss->getOpPtrVector().push_back(
        new OpTagsMeanVar(valueField, commonDataPtr, mField, mb_post));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), TagGauss);

    if (!skip_vtk) {
      string out_file_name;
      std::ostringstream strm;
      strm << "out_integ_pts_mean_var_" << commonDataPtr->monte_carlo_counter
           << ".h5m";
      out_file_name = strm.str();
      CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                "PARALLEL=WRITE_PART");
    }

    mb_post.delete_mesh();
  }

  MoFEMFunctionReturn(0);
}

// Operators

MoFEMErrorCode DiffusionDD::OpPointError::doWork(int side, EntityType type,
                                                 EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    const double area = getMeasure();
    auto t_w = getFTensor0IntegrationWeight();

    const size_t nb_gauss_pts = getGaussPts().size2();

    auto t_point_distance = getFTensor0FromVec(*(commonDataPtr->pointDistance));

    // Create storage for the local errors and area
    std::array<double, 4> element_local_value;
    std::fill(element_local_value.begin(), element_local_value.end(), 0.0);

    for (size_t gg = 0; gg != nb_gauss_pts; ++gg) {

      // accumulated point distance error
      element_local_value[0] += t_point_distance;
      // how many times the dataset is searched (number of integration points)
      element_local_value[1] += 1;
      // point distance error squared and relevant to the area
      element_local_value[2] +=
          t_point_distance * t_point_distance * t_w * area;

      ++t_point_distance;
      ++t_w;
    }
    // Set array of indices
    constexpr std::array<int, 3> indices = {CommonDataDD::ACCUM_POINT_ERROR,
                                            CommonDataDD::GAUSS_COUNT,
                                            CommonDataDD::POINT_ERROR};

    // Assemble first moment of inertia
    CHKERR ::VecSetValues(commonDataPtr->petscVec, indices.size(),
                          indices.data(), &element_local_value[0], ADD_VALUES);
  }
  MoFEMFunctionReturn(0);
}

// post processing
template <int N>
MoFEMErrorCode
DiffusionDD::OpPostProcPrintingIntegPts<N>::doWork(int side, EntityType type,
                                                   EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  FTensor::Index<'i', 2> i;

  const int nb_integration_pts = data.getN().size1();

  int tag_length_err = 1;
  double err_VAL[tag_length_err];
  bzero(err_VAL, tag_length_err * sizeof(double));
  Tag th_error;
  CHKERR outputMesh.tag_get_handle("P_difference", tag_length_err,
                                   MB_TYPE_DOUBLE, th_error,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, err_VAL);

  Tag th_pVal;
  CHKERR outputMesh.tag_get_handle("T", tag_length_err, MB_TYPE_DOUBLE, th_pVal,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, err_VAL);

  Tag th_pVal_star;
  CHKERR outputMesh.tag_get_handle("P_STAR", tag_length_err, MB_TYPE_DOUBLE,
                                   th_pVal_star, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   err_VAL);

  Tag th_pAna;
  CHKERR outputMesh.tag_get_handle("P_analytical", tag_length_err,
                                   MB_TYPE_DOUBLE, th_pAna,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, err_VAL);

  Tag th_point_distance;
  CHKERR outputMesh.tag_get_handle("Point_distance", tag_length_err,
                                   MB_TYPE_DOUBLE, th_point_distance,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, err_VAL);

  auto t_val = getFTensor0FromVec(*(commonDataPtr->mValPtr));
  auto t_val_star = getFTensor0FromVec(*(commonDataPtr->mValStarPtr));

  int tag_length = 3;
  double def_VAL[tag_length];
  bzero(def_VAL, tag_length * sizeof(double));

  Tag th_q_star;
  CHKERR outputMesh.tag_get_handle("Q_STAR", tag_length, MB_TYPE_DOUBLE,
                                   th_q_star, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);

  Tag th_grap_p_star;
  CHKERR outputMesh.tag_get_handle("GRAD(P)_STAR", tag_length, MB_TYPE_DOUBLE,
                                   th_grap_p_star, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);

  // Q and GRAD(P)
  Tag th_q;
  CHKERR outputMesh.tag_get_handle("Q", tag_length, MB_TYPE_DOUBLE, th_q,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, def_VAL);

  Tag th_grap_p;
  CHKERR outputMesh.tag_get_handle("GRAD(P)", tag_length, MB_TYPE_DOUBLE,
                                   th_grap_p, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);

  // normal vector
  Tag th_normal;
  CHKERR outputMesh.tag_get_handle("NORMAL", tag_length, MB_TYPE_DOUBLE,
                                   th_normal, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);

  //  K* and K*correlation

  int tag_length_9 = 9;
  double def_VAL_9[tag_length_9];
  bzero(def_VAL_9, tag_length_9 * sizeof(double));
  Tag th_k_star;
  CHKERR outputMesh.tag_get_handle("K_STAR", tag_length_9, MB_TYPE_DOUBLE,
                                   th_k_star, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL_9);

  // Tag th_k_correlation;
  // CHKERR outputMesh.tag_get_handle("K_STAR_CORRELATION", tag_length,
  //                                  MB_TYPE_DOUBLE, th_k_correlation,
  //                                  MB_TAG_CREAT | MB_TAG_SPARSE, def_VAL);

  // print stars
  auto t_grad = getFTensor1FromMat<2>(*(commonDataPtr->mGradPtr));
  auto t_flux = getFTensor1FromMat<N>(*(commonDataPtr->mFluxPtr));
  auto t_grad_star = getFTensor1FromMat<2>(*(commonDataPtr->mGradStarPtr));
  auto t_flux_star = getFTensor1FromMat<2>(*(commonDataPtr->mFluxStarPtr));
  auto t_point_distance = getFTensor0FromVec(*(commonDataPtr->pointDistance));

  // bool flg_k_star = true;
  if (!commonDataPtr->mKStarPtr->size1()) {
    commonDataPtr->mKStarPtr->resize(3 * 3, nb_integration_pts);
    commonDataPtr->mKStarPtr->clear();
    // commonDataPtr->mKStarCorrelationPtr->resize(2, nb_integration_pts);
    // commonDataPtr->mKStarCorrelationPtr->clear();
    // flg_k_star = false;
  }
  auto t_k_star = getFTensor2FromMat<3, 3>(*(commonDataPtr->mKStarPtr));

  // auto t_k_correlation =
  //     getFTensor1FromMat<2>(*(commonDataPtr->mKStarCorrelationPtr));

  // get coordinates at integration point
  auto t_coords = getFTensor1CoordsAtGaussPts();

  FTensor::Tensor1<double, 3> t_q_star_out{0., 0., 0.};
  FTensor::Tensor1<double, 3> t_grad_star_out{0., 0., 0.};
  FTensor::Tensor1<double, 3> t_q_out{0., 0., 0.};
  FTensor::Tensor1<double, 3> t_grad_out{0., 0., 0.};
  FTensor::Tensor1<double, 9> t_k_star_out;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    double analytical_term =
        analyticalFunc(t_coords(0), t_coords(1), t_coords(2));

    // find error on gauss points
    double error = analytical_term - t_val;

    // create new mapping for gauss points
    EntityHandle new_vertex;
    const double *coords_ptr = &(getCoordsAtGaussPts()(gg, 0));
    CHKERR outputMesh.create_vertex(coords_ptr, new_vertex);

    // output error
    CHKERR outputMesh.tag_set_data(th_error, &new_vertex, 1, &error);
    CHKERR outputMesh.tag_set_data(th_pVal, &new_vertex, 1, &t_val);
    CHKERR outputMesh.tag_set_data(th_pAna, &new_vertex, 1, &analytical_term);
    CHKERR outputMesh.tag_set_data(th_point_distance, &new_vertex, 1,
                                   &t_point_distance);
    if (commonDataPtr->mValStarPtr->size())
      CHKERR outputMesh.tag_set_data(th_pVal_star, &new_vertex, 1, &t_val_star);

    // due to vtk being 3D
    t_q_star_out(i) = t_flux_star(i);
    t_grad_star_out(i) = t_grad_star(i);
    t_q_out(i) = t_flux(i);
    t_grad_out(i) = t_grad(i);

    if (commonDataPtr->flgPrintK) {

      for (int ii = 0; ii < 3; ii++)
        for (int jj = 0; jj < 3; jj++)
          t_k_star_out(ii * 3 + jj) = t_k_star(ii, jj);

      // output k* and k*correlation on the new vertexes
      CHKERR outputMesh.tag_set_data(th_k_star, &new_vertex, 1,
                                     &t_k_star_out(0));
      // CHKERR outputMesh.tag_set_data(th_k_correlation, &new_vertex, 1,
      //                                &t_k_correlation_out);
    }

    // output q* and grad(p)* on the new vertexes
    CHKERR outputMesh.tag_set_data(th_q_star, &new_vertex, 1, &t_q_star_out(0));
    CHKERR outputMesh.tag_set_data(th_grap_p_star, &new_vertex, 1,
                                   &t_grad_star_out(0));

    // output q and grad(p) on the new vertexes
    CHKERR outputMesh.tag_set_data(th_q, &new_vertex, 1, &t_q_out(0));
    CHKERR outputMesh.tag_set_data(th_grap_p, &new_vertex, 1, &t_grad_out(0));

    // output normals on the new vertexes
    auto t_normal = getFTensor1Normal();
    CHKERR outputMesh.tag_set_data(th_normal, &new_vertex, 1, &t_normal(0));

    ++t_flux_star;
    ++t_grad_star;
    ++t_flux;
    ++t_grad;
    ++t_val;
    ++t_val_star;
    ++t_point_distance;
    ++t_k_star;
    // ++t_k_correlation;
    ++t_coords;
  }

  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode
DiffusionDD::OpPostProcPrintFarIntegPts<N>::doWork(int side, EntityType type,
                                                   EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  auto t_point_distance = getFTensor0FromVec(*(commonDataPtr->pointDistance));

  auto t_val = getFTensor0FromVec(*(commonDataPtr->mValPtr));
  auto t_grad = getFTensor1FromMat<2>(*(commonDataPtr->mGradPtr));
  auto t_flux = getFTensor1FromMat<N>(*(commonDataPtr->mFluxPtr));

  const int nb_integration_pts = data.getN().size1();

  std::ofstream myfile;
  myfile.open(csvName, std::ofstream::out | std::ofstream::app);
  myfile.precision(17);

  std::ofstream file_for_the_rest;
  file_for_the_rest.open("not_" + csvName,
                         std::ofstream::out | std::ofstream::app);
  file_for_the_rest.precision(17);

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    if (t_point_distance > comperativePointDistance)
      myfile << t_grad(0) << "," << t_grad(1) << "," << t_flux(0) << ","
             << t_flux(1) << "," << t_val << "\n";
    else {
      file_for_the_rest << t_grad(0) << "," << t_grad(1) << "," << t_flux(0)
                        << "," << t_flux(1) << "," << t_val << "\n";
    }

    ++t_point_distance;
    ++t_val;
    ++t_flux;
    ++t_grad;
  }

  myfile.close();
  file_for_the_rest.close();

  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode
DiffusionDD::OpPostProcErrorFluxIntegPts<N>::doWork(int side, EntityType type,
                                                    EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  FTensor::Index<'i', 2> i;

  const int nb_integration_pts = data.getN().size1();

  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();

  auto t_flux = getFTensor1FromMat<N>(*(commonDataPtr->mFluxPtr));

  // get coordinates at integration point
  auto t_coords = getFTensor1CoordsAtGaussPts();

  // Create storage for the local errors and area
  std::array<double, 1> element_local_value;
  std::fill(element_local_value.begin(), element_local_value.end(), 0.0);

  FTensor::Tensor1<double, 2> t_flux_diff;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    VectorDouble vec_flux =
        analyticalFlux(t_coords(0), t_coords(1), t_coords(2));
    auto t_analytical_flux = getFTensor1FromArray<2, 1>(vec_flux);

    // find error on gauss points
    t_flux_diff(i) = t_flux(i) - t_analytical_flux(i);

    element_local_value[0] +=
        t_flux_diff(i) * t_flux_diff(i) * area * t_w; // RMS flux error

    ++t_flux;
    ++t_w;
    ++t_coords;
  }

  // Set array of indices
  constexpr std::array<int, 1> indices = {CommonDataDD::FLUX_ERROR};

  // Assemble first moment of inertia
  CHKERR ::VecSetValues(commonDataPtr->petscVec, indices.size(), indices.data(),
                        &element_local_value[0], ADD_VALUES);

  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode DiffusionDD::OpPrintStarIntegPts<N>::doWork(int side,
                                                           EntityType type,
                                                           EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  int tag_length_err = 1;
  double err_VAL[tag_length_err];
  bzero(err_VAL, tag_length_err * sizeof(double));

  Tag th_point_distance;
  CHKERR outputMesh.tag_get_handle("Point_distance", tag_length_err,
                                   MB_TYPE_DOUBLE, th_point_distance,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, err_VAL);

  Tag th_pVal;
  CHKERR outputMesh.tag_get_handle("T", tag_length_err, MB_TYPE_DOUBLE, th_pVal,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, err_VAL);

  Tag th_pVal_star;
  CHKERR outputMesh.tag_get_handle("P_STAR", tag_length_err, MB_TYPE_DOUBLE,
                                   th_pVal_star, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   err_VAL);

  auto t_point_distance = getFTensor0FromVec(*(commonDataPtr->pointDistance));
  auto t_val = getFTensor0FromVec(*(commonDataPtr->mValPtr));
  auto t_val_star = getFTensor0FromVec(*(commonDataPtr->mValStarPtr));

  int tag_length = 3;
  double def_VAL[tag_length];
  bzero(def_VAL, tag_length * sizeof(double));

  Tag th_q_star;
  CHKERR outputMesh.tag_get_handle("Q_STAR", tag_length, MB_TYPE_DOUBLE,
                                   th_q_star, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);

  Tag th_grap_p_star;
  CHKERR outputMesh.tag_get_handle("GRAD(P)_STAR", tag_length, MB_TYPE_DOUBLE,
                                   th_grap_p_star, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);

  auto t_grad_star = getFTensor1FromMat<2>(*(commonDataPtr->mGradStarPtr));
  auto t_flux_star = getFTensor1FromMat<2>(*(commonDataPtr->mFluxStarPtr));

  // Q and GRAD(P)
  Tag th_q;
  CHKERR outputMesh.tag_get_handle("Q", tag_length, MB_TYPE_DOUBLE, th_q,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, def_VAL);

  Tag th_grap_p;
  CHKERR outputMesh.tag_get_handle("GRAD(P)", tag_length, MB_TYPE_DOUBLE,
                                   th_grap_p, MB_TAG_CREAT | MB_TAG_SPARSE,
                                   def_VAL);

  auto t_grad = getFTensor1FromMat<2>(*(commonDataPtr->mGradPtr));
  auto t_flux = getFTensor1FromMat<N>(*(commonDataPtr->mFluxPtr));

  const int nb_integration_pts = data.getN().size1();

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    // due to vtk being 3D
    FTensor::Tensor1<double, 3> t_q_star_out;
    t_q_star_out(0) = t_flux_star(0);
    t_q_star_out(1) = t_flux_star(1);
    t_q_star_out(2) = 0.;
    FTensor::Tensor1<double, 3> t_grad_star_out;
    t_grad_star_out(0) = t_grad_star(0);
    t_grad_star_out(1) = t_grad_star(1);
    t_grad_star_out(2) = 0.;

    // due to vtk being 3D
    FTensor::Tensor1<double, 3> t_q_out;
    t_q_out(0) = t_flux(0);
    t_q_out(1) = t_flux(1);
    t_q_out(2) = 0.;
    FTensor::Tensor1<double, 3> t_grad_out;
    t_grad_out(0) = t_grad(0);
    t_grad_out(1) = t_grad(1);
    t_grad_out(2) = 0.;

    // create new mapping for gauss points
    EntityHandle new_vertex;
    const double *coords_ptr = &(getCoordsAtGaussPts()(gg, 0));
    CHKERR outputMesh.create_vertex(coords_ptr, new_vertex);

    CHKERR outputMesh.tag_set_data(th_point_distance, &new_vertex, 1,
                                   &t_point_distance);

    if (commonDataPtr->mValStarPtr->size()) {
      CHKERR outputMesh.tag_set_data(th_pVal, &new_vertex, 1, &t_val);

      CHKERR outputMesh.tag_set_data(th_pVal_star, &new_vertex, 1, &t_val_star);
    }

    // output q* and grad(p)* on the new vertexes
    CHKERR outputMesh.tag_set_data(th_q_star, &new_vertex, 1, &t_q_star_out(0));
    CHKERR outputMesh.tag_set_data(th_grap_p_star, &new_vertex, 1,
                                   &t_grad_star_out(0));

    // output q* and grad(p)* on the new vertexes
    CHKERR outputMesh.tag_set_data(th_q, &new_vertex, 1, &t_q_out(0));
    CHKERR outputMesh.tag_set_data(th_grap_p, &new_vertex, 1, &t_grad_out(0));

    ++t_point_distance;
    ++t_val;
    ++t_val_star;
    ++t_flux_star;
    ++t_grad_star;
    ++t_flux;
    ++t_grad;
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::OpResizeMonteMatrix::doWork(int side,
                                                        EntityType type,
                                                        EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    const EntityHandle ent = getFEEntityHandle();
    const size_t nb_gauss_pts = getGaussPts().size2();

    MatrixDouble placeholder_pressure;
    placeholder_pressure.resize(1 * nb_gauss_pts, monteNumberTotal, false);

    std::string tag_name;
    tag_name = "PRESSURE_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    Tag th_pressure;
    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 1 * nb_gauss_pts * monteNumberTotal, MB_TYPE_DOUBLE,
        th_pressure, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_set_data(
        th_pressure, &ent, 1, &*placeholder_pressure.data().begin());

    tag_name = "PRESSURE_GRADIENT_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    MatrixDouble placeholder_2D;
    placeholder_2D.resize(2 * nb_gauss_pts, monteNumberTotal, false);

    Tag th_gradients;
    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * monteNumberTotal, MB_TYPE_DOUBLE,
        th_gradients, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_set_data(th_gradients, &ent, 1,
                                          &*placeholder_2D.data().begin());

    tag_name = "FLUX_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    Tag th_flux;
    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * monteNumberTotal, MB_TYPE_DOUBLE,
        th_flux, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_set_data(th_flux, &ent, 1,
                                          &*placeholder_2D.data().begin());
  }
  MoFEMFunctionReturn(0);
}

template <int N>
MoFEMErrorCode DiffusionDD::OpAddResultToMonteMatrix<N>::doWork(int side,
                                                                EntityType type,
                                                                EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    const EntityHandle ent = getFEEntityHandle();

    const size_t nb_gauss_pts = getGaussPts().size2();

    // pressure
    Tag th_pressure;
    MatrixDouble tag_data_pressure;
    tag_data_pressure.resize(1 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                             false);
    tag_data_pressure.clear();

    std::string tag_name;
    tag_name = "PRESSURE_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 1 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_pressure, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_pressure, &ent, 1,
                                          &*tag_data_pressure.data().begin());
    // grad
    Tag th_gradients;
    MatrixDouble tag_data_gradient;
    tag_data_gradient.resize(2 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                             false);
    tag_data_gradient.clear();

    tag_name = "PRESSURE_GRADIENT_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_gradients, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_gradients, &ent, 1,
                                          &*tag_data_gradient.data().begin());

    // flux
    Tag th_flux;
    MatrixDouble tag_data_flux;
    tag_data_flux.resize(2 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                         false);
    tag_data_flux.clear();

    tag_name = "FLUX_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_flux, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_flux, &ent, 1,
                                          &*tag_data_flux.data().begin());

    // get values which will be saved on tags
    auto t_val = getFTensor0FromVec(*(commonDataPtr->mValPtr));
    auto t_grad = getFTensor1FromMat<2>(*(commonDataPtr->mGradPtr));
    auto t_flux = getFTensor1FromMat<N>(*(commonDataPtr->mFluxPtr));

    // save values to matrices
    for (size_t gg = 0; gg != nb_gauss_pts; ++gg) {
      tag_data_pressure(gg, commonDataPtr->monte_carlo_counter) = t_val;

      tag_data_gradient(2 * gg, commonDataPtr->monte_carlo_counter) = t_grad(0);
      tag_data_gradient(2 * gg + 1, commonDataPtr->monte_carlo_counter) =
          t_grad(1);

      tag_data_flux(2 * gg, commonDataPtr->monte_carlo_counter) = t_flux(0);
      tag_data_flux(2 * gg + 1, commonDataPtr->monte_carlo_counter) = t_flux(1);

      ++t_val;
      ++t_flux;
      ++t_grad;
    }

    // update the matrices saved on tags
    CHKERR mField.get_moab().tag_set_data(th_pressure, &ent, 1,
                                          &*tag_data_pressure.data().begin());
    CHKERR mField.get_moab().tag_set_data(th_gradients, &ent, 1,
                                          &*tag_data_gradient.data().begin());
    CHKERR mField.get_moab().tag_set_data(th_flux, &ent, 1,
                                          &*tag_data_flux.data().begin());
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::OpGetMeanVar::doWork(int side, EntityType type,
                                                 EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    const EntityHandle ent = getFEEntityHandle();
    const size_t nb_gauss_pts = getGaussPts().size2();

    // average values
    commonDataPtr->mValAvePtr->resize(nb_gauss_pts);
    commonDataPtr->mValAvePtr->clear();
    commonDataPtr->mGradAvePtr->resize(2, nb_gauss_pts);
    commonDataPtr->mGradAvePtr->clear();
    commonDataPtr->mFluxAvePtr->resize(2, nb_gauss_pts);
    commonDataPtr->mFluxAvePtr->clear();

    auto t_val_ave = getFTensor0FromVec(*(commonDataPtr->mValAvePtr));
    auto t_grad_ave = getFTensor1FromMat<2>(*(commonDataPtr->mGradAvePtr));
    auto t_flux_ave = getFTensor1FromMat<2>(*(commonDataPtr->mFluxAvePtr));

    // standard deviations
    commonDataPtr->mValSigmaPtr->resize(nb_gauss_pts);
    commonDataPtr->mValSigmaPtr->clear();
    commonDataPtr->mGradSigmaPtr->resize(2, nb_gauss_pts);
    commonDataPtr->mGradSigmaPtr->clear();
    commonDataPtr->mFluxSigmaPtr->resize(2, nb_gauss_pts);
    commonDataPtr->mFluxSigmaPtr->clear();

    auto t_val_variance = getFTensor0FromVec(*(commonDataPtr->mValSigmaPtr));
    auto t_grad_variance =
        getFTensor1FromMat<2>(*(commonDataPtr->mGradSigmaPtr));
    auto t_flux_variance =
        getFTensor1FromMat<2>(*(commonDataPtr->mFluxSigmaPtr));

    // get values which are saved on tags
    // pressure
    Tag th_pressure;
    MatrixDouble tag_data_pressure;
    tag_data_pressure.resize(1 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                             false);
    tag_data_pressure.clear();

    std::string tag_name;
    tag_name = "PRESSURE_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 1 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_pressure, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_pressure, &ent, 1,
                                          &*tag_data_pressure.data().begin());

    // grad
    Tag th_gradients;
    MatrixDouble tag_data_gradient;
    tag_data_gradient.resize(2 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                             false);
    tag_data_gradient.clear();

    tag_name = "PRESSURE_GRADIENT_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_gradients, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_gradients, &ent, 1,
                                          &*tag_data_gradient.data().begin());

    // flux
    Tag th_flux;
    MatrixDouble tag_data_flux;
    tag_data_flux.resize(2 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                         false);
    tag_data_flux.clear();

    tag_name = "FLUX_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_flux, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_flux, &ent, 1,
                                          &*tag_data_flux.data().begin());

    double sum_sigma_p = 0;
    double sum_sigma_grad_x = 0;
    double sum_sigma_grad_y = 0;
    double sum_sigma_flux_x = 0;
    double sum_sigma_flux_y = 0;

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {

      // calculate mean and standard deviation

      // pressure
      boost::numeric::ublas::matrix_row<MatrixDouble> data_pressure_row(
          tag_data_pressure, gg);

      VectorDouble pressures_at_gp = data_pressure_row;

      t_val_ave =
          sum(pressures_at_gp) / (commonDataPtr->monte_carlo_counter + 1);

      double p_difference_squared = 0;
      for (int ii = 0; ii < commonDataPtr->monte_carlo_counter + 1; ii++) {
        p_difference_squared += square(pressures_at_gp(ii) - t_val_ave);
      }
      t_val_variance =
          sqrt(p_difference_squared / (commonDataPtr->monte_carlo_counter + 1));

      sum_sigma_p += t_val_variance;

      // grad
      // FTensor::Tensor1<double, 3> t_grad_ave;
      // t_grad_ave(2) variance
      // FTensor::Tensor1<double, 3> t_grad_variance;
      // t_grad_variance(2) = 0.;
      for (int jj = 0; jj < 2; jj++) {
        boost::numeric::ublas::matrix_row<MatrixDouble> data_grad_row(
            tag_data_gradient, gg * 2 + jj);

        VectorDouble grad_at_gp = data_grad_row;

        t_grad_ave(jj) =
            sum(grad_at_gp) / (commonDataPtr->monte_carlo_counter + 1);

        double grad_difference_squared = 0;
        for (int ii = 0; ii < commonDataPtr->monte_carlo_counter + 1; ii++) {
          grad_difference_squared += square(grad_at_gp(ii) - t_grad_ave(jj));
        }
        t_grad_variance(jj) = sqrt(grad_difference_squared /
                                   (commonDataPtr->monte_carlo_counter + 1));
      }

      sum_sigma_grad_x += t_grad_variance(0);
      sum_sigma_grad_y += t_grad_variance(1);

      // // flux
      // FTensor::Tensor1<double, 3> t_flux_ave;
      // t_flux_ave(2) = 0.;
      // FTensor::Tensor1<double, 3> t_flux_variance;
      // t_flux_variance(2) = 0.;
      for (int jj = 0; jj < 2; jj++) {
        boost::numeric::ublas::matrix_row<MatrixDouble> data_flux_row(
            tag_data_flux, gg * 2 + jj);

        VectorDouble flux_at_gp = data_flux_row;

        t_flux_ave(jj) =
            sum(flux_at_gp) / (commonDataPtr->monte_carlo_counter + 1);

        double flux_difference_squared = 0;
        for (int ii = 0; ii < commonDataPtr->monte_carlo_counter + 1; ii++) {
          flux_difference_squared += square(flux_at_gp(ii) - t_flux_ave(jj));
        }
        t_flux_variance(jj) = sqrt(flux_difference_squared /
                                   (commonDataPtr->monte_carlo_counter + 1));
      }

      sum_sigma_flux_x += t_flux_variance(0);
      sum_sigma_flux_y += t_flux_variance(1);

      { // check max_sigmas
        // pressure
        if (t_val_variance > commonDataPtr->integ_sigma_max_p)
          commonDataPtr->integ_sigma_max_p = t_val_variance;
        // grad
        if (t_grad_variance(0) > commonDataPtr->integ_sigma_max_grad_x)
          commonDataPtr->integ_sigma_max_grad_x = t_grad_variance(0);
        if (t_grad_variance(1) > commonDataPtr->integ_sigma_max_grad_y)
          commonDataPtr->integ_sigma_max_grad_y = t_grad_variance(1);
        // flux
        if (t_flux_variance(0) > commonDataPtr->integ_sigma_max_flux_x)
          commonDataPtr->integ_sigma_max_flux_x = t_flux_variance(0);
        if (t_flux_variance(1) > commonDataPtr->integ_sigma_max_flux_y)
          commonDataPtr->integ_sigma_max_flux_y = t_flux_variance(1);
      }
      ++t_val_ave;
      ++t_grad_ave;
      ++t_flux_ave;
      ++t_val_variance;
      ++t_grad_variance;
      ++t_flux_variance;
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::OpTagsMeanVar::doWork(int side, EntityType type,
                                                  EntData &data) {
  MoFEMFunctionBegin;

  if (type == MBVERTEX) {

    const EntityHandle ent = getFEEntityHandle();
    const size_t nb_gauss_pts = getGaussPts().size2();

    // pressure
    Tag th_pressure;
    MatrixDouble tag_data_pressure;
    tag_data_pressure.resize(1 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                             false);
    tag_data_pressure.clear();

    std::string tag_name;
    tag_name = "PRESSURE_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 1 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_pressure, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_pressure, &ent, 1,
                                          &*tag_data_pressure.data().begin());

    // grad
    Tag th_gradients;
    MatrixDouble tag_data_gradient;
    tag_data_gradient.resize(2 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                             false);
    tag_data_gradient.clear();

    tag_name = "PRESSURE_GRADIENT_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_gradients, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_gradients, &ent, 1,
                                          &*tag_data_gradient.data().begin());

    // flux
    Tag th_flux;
    MatrixDouble tag_data_flux;
    tag_data_flux.resize(2 * nb_gauss_pts, commonDataPtr->numberMonteCarlo,
                         false);
    tag_data_flux.clear();

    tag_name = "FLUX_CONVERGED_";
    tag_name.append(boost::lexical_cast<std::string>(nb_gauss_pts));

    CHKERR mField.get_moab().tag_get_handle(
        tag_name.c_str(), 2 * nb_gauss_pts * commonDataPtr->numberMonteCarlo,
        MB_TYPE_DOUBLE, th_flux, MB_TAG_CREAT | MB_TAG_SPARSE);

    CHKERR mField.get_moab().tag_get_data(th_flux, &ent, 1,
                                          &*tag_data_flux.data().begin());

    // save on new mesh
    // pressure
    int tag_length_pressure = 1;
    double pressure_VAL[tag_length_pressure];
    bzero(pressure_VAL, tag_length_pressure * sizeof(double));
    Tag th_p_average;
    CHKERR outputMesh.tag_get_handle(
        "P_average", tag_length_pressure, MB_TYPE_DOUBLE, th_p_average,
        MB_TAG_CREAT | MB_TAG_SPARSE, pressure_VAL);

    Tag th_p_variance;
    CHKERR outputMesh.tag_get_handle(
        "P_variance", tag_length_pressure, MB_TYPE_DOUBLE, th_p_variance,
        MB_TAG_CREAT | MB_TAG_SPARSE, pressure_VAL);

    // grad
    int tag_length_3d = 3;
    double def_3d_VAL[tag_length_3d];
    bzero(def_3d_VAL, tag_length_3d * sizeof(double));
    Tag th_grad_average;
    CHKERR outputMesh.tag_get_handle("G_average", tag_length_3d, MB_TYPE_DOUBLE,
                                     th_grad_average,
                                     MB_TAG_CREAT | MB_TAG_SPARSE, def_3d_VAL);

    Tag th_grad_variance;
    CHKERR outputMesh.tag_get_handle("G_variance", tag_length_3d,
                                     MB_TYPE_DOUBLE, th_grad_variance,
                                     MB_TAG_CREAT | MB_TAG_SPARSE, def_3d_VAL);

    // flux
    Tag th_flux_average;
    CHKERR outputMesh.tag_get_handle("Q_average", tag_length_3d, MB_TYPE_DOUBLE,
                                     th_flux_average,
                                     MB_TAG_CREAT | MB_TAG_SPARSE, def_3d_VAL);

    Tag th_flux_variance;
    CHKERR outputMesh.tag_get_handle("Q_variance", tag_length_3d,
                                     MB_TYPE_DOUBLE, th_flux_variance,
                                     MB_TAG_CREAT | MB_TAG_SPARSE, def_3d_VAL);

    double sum_sigma_p = 0;
    double sum_sigma_grad_x = 0;
    double sum_sigma_grad_y = 0;
    double sum_sigma_flux_x = 0;
    double sum_sigma_flux_y = 0;

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {

      // calculate mean and standard deviation

      // pressure
      boost::numeric::ublas::matrix_row<MatrixDouble> data_pressure_row(
          tag_data_pressure, gg);

      VectorDouble pressures_at_gp = data_pressure_row;

      double p_average =
          sum(pressures_at_gp) / (commonDataPtr->monte_carlo_counter + 1);

      double p_difference_squared = 0;
      for (int ii = 0; ii < commonDataPtr->monte_carlo_counter + 1; ii++) {
        p_difference_squared += square(pressures_at_gp(ii) - p_average);
      }
      double p_variance =
          sqrt(p_difference_squared / (commonDataPtr->monte_carlo_counter + 1));

      sum_sigma_p += p_variance;

      // grad
      FTensor::Tensor1<double, 3> t_grad_ave;
      t_grad_ave(2) = 0.;
      FTensor::Tensor1<double, 3> t_grad_variance;
      t_grad_variance(2) = 0.;
      for (int jj = 0; jj < 2; jj++) {
        boost::numeric::ublas::matrix_row<MatrixDouble> data_grad_row(
            tag_data_gradient, gg * 2 + jj);

        VectorDouble grad_at_gp = data_grad_row;

        t_grad_ave(jj) =
            sum(grad_at_gp) / (commonDataPtr->monte_carlo_counter + 1);

        double grad_difference_squared = 0;
        for (int ii = 0; ii < commonDataPtr->monte_carlo_counter + 1; ii++) {
          grad_difference_squared += square(grad_at_gp(ii) - t_grad_ave(jj));
        }
        t_grad_variance(jj) = sqrt(grad_difference_squared /
                                   (commonDataPtr->monte_carlo_counter + 1));
      }

      sum_sigma_grad_x += t_grad_variance(0);
      sum_sigma_grad_y += t_grad_variance(1);

      // flux
      FTensor::Tensor1<double, 3> t_flux_ave;
      t_flux_ave(2) = 0.;
      FTensor::Tensor1<double, 3> t_flux_variance;
      t_flux_variance(2) = 0.;
      for (int jj = 0; jj < 2; jj++) {
        boost::numeric::ublas::matrix_row<MatrixDouble> data_flux_row(
            tag_data_flux, gg * 2 + jj);

        VectorDouble flux_at_gp = data_flux_row;

        t_flux_ave(jj) =
            sum(flux_at_gp) / (commonDataPtr->monte_carlo_counter + 1);

        double flux_difference_squared = 0;
        for (int ii = 0; ii < commonDataPtr->monte_carlo_counter + 1; ii++) {
          flux_difference_squared += square(flux_at_gp(ii) - t_flux_ave(jj));
        }
        t_flux_variance(jj) = sqrt(flux_difference_squared /
                                   (commonDataPtr->monte_carlo_counter + 1));
      }

      sum_sigma_flux_x += t_flux_variance(0);
      sum_sigma_flux_y += t_flux_variance(1);

      { // check max_sigmas
        // pressure
        if (p_variance > commonDataPtr->integ_sigma_max_p)
          commonDataPtr->integ_sigma_max_p = p_variance;
        // grad
        if (t_grad_variance(0) > commonDataPtr->integ_sigma_max_grad_x)
          commonDataPtr->integ_sigma_max_grad_x = t_grad_variance(0);
        if (t_grad_variance(1) > commonDataPtr->integ_sigma_max_grad_y)
          commonDataPtr->integ_sigma_max_grad_y = t_grad_variance(1);
        // flux
        if (t_flux_variance(0) > commonDataPtr->integ_sigma_max_flux_x)
          commonDataPtr->integ_sigma_max_flux_x = t_flux_variance(0);
        if (t_flux_variance(1) > commonDataPtr->integ_sigma_max_flux_y)
          commonDataPtr->integ_sigma_max_flux_y = t_flux_variance(1);
      }

      { // create new mapping for gauss points
        EntityHandle new_vertex;
        const double *coords_ptr = &(getCoordsAtGaussPts()(gg, 0));
        CHKERR outputMesh.create_vertex(coords_ptr, new_vertex);

        // output error
        // pressure
        CHKERR outputMesh.tag_set_data(th_p_average, &new_vertex, 1,
                                       &p_average);
        CHKERR outputMesh.tag_set_data(th_p_variance, &new_vertex, 1,
                                       &p_variance);

        // grad
        CHKERR outputMesh.tag_set_data(th_grad_average, &new_vertex, 1,
                                       &t_grad_ave(0));
        CHKERR outputMesh.tag_set_data(th_grad_variance, &new_vertex, 1,
                                       &t_grad_variance(0));

        // flux
        CHKERR outputMesh.tag_set_data(th_flux_average, &new_vertex, 1,
                                       &t_flux_ave(0));
        CHKERR outputMesh.tag_set_data(th_flux_variance, &new_vertex, 1,
                                       &t_flux_variance(0));
      }
    }

    std::vector<Tag> tag_handles;
    tag_handles.resize(5);
    CHKERR ClassicDiffusionProblem::getTagHandle(
        mField, "SIGMA_T", MB_TYPE_DOUBLE, tag_handles[0]);
    CHKERR ClassicDiffusionProblem::getTagHandle(
        mField, "SIGMA_GRAD_X", MB_TYPE_DOUBLE, tag_handles[1]);
    CHKERR ClassicDiffusionProblem::getTagHandle(
        mField, "SIGMA_GRAD_Y", MB_TYPE_DOUBLE, tag_handles[2]);
    CHKERR ClassicDiffusionProblem::getTagHandle(
        mField, "SIGMA_FLUX_X", MB_TYPE_DOUBLE, tag_handles[3]);
    CHKERR ClassicDiffusionProblem::getTagHandle(
        mField, "SIGMA_FLUX_Y", MB_TYPE_DOUBLE, tag_handles[4]);

    CHKERR mField.get_moab().tag_set_data(tag_handles[0], &ent, 1,
                                          &sum_sigma_p);
    CHKERR mField.get_moab().tag_set_data(tag_handles[1], &ent, 1,
                                          &sum_sigma_grad_x);
    CHKERR mField.get_moab().tag_set_data(tag_handles[2], &ent, 1,
                                          &sum_sigma_grad_y);
    CHKERR mField.get_moab().tag_set_data(tag_handles[3], &ent, 1,
                                          &sum_sigma_flux_x);
    CHKERR mField.get_moab().tag_set_data(tag_handles[4], &ent, 1,
                                          &sum_sigma_flux_y);
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionDD::OpPostProcRefErrorIntegPts::doWork(int side,
                                                               EntityType type,
                                                               EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  const int nb_integration_pts = data.getN().size1();

  const double area = getMeasure();
  auto t_w = getFTensor0IntegrationWeight();

  auto t_val = getFTensor0FromVec(*(commonDataPtr->mValPtr));

  auto t_val_ref = getFTensor0FromVec(*(commonDataPtr->mValRefPtr));

  // Create storage for the local errors and area
  std::array<double, 2> element_local_value;
  std::fill(element_local_value.begin(), element_local_value.end(), 0.0);

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    double error = t_val_ref - t_val;

    element_local_value[0] += error * error * area * t_w; // RMS error
    element_local_value[1] += t_w * area;

    ++t_val;
    ++t_val_ref;
    ++t_w;
  }

  // Set array of indices
  constexpr std::array<int, 2> indices = {CommonDataClassic::VALUE_ERROR,
                                          CommonDataClassic::VOL_E};

  // Assemble first moment of inertia
  CHKERR ::VecSetValues(commonDataPtr->petscVec, indices.size(), indices.data(),
                        &element_local_value[0], ADD_VALUES);

  MoFEMFunctionReturn(0);
}
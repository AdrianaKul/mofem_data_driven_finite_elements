#include <HDivDataDrivenDiffusionProblem.hpp>

using OpHdivHdiv = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<3, 3>;

using OpHdivHdivEdge = FormsIntegrators<OpEdgeEle>::Assembly<
    PETSC>::BiLinearForm<GAUSS>::OpMass<3, 3>;

using OpMassMat = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMass<1, 2>;

// Scalars
using OpHdivU = FormsIntegrators<OpFaceEle>::Assembly<PETSC>::BiLinearForm<
    GAUSS>::OpMixDivTimesScalar<2>;

using OpPressureBC = FormsIntegrators<OpEdgeEle>::Assembly<PETSC>::LinearForm<
    GAUSS>::OpNormalMixVecTimesScalar<2>;

using OpDomainSourceRhs = FormsIntegrators<OpFaceEle>::Assembly<
    PETSC>::LinearForm<GAUSS>::OpSource<1, 1>;

// boundary conditions (start)
constexpr AssemblyType A = AssemblyType::PETSC; //< selected assembly type
constexpr IntegrationType I =
    IntegrationType::GAUSS; //< selected integration type

struct DomainBCs {};
struct BoundaryBCs {};
struct BoundaryScalarBCs {};

using DomainRhsBCs = NaturalBC<OpFaceEle>::Assembly<A>::LinearForm<I>;
using OpDomainRhsBCs = DomainRhsBCs::OpFlux<DomainBCs, 1, 1>;
using BoundaryRhsBCs = NaturalBC<OpEdgeEle>::Assembly<A>::LinearForm<I>;
using OpBoundaryRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryBCs, 3, 2>;
using OpBoundaryScalarRhsBCs = BoundaryRhsBCs::OpFlux<BoundaryScalarBCs, 3, 2>;

#include <NaturalDomainBC.hpp>
#include <NaturalBoundaryBC.hpp>
#include <ScalarBoundaryBC.hpp>
// boundary conditions (end)

MoFEMErrorCode DiffusionHDivDD::setupProblem() {
  MoFEMFunctionBegin;

  DDLogTag;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-p_ref_all",
                             &flg_refine_order_all, PETSC_NULL);
  CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-skip_all_vtk", &flg_skip_all_vtk,
                             PETSC_NULL);
  if (flg_refine_order_all)
    MOFEM_LOG("WORLD", Sev::inform)
        << "-p_ref_all used (order refines at all of the elements)";
  if (flg_skip_all_vtk)
    MOFEM_LOG("WORLD", Sev::inform)
        << "-skip_all_vtk used (no .h5m files produced)";

  int nb_quads = 0;
  CHKERR mField.get_moab().get_number_entities_by_type(0, MBQUAD, nb_quads);
  auto base = AINSWORTH_LEGENDRE_BASE;
  if (nb_quads) {
    // AINSWORTH_LEGENDRE_BASE is not implemented for HDIV/HCURL space on quads
    base = DEMKOWICZ_JACOBI_BASE;
  }

  // domain fields
  CHKERR simpleInterface->addDomainField(valueField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDomainField(gradField, L2, AINSWORTH_LEGENDRE_BASE,
                                         2);
  CHKERR simpleInterface->addDomainField(fluxField, HCURL, base, 1);
  CHKERR simpleInterface->addDomainField(tauField, HCURL, base, 1);
  CHKERR simpleInterface->addDomainField(lagrangeField, L2,
                                         AINSWORTH_LEGENDRE_BASE, 1);

  // boundary fields
  CHKERR simpleInterface->addBoundaryField(fluxField, HCURL,
                                           AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addBoundaryField(tauField, HCURL,
                                           AINSWORTH_LEGENDRE_BASE, 1);

  // skeleton fields
  CHKERR simpleInterface->addSkeletonField(fluxField, HCURL, base, 1);

  // TODO: check orders
  CHKERR simpleInterface->setFieldOrder(valueField, oRder);
  CHKERR simpleInterface->setFieldOrder(gradField, oRder + 1);
  CHKERR simpleInterface->setFieldOrder(fluxField, oRder + 1);
  CHKERR simpleInterface->setFieldOrder(tauField, oRder + 1);
  CHKERR simpleInterface->setFieldOrder(lagrangeField, oRder);

  CHKERR simpleInterface->setUp();

  // create and share common data pointers across the inheritance
  commonDataPtr = boost::make_shared<CommonDataHDivDD>();
  ClassicDiffusionProblem::commonDataPtr = commonDataPtr;
  HDivDiffusionProblem::commonDataPtr = commonDataPtr;
  DiffusionDD::commonDataPtr = commonDataPtr;

  MoFEMFunctionReturn(0);
}

//! [Create common data]
MoFEMErrorCode DiffusionHDivDD::createDDHdivCommonData() {
  MoFEMFunctionBegin;

  commonDataPtr->k_minus = dummy_k;

  commonDataPtr->mPeturbationPtr = boost::make_shared<MatrixDouble>();

  // MatCreate(PETSC_COMM_SELF, &matGlobalResults);

  // max sigma for for integ point
  commonDataPtr->integ_sigma_max_p = 0.;
  commonDataPtr->integ_sigma_max_grad_x = 0.;
  commonDataPtr->integ_sigma_max_grad_y = 0.;
  commonDataPtr->integ_sigma_max_flux_x = 0.;
  commonDataPtr->integ_sigma_max_flux_y = 0.;

  commonDataPtr->perturb_multiplier = 10.;
  commonDataPtr->monte_patch_number = 2;

  MoFEMFunctionReturn(0);
}
//! [Create common data]

MoFEMErrorCode DiffusionHDivDD::removeLambda() {
  MoFEMFunctionBegin;
  auto remove_lambda = [&](auto &ents) {
    auto problem_manager = mField.getInterface<ProblemsManager>();
    CHKERR problem_manager->removeDofsOnEntities(
        simpleInterface->getProblemName(), tauField, ents, 0, 1);
  };
  remove_lambda(fluxBcRange);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivDD::assembleSystem() {
  MoFEMFunctionBegin;

  // clearing previous settings
  feDomainLhsPtr->getOpPtrVector().clear();
  feDomainRhsPtr->getOpPtrVector().clear();
  feBoundaryLhsPtr->getOpPtrVector().clear();
  feBoundaryRhsPtr->getOpPtrVector().clear();

  // calculating jacobian and other general necessary parts
  CHKERR calculateHdivJacobian(feDomainLhsPtr);
  CHKERR calculateHdivJacobian(feDomainRhsPtr);
  feBoundaryRhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());
  feBoundaryLhsPtr->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformOnEdge2D());

  { //  LHS domain elements

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpSetBc(fluxField, true, boundaryMarker));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivHdiv(fluxField, fluxField, s_q_Function));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpMassMat(gradField, gradField, s_p_Function));

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OperatorsForDD::OpMixHdivTimesVecLhs(tauField, gradField,
                                                 oneFunction));

    auto unity = []() { return 1.0; };

    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivU(tauField, valueField, unity, true));
    feDomainLhsPtr->getOpPtrVector().push_back(
        new OpHdivU(fluxField, lagrangeField, unity, true));

    feDomainLhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
  }

  { // RHS boundary elements - neuman bc (pressure)
    // using tauField in boundary markers to avoid flux boundary conditions
    // repeating
    feBoundaryRhsPtr->getOpPtrVector().push_back(
        new OpSetBc(tauField, true, boundaryMarker));
    CHKERR BoundaryRhsBCs::AddFluxToPipeline<OpBoundaryScalarRhsBCs>::add(
        feBoundaryRhsPtr->getOpPtrVector(), mField, tauField, dummy_k,
        Sev::inform);
    feBoundaryRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(tauField));
  }

  { // Rhs domain (source)
    CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::add(
        feDomainRhsPtr->getOpPtrVector(), mField, lagrangeField, dummy_k,
        Sev::inform);
  }

  {   // Rhs DD domain
    { // get gradients of pressure and flux to find in the data set
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateVectorFieldValues<2>(gradField,
                                              commonDataPtr->mGradPtr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateHVecVectorField<3>(fluxField,
                                            commonDataPtr->mFluxPtr));
    }

    { // find g* and q* in the data set and get point distance errors
      if (!flg_use_line) {
        switch (datasetDim) {
        case 4:
          feDomainRhsPtr->getOpPtrVector().push_back(
              new OperatorsForDD::OpFindData4D<3>(valueField, searchData));
          break;
        case 5:
          feDomainRhsPtr->getOpPtrVector().push_back(
              new OperatorsForDD::OpFindData5D<3>(valueField, searchData));
          break;
        }
      }
      if (flg_use_line)
        feDomainRhsPtr->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindClosest<3>(valueField, searchData));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpPointError(valueField, commonDataPtr));
    }

    { // RHS of domain elements - g* and q*
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpSetBc(fluxField, true, boundaryMarker));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpL2StarRhs<2>(
              gradField, commonDataPtr->mGradStarPtr, s_p_Function));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OperatorsForDD::OpHdivStarRhs<2>(
              fluxField, commonDataPtr->mFluxStarPtr, s_q_Function));
      feDomainRhsPtr->getOpPtrVector().push_back(new OpUnSetBc(fluxField));
    }
  }

  // Hdiv essential bsc - flux
  CHKERR assembleEssentialBoundary();

  if (print_moab == PETSC_TRUE) { // if whole moab output is desired
    feDomainRhsPtr->getOpPtrVector().push_back(new OpPrintStarIntegPts<3>(
        valueField, commonDataPtr, mField.get_moab()));
  }

  MoFEMFunctionReturn(0);
}

//! [Refine]
MoFEMErrorCode DiffusionHDivDD::refineOrder() {
  MoFEMFunctionBegin;
  Tag th_error_ind, th_order;
  CHKERR getTagHandle(mField, "ERROR_ESTIMATOR", MB_TYPE_DOUBLE, th_error_ind);
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER, th_order);

  Range ele_to_consider;
  // tolerance percentage for refinement control
  double tol_distance_ref = 0.0;
  // get tolerance for distance refinement
  CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-tol_distance_ref",
                             &tol_distance_ref, PETSC_NULL);

  // if tolerance is defined and not zero
  if (tol_distance_ref > 1e-10) {
    Tag th_distance;
    CHKERR getTagHandle(mField, "DD_DISTANCE_AVE", MB_TYPE_DOUBLE, th_distance);

    for (auto ent : domainEntities) {
      double distance;
      // get distance to the dataset
      CHKERR mField.get_moab().tag_get_data(th_distance, &ent, 1, &distance);
      // if distance is smaller than the tolerance percentage times the rms norm
      // of the fields consider the element for order refinement
      if (distance < 0.01 * tol_distance_ref * commonDataPtr->normFields) {
        ele_to_consider.insert(ent);
      }
    }
  } else
    ele_to_consider = domainEntities; // if not tolerance is set consider all
                                      // elements for order refinement

  std::vector<Range> refinement_levels;
  refinement_levels.resize(refIterNum + 1);
  for (auto ent : ele_to_consider) {
    double err_indic = 0;
    CHKERR mField.get_moab().tag_get_data(th_error_ind, &ent, 1, &err_indic);
    int order, new_order;
    CHKERR mField.get_moab().tag_get_data(th_order, &ent, 1, &order);
    new_order = order + 1;
    Range refined_ents;
    if (err_indic >
            refControl * sqrt(errorIndicatorIntegral / totalElementNumber) ||
        flg_refine_order_all) {
      refined_ents.insert(ent);
      Range adj;
      CHKERR mField.get_moab().get_adjacencies(&ent, 1, 1, false, adj,
                                               moab::Interface::UNION);
      refined_ents.merge(adj);
      refinement_levels[new_order - oRder].merge(refined_ents);
      CHKERR mField.get_moab().tag_set_data(th_order, &ent, 1, &new_order);
    }
  }

  Range entities_to_check;
  Range surrounding_edges;
  // for (int ll = 1; ll < refinement_levels.size(); ll++) {
  for (int ll = refinement_levels.size() - 1; ll > 0; ll--) {
    int order_to_compare = oRder + ll;
    int new_order = order_to_compare - 1;
    surrounding_edges.merge(refinement_levels[ll]);
    Range adj_faces;
    CHKERR mField.get_moab().get_adjacencies(surrounding_edges, 2, false,
                                             adj_faces, moab::Interface::UNION);
    entities_to_check.merge(adj_faces);
    Range adj_edges;
    CHKERR mField.get_moab().get_adjacencies(entities_to_check, 1, false,
                                             adj_edges, moab::Interface::UNION);
    surrounding_edges.merge(adj_edges);

    for (auto &ent : entities_to_check) {
      int order;
      CHKERR mField.get_moab().tag_get_data(th_order, &ent, 1, &order);

      if (order < order_to_compare - 1) {
        refinement_levels[ll - 1].insert(ent);
        CHKERR mField.get_moab().tag_set_data(th_order, &ent, 1, &new_order);
      }
    }
  }

  for (int ll = 1; ll < refinement_levels.size(); ll++) {
    CHKERR mField.getInterface<CommInterface>()->synchroniseEntities(
        refinement_levels[ll]);
    CHKERR mField.set_field_order(refinement_levels[ll], fluxField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], gradField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], tauField,
                                  oRder + ll + 1);
    CHKERR mField.set_field_order(refinement_levels[ll], valueField,
                                  oRder + ll);
    CHKERR mField.set_field_order(refinement_levels[ll], lagrangeField,
                                  oRder + ll);
  }

  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      fluxField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      gradField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      tauField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      valueField);
  CHKERR mField.getInterface<CommInterface>()->synchroniseFieldEntities(
      lagrangeField);
  CHKERR mField.build_fields();
  CHKERR mField.build_finite_elements();
  CHKERR mField.build_adjacencies(simpleInterface->getBitRefLevel());
  mField.getInterface<ProblemsManager>()->buildProblemFromFields = PETSC_TRUE;
  CHKERR DMSetUp_MoFEM(simpleInterface->getDM());
  MoFEMFunctionReturn(0);
}
//! [Refine]

//! [Check error]
MoFEMErrorCode DiffusionHDivDD::checkError(int iter_num, bool print,
                                           std::string vtk_name) {
  MoFEMFunctionBegin;

  // scale fields
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(
      1. / commonDataPtr->scaleV, valueField);

  boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
      new PatchIntegFaceEle(mField, patchIntegNum));
  PostProcGauss->getRuleHook =
      feDomainRhsPtr->getRuleHook; // use the same integration rule as domainRhs

  CHKERR calculateHdivJacobian(PostProcGauss);

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>(valueField,
                                            commonDataPtr->mValGradPtr));
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateVectorFieldValues<2>(gradField, commonDataPtr->mGradPtr));
  PostProcGauss->getOpPtrVector().push_back(new OpErrorIndGrad<2>(
      commonDataPtr->mValGradPtr, commonDataPtr->mGradPtr,
      commonDataPtr->petscRefineErrorVec, CommonDataHDiv::ERROR_INDICATOR_GRAD,
      mField, "ERROR_INDICATOR_GRAD", 1.0));

  auto source_val_ptr = boost::make_shared<VectorDouble>();
  CHKERR DomainRhsBCs::AddFluxToPipeline<OpDomainRhsBCs>::get_val(
      PostProcGauss->getOpPtrVector(), mField, fluxField, source_val_ptr,
      dummy_k, Sev::inform);

  auto div_flux_ptr = boost::make_shared<VectorDouble>();
  PostProcGauss->getOpPtrVector().push_back(
      new OpCalculateHdivVectorDivergence<3, 2>(fluxField, div_flux_ptr));

  PostProcGauss->getOpPtrVector().push_back(new OpErrorIndDiv(
      div_flux_ptr, source_val_ptr, commonDataPtr->petscRefineErrorVec,
      CommonDataHDiv::ERROR_INDICATOR_DIV, mField, "ERROR_INDICATOR_DIV"));

  // PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
  //     source_val_ptr, commonDataPtr->petscRefineErrorVec,
  //     CommonDataHDiv::ERROR_INDICATOR_DIV, div_flux_ptr));

  // PostProcGauss->getOpPtrVector().push_back(
  //     new OpScalarTags(div_flux_ptr, "ERROR_INDICATOR_DIV", mField));

  // data-related errors
  {
    if (!flg_use_line) {
      switch (datasetDim) {
      case 4:
        PostProcGauss->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData4D<3>(valueField, searchData));
        break;
      case 5:
        PostProcGauss->getOpPtrVector().push_back(
            new OperatorsForDD::OpFindData5D<3>(valueField, searchData,
                                                commonDataPtr->scaleV));
        break;
      }
    }
    if (flg_use_line)
      PostProcGauss->getOpPtrVector().push_back(
          new OperatorsForDD::OpFindClosest<3>(valueField, searchData));

    // PostProcGauss->getOpPtrVector().push_back(
    //     new OpScalarTags(commonDataPtr->pointDistance, "DD_DISTANCE",
    //     mField));

    // PostProcGauss->getOpPtrVector().push_back(
    //     new OperatorsForDD::OpScalarVarTags(commonDataPtr->pointDistance,
    //                                         "DD_DISTANCE_AVE",
    //                                         "DD_DISTANCE_VAR", mField));
  }

  PostProcGauss->getOpPtrVector().push_back(
      new OpCalcNormL2Tensor0(commonDataPtr->mValPtr, commonDataPtr->petscVec,
                              CommonDataClassic::NORM_T));

  PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
      commonDataPtr->mGradPtr, commonDataPtr->petscVec,
      CommonDataClassic::NORM_G));

  PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<3>(
      commonDataPtr->mFluxPtr, commonDataPtr->petscVec,
      CommonDataClassic::NORM_Q));

  CHKERR postGetAnalyticalValuesHdiv(PostProcGauss);

  if (flgAnalytical) {

    auto kFunction = [&](const double, const double, const double) {
      return dummy_k;
    };

    PostProcGauss->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(fluxField, commonDataPtr->mFluxPtr));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor0(
        commonDataPtr->mValPtr, commonDataPtr->petscVec,
        CommonDataClassic::VALUE_ERROR, commonDataPtr->mValFuncPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OpScalarTags(commonDataPtr->mValFuncPtr, "ERROR_L2_NORM", mField));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<2>(
        commonDataPtr->mValGradPtr, commonDataPtr->petscVec,
        CommonDataClassic::GRAD_ERROR, commonDataPtr->mGradFuncPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpVectorNormTags<2>(commonDataPtr->mGradFuncPtr,
                                                "ERROR_H1_SEMINORM", mField));

    PostProcGauss->getOpPtrVector().push_back(new OpCalcNormL2Tensor1<3>(
        commonDataPtr->mFluxPtr, commonDataPtr->petscVec,
        CommonDataClassic::FLUX_ERROR, commonDataPtr->mFluxFuncPtr));

    PostProcGauss->getOpPtrVector().push_back(
        new OperatorsForDD::OpVectorNormTags<3>(commonDataPtr->mFluxFuncPtr,
                                                "ERROR_FLUX", mField));
  }

  PostProcGauss->getOpPtrVector().push_back(new OpGetGaussCount(
      commonDataPtr->petscVec, CommonDataClassic::GAUSS_COUNT));
  PostProcGauss->getOpPtrVector().push_back(
      new OpGetArea(commonDataPtr->petscVec, CommonDataClassic::VOL_E));

  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);

  CHKERR VecAssemblyBegin(commonDataPtr->petscRefineErrorVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscRefineErrorVec);

  CHKERR VecZeroEntries(commonDataPtr->petscVec);
  CHKERR VecZeroEntries(commonDataPtr->petscRefineErrorVec);

  CHKERR DMoFEMLoopFiniteElements(
      dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

  CHKERR getJumps(iter_num);

  std::vector<Tag> tag_handles_calculated;
  enum tag_handles_calculated_enum {
    ORDER = 0,
    ERROR_INDICATOR_GRAD,
    ERROR_INDICATOR_DIV,
    ERROR_FLUX,
    JUMP_L2,
    ERROR_L2_NORM,
    ERROR_H1_SEMINORM,
    ERROR_ESTIMATOR,
    DD_DISTANCE_AVE,
    DD_DISTANCE_VAR,
    LAST_ELEMENT
  };

  tag_handles_calculated.resize(LAST_ELEMENT);
  CHKERR getTagHandle(mField, "ERROR_INDICATOR_GRAD", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_INDICATOR_GRAD]);
  CHKERR getTagHandle(mField, "ORDER", MB_TYPE_INTEGER,
                      tag_handles_calculated[ORDER]);
  CHKERR getTagHandle(mField, "JUMP_L2", MB_TYPE_DOUBLE,
                      tag_handles_calculated[JUMP_L2]);
  CHKERR getTagHandle(mField, "ERROR_INDICATOR_DIV", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_INDICATOR_DIV]);

  CHKERR getTagHandle(mField, "ERROR_L2_NORM", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_L2_NORM]);
  CHKERR getTagHandle(mField, "ERROR_H1_SEMINORM", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_H1_SEMINORM]);
  CHKERR getTagHandle(mField, "ERROR_FLUX", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_FLUX]);

  CHKERR getTagHandle(mField, "ERROR_ESTIMATOR", MB_TYPE_DOUBLE,
                      tag_handles_calculated[ERROR_ESTIMATOR]);

  CHKERR getTagHandle(mField, "DD_DISTANCE_AVE", MB_TYPE_DOUBLE,
                      tag_handles_calculated[DD_DISTANCE_AVE]);
  CHKERR getTagHandle(mField, "DD_DISTANCE_VAR", MB_TYPE_DOUBLE,
                      tag_handles_calculated[DD_DISTANCE_VAR]); // variance

  double test_area = 0.;
  double error_estimator_iter = 0.;
  double err_indic_grad = 0.;
  double err_indic_div = 0.;
  double jump_l2 = 0.;

  for (auto ent : domainEntities) {

    CHKERR mField.get_moab().tag_get_data(
        tag_handles_calculated[ERROR_INDICATOR_GRAD], &ent, 1, &err_indic_grad);

    CHKERR mField.get_moab().tag_get_data(
        tag_handles_calculated[ERROR_INDICATOR_DIV], &ent, 1, &err_indic_div);

    CHKERR mField.get_moab().tag_get_data(tag_handles_calculated[JUMP_L2], &ent,
                                          1, &jump_l2);

    double error_estimator_ele =
        sqrt(err_indic_grad * err_indic_grad + err_indic_div * err_indic_div +
             jump_l2 * jump_l2);

    // CHKERR VecSetValue(commonDataPtr->petscRefineErrorVec,
    //                    CommonDataHDiv::ERROR_ESTIMATOR, error_estimator_ele,
    //                    ADD_VALUES);

    CHKERR mField.get_moab().tag_set_data(
        tag_handles_calculated[ERROR_ESTIMATOR], &ent, 1, &error_estimator_ele);
  }

  CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscVec);
  CHKERR VecAssemblyBegin(commonDataPtr->petscRefineErrorVec);
  CHKERR VecAssemblyEnd(commonDataPtr->petscRefineErrorVec);

  const double *array;
  CHKERR VecGetArrayRead(commonDataPtr->petscRefineErrorVec, &array);

  const double *array_classical;
  CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array_classical);

  if (mField.get_comm_rank() == 0) {

    // TODO: check if errorIndicatorIntegral works for multiple processors
    jumpL2.push_back(std::sqrt(array[CommonDataHDiv::JUMP_L2]));
    jumpHdiv.push_back(std::sqrt(array[CommonDataHDiv::JUMP_HDIV]));
    errorIndicatorGrad.push_back(
        std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_GRAD]));
    errorIndicatorDiv.push_back(
        std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_DIV]));
    errorEstimator.push_back(std::sqrt(square(errorIndicatorGrad.back()) +
                                       square(errorIndicatorDiv.back()) +
                                       square(jumpL2.back())));

    MOFEM_LOG("WORLD", Sev::inform)
        << "Global error indicator grad: "
        << std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_GRAD]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global error indicator div: "
        << std::sqrt(array[CommonDataHDiv::ERROR_INDICATOR_DIV]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global jump of values: "
        << std::sqrt(array[CommonDataHDiv::JUMP_L2]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global jump of fluxes: "
        << std::sqrt(array[CommonDataHDiv::JUMP_HDIV]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global error estimators: " << errorEstimator.back();
    MOFEM_LOG("WORLD", Sev::inform) << "Total number of elements: "
                                    << (int)array[CommonDataHDiv::TOTAL_NUMBER];

    // errorIndicatorIntegral = array[CommonDataHDiv::ERROR_INDICATOR_GRAD];
    errorIndicatorIntegral = array[CommonDataHDiv::ERROR_INDICATOR_GRAD] +
                             array[CommonDataHDiv::ERROR_INDICATOR_DIV] +
                             array[CommonDataHDiv::JUMP_L2];
    MOFEM_LOG("WORLD", Sev::inform)
        << "Global error indicator integral: " << errorIndicatorIntegral;

    totalElementNumber = (int)array[CommonDataHDiv::TOTAL_NUMBER];

    commonDataPtr->vOlume = array_classical[CommonDataClassic::VOL_E];
    commonDataPtr->rmsErr.push_back(
        sqrt((array_classical[CommonDataClassic::VALUE_ERROR]) /
             array_classical[CommonDataClassic::VOL_E]));
    commonDataPtr->gradErr.push_back(
        sqrt((array_classical[CommonDataClassic::GRAD_ERROR]) /
             array_classical[CommonDataClassic::VOL_E]));
    commonDataPtr->fluxErr.push_back(
        sqrt((array_classical[CommonDataClassic::FLUX_ERROR]) /
             array_classical[CommonDataClassic::VOL_E]));
    commonDataPtr->numberIntegrationPoints.push_back(
        array_classical[CommonDataClassic::GAUSS_COUNT]);

    MOFEM_LOG("WORLD", Sev::inform) << "order: " << oRder;
    MOFEM_LOG("WORLD", Sev::inform)
        << "gaussnum: " << array_classical[CommonDataClassic::GAUSS_COUNT];
    MOFEM_LOG("WORLD", Sev::inform)
        << "volume: " << array_classical[CommonDataClassic::VOL_E];
    MOFEM_LOG("WORLD", Sev::inform)
        << "L2norm: "
        << sqrt((array_classical[CommonDataClassic::VALUE_ERROR]) /
                array_classical[CommonDataClassic::VOL_E]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "H1seminorm: "
        << sqrt((array_classical[CommonDataClassic::GRAD_ERROR]) /
                array_classical[CommonDataClassic::VOL_E]);
    MOFEM_LOG("WORLD", Sev::inform)
        << "fluxErr: "
        << sqrt((array_classical[CommonDataClassic::FLUX_ERROR]) /
                array_classical[CommonDataClassic::VOL_E]);
  }

  commonDataPtr->normT.push_back(std::sqrt(
      array_classical[CommonDataClassic::NORM_T] * commonDataPtr->scaleV /
      array_classical[CommonDataClassic::VOL_E]));
  commonDataPtr->normG.push_back(
      std::sqrt(array_classical[CommonDataClassic::NORM_G] /
                array_classical[CommonDataClassic::VOL_E]));
  commonDataPtr->normQ.push_back(
      std::sqrt(array_classical[CommonDataClassic::NORM_Q] /
                array_classical[CommonDataClassic::VOL_E]));

  MOFEM_LOG("WORLD", Sev::inform)
      << "L2 norm of T: " << commonDataPtr->normT.back();
  MOFEM_LOG("WORLD", Sev::inform)
      << "L2 norm of G: " << commonDataPtr->normG.back();
  MOFEM_LOG("WORLD", Sev::inform)
      << "L2 norm of Q: " << commonDataPtr->normQ.back();

  if (datasetDim == 5) {
    commonDataPtr->normFields = sqrt(square(commonDataPtr->normT.back()) +
                                     square(commonDataPtr->normG.back()) +
                                     square(commonDataPtr->normQ.back()));
  } else {
    commonDataPtr->normFields = sqrt(square(commonDataPtr->normG.back()) +
                                     square(commonDataPtr->normQ.back()));
  }

  // rms of all norms
  MOFEM_LOG("WORLD", Sev::inform)
      << "RMS of all norms: " << commonDataPtr->normFields;

  MOFEM_LOG("WORLD", Sev::inform)
      << "RMS point distance error: " << commonDataPtr->rmsPointDistErr.back();

  CHKERR VecRestoreArrayRead(commonDataPtr->petscRefineErrorVec, &array);
  CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array_classical);

  if (print && flg_skip_all_vtk == false) {
    ParallelComm *pcomm =
        ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
    if (pcomm == NULL)
      SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
              "Communicator not set");

    auto bit_level1 = BitRefLevel().set(meshRefinementCounter);
    auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
    CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
        bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);
    tag_handles_calculated.push_back(pcomm->part_tag());
    std::ostringstream strm;
    strm << vtk_name << iter_num << ".h5m";
    CHKERR mField.get_moab().write_file(
        strm.str().c_str(), "MOAB", "PARALLEL=WRITE_PART",
        out_meshset_tri_ptr->get_ptr(), 1, tag_handles_calculated.data(),
        tag_handles_calculated.size());
  }

  // scale fields
  CHKERR mField.getInterface<FieldBlas>()->fieldScale(commonDataPtr->scaleV,
                                                      valueField);
  MoFEMFunctionReturn(0);
}
//! [Check error]

MoFEMErrorCode DiffusionHDivDD::solveDDHDivLoopSystem() {
  MoFEMFunctionBegin;

  CHKERR getKSPinitial();

  CHKERR errorFileNameSetUp();

  double go = 0.;
  CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE, &go_per_proc);

  bool allowed = false;

  while (go < 10e-5) {

    // Zero global vector
    CHKERR VecZeroEntries(commonDataPtr->petscVec);

    CHKERR solveSystem();

    CHKERR outputResults(commonDataPtr->counter,
                         skip_vtk != PETSC_TRUE &&
                             flg_out_integ ==
                                 PETSC_TRUE); // also post processing errors

    // add to the counter
    commonDataPtr->counter = commonDataPtr->counter + 1;

    CHKERR errorHandlingWithinLoop(allowed);

    CHKERR checkError(
        commonDataPtr->counter,
        skip_vtk != PETSC_TRUE &&
            flg_out_error ==
                PETSC_TRUE); // post processing errors for Hdiv bits

    MOFEM_LOG("WORLD", Sev::inform)
        << "difference in accumulated point distance error: "
        << std::abs(std::abs(commonDataPtr->distVec.back()) -
                    std::abs(commonDataPtr->distVec.end()[-2]));

    if (((std::abs(std::abs(commonDataPtr->distVec.back()) -
                   std::abs(commonDataPtr->distVec.end()[-2])) < tol_poi_dist &&
          commonDataPtr->counter > 2) ||
         commonDataPtr->counter >=
             max_iter * (orderRefinementCounter + meshRefinementCounter + 1)) &&
        (orderRefinementCounter <= refIterNum ||
         meshRefinementCounter <= refIterNum)) {

      if (orderRefinementCounter == refIterNum ||
          meshRefinementCounter == refIterNum)
        allowed = true;

      CHKERR outputHdivVtkFields(meshRefinementCounter + orderRefinementCounter,
                                 "out_result_refinement_");
      CHKERR checkError(meshRefinementCounter + orderRefinementCounter, true,
                        "out_error_refinement_");

      if (allowed == false) {
        switch (refinementStyle) {
        case 1:
          CHKERR refineOrder();
          orderRefinementCounter += 1;
          break;
        case 2:
          CHKERR refineMesh();
          break;
        default:
          MOFEM_LOG("WORLD", Sev::inform) << ".. no refinement";
        }
        CHKERR boundaryCondition();
        CHKERR markFluxBc();
        CHKERR removeLambda();
        CHKERR assembleSystem();
        CHKERR getKSPinitial();
      }

      if (orderRefinementCounter == refIterNum ||
          meshRefinementCounter == refIterNum)
        allowed = true;
    }

    CHKERR VecAssemblyBegin(go_per_proc);
    CHKERR VecAssemblyEnd(go_per_proc);
    CHKERR VecSum(go_per_proc, &go);
  }

  CHKERR outputHdivVtkFields(meshRefinementCounter + orderRefinementCounter,
                             "out_result_refinement_");
  CHKERR checkError(meshRefinementCounter + orderRefinementCounter, true,
                    "out_error_refinement_");

  // printing global results to .csv file
  if (mField.get_comm_rank() == 0) {

    const double *array;
    CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array);

    DDLogTag;
    MOFEM_LOG("WORLD", Sev::inform)
        << "Finished with " << commonDataPtr->distVec.size() << " iterations. "
        << "Convergence: ";

    // print the errors at the end of analysis
    for (int i = 0; i < commonDataPtr->distVec.size(); i++) {
      MOFEM_LOG("WORLD", Sev::inform)
          << commonDataPtr->rmsPointDistErr[i]
          << " <- rmsPointDistErr ; rmsErr -> " << commonDataPtr->rmsErr[i];
    }

    // add to the file capturing the end of the analysis "sumanalys.csv"
    std::ofstream sumanalysis;
    sumanalysis.open("sumanalys.csv", std::ofstream::out | std::ofstream::app);

    sumanalysis << oRder << ", "
                << commonDataPtr->numberIntegrationPoints.back() << ", "
                << commonDataPtr->rmsErr.size() << ", " << commonDataPtr->vOlume
                << ", " << dummy_count << ", "
                << commonDataPtr->rmsPointDistErr.back() << ", "
                << errorEstimator.back() << ", " << commonDataPtr->rmsErr.back()
                << ", " << commonDataPtr->gradErr.back() << ", "
                << commonDataPtr->fluxErr.back() << ", "
                << orderRefinementCounter << ", " << errorIndicatorGrad.back()
                << ", " << errorIndicatorDiv.back() << ", " << jumpL2.back()
                << ", " << jumpHdiv.back() << ", " << totalElementNumber
                << std::endl;

    sumanalysis.close();

    CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivDD::runMonteCarlo() {
  MoFEMFunctionBegin;

  if (!flg_monte_carlo)
    MoFEMFunctionReturnHot(0);

  // restart the counter
  commonDataPtr->counter = 0;
  double go = 0.;
  CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE, &go_per_proc);

  for (int i = 0; i < commonDataPtr->numberMonteCarlo; i++) {

    // restart the counter
    commonDataPtr->counter = 0;
    // restart the iterations
    go = 0.;
    CHKERR VecZeroEntries(go_per_proc);

    { // restart max_sigmas
      // max sigma for for integ point
      commonDataPtr->integ_sigma_max_p = 0;
      commonDataPtr->integ_sigma_max_grad_x = 0;
      commonDataPtr->integ_sigma_max_grad_y = 0;
      commonDataPtr->integ_sigma_max_flux_x = 0;
      commonDataPtr->integ_sigma_max_flux_y = 0;
    }

    if (!skip_vtk)
      CHKERR outputHdivVtkFields(commonDataPtr->monte_carlo_counter,
                                 "out_before_perturb_");

    // Perturb fields relevant to material data search
    {
      std::cout << "rmsPointDistErr used for perturbation = "
                << commonDataPtr->rmsPointDistErr.back() << std::endl;

      // random
      PetscRandom rctx;
      PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
      PetscRandomSetSeed(rctx, (unsigned long)std::time(0));
      PetscRandomSeed(rctx);

      // make scale an input
      double scale = 10;
      CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-rand_ini_scale", &scale,
                                 PETSC_NULL);

      if (scale < 10e-4)
        scale = commonDataPtr->rmsPointDistErr.back();

      // set random Q field (L2)
      for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, gradField, it)) {
        if (it->get()->getEntType() == MBTRI || MBQUAD) {
          auto field_data = (*it)->getEntFieldData();
          double value;
          // double scale = 1.0;
          PetscRandomGetValueReal(rctx, &value);
          field_data[0] += (value - 0.5) * scale * 5; // value * scale
          PetscRandomGetValueReal(rctx, &value);
          field_data[1] += (value - 0.5) * scale * 5;
        }
      }

      auto vector_times_scalar_field =
          [&](boost::shared_ptr<FieldEntity> ent_ptr_x) {
            MoFEMFunctionBeginHot;
            auto x_data = ent_ptr_x->getEntFieldData();
            double value;
            PetscRandomGetValueReal(rctx, &value);

            for (auto &v : x_data) {
              PetscRandomGetValueReal(rctx, &value);
              v += (value - 0.5) * scale;
            }

            MoFEMFunctionReturnHot(0);
          };

      CHKERR mField.getInterface<FieldBlas>()->fieldLambdaOnEntities(
          vector_times_scalar_field, fluxField);
    }

    if (!skip_vtk)
      CHKERR outputHdivVtkFields(commonDataPtr->monte_carlo_counter,
                                 "out_after_perturb_");

    while (go < 10e-5) {

      // Assemble MPI vector
      CHKERR VecAssemblyBegin(commonDataPtr->petscVec);
      CHKERR VecAssemblyEnd(commonDataPtr->petscVec);

      // Zero global vector
      CHKERR VecZeroEntries(commonDataPtr->petscVec);

      CHKERR solveSystem();

      CHKERR outputResults(commonDataPtr->monte_carlo_counter,
                           false); // also post processing errors

      CHKERR errorHandlingWithinLoop(true);

      if (flg_skip_all_vtk == false)
        CHKERR checkError(commonDataPtr->counter,
                          false); // post processing errors for Hdiv bits

      // add to the counter
      commonDataPtr->counter = commonDataPtr->counter + 1;

      CHKERR VecAssemblyBegin(go_per_proc);
      CHKERR VecAssemblyEnd(go_per_proc);
      CHKERR VecSum(go_per_proc, &go);
    }

    CHKERR monteSaveHdivResults();

    if (flg_skip_all_vtk == false) {
      CHKERR outputHdivVtkFields(commonDataPtr->monte_carlo_counter,
                                 "out_converged_after_monte_");
    }

    // if (flg_skip_all_vtk == false) {
    std::vector<Tag> tag_handles;
    tag_handles.resize(5);
    CHKERR getTagHandle(mField, "SIGMA_T", MB_TYPE_DOUBLE, tag_handles[0]);
    CHKERR getTagHandle(mField, "SIGMA_GRAD_X", MB_TYPE_DOUBLE, tag_handles[1]);
    CHKERR getTagHandle(mField, "SIGMA_GRAD_Y", MB_TYPE_DOUBLE, tag_handles[2]);
    CHKERR getTagHandle(mField, "SIGMA_FLUX_X", MB_TYPE_DOUBLE, tag_handles[3]);
    CHKERR getTagHandle(mField, "SIGMA_FLUX_Y", MB_TYPE_DOUBLE, tag_handles[4]);

    ParallelComm *pcomm =
        ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
    if (pcomm == NULL)
      SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
              "Communicator not set");

    auto bit_level1 = BitRefLevel().set(meshRefinementCounter);
    auto out_meshset_tri_ptr = get_temp_meshset_ptr(mField.get_moab());
    CHKERR mField.getInterface<BitRefManager>()->getEntitiesByDimAndRefLevel(
        bit_level1, BitRefLevel().set(), 2, *out_meshset_tri_ptr);
    tag_handles.push_back(pcomm->part_tag());
    std::ostringstream strm;
    strm << "out_sigma_ele_" << commonDataPtr->monte_carlo_counter << ".h5m";
    CHKERR mField.get_moab().write_file(strm.str().c_str(), "MOAB",
                                        "PARALLEL=WRITE_PART",
                                        out_meshset_tri_ptr->get_ptr(), 1,
                                        tag_handles.data(), tag_handles.size());
    // }

    commonDataPtr->monte_carlo_counter++;

    if (mField.get_comm_rank() == 0 && flg_long_error_file) {
      // open the error file with appending to the file
      std::ofstream error_file;
      error_file.open("standard_deviation_max.csv", ios::app);
      // populate the file with points
      error_file << commonDataPtr->integ_sigma_max_p << ", "
                 << commonDataPtr->integ_sigma_max_grad_x << ", "
                 << commonDataPtr->integ_sigma_max_grad_y << ", "
                 << commonDataPtr->integ_sigma_max_flux_x << ", "
                 << commonDataPtr->integ_sigma_max_flux_y << "\n";
      // close writing into file
      error_file.close();
    }
  }

  // add to the file capturing the end of the analysis "sumanalys.csv"
  std::ofstream sumanalysis;
  sumanalysis.open("sumanalys_monte_sigma.csv",
                   std::ofstream::out | std::ofstream::app);

  sumanalysis << commonDataPtr->integ_sigma_max_p << ", "
              << commonDataPtr->integ_sigma_max_grad_x << ", "
              << commonDataPtr->integ_sigma_max_grad_y << ", "
              << commonDataPtr->integ_sigma_max_flux_x << ", "
              << commonDataPtr->integ_sigma_max_flux_y << "\n";

  sumanalysis.close();

  CHKERR checkError(commonDataPtr->monte_carlo_counter, false);

  // add to the file capturing the end of the analysis "sumanalys.csv"
  // std::ofstream sumanalysis;
  sumanalysis.open("sumanalys_monte.csv",
                   std::ofstream::out | std::ofstream::app);

  const double *array;
  CHKERR VecGetArrayRead(commonDataPtr->petscVec, &array);

  sumanalysis << oRder << ", " << array[CommonDataHDivDD::GAUSS_COUNT] << ", "
              << dummy_count << ", " << errorH1.back() << ", " << errorL2.back()
              << ", " << commonDataPtr->rmsPointDistErr.back() << ", "
              << orderRefinementCounter << ", " << commonDataPtr->rmsErr.size()
              << ", " << errorIndicatorGrad.back() << "\n";

  sumanalysis.close();

  CHKERR VecRestoreArrayRead(commonDataPtr->petscVec, &array);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivDD::monteSaveHdivResults() {
  MoFEMFunctionBegin;

  // calculation with TAGS
  { // populate the tag matrices

    moab::Core mb_post;                   // create database
    moab::Interface &moab_proc = mb_post; // create interface to database

    int patch_integ_num = commonDataPtr->monte_patch_number;
    boost::shared_ptr<PatchIntegFaceEle> TagGauss(
        new PatchIntegFaceEle(mField, patch_integ_num));

    auto domain_rule_tag = [](int, int, int p) -> int { return -1; };

    TagGauss->getRuleHook = domain_rule_tag;

    CHKERR calculateHdivJacobian(TagGauss);

    TagGauss->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
    TagGauss->getOpPtrVector().push_back(new OpCalculateVectorFieldValues<2>(
        gradField, commonDataPtr->mGradPtr));

    TagGauss->getOpPtrVector().push_back(
        new OpCalculateHVecVectorField<3>(fluxField, commonDataPtr->mFluxPtr));

    TagGauss->getOpPtrVector().push_back(
        new OpAddResultToMonteMatrix<3>(valueField, commonDataPtr, mField));

    TagGauss->getOpPtrVector().push_back(
        new OpTagsMeanVar(valueField, commonDataPtr, mField, mb_post));

    CHKERR DMoFEMLoopFiniteElements(
        dM, simpleInterface->getDomainFEName().c_str(), TagGauss);

    if (!skip_vtk) {
      string out_file_name;
      std::ostringstream strm;
      strm << "out_integ_pts_mean_var_" << commonDataPtr->monte_carlo_counter
           << ".h5m";
      out_file_name = strm.str();
      CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                "PARALLEL=WRITE_PART");
    }

    mb_post.delete_mesh();
  }

  if (flg_skip_all_vtk == false)
    CHKERR checkError(
        commonDataPtr->monte_carlo_counter, true,
        "out_monte_ele_errors_"); // post processing errors for Hdiv bits

  CHKERR outputResults(commonDataPtr->monte_carlo_counter, true,
                       "out_monte_integ_star_"); // also post processing errors

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode DiffusionHDivDD::outputResults(int iter_num, bool print,
                                              std::string vtk_name) {
  MoFEMFunctionBegin;

  // // scale fields
  // CHKERR mField.getInterface<FieldBlas>()->fieldScale(commonDataPtr->scaleQ,
  //                                                     fluxField);

  if (skip_vtk != PETSC_TRUE && flg_monte_carlo == false)
    CHKERR outputHdivVtkFields(commonDataPtr->counter);

  if (print_moab == PETSC_TRUE &&
      flg_skip_all_vtk == false) { // if whole moab output is desired
    string gauss_name;
    std::ostringstream strma;
    strma << "out_moab_" << commonDataPtr->counter << ".h5m";
    gauss_name = strma.str();
    CHKERR mField.get_moab().write_file(gauss_name.c_str(), "MOAB",
                                        "PARALLEL=WRITE_PART");
  }
  { // post processing on integration points
    {
      moab::Core mb_post;                   // create database
      moab::Interface &moab_proc = mb_post; // create interface to database

      boost::shared_ptr<PatchIntegFaceEle> PostProcGauss(
          new PatchIntegFaceEle(mField, patchIntegNum));
      PostProcGauss->getRuleHook =
          feDomainRhsPtr
              ->getRuleHook; // use the same integration rule as domainRhs

      // auto analyticalFunctionFlux = [&](const double x, const double y,
      //                                   const double z) {
      //   VectorDouble res;
      //   res.resize(2);
      //   res[0] =
      //       -commonDataPtr->scaleQ * commonDataPtr->linear_k *
      //       (-exp(-100. * (square(x) + square(y))) *
      //        (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) * cos(M_PI *
      //        y));
      //   res[1] =
      //       -commonDataPtr->scaleQ * commonDataPtr->linear_k *
      //       (-exp(-100. * (square(x) + square(y))) *
      //        (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) * cos(M_PI *
      //        x));
      //   return res;
      // };

      CHKERR calculateHdivJacobian(PostProcGauss);

      PostProcGauss->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues(valueField, commonDataPtr->mValPtr));
      PostProcGauss->getOpPtrVector().push_back(
          new OpCalculateVectorFieldValues<2>(gradField,
                                              commonDataPtr->mGradPtr));

      PostProcGauss->getOpPtrVector().push_back(
          new OpCalculateHVecVectorField<3>(fluxField,
                                            commonDataPtr->mFluxPtr));

      // if (commonDataPtr->pReference) {
      //   PostProcGauss->getOpPtrVector().push_back(
      //       new OpCalculateScalarFieldValues("P_reference",
      //                                        commonDataPtr->mValRefPtr));
      //   PostProcGauss->getOpPtrVector().push_back(
      //       new OpPostProcRefErrorIntegPts(valueField, commonDataPtr));
      // } else {
      //   PostProcGauss->getOpPtrVector().push_back(
      //       new OpPostProcErrorPressureIntegPts(valueField, commonDataPtr,
      //                                           analyticalFunction,
      //                                           analyticalFunctionGrad));
      //   PostProcGauss->getOpPtrVector().push_back(
      //       new OpPostProcErrorFluxIntegPts<3>(fluxField, commonDataPtr,
      //                                          analyticalFunctionFlux));
      // }

      if (print) {
        // get gradients of pressure and flux from previous step to find in
        // the data set
        if (commonDataPtr->rtree4D.size() > 1) {
          switch (datasetDim) {
          case 4:
            PostProcGauss->getOpPtrVector().push_back(
                new OperatorsForDD::OpFindData4D<3>(valueField, searchData,
                                                    commonDataPtr->scaleQ));
            break;
          case 5:
            PostProcGauss->getOpPtrVector().push_back(
                new OpCalculateScalarFieldValues(valueField,
                                                 commonDataPtr->mValPtr));
            PostProcGauss->getOpPtrVector().push_back(
                new OperatorsForDD::OpFindData5D<3>(valueField, searchData));
            break;
          }
        } else
          PostProcGauss->getOpPtrVector().push_back(
              new OperatorsForDD::OpFindClosest<3>(valueField, searchData,
                                                   commonDataPtr->scaleQ));
        PostProcGauss->getOpPtrVector().push_back(
            new OpPostProcPrintingIntegPts<3>(valueField, commonDataPtr,
                                              mb_post, analyticalFunction));
      }

      // mb_post.delete_mesh();

      CHKERR DMoFEMLoopFiniteElements(
          dM, simpleInterface->getDomainFEName().c_str(), PostProcGauss);

      if (print && flg_skip_all_vtk == false) {
        {
          string out_file_name;
          std::ostringstream strm;
          strm << vtk_name << iter_num << ".h5m";
          out_file_name = strm.str();
          CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                    "PARALLEL=WRITE_PART");
        }
      }
    }

    // // scale fields
    // CHKERR mField.getInterface<FieldBlas>()->fieldScale(
    //     1. / commonDataPtr->scaleQ, fluxField);
  }

  MoFEMFunctionReturn(0);
}

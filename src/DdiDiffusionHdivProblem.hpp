#ifndef __DDI_DIFFUSION_HDIV_HPP__
#define __DDI_DIFFUSION_HDIV_HPP__

#include <MoFEM.hpp>
#include <BasicFiniteElements.hpp>
#include <iostream>
#include <patch_integ_2D.hpp>
#include <OperatorsForDD.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

#define DDLogTag                                                               \
  MOFEM_LOG_CHANNEL("WORLD");                                                  \
  MOFEM_LOG_FUNCTION();                                                        \
  MOFEM_LOG_TAG("WORLD", "DD");

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct DdiDiffusionHdiv {

  DdiDiffusionHdiv(MoFEM::Interface &m_field);

  MoFEMErrorCode runProblem();

  // Declaration of main functions
  MoFEMErrorCode setParamValues();
  MoFEMErrorCode readMesh();
  MoFEMErrorCode setupProblem();
  MoFEMErrorCode setIntegrationRules();
  MoFEMErrorCode createClassicCommonData();
  MoFEMErrorCode boundaryCondition();
  MoFEMErrorCode markFluxBc();
  MoFEMErrorCode calculateHdivJacobian(boost::shared_ptr<FaceEle> fe_ptr);
  MoFEMErrorCode assembleSystem();
  MoFEMErrorCode getKSPinitial();
  MoFEMErrorCode solveSystem();
  MoFEMErrorCode outputHdivVtkFields(int ii,
                                     std::string vtk_name = "out_result_");
  MoFEMErrorCode
  outputCsvDataset(std::string csv_name = "dataset_generated.csv");

  // param file values
  PetscInt oRder = 1;
  PetscReal dummy_k = 3;
  int patchIntegNum = 1;

  struct CommonDataDDI {

    // main field pointers
    boost::shared_ptr<VectorDouble> mValRefPtr;
    boost::shared_ptr<MatrixDouble> mGradRefPtr;
    boost::shared_ptr<MatrixDouble> mGradPtr;
    boost::shared_ptr<MatrixDouble> mFluxPtr;

    bool pReference = false;
    bool gReference = false;

    /**
     * @brief Vector to indicate indices for storing zero, first and second
     * moments of inertia.
     *
     */
    enum VecElements {
      VALUE_ERROR = 0,   // sum(error^2 * area) between FEM results without and
                         // with data search
      ACCUM_POINT_ERROR, // distance accumulated between result and data
                         // point from all searches
      GAUSS_COUNT,       // how many times was the data set searched
      POINT_ERROR,       // sum of ((distance between points)^2 * area covered)
      VOL_E,             // volume or area of the mesh
      GRAD_ERROR,
      FLUX_ERROR,
      LAST_ELEMENT
    };

    SmartPetscObj<Vec>
        petscVec; ///< Smart pointer which stores PETSc distributed vector
  };

  // Simple functions
  static double minusFunction(const double x, const double y, const double z) {
    return -1.0;
  }
  static double oneFunction(const double x, const double y, const double z) {
    return 1.0;
  }

  // Function to calculate the essential boundary term (pressure)
  static double pressureBcFunction(const double x, const double y,
                                   const double z) {
    return 0;
    // return cos(M_PI * (x - 0.5)) * cos(M_PI * (y - 0.5));
  }

  static double fluxBcFunction(const double x, const double y, const double z) {
    // return sin((x + 0.5) * M_PI) * cosh(M_PI) * M_PI / sinh(M_PI);
    // return 10;
    return 0;
  }

  static double square(double x) {
    return x * x;
  } // TODO: move this somewhere more sensible

  //! [Analytical function]
  static double analyticalFunction(const double x, const double y,
                                   const double z) {
    return exp(-100. * (square(x) + square(y))) * cos(M_PI * x) * cos(M_PI * y);
    // return cos(M_PI * (x - 0.5)) * cos(M_PI * (y - 0.5));
  }
  //! [Analytical function]

  //! [Analytical function gradient]
  static VectorDouble analyticalFunctionGrad(const double x, const double y,
                                             const double z) {
    VectorDouble res;
    res.resize(2);
    res[0] = -exp(-100. * (square(x) + square(y))) *
             (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) * cos(M_PI * y);
    res[1] = -exp(-100. * (square(x) + square(y))) *
             (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) * cos(M_PI * x);
    return res;
  }
  //! [Analytical function gradient]

  // Object to mark boundary entities for the assembling of domain elements
  boost::shared_ptr<std::vector<unsigned char>> boundaryMarker;

  // MoFEM working Pipelines for LHS and RHS of domain and boundary
  boost::shared_ptr<FaceEle> feDomainLhsPtr;
  boost::shared_ptr<FaceEle> feDomainRhsPtr;
  boost::shared_ptr<EdgeEle> feBoundaryLhsPtr;
  boost::shared_ptr<EdgeEle> feBoundaryRhsPtr;

  // Object needed for postprocessing
  boost::shared_ptr<PostProcFaceOnRefinedMesh> fePostProcPtr;

  // range of edges for qbar and pbar
  Range fluxBcRange;
  Range pressureBcRange;

  // Field name and approximation order
  std::string gStarField;    // g* gradient data field
  std::string qStarField;    // q* flux data field
  std::string fluxField;     // q flux field
  std::string lagrangeField; // Lagrange multiplier field

  // common data
  boost::shared_ptr<CommonDataDDI> commonDataPtr;

  // Main interfaces
  Simple *simpleInterface;
  MoFEM::Interface &mField;

  // mpi parallel communicator
  MPI_Comm mpiComm;
  // Number of processors
  const int mpiRank;

  // Discrete Manager and linear KSP solver using SmartPetscObj
  SmartPetscObj<DM> dM;
  SmartPetscObj<KSP> kspSolver;
};

#endif //__DDI_DIFFUSION_HDIV_HPP__
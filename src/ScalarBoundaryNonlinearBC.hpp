#ifndef __SCALAR_BOUNDARY_NONLINEAR_BCS__
#define __SCALAR_BOUNDARY_NONLINEAR_BCS__

/**
 * @file ScalarBoundaryNonlinearBC.hpp
 * @date 2023-10-01
 *
 *
 */

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

namespace DDconstrains {

template <CubitBC BC> struct SclNonlinearBcType {};

template <CubitBC> struct GetScalarBlocksets {
  GetScalarBlocksets() = delete;

  static MoFEMErrorCode GetBlocksets(double &block_scale,
                                     boost::shared_ptr<Range> &ents,
                                     MoFEM::Interface &m_field, int ms_id) {
    MoFEMFunctionBegin;

    auto cubit_meshset_ptr =
        m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(ms_id,
                                                                    BLOCKSET);
    std::vector<double> block_data;
    CHKERR cubit_meshset_ptr->getAttributes(block_data);
    if (block_data.size() != 1) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Expected that block has two attribute");
    }
    block_scale = block_data[0];

    ents = boost::make_shared<Range>();
    CHKERR
    m_field.get_moab().get_entities_by_handle(cubit_meshset_ptr->meshset,
                                              *(ents), true);

    MoFEMFunctionReturn(0);
  }
};
} // namespace DDconstrains

template <int FIELD_DIM, AssemblyType A, typename EleOp>
struct OpFluxRhsImpl<DDconstrains::SclNonlinearBcType<BLOCKSET>, 3, FIELD_DIM,
                     A, GAUSS, EleOp> : OpBaseImpl<A, EleOp> {

  using OpBase = OpBaseImpl<A, EleOp>;

  OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id, std::string field_name,
                ScalarFunc source_fun, double rhs_scale, double nonlinear_a,
                double nonlinear_b, double nonlinear_c);

protected:
  double blockScale;
  double rhsScale;
  double nonlinearA;
  double nonlinearB;
  double nonlinearC;
  ScalarFunc sourceFun;
  MoFEMErrorCode iNtegrate(EntitiesFieldData::EntData &data);
};

#define sign(a) (((a) < 0) ? -1 : ((a) > 0))

template <int FIELD_DIM, AssemblyType A, typename EleOp>
OpFluxRhsImpl<DDconstrains::SclNonlinearBcType<BLOCKSET>, 3, FIELD_DIM, A,
              GAUSS, EleOp>::OpFluxRhsImpl(MoFEM::Interface &m_field, int ms_id,
                                           std::string field_name,
                                           ScalarFunc source_fun,
                                           double rhs_scale, double nonlinear_a,
                                           double nonlinear_b,
                                           double nonlinear_c)
    : OpBase(field_name, field_name, OpBase::OPROW), sourceFun(source_fun),
      rhsScale(rhs_scale), nonlinearA(nonlinear_a), nonlinearB(nonlinear_b),
      nonlinearC(nonlinear_c) {
  CHK_THROW_MESSAGE(DDconstrains::GetScalarBlocksets<BLOCKSET>::GetBlocksets(
                        this->blockScale, this->entsPtr, m_field, ms_id),
                    "Can not read source data from blockset");
}

template <int FIELD_DIM, AssemblyType A, typename EleOp>
MoFEMErrorCode
MoFEM::OpFluxRhsImpl<DDconstrains::SclNonlinearBcType<BLOCKSET>, 3, FIELD_DIM,
                     A, GAUSS, EleOp>::iNtegrate(EntitiesFieldData::EntData
                                                     &row_data) {
  MoFEMFunctionBegin;
  FTensor::Index<'m', 2> m;
  FTensor::Tensor1<double, 3> t_z{0., 0., 1.};

  const size_t nb_base_functions = row_data.getN().size2() / 3;
  // get element volume
  const double vol = OpBase::getMeasure();
  // get integration weights
  auto t_w = OpBase::getFTensor0IntegrationWeight();
  // get base function gradient on rows
  auto t_row_base = row_data.getFTensor1N<3>();
  // get coordinate at integration points
  auto t_coords = OpBase::getFTensor1CoordsAtGaussPts();

  // get tangent
  auto t_tangent = OpBase::getFTensor1TangentAtGaussPts();

  // loop over integration points
  for (int gg = 0; gg != OpBase::nbIntegrationPts; gg++) {
    // take into account Jacobian
    const double alpha = t_w * rhsScale * vol;

    // get normal
    FTensor::Tensor1<double, 3> t_normal;
    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    FTensor::Index<'k', 3> k;
    t_normal(i) = FTensor::levi_civita(i, j, k) * t_tangent(j) * t_z(k);
    // get normal vector length
    double length_normal = sqrt(t_normal(i) * t_normal(i));

    // source function
    auto source = sourceFun(t_coords(0), t_coords(1), t_coords(2)) * blockScale;

    // loop over rows base functions
    int rr = 0;
    for (; rr != OpBase::nbRows; ++rr) {
      // add to Rhs
      OpBase::locF[rr] +=
          alpha * t_row_base(m) * t_normal(m) / length_normal *
          (nonlinearA * source + nonlinearB * source * source / 2. +
           nonlinearC * source * source * source / 3.);
      ++t_row_base;
    }

    for (; rr < nb_base_functions; ++rr)
      ++t_row_base;
    ++t_coords;
    ++t_tangent;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <CubitBC BCTYPE, int BASE_DIM, int FIELD_DIM, AssemblyType A,
          IntegrationType I, typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<DDconstrains::SclNonlinearBcType<BCTYPE>, BASE_DIM, FIELD_DIM,
                  A, I, OpBase>,
    A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;
  // TODO: not here

  static MoFEMErrorCode add(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, ScalarFunc source_fun,
      double rhs_scale, double nonlinear_a, double nonlinear_b,
      double nonlinear_c, std::string block_name, Sev sev

  ) {
    MoFEMFunctionBegin;

    using OP = OpFluxRhsImpl<DDconstrains::SclNonlinearBcType<BLOCKSET>,
                             BASE_DIM, FIELD_DIM, PETSC, GAUSS, OpBase>;

    auto add_op = [&](auto &&meshset_vec_ptr) {
      for (auto m : meshset_vec_ptr) {
        MOFEM_TAG_AND_LOG("WORLD", sev, "OpSourceRhs") << "Add " << *m;
        pipeline.push_back(new OP(m_field, m->getMeshsetId(), field_name,
                                  source_fun, rhs_scale, nonlinear_a,
                                  nonlinear_b, nonlinear_c));
      }
      MOFEM_LOG_CHANNEL("WORLD");
    };

    switch (BCTYPE) {
    case BLOCKSET:
      add_op(

          m_field.getInterface<MeshsetsManager>()->getCubitMeshsetPtr(
              std::regex(

                  (boost::format("%s(.*)") % block_name).str()

                      ))

      );

      break;
    default:
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED,
              "Handling of bc type not implemented");
      break;
    }
    MoFEMFunctionReturn(0);
  }
};

template <int BASE_DIM, int FIELD_DIM, AssemblyType A, IntegrationType I,
          typename OpBase>
struct AddFluxToRhsPipelineImpl<

    OpFluxRhsImpl<BoundaryScalarNonlinearBCs, BASE_DIM, FIELD_DIM, A, I,
                  OpBase>,
    A, I, OpBase

    > {

  AddFluxToRhsPipelineImpl() = delete;

  using T =
      typename NaturalBC<OpBase>::template Assembly<A>::template LinearForm<I>;

  using OpFluxConstant =
      typename T::template OpFlux<NaturalMeshsetType<BLOCKSET>, BASE_DIM,
                                  FIELD_DIM>;

  using OpEdgeScalarFun =
      typename T::template OpFlux<DDconstrains::SclNonlinearBcType<BLOCKSET>,
                                  BASE_DIM, FIELD_DIM>;

  static MoFEMErrorCode add(

      boost::ptr_deque<ForcesAndSourcesCore::UserDataOperator> &pipeline,
      MoFEM::Interface &m_field, std::string field_name, double rhs_scale,
      double nonlinear_a, double nonlinear_b, double nonlinear_c, Sev sev

  ) {
    MoFEMFunctionBegin;

    auto pressure_Lshape = [](const double x, const double y, const double z) {
      // very sensitive to positive and negative values on part of the boundary
      double new_x = x, new_y = y;
      if (std::abs(y) < 10e-10)
        new_y = 0;
      if (std::abs(x) < 10e-10)
        new_x = 0;
      return pow(new_x * new_x + new_y * new_y, 1. / 3.) *
             sin((2. / 3. * (M_PI / 2. + atan2(new_y, new_x))));
    };

    auto pressure_uniform = [](const double x, const double y, const double z) {
      return 1.0;
    };

    CHKERR T::template AddFluxToPipeline<OpEdgeScalarFun>::add(
        pipeline, m_field, field_name, pressure_Lshape, rhs_scale, nonlinear_a,
        nonlinear_b, nonlinear_c, "PRESSURE_L", sev);

    // CHKERR T::template AddFluxToPipeline<OpFluxConstant>::add(
    //     pipeline, m_field, field_name, {}, "PRESSURE_UNIFORM", sev);

    CHKERR T::template AddFluxToPipeline<OpEdgeScalarFun>::add(
        pipeline, m_field, field_name, pressure_uniform, rhs_scale, nonlinear_a,
        nonlinear_b, nonlinear_c, "PRESSURE_UNIFORM", sev);

    auto pressure_octopus = [](const double x, const double y, const double z) {
      double value = exp(-10 * x * x * y * y);
      return value;
    };

    CHKERR T::template AddFluxToPipeline<OpEdgeScalarFun>::add(
        pipeline, m_field, field_name, pressure_octopus, rhs_scale, nonlinear_a,
        nonlinear_b, nonlinear_c, "PRESSURE_OCTOPUS", sev);

    MoFEMFunctionReturn(0);
  }
};

#endif //__SCALAR_BOUNDARY_NONLINEAR_BCS__
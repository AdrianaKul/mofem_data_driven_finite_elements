#ifndef __CLASSIC_DIFFUSION_PROBLEM_HPP__
#define __CLASSIC_DIFFUSION_PROBLEM_HPP__

#include <MoFEM.hpp>
#include <BasicFiniteElements.hpp>
#include <iostream>
#include <patch_integ_2D.hpp>
#include <OperatorsForDD.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

#define DDLogTag                                                               \
  MOFEM_LOG_CHANNEL("WORLD");                                                  \
  MOFEM_LOG_FUNCTION();                                                        \
  MOFEM_LOG_TAG("WORLD", "DD");

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct ClassicDiffusionProblem {

  ClassicDiffusionProblem(MoFEM::Interface &m_field);

  // Declaration of main functions
  virtual MoFEMErrorCode runProblem();
  MoFEMErrorCode setParamValues();
  MoFEMErrorCode readMesh();
  virtual MoFEMErrorCode setupProblem();
  MoFEMErrorCode setIntegrationRules();
  MoFEMErrorCode createClassicCommonData();
  virtual MoFEMErrorCode boundaryCondition();
  MoFEMErrorCode markPressureBc();
  virtual MoFEMErrorCode calculateJacobian(boost::shared_ptr<FaceEle> fe_ptr);
  virtual MoFEMErrorCode assembleSystem();
  MoFEMErrorCode getKSPinitial();
  MoFEMErrorCode solveSystem();
  virtual MoFEMErrorCode outputHVtkFields();
  // virtual MoFEMErrorCode outputHVtkFields(int ii);
  virtual MoFEMErrorCode
  postGetAnalyticalValues(boost::shared_ptr<FaceEle> fe_ptr);
  virtual MoFEMErrorCode postProcessErrors();
  virtual MoFEMErrorCode checkResults();

  // param file values
  PetscInt oRder = 1;
  PetscReal dummy_k = 3;
  PetscBool flgPatchInteg = PETSC_FALSE;
  bool flgAnalytical = false;
  int patchIntegNum = 3;

  struct CommonDataClassic {

    // main field pointers
    boost::shared_ptr<VectorDouble> mValPtr;
    boost::shared_ptr<MatrixDouble> mValGradPtr;
    boost::shared_ptr<MatrixDouble> mGradPtr;
    boost::shared_ptr<MatrixDouble> mFluxPtr;

    // values from analytical functions pointers
    boost::shared_ptr<VectorDouble> mValFuncPtr;
    boost::shared_ptr<MatrixDouble> mGradFuncPtr;
    boost::shared_ptr<MatrixDouble> mFluxFuncPtr;

    // vector storing root mean square error
    std::vector<double> rmsErr;
    std::vector<double> gradErr;
    std::vector<double> fluxErr;
    std::vector<int> numberIntegrationPoints;

    // vectors for staoring norms of the fields
    std::vector<double> normT;
    std::vector<double> normG;
    std::vector<double> normQ;
    double normFields = 0.0;

    // volume of the domain
    double vOlume = 0.;

    // scaling params
    double scaleQ = 1.;
    double scaleV = 1.;

    /**
     * @brief Vector to indicate indices for storing zero, first and second
     * moments of inertia.
     *
     */
    enum VecElements {
      START = 0,
      VALUE_ERROR,       // sum(error^2 * area) between FEM results without and
                         // with data search
      ACCUM_POINT_ERROR, // distance accumulated between result and data
                         // point from all searches
      GAUSS_COUNT,       // how many times was the data set searched
      POINT_ERROR,       // sum of ((distance between points)^2 * area covered)
      VOL_E,             // volume or area of the mesh
      GRAD_ERROR,
      FLUX_ERROR,
      DISTANCE_T,
      DISTANCE_G,
      DISTANCE_F,
      NORM_T,
      NORM_G,
      NORM_Q,
      LAST_ELEMENT
    };

    SmartPetscObj<Vec>
        petscVec; ///< Smart pointer which stores PETSc distributed vector
  };

  // Simple functions
  static double minusFunction(const double x, const double y, const double z) {
    return -1.0;
  }
  static double oneFunction(const double x, const double y, const double z) {
    return 1.0;
  }

  // Function to calculate the essential boundary term (pressure)
  static double pressureBcFunction(const double x, const double y,
                                   const double z) {
    return 0;
    // return cos(M_PI * (x - 0.5)) * cos(M_PI * (y - 0.5));
  }

  static double fluxBcFunction(const double x, const double y, const double z) {
    // return sin((x + 0.5) * M_PI) * cosh(M_PI) * M_PI / sinh(M_PI);
    // return 10;
    return 0;
  }

  static double square(double x) {
    return x * x;
  } // TODO: move this somewhere more sensible

  // new analytical functions [begin]

  // square top
  static double analyticalFunction_squareTop(const double x, const double y,
                                             const double z) {
    return (sin(M_PI * (x)) * sinh(M_PI * (y))) / (sinh(M_PI));
  }
  static VectorDouble analyticalFunctionGrad_squareTop(const double x,
                                                       const double y,
                                                       const double z) {
    VectorDouble res;
    res.resize(2);
    res[0] = M_PI * cos(M_PI * (x)) * sinh(M_PI * (y)) / sinh(M_PI);
    res[1] = M_PI * sin(M_PI * (x)) * cosh(M_PI * (y)) / sinh(M_PI);
    return res;
  }

  // square sin cos
  static double analyticalFunction_squareSinCos(const double x, const double y,
                                                const double z) {
    double a = 2.;
    return sin(M_PI * x * a) * cos(M_PI * y * a);
  }
  static VectorDouble analyticalFunctionGrad_squareSinCos(const double x,
                                                          const double y,
                                                          const double z) {
    VectorDouble res;
    res.resize(2);
    double a = 2.;
    res[0] = M_PI * a * cos(M_PI * x * a) * cos(M_PI * y * a);
    res[1] = -M_PI * a * sin(M_PI * x * a) * sin(M_PI * y * a);
    return res;
  }

  // mexi-hat
  static double analyticalFunction_mexiHat(const double x, const double y,
                                           const double z) {
    return exp(-100. * (square(x) + square(y))) * cos(M_PI * x) * cos(M_PI * y);
  }
  static VectorDouble analyticalFunctionGrad_mexiHat(const double x,
                                                     const double y,
                                                     const double z) {
    VectorDouble res;
    res.resize(2);
    res[0] = -exp(-100. * (square(x) + square(y))) *
             (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) * cos(M_PI * y);
    res[1] = -exp(-100. * (square(x) + square(y))) *
             (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) * cos(M_PI * x);
    return res;
  }

  // L-Shape
  static double analyticalFunction_LShape(const double x, const double y,
                                          const double z) {
    return pow(x * x + y * y, 1.0 / 3.0) * sin((M_PI + 2 * atan2(y, x)) / 3.0);
  }
  static VectorDouble analyticalFunctionGrad_LShape(const double x,
                                                    const double y,
                                                    const double z) {
    VectorDouble res;
    res.resize(2);
    double numerator = 2. * (y * cos((M_PI + 2. * atan2(y, x)) / 3.) -
                             x * sin((M_PI + 2. * atan2(y, x)) / 3.));
    double denominator = -3. * pow(pow(x, 2) + pow(y, 2), 2.0 / 3.0);
    res[0] = numerator / denominator;
    numerator = -2. * (x * cos((M_PI + 2. * atan2(y, x)) / 3.) +
                       y * sin((M_PI + 2. * atan2(y, x)) / 3.));
    res[1] = numerator / denominator;
    return res;
  }

  // new analytical functions [end]

  //! [Analytical function]
  static double analyticalFunction(const double x, const double y,
                                   const double z) {
    // L-Shape
    return pow(x * x + y * y, 1 / 3.) *
           sin((2 * (M_PI / 2. + atan2(y, x))) / 3.);
    //  mexi-hat
    // return exp(-100. * (square(x) + square(y))) * cos(M_PI * x) * cos(M_PI *
    // y); return cos(M_PI * (x - 0.5)) * cos(M_PI * (y - 0.5));
  }
  //! [Analytical function]

  //! [Analytical function gradient]
  static VectorDouble analyticalFunctionGrad(const double x, const double y,
                                             const double z) {
    VectorDouble res;
    res.resize(2);
    // L-Shape
    res[0] = (-2 * y * cos((2 * (M_PI / 2. + atan2(y, x))) / 3.)) /
                 (3. * pow(x * x + y * y, 2 / 3.)) +
             (2 * x * sin((2 * (M_PI / 2. + atan2(y, x))) / 3.)) /
                 (3. * pow(x * x + y * y, 2 / 3.));
    res[1] = (2 * x * cos((2 * (M_PI / 2. + atan2(y, x))) / 3.)) /
                 (3. * pow(x * x + y * y, 2 / 3.)) +
             (2 * y * sin((2 * (M_PI / 2. + atan2(y, x))) / 3.)) /
                 (3. * pow(x * x + y * y, 2 / 3.));
    //  mexi-hat
    // res[0] = -exp(-100. * (square(x) + square(y))) *
    //          (200. * x * cos(M_PI * x) + M_PI * sin(M_PI * x)) * cos(M_PI *
    //          y);
    // res[1] = -exp(-100. * (square(x) + square(y))) *
    //          (200. * y * cos(M_PI * y) + M_PI * sin(M_PI * y)) * cos(M_PI *
    //          x);
    return res;
  }
  //! [Analytical function gradient]

  // Object to mark boundary entities for the assembling of domain elements
  boost::shared_ptr<std::vector<unsigned char>> boundaryMarker;

  // MoFEM working Pipelines for LHS and RHS of domain and boundary
  boost::shared_ptr<FaceEle> feDomainLhsPtr;
  boost::shared_ptr<PatchIntegFaceEle> feDomainRhsPtr;
  boost::shared_ptr<EdgeEle> feBoundaryLhsPtr;
  boost::shared_ptr<EdgeEle> feBoundaryRhsPtr;

  // Object needed for postprocessing
  boost::shared_ptr<PostProcFaceOnRefinedMesh> fePostProcPtr;

  // Boundary entities marked for fieldsplit (block) solver - optional
  Range boundaryEntitiesForFieldsplit;

  // range of edges for qbar and pbar
  Range fluxBcRange;
  Range pressureBcRange;

  // Field name and approximation order
  std::string valueField; // displacement field

  // common data
  boost::shared_ptr<CommonDataClassic> commonDataPtr;

  // Main interfaces
  Simple *simpleInterface;
  MoFEM::Interface &mField;

  // mpi parallel communicator
  MPI_Comm mpiComm;
  // Number of processors
  const int mpiRank;

  // Discrete Manager and linear KSP solver using SmartPetscObj
  SmartPetscObj<DM> dM;
  SmartPetscObj<KSP> kspSolver;

  static MoFEMErrorCode getTagHandle(MoFEM::Interface &m_field,
                                     const char *name, DataType type,
                                     Tag &tag_handle) {
    MoFEMFunctionBegin;
    int int_val = -1;
    double double_val = 0;
    switch (type) {
    case MB_TYPE_INTEGER:
      CHKERR m_field.get_moab().tag_get_handle(
          name, 1, type, tag_handle, MB_TAG_CREAT | MB_TAG_SPARSE, &int_val);
      break;
    case MB_TYPE_DOUBLE:
      CHKERR m_field.get_moab().tag_get_handle(
          name, 1, type, tag_handle, MB_TAG_CREAT | MB_TAG_SPARSE, &double_val);
      break;
    default:
      SETERRQ1(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
               "Wrong data type %d for tag", type);
    }
    MoFEMFunctionReturn(0);
  }

  // Operator headers

  struct OpPostProcErrorPressureIntegPts : public OpFaceEle {
  public:
    OpPostProcErrorPressureIntegPts(
        std::string field_name,
        boost::shared_ptr<CommonDataClassic> common_data_ptr,
        ScalarFunc analytical_function, VectorFunc analytical_grad)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr), analyticalFunc(analytical_function),
          analyticalGrad(analytical_grad) {};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    ScalarFunc analyticalFunc;
    VectorFunc analyticalGrad;
    boost::shared_ptr<CommonDataClassic> commonDataPtr;
  };

  struct OpPostProcErrorFluxDerivedIntegPts : public OpFaceEle {
  public:
    OpPostProcErrorFluxDerivedIntegPts(
        std::string field_name,
        boost::shared_ptr<CommonDataClassic> common_data_ptr,
        VectorFunc analytical_flux, ScalarFunc k_function)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          commonDataPtr(common_data_ptr), analyticalFlux(analytical_flux),
          kFunction(k_function) {};

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    VectorFunc analyticalFlux;
    ScalarFunc kFunction;
    boost::shared_ptr<CommonDataClassic> commonDataPtr;
  };

  template <int BASE_DIM> struct OpGetFluxFromGrad : public OpFaceEle {
  public:
    OpGetFluxFromGrad(boost::shared_ptr<MatrixDouble> grad_ptr,
                      ScalarFunc scalar_function,
                      boost::shared_ptr<MatrixDouble> flux_ptr,
                      boost::shared_ptr<VectorDouble> value_ptr = nullptr)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), gradPtr(grad_ptr),
          sFunc(scalar_function), fluxPtr(flux_ptr), valuePtr(value_ptr) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    boost::shared_ptr<MatrixDouble> gradPtr;
    ScalarFunc sFunc;
    boost::shared_ptr<MatrixDouble> fluxPtr;
    boost::shared_ptr<VectorDouble> valuePtr;
  };

  struct OpGetArea : public OpFaceEle {
  public:
    OpGetArea(SmartPetscObj<Vec> data_vec, const int index)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), dataVec(data_vec),
          iNdex(index) {};
    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  protected:
    SmartPetscObj<Vec> dataVec;
    const int iNdex;
  };

  struct OpGetGaussCount : public OpFaceEle {
  public:
    OpGetGaussCount(SmartPetscObj<Vec> data_vec, const int index)
        : OpFaceEle(NOSPACE, OpFaceEle::OPSPACE), dataVec(data_vec),
          iNdex(index) {};
    MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  protected:
    SmartPetscObj<Vec> dataVec;
    const int iNdex;
  };
};

#endif //__CLASSIC_DIFFUSION_PROBLEM_HPP__
// Define name if it has not been defined yet
#ifndef __POINT_SETTER_HPP__
#define __POINT_SETTER_HPP__

namespace Rpoints {

// 1 dimension
boost::geometry::model::point<double, 1, boost::geometry::cs::cartesian>
point(const double v0) {
  boost::geometry::model::point<double, 1, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  return poi;
}

// 2 dimensions
boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian>
point(const double v0, const double v1) {
  boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  poi.set<1>(v1);
  return poi;
}

// 3 dimensions
boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian>
point(const double v0, const double v1, const double v2) {
  boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  poi.set<1>(v1);
  poi.set<2>(v2);
  return poi;
}

// 4 dimensions
boost::geometry::model::point<double, 4, boost::geometry::cs::cartesian>
point(const double v0, const double v1, const double v2, const double v3) {
  boost::geometry::model::point<double, 4, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  poi.set<1>(v1);
  poi.set<2>(v2);
  poi.set<3>(v3);
  return poi;
}

// 5 dimensions
boost::geometry::model::point<double, 5, boost::geometry::cs::cartesian>
point(const double v0, const double v1, const double v2, const double v3,
      const double v4) {
  boost::geometry::model::point<double, 5, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  poi.set<1>(v1);
  poi.set<2>(v2);
  poi.set<3>(v3);
  poi.set<4>(v4);
  return poi;
}

// 6 dimensions
boost::geometry::model::point<double, 6, boost::geometry::cs::cartesian>
point(const double v0, const double v1, const double v2, const double v3,
      const double v4, const double v5) {
  boost::geometry::model::point<double, 6, boost::geometry::cs::cartesian> poi;
  poi.set<0>(v0);
  poi.set<1>(v1);
  poi.set<2>(v2);
  poi.set<3>(v3);
  poi.set<4>(v4);
  poi.set<5>(v5);
  return poi;
}

} // namespace Rpoints

// 4 dimensions
void vectorToPoint(
    VectorDouble vector,
    boost::geometry::model::point<double, 4, boost::geometry::cs::cartesian>
        &poi) {
  poi.set<0>(vector[0]);
  poi.set<1>(vector[1]);
  poi.set<2>(vector[2]);
  poi.set<3>(vector[3]);
}
// 5 dimensions
void vectorToPoint(
    VectorDouble vector,
    boost::geometry::model::point<double, 5, boost::geometry::cs::cartesian>
        &poi) {
  poi.set<0>(vector[0]);
  poi.set<1>(vector[1]);
  poi.set<2>(vector[2]);
  poi.set<3>(vector[3]);
  poi.set<4>(vector[4]);
}

// 4 dimensions
void pointToVector(
    boost::geometry::model::point<double, 4, boost::geometry::cs::cartesian>
        poi,
    VectorDouble &vector) {
  vector.resize(4);
  vector[0] = poi.get<0>();
  vector[1] = poi.get<1>();
  vector[2] = poi.get<2>();
  vector[3] = poi.get<3>();
}
// 5 dimensions
void pointToVector(
    boost::geometry::model::point<double, 5, boost::geometry::cs::cartesian>
        poi,
    VectorDouble &vector) {
  vector.resize(5);
  vector[0] = poi.get<0>();
  vector[1] = poi.get<1>();
  vector[2] = poi.get<2>();
  vector[3] = poi.get<3>();
  vector[4] = poi.get<4>();
}

#endif //__POINT_SETTER_HPP__
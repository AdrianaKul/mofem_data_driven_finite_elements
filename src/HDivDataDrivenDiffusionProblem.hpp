#ifndef __HDIV_DIFFUSION_DD_HPP__
#define __HDIV_DIFFUSION_DD_HPP__

#include <DataDrivenDiffusionProblem.hpp>
#include <HDivDiffusionProblem.hpp>

// Random boost requirements
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/normal_distribution.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;

using OpFaceEle = MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator;
using OpEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using namespace MoFEM;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 4, bg::cs::cartesian> point;

typedef boost::function<double(const double, const double, const double)>
    ScalarFunc;

typedef boost::function<VectorDouble(const double, const double, const double)>
    VectorFunc;

struct DiffusionHDivDD : public HDivDiffusionProblem,
                         virtual public DiffusionDD {
public:
  DiffusionHDivDD(MoFEM::Interface &m_field)
      : DiffusionDD(m_field), HDivDiffusionProblem(m_field),
        ClassicDiffusionProblem(m_field), gradField("G"), tauField("tau"),
        fluxField("Q") {
    lagrangeField = "xi";
  };

  MoFEMErrorCode setupProblem() override;
  MoFEMErrorCode createDDHdivCommonData();
  MoFEMErrorCode removeLambda() override;
  MoFEMErrorCode assembleSystem() override;
  MoFEMErrorCode refineOrder();
  MoFEMErrorCode checkError(int iter_num, bool print,
                            std::string vtk_name = "out_error_");
  MoFEMErrorCode solveDDHDivLoopSystem();
  MoFEMErrorCode runMonteCarlo() override;
  MoFEMErrorCode monteSaveHdivResults();
  MoFEMErrorCode outputResults(int iter_num, bool print,
                               std::string vtk_name = "out_integ_pts_");

  PetscBool flg_refine_order_all = PETSC_FALSE;
  PetscBool flg_skip_all_vtk = PETSC_FALSE;

  // Field name and approximation order
  std::string fluxField;
  std::string gradField;
  std::string tauField;

  struct CommonDataHDivDD : public CommonDataHDiv, virtual public CommonDataDD {
    boost::shared_ptr<MatrixDouble> mPeturbationPtr;
  };

  boost::shared_ptr<CommonDataHDivDD> commonDataPtr;
};

#endif //__HDIV_DIFFUSION_DD_HPP__
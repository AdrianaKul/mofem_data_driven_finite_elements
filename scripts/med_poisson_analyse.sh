read_med -med_file *.med
mofem_part -my_file out.h5m -output_file input_mesh.h5m -my_nparts 1 -dim 2 -adj_dim 1
poisson_H -file_name input_mesh.h5m

poisson_dd_H -file_name analysis_mesh.h5m -my_order 1 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 0 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 2 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 0 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 3 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 0 -skip_vtk 1

poisson_dd_H -file_name analysis_mesh.h5m -my_order 1 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 1000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 2 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 1000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 3 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 1000 -my_dummy_noise 0.1 -skip_vtk 1

poisson_dd_H -file_name analysis_mesh.h5m -my_order 1 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 2 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 3 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 0.1 -skip_vtk 1

poisson_dd_H -file_name analysis_mesh.h5m -my_order 1 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 2 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 3 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 1 -skip_vtk 1 

poisson_dd_H -file_name analysis_mesh.h5m -my_order 1 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 100000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 2 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 100000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 3 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 100000 -my_dummy_noise 0.1 -skip_vtk 1

poisson_dd_H -file_name analysis_mesh.h5m -my_order 1 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 1000000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 2 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 1000000 -my_dummy_noise 0.1 -skip_vtk 1
poisson_dd_H -file_name analysis_mesh.h5m -my_order 3 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 1000000 -my_dummy_noise 0.1 -skip_vtk 1

poisson_dd_H -file_name analysis_mesh.h5m -my_order 2 -my_dummy_k -3 -my_dummy_range 5 -my_dummy_count 10000 -my_dummy_noise 0.1


convert.py out_*

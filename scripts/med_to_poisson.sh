read_med -med_file *.med 
mofem_part -my_file out.h5m -output_file input_mesh.h5m -my_nparts 1 -dim 2 -adj_dim 1
poisson_H -file_name input_mesh.h5m
poisson_dd_H -file_name analysis_mesh.h5m
convert.py out_* 

#include <r_tree_testing.hpp>

int main() {

  RTreeDummy dummy_read;

  dummy_read.setRTreeDummy(RtreeManipulation::loadFileData("dummy_tree.csv"));

  MOFEM_LOG_CHANNEL("DD");
  BOOST_LOG_SCOPED_THREAD_ATTR("Timeline", attrs::timer());

  int n_points = 10e05;

  MOFEM_LOG("DD", Sev::verbose) << "Number of points = " << n_points;
  MOFEM_LOG("DD", Sev::verbose) << "Hello DD! ";

  // RTreeDummy dummy(-3, 10, 1000);
  // RTreeDummy dummy(-3, 10, n_points, 0.05, PETSC_TRUE);
  RTreeDummy dummy;
  dummy.create_4D_rtree(1, 10, n_points, 0.0, PETSC_FALSE, PETSC_TRUE, 0);

  MOFEM_LOG("DD", Sev::verbose) << "Rtree created! ";

  point4D finding = Rpoints::point(2.3, -2.3, -6.9, 6.9);

  VectorDouble test_vec;
  pointToVector(finding, test_vec);
  test_vec[1] += 1;
  vectorToPoint(test_vec, finding);

  std::cout << bg::wkt<point4D>(finding) << ": point being looked for\n";

  MOFEM_LOG("DD", Sev::verbose) << "Point to find set! ";

  point4D result = dummy.returnNearest(finding);

  // point result = returnNearest(finding, dummy_read.rtree);

  // std::cout << "nearest 1: \n";
  // dummy.findNearest(finding);
  // std::cout << "nearest 3: \n";
  // dummy.showNearest(finding, 3);
  // std::cout << "within distance 2: \n";
  // dummy.showWithinD(finding,2);

  MOFEM_LOG("DD", Sev::verbose) << "Point found! ";

  std::cout << bg::wkt<point4D>(result) << ": returning the nearest point\n";

  return 0;
}

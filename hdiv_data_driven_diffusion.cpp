#include <ClassicDiffusionProblem.hpp>
#include <DataDrivenDiffusionProblem.hpp>
#include <HDivDataDrivenDiffusionProblem.hpp>

using namespace MoFEM;

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  // Initialisation of MoFEM/PETSc and MOAB data structures
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);

  // Error handling
  try {
    // Register MoFEM discrete manager in PETSc
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    // Create MOAB instance
    moab::Core mb_instance;              // mesh database
    moab::Interface &moab = mb_instance; // mesh database interface

    // Create MoFEM instance
    MoFEM::Core core(moab);           // finite element database
    MoFEM::Interface &m_field = core; // finite element interface

    // Run the main analysis
    DiffusionHDivDD poisson_problem(m_field);

    CHKERR poisson_problem.setParamValues();
    CHKERR poisson_problem.SetParamDDValues();
    CHKERR poisson_problem.readMesh();
    CHKERR poisson_problem.setupProblem();
    CHKERR poisson_problem.orderUpdateSetUp();
    CHKERR poisson_problem.setIntegrationRules();
    CHKERR poisson_problem.createClassicCommonData();
    CHKERR poisson_problem.createDDCommonData();
    CHKERR poisson_problem.createHdivCommonData();
    CHKERR poisson_problem.createDDHdivCommonData();
    CHKERR poisson_problem.boundaryCondition();
    CHKERR poisson_problem.markFluxBc();
    CHKERR poisson_problem.removeLambda();
    CHKERR poisson_problem.assembleSystem();
    CHKERR poisson_problem.solveDDHDivLoopSystem();
    CHKERR poisson_problem.setParamMonteCarlo();
    CHKERR poisson_problem.runMonteCarlo();
  }
  CATCH_ERRORS;

  // Finish work: cleaning memory, getting statistics, etc.
  MoFEM::Core::Finalize();

  return 0;
}
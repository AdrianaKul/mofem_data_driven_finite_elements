#include <DiffusionNonlinearGraphiteProblem.hpp>

using namespace MoFEM;

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  // Initialisation of MoFEM/PETSc and MOAB data structures
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);

  // Error handling
  try {
    // Register MoFEM discrete manager in PETSc
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    // Create MOAB instance
    moab::Core mb_instance;              // mesh database
    moab::Interface &moab = mb_instance; // mesh database interface

    // Create MoFEM instance
    MoFEM::Core core(moab);          // finite element database
    MoFEM::Interface &mField = core; // finite element interface

    // Simple interfaces
    Simple *simpleInterface;

    PetscBool test_jacobian = PETSC_TRUE;
    std::string valueField = "U";
    PetscReal nonlinear_a = 134.;
    PetscReal nonlinear_b = -0.1074;
    PetscReal nonlinear_c = 3.719e-5;

    boost::shared_ptr<FaceEle> feDomainLhsPtr =
        boost::shared_ptr<FaceEle>(new FaceEle(mField));
    boost::shared_ptr<FaceEle> feDomainRhsPtr =
        boost::shared_ptr<FaceEle>(new FaceEle(mField));

    CHKERR mField.getInterface(simpleInterface);
    CHKERR simpleInterface->getOptions();
    CHKERR simpleInterface->loadFile();

    CHKERR simpleInterface->addDomainField(valueField, H1,
                                           AINSWORTH_LEGENDRE_BASE, 1);

    PetscInt oRder = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-dt_order", &oRder, PETSC_NULL);
    CHKERR simpleInterface->setFieldOrder(valueField, oRder);

    CHKERR simpleInterface->setUp();

    auto domain_base = [&](boost::shared_ptr<FaceEle> fe_ptr) {
      MoFEMFunctionBegin;
      auto det_ptr = boost::make_shared<VectorDouble>();
      auto jac_ptr = boost::make_shared<MatrixDouble>();
      auto inv_jac_ptr = boost::make_shared<MatrixDouble>();

      fe_ptr->getOpPtrVector().push_back(new OpCalculateHOJacForFace(jac_ptr));
      fe_ptr->getOpPtrVector().push_back(
          new OpInvertMatrix<2>(jac_ptr, det_ptr, inv_jac_ptr));
      fe_ptr->getOpPtrVector().push_back(new OpSetInvJacH1ForFace(inv_jac_ptr));
      MoFEMFunctionReturn(0);
    };

    CHKERR domain_base(feDomainLhsPtr);
    CHKERR domain_base(feDomainRhsPtr);

    { // domain Lhs
      auto vec_val_ptr = boost::make_shared<VectorDouble>();
      auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
      feDomainLhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
      feDomainLhsPtr->getOpPtrVector().push_back(
          new DiffusionNonlinearGraphite::OpNonlinearLhs(
              valueField, valueField, vec_val_ptr, mat_grad_ptr, nonlinear_a,
              nonlinear_b, nonlinear_c));
    }
    { // domain Rhs
      auto vec_val_ptr = boost::make_shared<VectorDouble>();
      auto mat_grad_ptr = boost::make_shared<MatrixDouble>();
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldValues(valueField, vec_val_ptr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new OpCalculateScalarFieldGradient<2>(valueField, mat_grad_ptr));
      feDomainRhsPtr->getOpPtrVector().push_back(
          new DiffusionNonlinearGraphite::OpNonlinearRhs(
              valueField, vec_val_ptr, mat_grad_ptr, nonlinear_a, nonlinear_b,
              nonlinear_c));
    }

    { // set random field
      PetscRandom rctx;
      PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
      auto set_random_field = [&](VectorAdaptor &&field_data, double *x,
                                  double *y, double *z) {
        MoFEMFunctionBegin;
        double value;
        double scale = 1.0;
        PetscRandomGetValueReal(rctx, &value);
        field_data[0] = value * scale;
        MoFEMFunctionReturn(0);
      };

      CHKERR mField.getInterface<FieldBlas>()->setVertexDofs(set_random_field,
                                                             valueField);

      PetscRandomDestroy(&rctx);
    }

    // get Discrete Manager (SmartPetscObj)
    SmartPetscObj<DM> dm = simpleInterface->getDM();

    // Create nonlinear solver (SNES)
    SmartPetscObj<SNES> snes = createSNES(mField.get_comm());

    CHKERR SNESSetDM(snes, dm);

    { // Set operators for nonlinear equations solver (SNES) from MoFEM
      // Pipelines

      // Set operators for calculation of LHS and RHS of domain elements
      boost::shared_ptr<FaceEle> null_face;
      CHKERR DMMoFEMSNESSetJacobian(dm, simpleInterface->getDomainFEName(),
                                    feDomainLhsPtr, null_face, null_face);
      CHKERR DMMoFEMSNESSetFunction(dm, simpleInterface->getDomainFEName(),
                                    feDomainRhsPtr, null_face, null_face);
    }

    Vec x, f;
    CHKERR DMCreateGlobalVector(dm, &x);
    CHKERR VecDuplicate(x, &f);
    CHKERR VecSetOption(f, VEC_IGNORE_NEGATIVE_INDICES, PETSC_TRUE);
    CHKERR DMoFEMMeshToLocalVector(dm, x, INSERT_VALUES, SCATTER_FORWARD);

    Mat A, fdA;
    CHKERR DMCreateMatrix(dm, &A);
    CHKERR MatDuplicate(A, MAT_DO_NOT_COPY_VALUES, &fdA);

    CHKERR PetscOptionsGetBool(PETSC_NULL, "", "-test_jacobian", &test_jacobian,
                               PETSC_NULL);
    if (test_jacobian == PETSC_TRUE) {
      char testing_options[] =
          "-snes_test_jacobian -snes_test_jacobian_display "
          "-snes_no_convergence_test -snes_atol 0 -snes_rtol 0 -snes_max_it 1 ";
      //"-pc_type none";
      CHKERR PetscOptionsInsertString(NULL, testing_options);
    } else {
      char testing_options[] = "-snes_no_convergence_test -snes_atol 0 "
                               "-snes_rtol 0 "
                               "-snes_max_it 1 ";
      //"-pc_type none";
      CHKERR PetscOptionsInsertString(NULL, testing_options);
    }

    MoFEM::SnesCtx *snes_ctx;
    CHKERR DMMoFEMGetSnesCtx(dm, &snes_ctx);
    CHKERR SNESSetFunction(snes, f, SnesRhs, snes_ctx);
    CHKERR SNESSetJacobian(snes, A, A, SnesMat, snes_ctx);
    CHKERR SNESSetFromOptions(snes);

    CHKERR SNESSolve(snes, NULL, x);

    if (test_jacobian == PETSC_FALSE) {
      double nrm_A0;
      CHKERR MatNorm(A, NORM_INFINITY, &nrm_A0);

      char testing_options_fd[] = "-snes_fd";
      CHKERR PetscOptionsInsertString(NULL, testing_options_fd);

      CHKERR SNESSetFunction(snes, f, SnesRhs, snes_ctx);
      CHKERR SNESSetJacobian(snes, fdA, fdA, SnesMat, snes_ctx);
      CHKERR SNESSetFromOptions(snes);

      CHKERR SNESSolve(snes, NULL, x);
      CHKERR MatAXPY(A, -1, fdA, SUBSET_NONZERO_PATTERN);

      double nrm_A;
      CHKERR MatNorm(A, NORM_INFINITY, &nrm_A);
      PetscPrintf(PETSC_COMM_WORLD, "Matrix norms %3.4e %3.4e\n", nrm_A,
                  nrm_A / nrm_A0);
      nrm_A /= nrm_A0;

      const double tol = 1e-7;
      if (nrm_A > tol) {
        SETERRQ(PETSC_COMM_WORLD, MOFEM_ATOM_TEST_INVALID,
                "Difference between hand-calculated tangent matrix and finite "
                "difference matrix is too big");
      }
    }
  }
  CATCH_ERRORS;

  // Finish work: cleaning memory, getting statistics, etc.
  MoFEM::Core::Finalize();

  return 0;
}
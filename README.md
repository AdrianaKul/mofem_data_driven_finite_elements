## Data-Driven Finite Element Technology (DDFET)

Last dependant repository commits tested with:

| Repository       | Branch | Commit  |  
|------------------------|---|---|
| MoFEM core  | develop | c8dfd486983b6cce1ab0f77eedb54e0da63082db  |  
| MoFEM user modules | develop |  a33f909d11f6e3c265366f305f50bbc5c9d511e3 | 

<!-- Write few words about DD. -->

**Running code**

At this point, there are the following executables:

***hello_w***
Prints: ```Hello world!```

***create_csv_dataset***
Creates a completely artificial dataset according to user set parameters.  

default: linear material behaviour

$flux = k*gradient$


| *Linear*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-output_file| dummy_tree.csv | dataset created will be saved to *file* |
|-my_csv_dim| 4 | dimensions of generated dataset (4 or 5) |
|-my_dummy_k|3|*conductivity* $k$ applied to generated material model|
|-my_dummy_count|1000000|*number* of material datapoints generated|
|-my_dummy_noise_k|0.1|*standard deviation* of the noise applied to conductivity $k$|
| -my_dummy_range_dp |  4 | creates a random uniformly distributed range of $gradient$ values between -+*input* |
|-scaling|  | create a more normalised tree and save scaling parameters to scaling.in file |

Nonlinear artificial material models

$flux = k_N * gradient$

$k_N = 1 + k * |gradient| ^ \alpha$

| *Nonlinear*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-nonlinear_alpha|0| $\alpha$; triggers nonlinear material model creation|
This material model takes other parameters from the linear material model and does not have a scaling option.

Forchheirmer (outdated)

$gradient = -1/k * flux - k_2 * |flux| * flux$

| *Forchheirmer*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-forchheirmer|| triggers the simplified Forchheirmer material model |
|-forch_coeff_2| 0 | $k_2$ constant in the simplified Forchheirmer material model |
|-my_dummy_range_q |12|creates a random uniformly distributed range of $flux$ values between -+*input* |
This material model takes other parameters from the linear material model and does not have a 5D dataset option.

***classic_diffusion*** 
Standard implementation of a diffusion problem with additional outputs:

- the usual .h5m file containing resulting fields which can be converted to .vtk with mbconvert  
- .h5m file which can be used as an input of a data-driven analysis if comparison between the two is desired
- material dataset extracted from results evaluated at each integration point saved in .csv

| *classic_diffusion*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-file_name|| input mesh in .h5m .cub .msh .med format|
|-my_dummy_k| 3 | *conductivity* $k$ as per linear material model; e.g. $q = - k \nabla p$|
|-my_order| 2 | approximation order for FEM with hierarchical shape functions |

Unknown fields: 
|field name|functional space|explanation|
|:-:|:-:|-|
|P_reference| $H^1$ |pressure; could represent any main transport problem unknown, e.g. temperature|

***hdiv_diffusion***
Diffusion derived with mixed formulation. This enables adaptive refinement; note that adaptive mesh refinement works only with triangular meshes.
Takes the same input as *classic_diffusion* plus:

| *classic_diffusion*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-refinement_style|0| 0: no refinement; 1: order refinement; 2: mesh refinement; 3: mesh refinement with hanging nodes (3 in progress)|
|-ref_iter_num| 0 | how many times should the refinement take place|
|-ref_tolerance| 1 | *input* times average error indicator determines which elements should be refined (in progress)|

Unknown fields: 
|field name|functional space|explanation|
|:-:|:-:|-|
|P| $L^2$ |pressure; could represent any main transport problem unknown, e.g. temperature|
|Q| $H(div)$ |flux; could represent any flux transport problem unknown, e.g. temperature flux|

***diffusion_nonlinear_snes***

Nonlinear diffusion analysis using a nonlinear solver *snes* and follows:

$flux = k_N * gradient$

$k_N = 1 + k * |gradient| ^ \alpha$

Takes the same input as *classic_diffusion* plus:

| *diffusion_nonlinear_snes*   |     default      | explanation |
|:----------:|:-------------:|:-----------:|
|-nonlinear_alpha|1| $\alpha$|
|-nonlinear_k| 1 | $k$|
|-dummy_k| 3 | acts like a scaling factor for sources and boundary conditions |


Unknown fields: 
|field name|functional space|explanation|
|:-:|:-:|-|
|P_reference| $H^1$ |pressure; could represent any main transport problem unknown, e.g. temperature|

***data_driven_diffusion***

The most basic version of the data-driven implementation: uses liner solver *ksp*; derived in a standard way.

Takes the same input as *classic_diffusion* plus:
| *data_driven_diffusion*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-csv_tree_file|dummy_tree.csv| name of a .csv material dataset file|
|-data_dim| 4 | dimensions taken from material dataset; 4 or 5|
|-skip_vtk|  | set if vtk printing should be skipped; useful when doing statistical analysis |
|-write_long_error_file|  | set to see the convergence of errors through iterations |
|-print_integ|  | set if integration points should be printed to out_integ_*.h5m; useful as a part of debugging/understanding |
|-print_moab|  | do not use; whole moab prints to .h5m every iteration |
|-point_dist_tol|1e-5|tolerance for rms point distance error change between iterations (ksp only)|
|-max_iter|100|maximum number of iterations (ksp only)|
|-scaling||reads and applies scaling from scaling.in file|
|-use_line||uses a line equation $q = - k \nabla p$ instead of a material dataset to find a closest point at each integration point|
|-point_average||triggers averaging of the closest # found material datapoints; average value is used instead of a closest material datapoint|

| *Monte-Carlo analysis*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-monte_carlo||triggers statistical analysis using Monte-Carlo|
|-perturb_multiplier|10|how much should the calculated fields be perturbed by; e.g. random value with normal distribution = *input* times distance from the dataset at each integration point|
|-monte_patch_number|1| number of generated integration points used for the statistical analysis with patch integration|


Unknown fields: 
|field name|functional space|explanation|
|:-:|:-:|-|
|P| $H^1$ |pressure; could represent any main transport problem unknown, e.g. temperature|
|Q| $L^2$ |flux; could represent any flux transport problem unknown, e.g. temperature flux|
|L| $H^1$ |scalar field for Lagrange multipliers $\lambda$|

***data_driven_diffusion_snes***

Same derivation as *data_driven_diffusion* but implemented with a nonlinear solver *snes*; enabling the assumption of $g^*$ and $q^*$ dependence on $g$ and $q$.

Takes the same input as *classic_diffusion* and *data_driven_diffusion* plus:

| *data_driven_diffusion_snes*   |     default      | explanation |
|:----------:|:-------------:|-----------|
|-use_part_star|| triggers the use of dependence of $g^*$ and $q^*$ on $g$ and $q$ |
|-partial_part|| value between 0.1 and 1;  | 

***hdiv_data_driven_diffusion***

***hdiv_data_driven_diffusion_snes***

***ddi_diffusion_hdiv***
-work in progress

**Theory**

More detailed theory

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
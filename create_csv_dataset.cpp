#include <CreateCsvDataset.hpp>

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  // Initialisation of MoFEM/PETSc and MOAB data structures
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);

  // Error handling
  try {

    CreatingCsvMaterialModel dummy;

    // Set values from param file
    CHKERR dummy.setParamValues();
    CHKERR dummy.startCsvFile();
    CHKERR dummy.setSeed();

    DDLogTag;
    BOOST_LOG_SCOPED_THREAD_ATTR("Timeline", attrs::timer());
    MOFEM_LOG("WORLD", Sev::inform)
        << "Creating rtree with " << dummy.dummy_count << " points...";
    if (dummy.flg_forch) {
      CHKERR dummy.createForchheirmerMaterialModelData();
    } else if (dummy.flg_bnmd) {
      CHKERR dummy.createBasicNonlinearMaterialModelData();
    } else {
      CHKERR dummy.createBasicMaterialModelData();
    }
    MOFEM_LOG("WORLD", Sev::inform) << "rtree created";

    CHKERR dummy.saveScalingFactor();
    MOFEM_LOG("WORLD", Sev::inform) << "scaling used: " << dummy.scaling;
  }
  CATCH_ERRORS;

  // Finish work: cleaning memory, getting statistics, etc.
  MoFEM::Core::Finalize();

  return 0;
}